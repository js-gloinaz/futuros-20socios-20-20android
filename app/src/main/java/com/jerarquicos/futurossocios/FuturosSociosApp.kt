package com.jerarquicos.futurossocios

import android.app.Activity
import android.app.Application
import android.app.Service
import android.content.Context
import androidx.multidex.MultiDex
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.di.AppInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasServiceInjector
import io.fabric.sdk.android.Fabric
import java.util.*
import javax.inject.Inject

/**
 * Created by fgagneten on 15/05/2017.
 * Main Application Class
 */
class FuturosSociosApp : Application(), HasActivityInjector, HasServiceInjector {
	@JvmField
	@Inject
	var serviceInjector: DispatchingAndroidInjector<Service>? = null
	@JvmField
	@Inject
	var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>? = null
	private lateinit var firebaseAnalytics: FirebaseAnalytics
	
	override fun onCreate() {
		super.onCreate()
		firebaseAnalytics = FirebaseAnalytics.getInstance(this)
		Fabric.with(this, Crashlytics())
		AppInjector.init(this)
	}
	
	override fun attachBaseContext(base: Context) {
		super.attachBaseContext(base)
		MultiDex.install(this)
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
	}
	
	override fun activityInjector(): AndroidInjector<Activity>? {
		return dispatchingAndroidInjector
	}
	
	override fun serviceInjector(): AndroidInjector<Service>? {
		return serviceInjector
	}
}