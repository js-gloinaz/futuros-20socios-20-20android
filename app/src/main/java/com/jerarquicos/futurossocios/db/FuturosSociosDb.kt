package com.jerarquicos.futurossocios.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.jerarquicos.futurossocios.db.dao.AplicacionDao
import com.jerarquicos.futurossocios.db.dao.ExpedienteDao
import com.jerarquicos.futurossocios.db.dao.NotificacionFcmDao
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm

/**
 * Main database which contains DAOs.
 */
@Database(
        entities = [
            Expediente::class,
            DatosIniciales::class,
            NotificacionFcm::class
        ],
        version = 2,
        exportSchema = false
)
abstract class FuturosSociosDb : RoomDatabase() {

    abstract fun expedienteDao(): ExpedienteDao

    abstract fun aplicacionDao(): AplicacionDao

    abstract fun notificacionFcmDao(): NotificacionFcmDao
}
