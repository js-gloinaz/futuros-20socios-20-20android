package com.jerarquicos.futurossocios.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jerarquicos.futurossocios.db.entity.*
import java.util.*


class FuturosSociosTypeConverter {

    @TypeConverter
    fun toDate(value: Long?): Date? {
        return if (value == null) null else Date(value)
    }

    @TypeConverter
    fun toLong(value: Date?): Long? {
        return (value?.time)!!.toLong()
    }

    @TypeConverter
    fun fromDocumentacionAnexaList(documentacionAnexa: List<DocumentacionAnexa>?): String? {
        if (documentacionAnexa == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<DocumentacionAnexa>>() {

        }.type
        return gson.toJson(documentacionAnexa, type)
    }

    @TypeConverter
    fun toDocumentacionAnexaList(documentacionAnexaString: String?): List<DocumentacionAnexa>? {
        if (documentacionAnexaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<DocumentacionAnexa>>() {

        }.type
        return gson.fromJson<List<DocumentacionAnexa>>(documentacionAnexaString, type)
    }


    @TypeConverter
    fun fromEstadoExpediente(estadoExpediente: EstadoExpediente?): String? {
        if (estadoExpediente == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<EstadoExpediente>() {

        }.type
        return gson.toJson(estadoExpediente, type)
    }

    @TypeConverter
    fun toEstadoExpediente(estadoExpedienteString: String?): EstadoExpediente? {
        if (estadoExpedienteString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<EstadoExpediente>() {

        }.type
        return gson.fromJson<EstadoExpediente>(estadoExpedienteString, type)
    }

    @TypeConverter
    fun fromPlan(plan: Plan?): String? {
        if (plan == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Plan>() {

        }.type
        return gson.toJson(plan, type)
    }

    @TypeConverter
    fun toPlan(planString: String?): Plan? {
        if (planString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Plan>() {

        }.type
        return gson.fromJson<Plan>(planString, type)
    }

    @TypeConverter
    fun fromFormaPago(formaPago: FormaPago?): String? {
        if (formaPago == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<FormaPago>() {

        }.type
        return gson.toJson(formaPago, type)
    }

    @TypeConverter
    fun toFormaPago(formaPagoString: String?): FormaPago? {
        if (formaPagoString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<FormaPago>() {

        }.type
        return gson.fromJson<FormaPago>(formaPagoString, type)
    }

    @TypeConverter
    fun fromDeclaracionJurada(declaracionJurada: DeclaracionJurada?): String? {
        if (declaracionJurada == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<DeclaracionJurada>() {

        }.type
        return gson.toJson(declaracionJurada, type)
    }

    @TypeConverter
    fun toDeclaracionJurada(declaracionJuradaString: String?): DeclaracionJurada? {
        if (declaracionJuradaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<DeclaracionJurada>() {

        }.type
        return gson.fromJson<DeclaracionJurada>(declaracionJuradaString, type)
    }

    @TypeConverter
    fun fromPersonaDependienteList(personaDependiente: ArrayList<PersonaDependiente>?): String? {
        if (personaDependiente == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<PersonaDependiente>>() {

        }.type
        return gson.toJson(personaDependiente, type)
    }

    @TypeConverter
    fun toPersonaDependienteList(personaDependienteString: String?): ArrayList<PersonaDependiente>? {
        if (personaDependienteString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<PersonaDependiente>>() {

        }.type
        return gson.fromJson<ArrayList<PersonaDependiente>>(personaDependienteString, type)
    }

    @TypeConverter
    fun fromPlanList(plan: ArrayList<Plan>?): String? {
        if (plan == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Plan>>() {

        }.type
        return gson.toJson(plan, type)
    }

    @TypeConverter
    fun toPlanList(planString: String?): ArrayList<Plan>? {
        if (planString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Plan>>() {

        }.type
        return gson.fromJson<ArrayList<Plan>>(planString, type)
    }

    @TypeConverter
    fun fromTipoFormaPagoList(tipoFormaPago: ArrayList<TipoFormaPago>?): String? {
        if (tipoFormaPago == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoFormaPago>>() {

        }.type
        return gson.toJson(tipoFormaPago, type)
    }

    @TypeConverter
    fun toTipoFormaPagoList(tipoFormaPagoString: String?): ArrayList<TipoFormaPago>? {
        if (tipoFormaPagoString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoFormaPago>>() {

        }.type
        return gson.fromJson<ArrayList<TipoFormaPago>>(tipoFormaPagoString, type)
    }

    @TypeConverter
    fun fromTipoCuentaBancariaList(tipoCuentaBancaria: ArrayList<TipoCuentaBancaria>?): String? {
        if (tipoCuentaBancaria == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoCuentaBancaria>>() {

        }.type
        return gson.toJson(tipoCuentaBancaria, type)
    }

    @TypeConverter
    fun toTipoCuentaBancariaList(tipoCuentaBancariaString: String?): ArrayList<TipoCuentaBancaria>? {
        if (tipoCuentaBancariaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoCuentaBancaria>>() {

        }.type
        return gson.fromJson<ArrayList<TipoCuentaBancaria>>(tipoCuentaBancariaString, type)
    }

    @TypeConverter
    fun fromConviviente(conviviente: Conviviente?): String? {
        if (conviviente == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Conviviente>() {

        }.type
        return gson.toJson(conviviente, type)
    }

    @TypeConverter
    fun toConviviente(convivienteString: String?): Conviviente? {
        if (convivienteString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Conviviente>() {

        }.type
        return gson.fromJson<Conviviente>(convivienteString, type)
    }

    @TypeConverter
    fun toDocumentacionAnexa(documentacionAnexaString: String?): DocumentacionAnexa? {
        if (documentacionAnexaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<DocumentacionAnexa>() {

        }.type
        return gson.fromJson<DocumentacionAnexa>(documentacionAnexaString, type)
    }

    @TypeConverter
    fun fromTitular(titular: Titular?): String? {
        if (titular == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Titular>() {

        }.type
        return gson.toJson(titular, type)
    }

    @TypeConverter
    fun toTitular(titularString: String?): Titular? {
        if (titularString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<Titular>() {

        }.type
        return gson.fromJson<Titular>(titularString, type)
    }

    @TypeConverter
    fun fromLocalidadPlanoList(localidadPlano: ArrayList<LocalidadPlano>?): String? {
        if (localidadPlano == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<LocalidadPlano>>() {

        }.type
        return gson.toJson(localidadPlano, type)
    }

    @TypeConverter
    fun toLocalidadPlanoList(localidadPlanoString: String?): ArrayList<LocalidadPlano>? {
        if (localidadPlanoString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<LocalidadPlano>>() {

        }.type
        return gson.fromJson<ArrayList<LocalidadPlano>>(localidadPlanoString, type)
    }

    @TypeConverter
    fun fromPreguntaDeclaracionJuradaList(
            preguntaDeclaracionJurada: ArrayList<PreguntaDeclaracionJurada>?
    ): String? {
        if (preguntaDeclaracionJurada == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<PreguntaDeclaracionJurada>>() {

        }.type
        return gson.toJson(preguntaDeclaracionJurada, type)
    }

    @TypeConverter
    fun toPreguntaDeclaracionJuradaList(
            preguntaDeclaracionJuradaString: String?
    ): ArrayList<PreguntaDeclaracionJurada>? {
        if (preguntaDeclaracionJuradaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<PreguntaDeclaracionJurada>>() {

        }.type
        return gson.fromJson<ArrayList<PreguntaDeclaracionJurada>>(preguntaDeclaracionJuradaString, type)
    }

    @TypeConverter
    fun fromEstadoCivilList(estadoCivil: List<EstadoCivil>?): String? {
        if (estadoCivil == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<EstadoCivil>>() {

        }.type
        return gson.toJson(estadoCivil, type)
    }

    @TypeConverter
    fun toEstadoCivilList(estadoCivilString: String?): List<EstadoCivil>? {
        if (estadoCivilString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<EstadoCivil>>() {

        }.type
        return gson.fromJson<List<EstadoCivil>>(estadoCivilString, type)
    }

    @TypeConverter
    fun fromTipoDocumentacionAnexaList(tipoDocumentacionAnexa: ArrayList<TipoDocumentacionAnexa>?): String? {
        if (tipoDocumentacionAnexa == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoDocumentacionAnexa>>() {

        }.type
        return gson.toJson(tipoDocumentacionAnexa, type)
    }

    @TypeConverter
    fun toTipoDocumentacionAnexaList(tipoDocumentacionAnexaString: String?): ArrayList<TipoDocumentacionAnexa>? {
        if (tipoDocumentacionAnexaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoDocumentacionAnexa>>() {

        }.type
        return gson.fromJson<ArrayList<TipoDocumentacionAnexa>>(tipoDocumentacionAnexaString, type)
    }

    @TypeConverter
    fun fromCondicionLaboralList(condicionLaboral: ArrayList<CondicionLaboral>?): String? {
        if (condicionLaboral == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<CondicionLaboral>>() {

        }.type
        return gson.toJson(condicionLaboral, type)
    }

    @TypeConverter
    fun toCondicionLaboralList(condicionLaboralString: String?): ArrayList<CondicionLaboral>? {
        if (condicionLaboralString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<CondicionLaboral>>() {

        }.type
        return gson.fromJson<ArrayList<CondicionLaboral>>(condicionLaboralString, type)
    }

    @TypeConverter
    fun fromProvinciaList(provincia: ArrayList<Provincia>?): String? {
        if (provincia == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Provincia>>() {

        }.type
        return gson.toJson(provincia, type)
    }

    @TypeConverter
    fun toProvinciaList(provinciaString: String?): ArrayList<Provincia>? {
        if (provinciaString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Provincia>>() {

        }.type
        return gson.fromJson<ArrayList<Provincia>>(provinciaString, type)
    }

    @TypeConverter
    fun fromParentescoList(parentesco: List<Parentesco>?): String? {
        if (parentesco == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<Parentesco>>() {

        }.type
        return gson.toJson(parentesco, type)
    }

    @TypeConverter
    fun toParentescoList(parentescoString: String?): List<Parentesco>? {
        if (parentescoString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<List<Parentesco>>() {

        }.type
        return gson.fromJson<List<Parentesco>>(parentescoString, type)
    }

    @TypeConverter
    fun fromTipoTarjetaCreditoList(tipoTarjetaCredito: ArrayList<TipoTarjetaCredito>?): String? {
        if (tipoTarjetaCredito == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoTarjetaCredito>>() {

        }.type
        return gson.toJson(tipoTarjetaCredito, type)
    }

    @TypeConverter
    fun toTipoTarjetaCreditoList(tipoTarjetaCreditoString: String?): ArrayList<TipoTarjetaCredito>? {
        if (tipoTarjetaCreditoString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<TipoTarjetaCredito>>() {

        }.type
        return gson.fromJson<ArrayList<TipoTarjetaCredito>>(tipoTarjetaCreditoString, type)
    }

    @TypeConverter
    fun fromSexoList(sexo: ArrayList<Sexo>?): String? {
        if (sexo == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Sexo>>() {

        }.type
        return gson.toJson(sexo, type)
    }

    @TypeConverter
    fun toSexoList(sexoString: String?): ArrayList<Sexo>? {
        if (sexoString == null) {
            return null
        }
        val gson = Gson()
        val type = object : TypeToken<ArrayList<Sexo>>() {

        }.type
        return gson.fromJson<ArrayList<Sexo>>(sexoString, type)
    }
}
