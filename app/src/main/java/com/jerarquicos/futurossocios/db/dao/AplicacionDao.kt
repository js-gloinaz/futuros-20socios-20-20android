package com.jerarquicos.futurossocios.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jerarquicos.futurossocios.db.entity.DatosIniciales


/***
 * Aplicación DAO
 */
@Dao
abstract class AplicacionDao {

    @Query("SELECT * FROM DatosIniciales")
    abstract fun obtenerDatosIniciales(): LiveData<DatosIniciales>

    @Query("SELECT * FROM DatosIniciales")
    abstract fun obtenerDatosInicialesEntity(): DatosIniciales

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun guardarDatosIniciales(datosIniciales: DatosIniciales)
}