package com.jerarquicos.futurossocios.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import com.jerarquicos.futurossocios.db.entity.DocumentacionAnexa
import com.jerarquicos.futurossocios.db.entity.Expediente

/***
 * Expediente DAO
 */
@Dao
@TypeConverters(FuturosSociosTypeConverter::class)
abstract class ExpedienteDao {

    @Query("SELECT * FROM Expediente")
    abstract fun obtenerExpedientesBAK(): LiveData<List<Expediente>>

    @Query("SELECT * FROM Expediente")
    abstract fun obtenerExpedientes(): List<Expediente>

    @Query("SELECT * FROM Expediente WHERE idAplicacion = :idExpedienteAplicacion")
    abstract fun obtenerExpediente(idExpedienteAplicacion: String): Expediente

    @Query("SELECT * FROM Expediente WHERE idAplicacion = :idExpedienteAplicacion")
    abstract fun obtenerExpedienteDashboard(idExpedienteAplicacion: String): Expediente?

    @Query("SELECT * FROM Expediente WHERE idAplicacion = :idExpedienteAplicacion")
    abstract fun obtenerExpedienteLiveData(idExpedienteAplicacion: String): LiveData<Expediente>

    @Query("SELECT documentacionAnexa FROM Expediente WHERE idAplicacion = :idExpedienteAplicacion")
    abstract fun obtenerDocumentacionAnexa(idExpedienteAplicacion: String): LiveData<List<DocumentacionAnexa>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun guardarExpediente(expediente: Expediente)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun guardarExpedienteServidor(expediente: Expediente)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun guardarExpedientes(expedientes: List<Expediente>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun nuevoExpediente(expediente: Expediente): Long

    @Query("DELETE FROM Expediente")
    abstract fun borrarExpedientes()

    @Query("DELETE FROM Expediente WHERE idAplicacion = :idAplicacion")
    abstract fun borrarExpediente(idAplicacion: String)

}