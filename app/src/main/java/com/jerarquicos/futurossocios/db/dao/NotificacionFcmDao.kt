package com.jerarquicos.futurossocios.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm

/***
 * Notificacion DAO
 */

@Dao
abstract class NotificacionFcmDao {

    @Query("SELECT * FROM NotificacionFcm")
    abstract fun obtenerNotificaciones(): LiveData<List<NotificacionFcm>>
	
	@Query("SELECT COUNT(*) FROM NotificacionFcm WHERE NOT leida")
	abstract fun obtenerCantidadNotificacionesSinLeer(): LiveData<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun guardarNotificacionFcm(notificacion: NotificacionFcm)
	
	@Query("DELETE FROM NotificacionFcm")
	abstract fun borrarNotificaciones()
	
    @Query("DELETE FROM NotificacionFcm WHERE id = :id")
    abstract fun borrarNotificacion(id: Int)

	@Query("UPDATE NotificacionFcm SET leida = :estado")
	abstract fun actualizarEstadoNotificaciones(estado: Boolean)

}
