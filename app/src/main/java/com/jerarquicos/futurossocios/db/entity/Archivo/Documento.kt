package com.jerarquicos.futurossocios.db.entity.Archivo

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import java.io.Serializable

class Documento() : Serializable, Parcelable {

    var archivo: ByteArray? = null
    var path: String? = null
    var tamanio: Float = 0F
    var id: Int = 0
    var imagenBitmap: Bitmap? = null
    var esPdf: Boolean = false
    var archivoAgregadoExitosamente: Boolean = false
    var cargando: Boolean = false
    var rutaArchivo: String? = null

    constructor(parcel: Parcel) : this() {
        archivo = parcel.createByteArray()
        path = parcel.readString()
        tamanio = parcel.readFloat()
        id = parcel.readInt()
        imagenBitmap = parcel.readParcelable(Bitmap::class.java.classLoader)
        esPdf = parcel.readByte() != 0.toByte()
        archivoAgregadoExitosamente = parcel.readByte() != 0.toByte()
        cargando = parcel.readByte() != 0.toByte()
        rutaArchivo = parcel.readString()
    }

    override fun toString(): String {
        return "El documento es: $path con un tamaño de $tamanio MB"
    }

    fun reciclarImagenBitmap() {
        if (imagenBitmap != null && !imagenBitmap!!.isRecycled) {
            imagenBitmap!!.recycle()
            imagenBitmap = null
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByteArray(archivo)
        parcel.writeString(path)
        parcel.writeFloat(tamanio)
        parcel.writeInt(id)
        parcel.writeParcelable(imagenBitmap, flags)
        parcel.writeByte(if (esPdf) 1 else 0)
        parcel.writeByte(if (archivoAgregadoExitosamente) 1 else 0)
        parcel.writeByte(if (cargando) 1 else 0)
        parcel.writeString(rutaArchivo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Documento> {
        override fun createFromParcel(parcel: Parcel): Documento {
            return Documento(parcel)
        }

        override fun newArray(size: Int): Array<Documento?> {
            return arrayOfNulls(size)
        }
    }

}
