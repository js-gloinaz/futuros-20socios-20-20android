package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

@Entity
class CondicionLaboral(
        var id: Int,
        var descripcion: String,
        var detalle: String
) : Serializable