package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

/***
 * ContexturaFisica Entity
 */
@Entity
class ContexturaFisica(
        var id: Int,
        var peso: Float?,
        var talla: Float?
) : Serializable {
    constructor() : this(0, 0.0F, 0.0F)
}