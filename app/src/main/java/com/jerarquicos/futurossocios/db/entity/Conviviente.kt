package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable
import java.util.*

/***
 * Conviviente Entity
 */
@Entity
class Conviviente(
        var estadoCivil: EstadoCivil?,
        var cantidadHijos: Int?,
        var telefonoParticular: String?,
        var telefonoCelular: Telefono?,
        var domicilio: ArrayList<Domicilio>?,
        var empleador: Empleador?,
        var condicionLaboral: CondicionLaboral?,
        var email: String?
) : Persona(), Serializable {
    constructor() : this(null, null, null, null,
            null, null, null, null)
}
