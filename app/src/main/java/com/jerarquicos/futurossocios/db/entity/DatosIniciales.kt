package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable

/***
 * Domicilio Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class DatosIniciales(
        @PrimaryKey(autoGenerate = false)
        var id: Int = 1,
        var localidad: ArrayList<LocalidadPlano>,
        var provincia: ArrayList<Provincia>,
        var estadoCivil: List<EstadoCivil>,
        var parentesco: List<Parentesco>,
        var condicionLaboral: ArrayList<CondicionLaboral>,
        var tipoDocumentacionAnexa: ArrayList<TipoDocumentacionAnexa>,
        var preguntaDeclaracionJurada: ArrayList<PreguntaDeclaracionJurada>,
        var tipoFormaPago: ArrayList<TipoFormaPago>,
        var tipoCuentaBancaria: ArrayList<TipoCuentaBancaria>,
        var plan: ArrayList<Plan>,
        var sexo: ArrayList<Sexo>,
        var tipoTarjetaCredito: ArrayList<TipoTarjetaCredito>,
        var version: Int
) : Serializable