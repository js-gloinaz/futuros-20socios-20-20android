package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable
import java.util.*

/***
 * Expediente Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class DeclaracionJurada(
        var id: Int?,
        var respuestaPreguntaDeclaracionJurada: ArrayList<RespuestaPreguntaDeclaracionJurada>?
) : Serializable {
    constructor() : this(0, null)
}