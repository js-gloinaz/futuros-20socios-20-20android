package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import com.jerarquicos.futurossocios.db.entity.cuenta.Archivo
import java.io.Serializable
import java.util.*

/***
 * DocumentacionAnexa Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class DocumentacionAnexa(
        var id: Int,
        var tipoDocumentacionAnexa: TipoDocumentacionAnexa,
        var archivo: ArrayList<Archivo>
) : Serializable