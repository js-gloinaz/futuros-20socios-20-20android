package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

/***
 * Domicilio Entity
 */
@Entity
class Domicilio(
        var orden: Int?,
        var id: Int?,
        var calle: String?,
        var numero: String?,
        var piso: String?,
        var departamento: String?,
        var localidad: Localidad?,
        var envioCorrespondencia: Boolean?
) : Serializable {
    constructor(mOrden: Int) : this(mOrden, 0, null, null, null,
            null, null, null)
}