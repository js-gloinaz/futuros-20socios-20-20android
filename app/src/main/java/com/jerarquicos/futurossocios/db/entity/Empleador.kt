package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable
import java.util.*

@Entity
class Empleador(
        var id: Int?,
        var denominacion: String?,
        var cuit: String?,
        var cargo: String?,
        var sueldoBruto: Float?,
        var fechaInicio: Date?,
        var telefono: Telefono?,
        var email: String?,
        var domicilio: Domicilio?
) : Serializable {
    constructor() : this(0, null, null, null,
            null, null, null, null, null)
}