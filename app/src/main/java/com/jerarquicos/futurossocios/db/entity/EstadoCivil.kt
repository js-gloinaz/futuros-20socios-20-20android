package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

@Entity
class EstadoCivil(
        var id: Int?,
        var descripcion: String?
) : Serializable {
    constructor() : this(0, null)
}