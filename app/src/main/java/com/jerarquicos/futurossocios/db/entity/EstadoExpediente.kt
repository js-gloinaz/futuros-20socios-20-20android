package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class EstadoExpediente(
        @PrimaryKey(autoGenerate = false)
        var id: Int,
        var descripcion: String
) : Serializable