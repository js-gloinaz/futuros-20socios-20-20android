package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable
import java.util.*

/***
 * Expediente Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class Expediente(
        @PrimaryKey(autoGenerate = false)
        var idAplicacion: String,
        var id: Int?,
        var usuario: String?,
        var estado: EstadoExpediente,
        var documentacionAnexa: List<DocumentacionAnexa>?,
        var fechaCreacion: Date?,
        var personaDependiente: ArrayList<PersonaDependiente>?,
        var titular: Titular?,
        var conviviente: Conviviente?,
        var declaracionJurada: DeclaracionJurada?,
        var formaPago: FormaPago?,
        var plan: Plan?,
        var firmaGrafometrica: String?,
        var ingresaConviviente: Boolean?,
        var ingresaDependientes: Boolean?,
        var consentimientoDeclaracionJurada: Boolean?,
        var protesisOdontologicas: Boolean?,
        var version: Int?,
        var observacion: String?

) : Serializable {
    constructor(
            mIdAplicacion: String,
            mEstadoExpediente: EstadoExpediente,
            mFechaCreacion: Date
    ) : this(
            mIdAplicacion,
            0,
            null,
            mEstadoExpediente,
            null,
            mFechaCreacion,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null, null, null, null)

    constructor(
            estadoExpediente: EstadoExpediente
    ) : this(
            "0",
            0,
            null,
            estadoExpediente,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
    )
}