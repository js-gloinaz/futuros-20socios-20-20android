package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable
import java.util.*

/***
 * Forma de Pago Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class FormaPago(
        var id: Int,
        var tipoFormaPago: TipoFormaPago?,
        var tipoCuentaBancaria: TipoCuentaBancaria?,
        var tipoTarjetaCredito: TipoTarjetaCredito?,
        var cbu: String?,
        var numeroTarjeta: String?,
        var vencimiento: Date?

) : Serializable {
    constructor() : this(0, null, null,
            null, null, null, null)
}
