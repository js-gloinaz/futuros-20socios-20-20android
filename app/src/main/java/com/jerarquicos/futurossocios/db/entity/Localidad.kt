package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

@Entity
class Localidad(
        var id: Int?,
        var descripcion: String?,
        var codigoPostal: String?,
        var provincia: Provincia?
) : Serializable {
    constructor() : this(0, null, null, null)
}