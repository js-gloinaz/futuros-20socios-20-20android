package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

@Entity
class LocalidadPlano(
        var id: Int?,
        var descripcion: String?,
        var codigoPostal: String?,
        var idProvincia: Int?
) : Serializable {
    constructor() : this(0, null, null, null)
}