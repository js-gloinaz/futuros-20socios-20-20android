package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable

/***
 * Notificacion Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class NotificacionFcm(
        @PrimaryKey(autoGenerate = false)
        var id: Int,
        var titulo: String?,
        var mensaje: String?,
        var tipo: Int?,
        var usuario: String?,
        var dniTitular: String?,
        var idExpediente: String?,
        var leida: Boolean = false
) : Serializable {
        constructor() : this(1, null, null, null, null, null, null, false)
}

