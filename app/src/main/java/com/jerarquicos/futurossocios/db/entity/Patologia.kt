package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable

/***
 * Expediente Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class Patologia(
        var id: Int,
        var idInterno: Int?,
        var descripcionInterna: String?,
        var descripcionExterna: String?,
        var requiereObservacion: Boolean?,
        var leyendaObservacion: String?
) : Serializable {
    constructor() : this(0, null, null,
            null, null, null)
}