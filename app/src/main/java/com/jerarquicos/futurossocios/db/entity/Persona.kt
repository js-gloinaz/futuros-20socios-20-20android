package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable
import java.util.*

/***
 * Expediente Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
open class Persona(
        var id: Int,
        var nombre: String?,
        var apellido: String?,
        var dni: String?,
        var cuil: String?,
        var fechaNacimiento: Date?,
        var documentacionAnexa: ArrayList<DocumentacionAnexa>?,
        var contexturaFisica: ContexturaFisica?,
        var sexo: Sexo?
) : Serializable {
    constructor() : this(0, null, null, null,
            null, null, null, null, null)
}