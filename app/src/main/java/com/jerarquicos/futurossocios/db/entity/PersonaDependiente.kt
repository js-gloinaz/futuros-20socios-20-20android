package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

@Entity
class PersonaDependiente(
        var posicion: Int?,
        var parentesco: Parentesco?

) : Persona(), Serializable {
    constructor(mOrden: Int) : this(mOrden, null)
}