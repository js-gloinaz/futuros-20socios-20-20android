package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import com.jerarquicos.futurossocios.view.models.PersonaBasicoModel
import java.io.Serializable

/***
 * Expediente Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class PersonaPatologia(
        @PrimaryKey(autoGenerate = true)
        var id: Int,
        var persona: PersonaBasicoModel?,
        var patologia: Patologia?,
        var observacion: String?
) : Serializable {
    constructor() : this(0, null, null, null)
}