package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable
import java.util.*

/***
 * Expediente Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class PreguntaDeclaracionJurada(
        @PrimaryKey(autoGenerate = true)
        var id: Int,
        var orden: Int?,
        var tipo: Int?,
        var titulo: String?,
        var descripcion: String?,
        var patologia: ArrayList<Patologia>?
) : Serializable {
    constructor() : this(0, null, null, null, null, null)
}


