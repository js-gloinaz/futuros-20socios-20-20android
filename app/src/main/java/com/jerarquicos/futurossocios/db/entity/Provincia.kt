package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class Provincia(
        @PrimaryKey(autoGenerate = false)
        @Ignore
        var id: Int?,
        @Ignore
        var descripcion: String?,
        @Ignore
        var idPais: Int?
) : Serializable {
    constructor() : this(0, null, null)
}