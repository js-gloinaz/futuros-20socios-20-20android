package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import java.io.Serializable

/***
 * RespuestaPatologia Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class RespuestaPatologia(
        var id: Int,
        var valor: Boolean?,
        var personaPatologia: ArrayList<PersonaPatologia>?,
        var patologia: Patologia?
) : Serializable {
    constructor() : this(0, null, null, null)
}
