package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.TypeConverters
import com.jerarquicos.futurossocios.db.FuturosSociosTypeConverter
import com.jerarquicos.futurossocios.view.models.PreguntaBasicoModel
import java.io.Serializable
import java.util.*

/***
 * RespuestaPreguntaDeclaracionJurada Entity
 */
@Entity
@TypeConverters(FuturosSociosTypeConverter::class)
class RespuestaPreguntaDeclaracionJurada(
        var id: Int,
        var preguntaDeclaracionJurada: PreguntaBasicoModel?,
        var respuestaPatologia: ArrayList<RespuestaPatologia>?
) : Serializable {
    constructor() : this(0, null, null)
}