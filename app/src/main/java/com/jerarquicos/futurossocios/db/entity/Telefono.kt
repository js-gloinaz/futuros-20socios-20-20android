package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class Telefono(
        @PrimaryKey(autoGenerate = true)
        var id: Int?,
        var codigoInternacional: String?,
        var codigoArea: String?,
        var numero: String?
) : Serializable {
    constructor() : this(0, null, null, null)
}