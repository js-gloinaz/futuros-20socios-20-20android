package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

@Entity
class TipoDocumentacionAnexa(
        var id: Int,
        var descripcion: String
) : Serializable