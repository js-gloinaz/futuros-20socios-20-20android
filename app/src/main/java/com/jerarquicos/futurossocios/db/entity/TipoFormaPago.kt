package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable

/***
 * Tipo Cuenta Bancaria Entity
 */
@Entity
class TipoFormaPago(
        var id: Int,
        var descripcion: String?,
        var descripcionAdicional: String?
) : Serializable {
    constructor() : this(0, null, null)
}