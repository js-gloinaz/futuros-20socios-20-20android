package com.jerarquicos.futurossocios.db.entity

import androidx.room.Entity
import java.io.Serializable
import java.util.*

@Entity
class Titular(
        var estadoCivil: EstadoCivil?,
        var enConvivencia: Boolean?,
        var fechaInicioConvivencia: Boolean?,
        var cantidadHijos: Int?,
        var telefonoParticular: Telefono?,
        var telefonoCelular: Telefono?,
        var tieneObraSocial: Boolean?,
        var obraSocialActual: String?,
        var domicilio: ArrayList<Domicilio>?,
        var empleador: Empleador?,
        var email: String?,
        var condicionLaboral: CondicionLaboral?,
        var personaPoliticamenteExpuesta: Boolean?
) : Persona(), Serializable {
    constructor() : this(null, null, null, null,
            null, null, null, null, null,
            null, null, null, null)
}