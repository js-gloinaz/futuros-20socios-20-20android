package com.jerarquicos.futurossocios.db.entity.cuenta

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class Archivo(
        @PrimaryKey(autoGenerate = false)
        var id: Int,
        var nombre: String
) : Serializable
