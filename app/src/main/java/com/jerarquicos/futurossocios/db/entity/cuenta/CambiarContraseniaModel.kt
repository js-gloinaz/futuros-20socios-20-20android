package com.jerarquicos.futurossocios.db.entity.cuenta

class CambiarContraseniaModel(
        var userName: String?
        , var oldPassword: String?
        , var newPassword: String?
        , var confirmPassword: String?
        , var autenticado: Boolean?
) {
    constructor() : this(
            null,
            null,
            null,
            null,
            null
    )
}