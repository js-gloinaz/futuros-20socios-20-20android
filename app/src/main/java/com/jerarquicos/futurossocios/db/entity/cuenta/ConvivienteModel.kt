package com.jerarquicos.futurossocios.db.entity.cuenta

import com.jerarquicos.futurossocios.db.entity.Domicilio
import com.jerarquicos.futurossocios.db.entity.Empleador
import com.jerarquicos.futurossocios.db.entity.EstadoCivil
import java.io.Serializable

class ConvivienteModel(
        var apellido: String?,
        var nombre: String?,
        var estadoCivil: EstadoCivil?,
        var fechaNacimiento: String?,
        var enConvivencia: Boolean?,
        var fechaInicioConvivencia: Boolean?,
        var cantidadHijos: Integer?,
        var dni: String?,
        var telefonoParticular: String?,
        var telefonoCelular: String?,
        var cuil: String?,
        var domicilio: ArrayList<Domicilio>?,
        var empleador: Empleador?,
        var email: String?,
        var trabaja: Boolean?) : Serializable {

    constructor() : this(null, null, null, null, null, null,
            null, null, null, null, null,
            null, null, null, null)
}
