package com.jerarquicos.futurossocios.db.entity.cuenta

/***
 * Dispositivo Model
 */
class DispositivoModel(
        var usuario: String?,
        var registrationId: String?,
        var manufacter: String?,
        var model: String?,
        var serial: String?,
        var versionName: String?,
        var versionCode: String?,
        var versionAndroid: String?,
        var uuid: String?
) {
    constructor() : this(null, null, null, null, null, null, null, null, null)
}