package com.jerarquicos.futurossocios.db.entity.cuenta

import androidx.fragment.app.Fragment
import java.io.Serializable

class EstadoPasoExpediente(
        var idEstado: Int,
        var descripcion: Int,
        var habilitaPasoSiguiente: Boolean,
        var fragmentReferenciado: Fragment
) : Serializable {
    constructor(
            mEstado: Int,
            mDescripcion: Int,
            mFragmentReferenciado: Fragment
    ) : this(
            mEstado, mDescripcion,
            false,
            mFragmentReferenciado
    )
}