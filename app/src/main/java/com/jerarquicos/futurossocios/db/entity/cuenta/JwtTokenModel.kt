package com.jerarquicos.futurossocios.db.entity.cuenta

import java.util.*

/***
 * Token Model
 */
class JwtTokenModel(
        var value: String,
        var validTo: Date,
        var email: String,
        var usuario: String,
        var nombre: String,
        var apellido: String,
        var primerLogin: Boolean
)