package com.jerarquicos.futurossocios.db.entity.cuenta

/***
 * Login Entity
 */

class LoginModel(var usuario: String?, var password: String?) {
    constructor() : this(null, null)
}
