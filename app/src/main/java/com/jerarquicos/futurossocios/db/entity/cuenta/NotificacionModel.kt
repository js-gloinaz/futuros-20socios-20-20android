package com.jerarquicos.futurossocios.db.entity.cuenta

import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion

class NotificacionModel(var mensaje: String, var tipoNotificacion: TipoNotificacion)