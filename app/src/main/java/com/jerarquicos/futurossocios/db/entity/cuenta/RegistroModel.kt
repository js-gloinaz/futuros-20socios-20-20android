package com.jerarquicos.futurossocios.db.entity.cuenta

import java.util.*

class RegistroModel(var nombre: String?,
                    var apellido: String?,
                    var numeroDocumento: String?,
                    var email: String?,
                    var contrasenia: String?,
                    var contraseniaConfirmacion: String?,
                    var fechaNacimiento: Date?) {
    constructor() : this(null, null, null, null, null, null, null)
}
