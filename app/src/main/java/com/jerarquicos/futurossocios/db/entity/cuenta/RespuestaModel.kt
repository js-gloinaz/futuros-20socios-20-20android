package com.jerarquicos.futurossocios.db.entity.cuenta

class RespuestaModel<Any>(
        var modelo: Any?,
        var exito: Boolean,
        var notificacion: NotificacionModel
)