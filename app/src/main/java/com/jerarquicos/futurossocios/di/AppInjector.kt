package com.jerarquicos.futurossocios.di

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager


import com.jerarquicos.futurossocios.FuturosSociosApp
import com.jerarquicos.futurossocios.di.component.DaggerAppComponent

import dagger.android.AndroidInjection
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.HasSupportFragmentInjector


object AppInjector {

    fun init(futurosSociosApp: FuturosSociosApp) {
        DaggerAppComponent.builder().application(futurosSociosApp)
                .build().inject(futurosSociosApp)

        futurosSociosApp.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, bundle: Bundle?) {
                handleActivity(activity)
            }

            override fun onActivityStarted(activity: Activity) {
                // default implementation ignored
            }

            override fun onActivityResumed(activity: Activity) {
                // default implementation ignored
            }

            override fun onActivityPaused(activity: Activity) {
                // default implementation ignored
            }

            override fun onActivityStopped(activity: Activity) {
                // default implementation ignored
            }

            override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle?) {
                // default implementation ignored
            }

            override fun onActivityDestroyed(activity: Activity) {
                // default implementation ignored
            }
        })
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasSupportFragmentInjector) {
            AndroidInjection.inject(activity)
        }

        if (activity is FragmentActivity) {
            activity.supportFragmentManager
                    .registerFragmentLifecycleCallbacks(object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentAttached(fm: FragmentManager, f: Fragment, context: Context) {
                            if (f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    }, true)
        }
    }
}