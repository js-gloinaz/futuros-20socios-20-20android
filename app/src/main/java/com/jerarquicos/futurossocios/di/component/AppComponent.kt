package com.jerarquicos.futurossocios.di.component

import android.app.Application
import com.jerarquicos.futurossocios.FuturosSociosApp
import com.jerarquicos.futurossocios.di.module.AppModule
import com.jerarquicos.futurossocios.di.module.MainActivityModule
import com.jerarquicos.futurossocios.di.module.RoomModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton
import com.jerarquicos.futurossocios.di.module.ServicesModule




@Singleton
@Component(
        modules = [
            AndroidInjectionModule::class,
            AppModule::class,
            MainActivityModule::class,
            RoomModule::class,
            ServicesModule::class
        ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(futurosSociosApp: FuturosSociosApp)
}
