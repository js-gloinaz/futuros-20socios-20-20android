package com.jerarquicos.futurossocios.di.module

import android.app.Application
import android.content.Context
import com.jerarquicos.futurossocios.repository.apiService.controllers.IAplicacion
import com.jerarquicos.futurossocios.repository.apiService.controllers.ICuenta
import com.jerarquicos.futurossocios.repository.apiService.controllers.IDocumentos
import com.jerarquicos.futurossocios.repository.apiService.controllers.IExpedienteService
import com.jerarquicos.futurossocios.utils.SharedPreferencesConstants
import com.securepreferences.SecurePreferences
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = arrayOf(ViewModelModule::class, NetworkModule::class))
class AppModule {

    @Provides
    @Singleton
    internal fun provideCuentaService(retrofit: Retrofit): ICuenta {
        return retrofit.create(ICuenta::class.java)
    }

    @Provides
    @Singleton
    internal fun provideAplicacionService(retrofit: Retrofit): IAplicacion {
        return retrofit.create(IAplicacion::class.java)
    }

    @Provides
    @Singleton
    internal fun provideExpedienteService(retrofit: Retrofit): IExpedienteService {
        return retrofit.create(IExpedienteService::class.java)
    }

    @Provides
    @Singleton
    internal fun provideDocumentosService(retrofit: Retrofit): IDocumentos {
        return retrofit.create(IDocumentos::class.java)
    }

    @Singleton
    @Provides
    fun provideContext(app: Application): Context {
        return app.applicationContext
    }

    @Provides
    @Singleton
    fun providePreference(app: Application): SecurePreferences = SecurePreferences(
            app,
            SharedPreferencesConstants.PREFERENCE_FILENAME,
            SharedPreferencesConstants.PREFERENCE_FILENAME)
}
