package com.jerarquicos.futurossocios.di.module

import com.jerarquicos.futurossocios.view.aporteObraSocial.AporteObraSocialFragment
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.cambiarContrasenia.CambiarContraseniaFragment
import com.jerarquicos.futurossocios.view.cargaCbu.CargaCbuFragment
import com.jerarquicos.futurossocios.view.comienzo.ComienzoFragment
import com.jerarquicos.futurossocios.view.comienzoSeccion.ComienzoSeccionFragment
import com.jerarquicos.futurossocios.view.consentimientoDeclaracionJurada.ConsentimientoFragment
import com.jerarquicos.futurossocios.view.contexturaFisica.ContexturaFisicaFragment
import com.jerarquicos.futurossocios.view.conviviente.ConvivienteFragment
import com.jerarquicos.futurossocios.view.dashboard.DashboardFragment
import com.jerarquicos.futurossocios.view.declaracionJurada.PersonasBottomSheet
import com.jerarquicos.futurossocios.view.declaracionJurada.PreguntaDeclaracionJuradaFragment
import com.jerarquicos.futurossocios.view.declaracionJurada.PreguntaTipoDosFragment
import com.jerarquicos.futurossocios.view.domicilio.DomicilioFragment
import com.jerarquicos.futurossocios.view.empleador.DatosEmpleadorFragment
import com.jerarquicos.futurossocios.view.expedientesEnviados.ExpedientesEnviadosFragment
import com.jerarquicos.futurossocios.view.finalSeccion.FinalSeccionFragment
import com.jerarquicos.futurossocios.view.firmaGrafometrica.FirmaGrafometricaFragment
import com.jerarquicos.futurossocios.view.firmaGrafometrica.InformacionFirmaGrafometricaFragment
import com.jerarquicos.futurossocios.view.gestorDocumentos.gestorDocumentos.GestorDocumentosFragment
import com.jerarquicos.futurossocios.view.gestorDocumentos.informacionDocumentacionAnexa
        .InformacionDocumentacionAnexaFragment
import com.jerarquicos.futurossocios.view.gestorDocumentos.visualizadorImagen.VisualizadorImagenFragment
import com.jerarquicos.futurossocios.view.inicio.InicioFragment
import com.jerarquicos.futurossocios.view.listaResultado.ListaResultadoFragment
import com.jerarquicos.futurossocios.view.login.LoginFragment
import com.jerarquicos.futurossocios.view.notificacionFcm.NotificacionFcmFragment
import com.jerarquicos.futurossocios.view.observacionExpediente.ObservacionExpedienteFragment
import com.jerarquicos.futurossocios.view.personaACargo.PersonaACargoFragment
import com.jerarquicos.futurossocios.view.opciones.OpcionesFragment
import com.jerarquicos.futurossocios.view.presentacionSeccion.PresentacionSeccionFragment
import com.jerarquicos.futurossocios.view.presentacionSeccion.presentacionFormaPago.PresentacionFragment
import com.jerarquicos.futurossocios.view.registro.RegistroFragment
import com.jerarquicos.futurossocios.view.restablecerContrasenia.RestablecerContraseniaFragment
import com.jerarquicos.futurossocios.view.splash.SplashFragment
import com.jerarquicos.futurossocios.view.tarjetaDeCredito.TarjetaDeCreditoFragment
import com.jerarquicos.futurossocios.view.titular.TitularFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun contributeSplashFragment(): SplashFragment

    @ContributesAndroidInjector
    internal abstract fun contributeLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    internal abstract fun contributeInicioFragment(): InicioFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCambiarContraseniaFragment(): CambiarContraseniaFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRestablecerContraseniaFragment(): RestablecerContraseniaFragment

    @ContributesAndroidInjector
    internal abstract fun contributeRegistroFragment(): RegistroFragment

    @ContributesAndroidInjector
    internal abstract fun contributeComienzoFragment(): ComienzoFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFinalSeccionFragment(): FinalSeccionFragment

    @ContributesAndroidInjector
    internal abstract fun contributeGestorDocumentosFragment(): GestorDocumentosFragment

    @ContributesAndroidInjector
    internal abstract fun contributeInformacionDocumentacionAnexaFragment(): InformacionDocumentacionAnexaFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDatosEmpleadorFragment(): DatosEmpleadorFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDomicilioFragment(): DomicilioFragment

    @ContributesAndroidInjector
    internal abstract fun contributeListaResultadoFragment(): ListaResultadoFragment

    @ContributesAndroidInjector
    internal abstract fun contributeTitularFragment(): TitularFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDashboardFragment(): DashboardFragment

    @ContributesAndroidInjector
    internal abstract fun contributeConvivienteFragment(): ConvivienteFragment

    @ContributesAndroidInjector
    internal abstract fun contributeVisualizadorImagenFragment(): VisualizadorImagenFragment

    @ContributesAndroidInjector
    internal abstract fun contributeContexturaFisicaFragment(): ContexturaFisicaFragment

    @ContributesAndroidInjector
    internal abstract fun contributePreguntaDeclaracionJuradaFragment(): PreguntaDeclaracionJuradaFragment

    @ContributesAndroidInjector
    internal abstract fun contributeComienzoSeccionFragment(): ComienzoSeccionFragment

    @ContributesAndroidInjector
    internal abstract fun contributePersonaACargoFragment(): PersonaACargoFragment

    @ContributesAndroidInjector
    internal abstract fun contributePersonasBottomSheet(): PersonasBottomSheet

    @ContributesAndroidInjector
    internal abstract fun contributeAporteObraSocialFragment(): AporteObraSocialFragment

    @ContributesAndroidInjector
    internal abstract fun contributeConsentimientoDeclaracionJuradaFragment(): ConsentimientoFragment

    @ContributesAndroidInjector
    internal abstract fun contributePreguntaTipoDosFragment(): PreguntaTipoDosFragment

    @ContributesAndroidInjector
    internal abstract fun contributeInformacionFirmaGrafometricaFragment(): InformacionFirmaGrafometricaFragment

    @ContributesAndroidInjector
    internal abstract fun contributeFirmaGrafometricaFragment(): FirmaGrafometricaFragment

    @ContributesAndroidInjector
    internal abstract fun contributePresentacionSeccionFragment(): PresentacionSeccionFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCargaCbuFragment(): CargaCbuFragment

    @ContributesAndroidInjector
    internal abstract fun contributeTarjetaDeCreditoFragment(): TarjetaDeCreditoFragment

    @ContributesAndroidInjector
    internal abstract fun contributeObservacionExpedienteFragment(): ObservacionExpedienteFragment

    @ContributesAndroidInjector
    internal abstract fun contributeExpedientesEnviadosFragment(): ExpedientesEnviadosFragment

    @ContributesAndroidInjector
    internal abstract fun contributeNotificacionGcmFragment(): NotificacionFcmFragment

    @ContributesAndroidInjector
    internal abstract fun contributeOpcionesFragment(): OpcionesFragment

    @ContributesAndroidInjector
    internal abstract fun contributeBaseFragment(): BaseFragment

    @ContributesAndroidInjector
    internal abstract fun contributePresentacionFragment(): PresentacionFragment
}
