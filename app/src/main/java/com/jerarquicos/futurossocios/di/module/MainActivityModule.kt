package com.jerarquicos.futurossocios.di.module

import com.jerarquicos.futurossocios.view.base.BaseActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = arrayOf(FragmentBuildersModule::class))
    internal abstract fun contributeMainActivity(): BaseActivity

}