package com.jerarquicos.futurossocios.di.module

import android.content.Context
import com.google.gson.GsonBuilder
import com.jerarquicos.futurossocios.BuildConfig
import com.jerarquicos.futurossocios.repository.util.LiveDataCallAdapterFactory
import com.jerarquicos.futurossocios.repository.util.intereceptors.ConnectivityInterceptor
import com.jerarquicos.futurossocios.utils.AppConstants.CONNECT_TIMEOUT
import com.jerarquicos.futurossocios.utils.AppConstants.READ_TIMEOUT
import com.jerarquicos.futurossocios.utils.AppConstants.WRITE_TIMEOUT
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by fgagneten on 15/05/2017.
 */

@Module
class NetworkModule {


    /***
     * Provide information
     * @return
     */
    @Provides
    @Singleton
    internal fun provideOkHttpInterceptors(): HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(
                if (BuildConfig.DEBUG)
                    HttpLoggingInterceptor.Level.BODY
                else
                    HttpLoggingInterceptor.Level.NONE)
    }


    /***
     * Basic OkHttp configuration
     * @param httpLoggingInterceptor
     * @return
     */
    @Provides
    @Singleton
    internal fun okHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor,
                              context: Context): OkHttpClient {

        return OkHttpClient.Builder()
                .addInterceptor(ConnectivityInterceptor(context))
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(CONNECT_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT.toLong(), TimeUnit.SECONDS)
                .build()
    }


    /***
     * Basic Retrofit configuration
     * @param okHttpClient
     * @return
     */
    @Provides
    @Singleton
    internal fun provideRetrofitClient(okHttpClient: OkHttpClient): Retrofit {
        val gson = GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create()

        return Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}
