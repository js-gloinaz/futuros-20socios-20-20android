package com.jerarquicos.futurossocios.di.module

import android.app.Application
import androidx.room.Room
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.jerarquicos.futurossocios.db.FuturosSociosDb
import com.jerarquicos.futurossocios.db.dao.AplicacionDao
import com.jerarquicos.futurossocios.db.dao.ExpedienteDao
import com.jerarquicos.futurossocios.db.dao.NotificacionFcmDao
import com.jerarquicos.futurossocios.utils.MigrationUtils.MIGRATION_1_2

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by fgagneten on 15/05/2017.
 * Room module. Basic configurations
 */
@Module
class RoomModule {

    @Singleton
    @Provides
    internal fun providesRoomDatabase(app: Application): FuturosSociosDb {

        return Room.databaseBuilder(app, FuturosSociosDb::class.java, "seamos_socios_db")
                .allowMainThreadQueries()
                .addMigrations(MIGRATION_1_2)
                .build()
    }

    @Singleton
    @Provides
    internal fun provideExpedienteDao(db: FuturosSociosDb): ExpedienteDao {
        return db.expedienteDao()
    }

    @Singleton
    @Provides
    internal fun provideNotificacionFcmDao(db: FuturosSociosDb): NotificacionFcmDao {
        return db.notificacionFcmDao()
    }


    @Singleton
    @Provides
    internal fun provideAplicacionDao(db: FuturosSociosDb): AplicacionDao {
        return db.aplicacionDao()
    }

}
