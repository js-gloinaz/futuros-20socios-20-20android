package com.jerarquicos.futurossocios.di.module

import com.jerarquicos.futurossocios.repository.util.FirebaseMessagingService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServicesModule {
	
	@ContributesAndroidInjector
	internal abstract fun firebaseMessagingService(): FirebaseMessagingService
}