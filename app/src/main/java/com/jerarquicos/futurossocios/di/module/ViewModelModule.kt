package com.jerarquicos.futurossocios.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.jerarquicos.futurossocios.di.qualifires.ViewModelKey
import com.jerarquicos.futurossocios.view.base.DealerViewModel
import com.jerarquicos.futurossocios.view.aporteObraSocial.AporteObraSocialViewModel
import com.jerarquicos.futurossocios.view.cambiarContrasenia.CambiarContraseniaViewModel
import com.jerarquicos.futurossocios.view.cargaCbu.CargaCbuViewModel
import com.jerarquicos.futurossocios.view.comienzo.ComienzoViewModel
import com.jerarquicos.futurossocios.view.comienzoSeccion.ComienzoSeccionViewModel
import com.jerarquicos.futurossocios.view.consentimientoDeclaracionJurada.ConsentimientoViewModel
import com.jerarquicos.futurossocios.view.contexturaFisica.ContexturaFisicaViewModel
import com.jerarquicos.futurossocios.view.conviviente.ConvivienteViewModel
import com.jerarquicos.futurossocios.view.dashboard.DashboardViewModel
import com.jerarquicos.futurossocios.view.declaracionJurada.PersonasBottomSheetViewModel
import com.jerarquicos.futurossocios.view.declaracionJurada.PreguntaDeclaracionJuradaViewModel
import com.jerarquicos.futurossocios.view.declaracionJurada.PreguntaTipoDosViewModel
import com.jerarquicos.futurossocios.view.domicilio.DomicilioViewModel
import com.jerarquicos.futurossocios.view.empleador.DatosEmpleadorViewModel
import com.jerarquicos.futurossocios.view.expedientesEnviados.ExpedientesEnviadosViewModel
import com.jerarquicos.futurossocios.view.finalSeccion.FinalSeccionViewModel
import com.jerarquicos.futurossocios.view.firmaGrafometrica.FirmaGrafometricaViewModel
import com.jerarquicos.futurossocios.view.gestorDocumentos.gestorDocumentos.GestorDocumentosViewModel
import com.jerarquicos.futurossocios.view.gestorDocumentos.informacionDocumentacionAnexa
        .InformacionDocumentacionAnexaViewModel
import com.jerarquicos.futurossocios.view.gestorDocumentos.visualizadorImagen.VisualizadorImagenViewModel
import com.jerarquicos.futurossocios.view.inicio.InicioViewModel
import com.jerarquicos.futurossocios.view.listaResultado.ListaResultadoViewModel
import com.jerarquicos.futurossocios.view.login.LoginViewModel
import com.jerarquicos.futurossocios.view.notificacionFcm.NotificacionFcmViewModel
import com.jerarquicos.futurossocios.view.observacionExpediente.ObservacionExpedienteViewModel
import com.jerarquicos.futurossocios.view.personaACargo.PersonaACargoViewModel
import com.jerarquicos.futurossocios.view.presentacionSeccion.PresentacionSeccionViewModel
import com.jerarquicos.futurossocios.view.registro.RegistroViewModel
import com.jerarquicos.futurossocios.view.restablecerContrasenia.RestablecerContraseniaViewModel
import com.jerarquicos.futurossocios.view.splash.SplashViewModel
import com.jerarquicos.futurossocios.view.tarjetaDeCredito.TarjetaDeCreditoViewModel
import com.jerarquicos.futurossocios.view.titular.TitularViewModel
import com.jerarquicos.futurossocios.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by fgagneten on 15/05/2017.
 * ViewModel module.
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(
            splashViewModel: SplashViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(
            loginViewModel: LoginViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InicioViewModel::class)
    internal abstract fun bindInicioViewModel(
            inicioViewModel: InicioViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CambiarContraseniaViewModel::class)
    internal abstract fun bindCambiarContraseniaViewModel(
            cambiarContraseniaViewModel: CambiarContraseniaViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RestablecerContraseniaViewModel::class)
    internal abstract fun bindRestablecerContraseniaViewModel(
            restablecerContraseniaViewModel: RestablecerContraseniaViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegistroViewModel::class)
    internal abstract fun bindRegistroViewModel(
            registroViewModel: RegistroViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ComienzoSeccionViewModel::class)
    internal abstract fun bindComienzoSeccionViewModel(
            comienzoSeccionViewModel: ComienzoSeccionViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InformacionDocumentacionAnexaViewModel::class)
    internal abstract fun bindInformacionDocumentacionAnexaViewModel(
            informacionDocumentacionAnexaViewModel: InformacionDocumentacionAnexaViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(GestorDocumentosViewModel::class)
    internal abstract fun bindGestorDocumentosViewModel(
            gestorDocumentosViewModel: GestorDocumentosViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FinalSeccionViewModel::class)
    internal abstract fun bindFinalSeccionViewModel(
            finalSeccionViewModel: FinalSeccionViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DatosEmpleadorViewModel::class)
    internal abstract fun bindDatosEmpleadorViewModel(
            datosEmpleadorViewModel: DatosEmpleadorViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DomicilioViewModel::class)
    internal abstract fun bindDomicilioViewModel(
            domicilioViewModel: DomicilioViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ListaResultadoViewModel::class)
    internal abstract fun bindListaResultadoViewModel(
            listaResultadoViewModel: ListaResultadoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TitularViewModel::class)
    internal abstract fun bindTitularViewModel(
            titularViewModel: TitularViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConvivienteViewModel::class)
    internal abstract fun bindConvivienteViewModel(
            convivienteViewModel: ConvivienteViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DashboardViewModel::class)
    internal abstract fun bindDashboardViewModel(
            dashboardViewModel: DashboardViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DealerViewModel::class)
    internal abstract fun bindDealerViewModel(
            dealerViewModel: DealerViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VisualizadorImagenViewModel::class)
    internal abstract fun bindVisualizadorImagenViewModel(
            visualizadorImagenViewModel: VisualizadorImagenViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContexturaFisicaViewModel::class)
    internal abstract fun bindContexturaFisicaViewModel(
            contexturaFisicaViewModel: ContexturaFisicaViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PreguntaDeclaracionJuradaViewModel::class)
    internal abstract fun bindPreguntaDeclaracionJuradaViewModel(
            preguntaDeclaracionJuradaViewModel: PreguntaDeclaracionJuradaViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PersonaACargoViewModel::class)
    internal abstract fun bindPersonaACargoViewModel(
            personaACargoViewModel: PersonaACargoViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PersonasBottomSheetViewModel::class)
    internal abstract fun bindPersonasBottomSheetViewModel(
            personasBottomSheetViewModel: PersonasBottomSheetViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AporteObraSocialViewModel::class)
    internal abstract fun bindAporteObraSocialViewModel(
            aporteObraSocialViewModel: AporteObraSocialViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConsentimientoViewModel::class)
    internal abstract fun bindDeclaracionJuradaViewModel(
            consentimientoViewModel: ConsentimientoViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PreguntaTipoDosViewModel::class)
    internal abstract fun bindPreguntaTipoDosViewModel(
            preguntaTipoDosViewModel: PreguntaTipoDosViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FirmaGrafometricaViewModel::class)
    internal abstract fun bindFirmaGrafometricaViewModel(
            firmaGrafometricaViewModel: FirmaGrafometricaViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PresentacionSeccionViewModel::class)
    internal abstract fun bindPresentacionSeccionViewModel(
            presentacionSeccionViewModel: PresentacionSeccionViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TarjetaDeCreditoViewModel::class)
    internal abstract fun bindTarjetaDeCreditoViewModel(
            tarjetaDeCreditoViewModel: TarjetaDeCreditoViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CargaCbuViewModel::class)
    internal abstract fun bindCargaCbuViewModel(
            cargaCbuViewModel: CargaCbuViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ObservacionExpedienteViewModel::class)
    internal abstract fun bindObservacionExpedienteViewModel(
            observacionExpedienteViewModel: ObservacionExpedienteViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ExpedientesEnviadosViewModel::class)
    internal abstract fun bindExpedientesEnviadosViewModel(
            expedientesEnviadosViewModel: ExpedientesEnviadosViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificacionFcmViewModel::class)
    internal abstract fun bindNotificacionGcmViewModel(
            notificacionFcmViewModel: NotificacionFcmViewModel
    ): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ComienzoViewModel::class)
    internal abstract fun bindComienzoViewModel(
            comienzoViewModel: ComienzoViewModel
    ): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(
            viewModelFactory: ViewModelFactory
    ): ViewModelProvider.Factory
}
