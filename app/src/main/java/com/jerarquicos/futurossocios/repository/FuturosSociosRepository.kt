package com.jerarquicos.futurossocios.repository

import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.LiveData
import com.jerarquicos.futurossocios.db.dao.AplicacionDao
import com.jerarquicos.futurossocios.db.dao.ExpedienteDao
import com.jerarquicos.futurossocios.db.dao.NotificacionFcmDao
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
import com.jerarquicos.futurossocios.db.entity.Titular
import com.jerarquicos.futurossocios.db.entity.cuenta.*
import com.jerarquicos.futurossocios.repository.apiService.controllers.IAplicacion
import com.jerarquicos.futurossocios.repository.apiService.controllers.ICuenta
import com.jerarquicos.futurossocios.repository.apiService.controllers.IDocumentos
import com.jerarquicos.futurossocios.repository.apiService.controllers.IExpedienteService
import com.jerarquicos.futurossocios.repository.util.ApiResponse
import com.jerarquicos.futurossocios.repository.util.AppExecutors
import com.jerarquicos.futurossocios.repository.util.NetworkOnlyBoundResource
import com.jerarquicos.futurossocios.repository.util.OfflineBoundResource
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.SharedPreferencesConstants.EXPEDIENTE_ACTUAL_ID
import com.jerarquicos.futurossocios.view.common.utils.SecurePreferencesHelper
import com.jerarquicos.futurossocios.view.models.AplicacionModel
import com.jerarquicos.futurossocios.view.models.DashboardModel
import com.jerarquicos.futurossocios.view.models.SincronizacionExpedienteModel
import javax.inject.Inject
import javax.inject.Singleton

/**
 * This class helps manage communication from repository to ViewModels
 */
@Singleton
class FuturosSociosRepository @Inject
internal constructor(
		private val mAppExecutors: AppExecutors,
		private val miCuenta: ICuenta,
		private val miDocumento: IDocumentos,
		private val mAplicacionService: IAplicacion,
		private val mExpedienteService: IExpedienteService,
		private val mAplicacionDao: AplicacionDao,
		private val mExpedienteDao: ExpedienteDao,
		private val mNotificacionFcmDao: NotificacionFcmDao,
		var preferences: SecurePreferencesHelper) {
	
	fun guardarNuevoToken(token: String?){
		//ToDo: Implementar el envio de los nuevos datos
	}
	
	fun obtenerExpedientes(): List<Expediente> {
		return mExpedienteDao.obtenerExpedientes()
	}
	
	fun obtenerExpedientesOnline():
			LiveData<EstadoRespuesta<RespuestaModel<DashboardModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<DashboardModel>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<DashboardModel>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<DashboardModel>>> {
				val token = generarHeaderToken()
				return mExpedienteService.obtenerExpedientes(token)
			}
		}.asLiveData()
	}
	
	fun generarHeaderToken(): String {
		return "Bearer " + preferences.getJwtTokenModel()?.value
	}
	
	fun obtenerExpediente(): Expediente {
		val id = preferences.getString(EXPEDIENTE_ACTUAL_ID)
		val datos = mExpedienteDao.obtenerExpediente(id)
		return datos
	}
	
	fun obtenerCantidadNotificacionesSinLeer(): LiveData<EstadoRespuesta<Int>> {
		return object : OfflineBoundResource<Int>(mAppExecutors) {
			override fun guardarBD() {
				//No hace nada
			}
			
			override fun obtenerDesdeBD(): LiveData<Int> {
				return mNotificacionFcmDao.obtenerCantidadNotificacionesSinLeer()
			}
		}.asLiveData()
	}
	
	fun seleccionarExpediente(idAplicacion: String) {
		preferences.setString(EXPEDIENTE_ACTUAL_ID, idAplicacion)
	}
	
	fun obtenerDatosInicialesBaseLocal(): LiveData<EstadoRespuesta<DatosIniciales>> {
		return object : OfflineBoundResource<DatosIniciales>(mAppExecutors) {
			override fun guardarBD() {
				//No hace nada
			}
			
			override fun obtenerDesdeBD(): LiveData<DatosIniciales> {
				return mAplicacionDao.obtenerDatosIniciales()
			}
		}.asLiveData()
	}
	
	fun borrarExpediente(idAplicacion: String) {
		mExpedienteDao.borrarExpediente(idAplicacion)
	}
	
	fun borrarExpedientes() {
		mExpedienteDao.borrarExpedientes()
	}
	
	fun borrarNotificaciones() {
		mNotificacionFcmDao.borrarNotificaciones()
	}
	
	fun crearNuevoExpediente(expediente: Expediente) {
		mExpedienteDao.guardarExpediente(expediente)
		preferences.setString(EXPEDIENTE_ACTUAL_ID, expediente.idAplicacion)
	}
	
	fun guardarExpediente(expediente: Expediente) {
		expediente.idAplicacion = preferences.getString(EXPEDIENTE_ACTUAL_ID)
		mExpedienteDao.guardarExpediente(expediente)
	}
	fun guardarExpedientes(expedientes: List<Expediente>) {
		mExpedienteDao.guardarExpedientes(expedientes)
	}

	fun obtenerDatosIniciales(): LiveData<EstadoRespuesta<RespuestaModel<DatosIniciales>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<DatosIniciales>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<DatosIniciales>) {
				if (item.exito && item.modelo != null) {
					mAplicacionDao.guardarDatosIniciales(item.modelo!!)
				}
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<DatosIniciales>>> {
				return mAplicacionService.datosIniciales
			}
		}.asLiveData()
	}
	
	fun obtenerVersiones(): LiveData<EstadoRespuesta<RespuestaModel<AplicacionModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<AplicacionModel>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<AplicacionModel>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<AplicacionModel>>> {
				return mAplicacionService.aplicacionModel
			}
		}.asLiveData()
	}
	
	fun login(login: LoginModel): LiveData<EstadoRespuesta<RespuestaModel<JwtTokenModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<JwtTokenModel>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<JwtTokenModel>) {
				preferences.setJwtTokenModel(item.modelo!!)
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>> {
				return miCuenta.login(login)
			}
		}.asLiveData()
	}
	
	fun guardarDispositivo(
			dispositivoModel: DispositivoModel
	): LiveData<EstadoRespuesta<RespuestaModel<Boolean>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<Boolean>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<Boolean>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<Boolean>>> {
				return mAplicacionService.guardarDispositivo(dispositivoModel)
			}
		}.asLiveData()
	}
	
	fun enviarExpediente( id: Int ): LiveData<EstadoRespuesta<RespuestaModel<Boolean>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<Boolean>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<Boolean>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<Boolean>>> {
				val token = generarHeaderToken()
				return mExpedienteService.enviarExpediente(token, id)
			}
		}.asLiveData()
	}
	
	fun eliminarDispositivo(
			dispositivoModel: DispositivoModel
	): LiveData<EstadoRespuesta<RespuestaModel<Boolean>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<Boolean>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<Boolean>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<Boolean>>> {
				return mAplicacionService.eliminarDispositivo(dispositivoModel)
			}
		}.asLiveData()
	}
	
	fun sincronizarExpediente(expediente: Expediente)
			: LiveData<EstadoRespuesta<RespuestaModel<SincronizacionExpedienteModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<SincronizacionExpedienteModel>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<SincronizacionExpedienteModel>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<SincronizacionExpedienteModel>>> {
				val token = generarHeaderToken()
				return mExpedienteService.sincronizarExpediente(token, expediente)
			}
		}.asLiveData()
	}
	
	fun restablecerContrasenia(
			restablecerContrasenia: RestablecerContraseniaModel
	): LiveData<EstadoRespuesta<RespuestaModel<Any>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<Any>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<Any>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<Any>>> {
				return miCuenta.restablecerContrasenia(restablecerContrasenia)
			}
		}.asLiveData()
	}
	
	fun cambiarContrasenia(
			cambiarContrasenia: CambiarContraseniaModel
	): LiveData<EstadoRespuesta<RespuestaModel<Any>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<Any>>(
				mAppExecutors,
				preferences,
				miCuenta
		) {
			override fun guardarRespuesta(item: RespuestaModel<Any>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<Any>>> {
				return miCuenta.cambiarContrasenia(cambiarContrasenia)
			}
		}.asLiveData()
	}
	
	fun registro(registro: RegistroModel): LiveData<EstadoRespuesta<RespuestaModel<JwtTokenModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<JwtTokenModel>>(mAppExecutors, preferences, miCuenta) {
			override fun guardarRespuesta(item: RespuestaModel<JwtTokenModel>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>> {
				return miCuenta.registro(registro)
			}
		}.asLiveData()
	}
	
	fun principal(principal: PrincipalModel): LiveData<EstadoRespuesta<RespuestaModel<Any>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<Any>>(mAppExecutors, preferences, miCuenta) {
			override fun guardarRespuesta(item: RespuestaModel<Any>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<Any>>> {
				return miCuenta.principal(principal)
			}
		}.asLiveData()
	}
	
	fun titular(titular: Titular): LiveData<EstadoRespuesta<RespuestaModel<JwtTokenModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<JwtTokenModel>>(mAppExecutors, preferences, miCuenta) {
			override fun guardarRespuesta(item: RespuestaModel<JwtTokenModel>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>> {
				return miCuenta.titular(titular)
			}
		}.asLiveData()
	}
	
	fun conviviente(conviviente: ConvivienteModel): LiveData<EstadoRespuesta<RespuestaModel<JwtTokenModel>>> {
		return object : NetworkOnlyBoundResource<RespuestaModel<JwtTokenModel>>(mAppExecutors, preferences, miCuenta) {
			override fun guardarRespuesta(item: RespuestaModel<JwtTokenModel>) {
				//No hace nada
			}
			
			override fun crearLlamada(): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>> {
				return miCuenta.conviviente(conviviente)
			}
		}.asLiveData()
	}
	
	fun guardarNotificacionFcm(notificacionFcm: NotificacionFcm) {
		Log.d("TOKEN", "Entre al futurosSociosRepository")
		mNotificacionFcmDao.guardarNotificacionFcm(notificacionFcm)
	}
	
	fun obtenerNotificacionFcmBaseLocal(): LiveData<EstadoRespuesta<List<NotificacionFcm>>> {
		return object : OfflineBoundResource<List<NotificacionFcm>>(mAppExecutors) {
			override fun guardarBD() {
				//No hace nada
			}
			
			override fun obtenerDesdeBD(): LiveData<List<NotificacionFcm>> {
				return mNotificacionFcmDao.obtenerNotificaciones()
			}
		}.asLiveData()
	}

	fun borrarNotificacion(notificacionFcm: NotificacionFcm) {
		mNotificacionFcmDao.borrarNotificacion(notificacionFcm.id)
	}
	fun actualizarEstadoNotificaciones(estado: Boolean) {
		mNotificacionFcmDao.actualizarEstadoNotificaciones(estado)
	}
}
