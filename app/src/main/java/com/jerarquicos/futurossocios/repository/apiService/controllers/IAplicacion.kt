package com.jerarquicos.futurossocios.repository.apiService.controllers

import androidx.lifecycle.LiveData
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.cuenta.DispositivoModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.util.ApiResponse
import com.jerarquicos.futurossocios.view.models.AplicacionModel
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface IAplicacion {

    @get:GET("Aplicacion/OpcionesGenerales")
    val datosIniciales: LiveData<ApiResponse<RespuestaModel<DatosIniciales>>>

    @get:GET("Aplicacion")
    val aplicacionModel: LiveData<ApiResponse<RespuestaModel<AplicacionModel>>>


    @POST("Aplicacion/grabarDispositivo")
    fun guardarDispositivo(@Body dispositivo: DispositivoModel): LiveData<ApiResponse<RespuestaModel<Boolean>>>

    @POST("Aplicacion/eliminarDispositivo")
    fun eliminarDispositivo(@Body dispositivo: DispositivoModel): LiveData<ApiResponse<RespuestaModel<Boolean>>>
}
