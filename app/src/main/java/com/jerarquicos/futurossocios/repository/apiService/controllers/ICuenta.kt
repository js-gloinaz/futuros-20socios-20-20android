package com.jerarquicos.futurossocios.repository.apiService.controllers

import androidx.lifecycle.LiveData
import com.jerarquicos.futurossocios.db.entity.Titular
import com.jerarquicos.futurossocios.db.entity.cuenta.*
import com.jerarquicos.futurossocios.repository.util.ApiResponse
import retrofit2.http.Body
import retrofit2.http.POST


interface ICuenta {

    @POST("Cuenta/login")
    fun login(
            @Body loginViewModel: LoginModel
    ): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>>

    @POST("Cuenta/login")
    fun regenerarToken(
            @Body loginViewModel: LoginModel
    ): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>>

    @POST("Cuenta/cambiarPassword")
    fun cambiarContrasenia(
            @Body cambiarContraseniaViewModel: CambiarContraseniaModel
    ): LiveData<ApiResponse<RespuestaModel<Any>>>

    @POST("Cuenta/restablecerpassword")
    fun restablecerContrasenia(
            @Body restablecerContraseniaViewModel: RestablecerContraseniaModel
    ): LiveData<ApiResponse<RespuestaModel<Any>>>

    @POST("Cuenta/registro")
    fun registro(
            @Body registroViewModel: RegistroModel
    ): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>>

    @POST("Cuenta/principalfuturossocios")
    fun principal(
            @Body principalViewModel: PrincipalModel
    ): LiveData<ApiResponse<RespuestaModel<Any>>>

    @POST("Cuenta/titular")
    fun titular(
            @Body titularViewModel: Titular
    ): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>>

    @POST("Cuenta/conviviente")
    fun conviviente(
            @Body convivienteViewModel: ConvivienteModel
    ): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>>
}
