package com.jerarquicos.futurossocios.repository.apiService.controllers

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface IDocumentos {
    @Multipart
    @POST("DocumentacionAnexa/subirarchivo/{idExpedienteApp}")
    fun enviarDocumento(
            @Part file: MultipartBody.Part,
            @Path("idExpedienteApp") idExpedienteApp: String
    ): Call<ResponseBody>

}