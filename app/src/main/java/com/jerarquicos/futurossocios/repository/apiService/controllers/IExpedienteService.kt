package com.jerarquicos.futurossocios.repository.apiService.controllers

import androidx.lifecycle.LiveData
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.util.ApiResponse
import com.jerarquicos.futurossocios.view.models.DashboardModel
import com.jerarquicos.futurossocios.view.models.SincronizacionExpedienteModel
import retrofit2.http.*

interface IExpedienteService {

    @POST("Expedientes/grabarExpediente")
    fun sincronizarExpediente(
            @Header("Authorization") token: String,
            @Body expediente: Expediente
    ): LiveData<ApiResponse<RespuestaModel<SincronizacionExpedienteModel>>>

    @GET("Expedientes/dashboard")
    fun obtenerExpedientes(
            @Header("Authorization") token: String
    ): LiveData<ApiResponse<RespuestaModel<DashboardModel>>>

    @GET("Expedientes/enviarexpediente/{id}")
    fun enviarExpediente(
            @Header("Authorization") token: String,
            @Path ("id") id: Int
    ): LiveData<ApiResponse<RespuestaModel<Boolean>>>
}
