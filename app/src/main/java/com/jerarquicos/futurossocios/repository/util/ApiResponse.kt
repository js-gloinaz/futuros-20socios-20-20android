package com.jerarquicos.futurossocios.repository.util

import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import retrofit2.Response

sealed class ApiResponse<T> {
    companion object {
        val NO_ES_POSIBLE_PROCESAR_SOLICITUD =
                "No es posible procesar la solicitud. Por favor, intentá más tarde."
        val SIN_CONEXION =
                "Sin conexión. Conectate a internet para poder continuar."
        val NO_AUTENTICADO =
                "No autenticado. Por favor, iniciá sesión nuevamente."

        fun <T> crearRespuestaError(
                notificacion: NotificacionModel,
                errorControlado: Boolean
        ): RespuestaError<RespuestaModel<T>> {

            val notificacionError: NotificacionModel = if (errorControlado) {
                NotificacionModel(notificacion.mensaje, notificacion.tipoNotificacion)
            } else {
                NotificacionModel(NO_ES_POSIBLE_PROCESAR_SOLICITUD, notificacion.tipoNotificacion)
            }

            val respuestaError = RespuestaModel<T>(null, false, notificacionError)
            return RespuestaError(respuestaError)

        }

        fun <T> crearRespuestaSinConexion(): RespuestaSinConexion<RespuestaModel<T>> {
            val notificacionSinConexion = NotificacionModel(SIN_CONEXION,
                    TipoNotificacion.Informacion)
            val repuestaSinConexion = RespuestaModel<T>(
                    null,
                    false,
                    notificacionSinConexion
            )
            return RespuestaSinConexion(repuestaSinConexion)
        }

        fun <T> create(response: Response<T>): ApiResponse<RespuestaModel<T>> {
            return when {
                response.code() == 401 -> {
                    val notificacion = NotificacionModel(NO_AUTENTICADO, TipoNotificacion.Error)
                    val noAutenticado: RespuestaModel<T> = RespuestaModel(
                            null,
                            false,
                            notificacion
                    )
                    RespuestaNoAutenticado(
                            body = noAutenticado
                    )
                }
                response.isSuccessful -> {
                    val body = response.body() as RespuestaModel<T>
                    if (body.exito) {
                        RespuestaExitosa(
                                body = body
                        )
                    } else {
                        val notificacion = NotificacionModel(
                                body.notificacion.mensaje,
                                body.notificacion.tipoNotificacion
                        )
                        crearRespuestaError(notificacion, true)
                    }
                }
                else -> {
                    val msg = response.errorBody()?.string()

                    val errorMsg = if (msg.isNullOrEmpty()) {
                        response.message()
                    } else {
                        msg
                    }
                    val notificacion = NotificacionModel(errorMsg, TipoNotificacion.Error)
                    crearRespuestaError(notificacion, false)
                }
            }
        }
    }
}

data class RespuestaExitosa<T>(val body: T) : ApiResponse<T>()

data class RespuestaSinConexion<T>(var body: T) : ApiResponse<T>()

data class RespuestaNoAutenticado<T>(var body: T) : ApiResponse<T>()

data class RespuestaError<T>(val body: T) : ApiResponse<T>()
