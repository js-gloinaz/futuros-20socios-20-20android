package com.jerarquicos.futurossocios.repository.util

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
import com.jerarquicos.futurossocios.db.entity.cuenta.DispositivoModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.base.BaseActivity
import javax.inject.Inject
import dagger.android.AndroidInjection

class FirebaseMessagingService : FirebaseMessagingService(){
	
	@Inject
	lateinit var futurosSociosRepository: FuturosSociosRepository
	
	override fun onCreate() {
		AndroidInjection.inject(this)
		super.onCreate()
	}
	
	override fun onMessageReceived(remoteMessage: RemoteMessage?) {
		super.onMessageReceived(remoteMessage)
		sendNotification(remoteMessage)
	}
	
	override fun onNewToken(token: String?) {
		futurosSociosRepository.guardarNuevoToken(token)
	}
	
	private fun sendNotification(message: RemoteMessage?) {
		val gson = Gson()
		val type = object : TypeToken<NotificacionFcm>() {}.type
		var notificacionModel = gson.fromJson<NotificacionFcm>(message!!.data["notification"], type)
		
		try {
			val intent = Intent(this, BaseActivity::class.java)
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
			val pendingIntent =
					PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
			val channelId = getString(R.string.default_notification_channel_id)
			val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
			val notificationBuilder = NotificationCompat.Builder(this, channelId)
					.setSmallIcon(R.drawable.ic_seamos_socios)
					.setContentTitle(notificacionModel.titulo)
					.setContentText(notificacionModel.mensaje)
					.setAutoCancel(true)
					.setSound(defaultSoundUri)
					.setStyle(NotificationCompat.BigTextStyle().bigText(notificacionModel.mensaje))
					.setContentIntent(pendingIntent)
			val notificationManager =
					getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
			// Since android Oreo notification channel is needed.
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				val channel = NotificationChannel(
						channelId,
						getString(R.string.default_notification_channel_id),
						NotificationManager.IMPORTANCE_DEFAULT
				)
				notificationManager.createNotificationChannel(channel)
			}
			notificationManager.notify(0, notificationBuilder.build())
		} catch (ex: Exception) {
			Log.d("NOTIFICACION", ex.message)
		}
		
		futurosSociosRepository.guardarNotificacionFcm(notificacionModel)
		
	}

}