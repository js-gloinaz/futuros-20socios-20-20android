//package com.jerarquicos.futurossocios.repository.util
//
//import android.util.Log
//import com.google.gson.Gson
//import com.google.gson.reflect.TypeToken
//import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
//import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
//import javax.inject.Inject
//import javax.inject.Singleton
//
//class GestorNotificaciones @Inject constructor(var repositorio: FuturosSociosRepository) {
//
//    var body: String? = null
//    var notificacionModel: NotificacionFcm = NotificacionFcm()
//
//    fun guardarNotificacion() {
//        Log.d("NOTIFICACION", "Se debe guardar la notificacion en la bd local")
//        Log.d("NOTIFICACION", "Cuerpo de la notificacion: $body")
//        val gson = Gson()
//        val type = object : TypeToken<NotificacionFcm>() {}.type
//        notificacionModel = gson.fromJson<NotificacionFcm>(body, type)
//
//        Log.d("NOTIFICACION", "El valor id es: ${notificacionModel.id}")
//        Log.d("NOTIFICACION", "El valor titulo es: ${notificacionModel.titulo}")
//        Log.d("NOTIFICACION", "El valor mensaje es: ${notificacionModel.mensaje}")
//        Log.d("NOTIFICACION", "El valor tipo es: ${notificacionModel.tipo}")
//        Log.d("NOTIFICACION", "El valor Usuario es: ${notificacionModel.usuario}")
//        Log.d("NOTIFICACION", "El valor dniTitular es: ${notificacionModel.dniTitular}")
//        Log.d("NOTIFICACION", "El valor idExpediente es: ${notificacionModel.idExpediente}")
//
//        repositorio.guardarNotificacionFcm(notificacionModel)
//
//    }
//}