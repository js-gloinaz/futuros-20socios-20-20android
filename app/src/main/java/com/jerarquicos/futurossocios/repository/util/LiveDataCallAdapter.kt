package com.jerarquicos.futurossocios.repository.util


import android.util.Log
import androidx.lifecycle.LiveData
import com.crashlytics.android.Crashlytics
import com.google.gson.Gson
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.util.intereceptors.SinConexionException
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.atomic.AtomicBoolean


/**
 * Adaptador genérico de Retrofit para convertir las llamadas en LiveData de tipo ApiResponse.
 * @param <R>
</R> */
class LiveDataCallAdapter<R>(private val responseType: Type) :
        CallAdapter<R, LiveData<ApiResponse<RespuestaModel<R>>>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<R>): LiveData<ApiResponse<RespuestaModel<R>>> {
        return object : LiveData<ApiResponse<RespuestaModel<R>>>() {
            private var started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()



                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : Callback<R> {
                        override fun onResponse(call: Call<R>, response: Response<R>) {
                            val gson = Gson().toJson(response.body())
                            Log.d("json", gson)
                            postValue(ApiResponse.create(response))
                        }

                        override fun onFailure(call: Call<R>, throwable: Throwable) {
                            if (throwable is SinConexionException) {
                                postValue(ApiResponse.crearRespuestaSinConexion())
                            } else {
                                Crashlytics.logException(throwable)
                                val notificacion = NotificacionModel(throwable.message!!, TipoNotificacion.Error)
                                postValue(ApiResponse.crearRespuestaError(notificacion, false))
                            }

                        }
                    })
                }
            }
        }
    }


}
