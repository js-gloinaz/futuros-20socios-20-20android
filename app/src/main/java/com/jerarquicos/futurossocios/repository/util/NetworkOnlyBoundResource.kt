package com.jerarquicos.futurossocios.repository.util


import android.util.Log
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.LoginModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.apiService.controllers.ICuenta
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.common.utils.SecurePreferencesHelper
import java.util.*


/**
 * Obtener datos desde la API
 * */
abstract class NetworkOnlyBoundResource<ResultType> @MainThread
internal constructor(private val appExecutors: AppExecutors,
                     private val preferences: SecurePreferencesHelper,
                     private var cuentaService: ICuenta
) {

    private val result = MediatorLiveData<EstadoRespuesta<ResultType>>()

    init {
        result.value = EstadoRespuesta.cargando(null)
        if (!tokenValido()) {
            intentarValidarToken()
        }
        val apiResponse = crearLlamada()
        result.addSource(apiResponse) { response ->

            when (response) {
                is RespuestaExitosa -> {
                    appExecutors.diskIO().execute {
                        Log.d("prueba", "se guardó la llamada")
                        guardarRespuesta(response.body)
                    }
                    appExecutors.mainThread().execute {
                        result.value = EstadoRespuesta.exito(response.body)
                        asLiveData()
                    }
                }

                is RespuestaSinConexion -> {
                    appExecutors.mainThread().execute {
                        result.value = EstadoRespuesta.sinConexionInternet(response.body)
                        asLiveData()
                    }
                }

                is RespuestaNoAutenticado -> {
                    appExecutors.mainThread().execute {
                        result.value = EstadoRespuesta.noAutenticado(response.body)
                        asLiveData()
                    }
                }

                is RespuestaError -> {
                    appExecutors.mainThread().execute {
                        result.value = EstadoRespuesta.error(response.body)
                        asLiveData()
                    }
                }

            }

        }

    }

    fun intentarValidarToken() {
        val loginModel = preferences.getLoginModel()
        if (loginModel != null) {
            val apiResponse = regenerarToken(loginModel)
            if (apiResponse != null) {
                result.addSource(apiResponse) { response ->

                    when (response) {
                        is RespuestaExitosa -> {

                            appExecutors.mainThread().execute {
                                val respuestaModel = response.body
                                if (respuestaModel.exito) {
                                    Log.d("prueba", "se guardó el token")
                                    preferences.setJwtTokenModel(response.body.modelo!!)
                                }
                            }
                        }

                    }

                }
            }

        }
    }

    fun tokenValido(): Boolean {
        val token = preferences.getJwtTokenModel()
        if (token != null) {

            val calendar = Calendar.getInstance()
            val fechaActual = calendar.time
            if (fechaActual >= token.validTo) {
                Log.d("prueba", "Se va a intentar regenerar el token baby")
                return false
            }

            return true
        }

        return false

    }

    @MainThread
    protected open fun regenerarToken(loginModel: LoginModel): LiveData<ApiResponse<RespuestaModel<JwtTokenModel>>>? {
        return cuentaService.regenerarToken(loginModel)
    }

    fun asLiveData(): LiveData<EstadoRespuesta<ResultType>> {
        return result
    }

    @WorkerThread
    protected abstract fun guardarRespuesta(item: ResultType)

    @MainThread
    protected abstract fun crearLlamada(): LiveData<ApiResponse<ResultType>>
}
