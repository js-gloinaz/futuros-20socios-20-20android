package com.jerarquicos.futurossocios.repository.util

import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import javax.inject.Inject

class NotificacionViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository
) : ViewModel() {

    fun guardarNotificacionFcm(notificacionFcm: NotificacionFcm) {
        futurosSociosRepository.guardarNotificacionFcm(notificacionFcm)
    }
}