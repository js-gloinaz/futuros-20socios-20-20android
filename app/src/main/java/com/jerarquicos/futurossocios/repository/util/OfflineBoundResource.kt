package com.jerarquicos.futurossocios.repository.util

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.Objects


/**
 * Obtener datos desde la base de datos local
 * */
abstract class OfflineBoundResource<ResultType> @MainThread
constructor(appExecutors: AppExecutors) {

    private val result = MediatorLiveData<EstadoRespuesta<ResultType>>()

    init {
        result.value = EstadoRespuesta.cargando(null)

        appExecutors.diskIO().execute {
            guardarBD()
        }
        val dbSource = obtenerDesdeBD()
        result.addSource(dbSource) {
            result.removeSource(dbSource)
            result.addSource(dbSource) { newData -> setValue(EstadoRespuesta.exito(newData)) }
        }
    }

    @MainThread
    private fun setValue(newValue: EstadoRespuesta<ResultType>) {
        if (!Objects.equals(result.value, newValue)) {
            result.value = newValue
        }
    }

    fun asLiveData(): LiveData<EstadoRespuesta<ResultType>> {
        return result
    }

    @MainThread
    protected abstract fun obtenerDesdeBD(): LiveData<ResultType>

    @WorkerThread
    protected abstract fun guardarBD()
}
