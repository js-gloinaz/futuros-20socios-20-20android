package com.jerarquicos.futurossocios.repository.util.estado

/**
 * Created by fgagneten on 15/05/2017.
 * Estados posibles que un LiveData puede contener.
 */
enum class Estado {
    EXITO,
    ERROR,
    CARGANDO,
    SIN_CONEXION_INTERNET,
    NO_AUTENTICADO
}
