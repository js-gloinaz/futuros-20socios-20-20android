package com.jerarquicos.futurossocios.repository.util.estado

/**
 * Clase que gestionará los distintos tipos estados para una clase genérica específica
 * */
data class EstadoRespuesta<T>(val estado: Estado, val data: T?, val message: String?) {
    companion object {
        fun <T> exito(data: T?): EstadoRespuesta<T> {
            return EstadoRespuesta(Estado.EXITO, data, null)
        }

        fun <T> sinConexionInternet(data: T?): EstadoRespuesta<T> {
            return EstadoRespuesta(Estado.SIN_CONEXION_INTERNET, data, null)
        }

        fun <T> noAutenticado(data: T?): EstadoRespuesta<T> {
            return EstadoRespuesta(Estado.NO_AUTENTICADO, data, null)
        }

        fun <T> error(data: T?): EstadoRespuesta<T> {
            return EstadoRespuesta(Estado.ERROR, data, null)
        }

        fun <T> cargando(data: T?): EstadoRespuesta<T> {
            return EstadoRespuesta(Estado.CARGANDO, data, null)
        }
    }
}

