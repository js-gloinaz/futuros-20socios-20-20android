package com.jerarquicos.futurossocios.repository.util.intereceptors

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo

internal object NetworkUtil {

    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = connectivityManager.activeNetworkInfo
        return netInfo != null && netInfo.isConnected
    }

}
