package com.jerarquicos.futurossocios.repository.util.intereceptors

import java.io.IOException

class SinConexionException : IOException() {

    override val message: String?
        get() = "Excepción de sin conexion"
}