package com.jerarquicos.futurossocios.utils


object AppConstants {
    /**
     * Connection timeout duration
     */
    const val CONNECT_TIMEOUT = 60 * 1000
    /**
     * Connection Read timeout duration
     */
    const val READ_TIMEOUT = 60 * 1000
    /**
     * Connection write timeout duration
     */
    const val WRITE_TIMEOUT = 60 * 1000

    /**
     * Time for Splash
     */
    const val SPLASH_SCREEN_DELAY: Long = 2000
    const val SPLASH_ALPHA_1 = 1.0f
    const val SPLASH_ALPHA_05 = 0.5f
    const val SPLASH_ALPHA_0 = 0.0f

    /**
     * Request CODE
     */
    const val REQUEST_CODE = 1
    const val SOLICITUD_TOMAR_FOTO = 1
    const val SOLICITUD_SELECCIONAR_FOTO = 2
    const val SOLICITUD_SELECCIONAR_PDF = 3
    const val VISUALIZAR_IMAGEN = 4
    const val ELIMINAR_IMAGEN = 5
    const val CERRAR_IMAGEN = 6

    /**
     * Alpha Obra Social
     */
    const val ALPHA_1 = 1.0f
    const val ALPHA_05 = 0.5f
    const val ALPHA_0 = 0.0f

    /**
     * Keys Crashalytics
     */
    const val USUARIO = "Usuario"

}
