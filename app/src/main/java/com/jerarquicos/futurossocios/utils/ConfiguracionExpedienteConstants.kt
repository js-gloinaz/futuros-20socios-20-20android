package com.jerarquicos.futurossocios.utils

object ConfiguracionExpedienteConstants {

    const val CANTIDAD_MINIMA_IMAGENES_DNI = 2
    const val CANTIDAD_MAXIMA_IMAGENES_DNI = 2

    const val CANTIDAD_MINIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO = 2
    const val CANTIDAD_MAXIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO = 5

    const val CANTIDAD_MINIMA_IMAGENES_RECIBO_SUELDO = 1
    const val CANTIDAD_MAXIMA_IMAGENES_RECIBO_SUELDO = 5

    const val CANTIDAD_MINIMA_IMAGENES_INFORMACION_ADICIONAL = 0
    const val CANTIDAD_MAXIMA_IMAGENES_INFORMACION_ADICIONAL = 5

    const val CANTIDAD_MINIMA_IMAGENES_FORMA_PAGO = 1
    const val CANTIDAD_MAXIMA_IMAGENES_FORMA_PAGO = 1


    const val DOCUMENTACION_TITULAR = 1
    const val DOCUMENTACION_CONVIVIENTE = 2
    const val DOCUMENTACION_PERSONA_A_CARGO = 3


}
