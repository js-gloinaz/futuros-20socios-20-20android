package com.jerarquicos.futurossocios.utils

import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.DocumentacionAnexa
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.cuenta.Archivo
import com.jerarquicos.futurossocios.db.entity.cuenta.EstadoPasoExpediente
import com.jerarquicos.futurossocios.utils.enums.FormaPagoEnum
import com.jerarquicos.futurossocios.utils.enums.OpcionBooleanaEnum
import com.jerarquicos.futurossocios.utils.enums.TipoDocumentacionAnexaEnum
import com.jerarquicos.futurossocios.view.models.OpcionModel

object DealerHelper {

    fun obtenerListaGenericaOpcionesSiNo(
            opcionSiSelccionado: Boolean,
            opcionNoSelccionado: Boolean
    ): ArrayList<OpcionModel> {
        val opciones = ArrayList<OpcionModel>()
        val opcionSi = OpcionModel(
                OpcionBooleanaEnum.Si.valor,
                OpcionBooleanaEnum.Si.name,
                "",
                opcionSiSelccionado
        )
        val opcionNo = OpcionModel(
                OpcionBooleanaEnum.No.valor,
                OpcionBooleanaEnum.No.name,
                "",
                opcionNoSelccionado
        )
        opciones.add(opcionSi)
        opciones.add(opcionNo)
        return opciones
    }

    /**
     * Se analiza si se habilita o no el paso que le sigue a la sección de datos personales
     */
    fun analizarHabilitarPasoSiguienteDatosPersonales(expediente: Expediente,
         estadoDatosPersonales: EstadoPasoExpediente): EstadoPasoExpediente {
        //region TITULAR
        if (expediente.titular == null) {
            // Todavía no empezó a cargar los datos
            estadoDatosPersonales.habilitaPasoSiguiente = false
            return estadoDatosPersonales
        }
        //endregion
        //region CONYUGE
        if (expediente.ingresaConviviente == null) {
            estadoDatosPersonales.habilitaPasoSiguiente = false
            return estadoDatosPersonales
        }
        if (expediente.ingresaConviviente!!) {
            if (expediente.conviviente == null) {
                // Falta cargar el conviviente
                estadoDatosPersonales.habilitaPasoSiguiente = false
                return estadoDatosPersonales
            }
            if (expediente.conviviente?.nombre == null) {
                // Falta cargar el conviviente
                estadoDatosPersonales.habilitaPasoSiguiente = false
                return estadoDatosPersonales
            }
        }
        //endregion
        //region PERSONAS A CARGO
        if (expediente.ingresaDependientes == null) {
            // Falta marcar si agrega personas a cargo o no
            estadoDatosPersonales.habilitaPasoSiguiente = false
            return estadoDatosPersonales
        }

        if (expediente.ingresaDependientes!!) {
            if (expediente.personaDependiente == null) {
                // Se marcó que iba a agregar hijos pero nunca agregó a nadie
                estadoDatosPersonales.habilitaPasoSiguiente = false
                return estadoDatosPersonales
            }
            expediente.personaDependiente!!.forEach {
                // Corroboramos que el modelo no sea nulo sólo con comprobar el nombre
                if (it.nombre == null) {
                    estadoDatosPersonales.habilitaPasoSiguiente = false
                    return estadoDatosPersonales
                }
            }
        }
        //endregion
        estadoDatosPersonales.habilitaPasoSiguiente = true
        return estadoDatosPersonales
    }

    /**
     * Se analiza si se habilita o no el paso que le sigue a la sección de declaración jurada
     */
    fun analizarHabilitarPasoSiguienteDeclaracionJurada(
            expediente: Expediente,
            datosIniciales: DatosIniciales,
            estadoDeclaracionJurada: EstadoPasoExpediente
    ): EstadoPasoExpediente {
        if (expediente.declaracionJurada == null) {
            estadoDeclaracionJurada.habilitaPasoSiguiente = false
            return estadoDeclaracionJurada
        }
        val cantidadPreguntas = datosIniciales.preguntaDeclaracionJurada.size

        if (cantidadPreguntas != expediente.declaracionJurada?.respuestaPreguntaDeclaracionJurada?.size) {
            estadoDeclaracionJurada.habilitaPasoSiguiente = false
            return estadoDeclaracionJurada
        }
        if (expediente.titular?.contexturaFisica?.peso == null) {
            estadoDeclaracionJurada.habilitaPasoSiguiente = false
            return estadoDeclaracionJurada
        }
        estadoDeclaracionJurada.habilitaPasoSiguiente = true

        return estadoDeclaracionJurada
    }

    fun analizarHabilitarPasoSiguienteFormaPago(
            expediente: Expediente, estadoFormaPago: EstadoPasoExpediente): EstadoPasoExpediente {
        if (expediente.formaPago == null) {
            estadoFormaPago.habilitaPasoSiguiente = false
            return estadoFormaPago
        }

        if (expediente.formaPago?.tipoFormaPago == null) {
            estadoFormaPago.habilitaPasoSiguiente = false
            return estadoFormaPago
        }

        when (expediente.formaPago?.tipoFormaPago?.id) {
            FormaPagoEnum.DebitoTarjetaCredito.valor -> {
                val cantidadListaImagenesTarjetaCredito = expediente.titular?.
                        documentacionAnexa!!.filter { documentacionAnexa ->
                    documentacionAnexa.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.TarjetaBancaria.valor
                }

                if (cantidadListaImagenesTarjetaCredito.isEmpty()) {
                    estadoFormaPago.habilitaPasoSiguiente = false
                    return estadoFormaPago
                }
                val cantidadImagenesTarjetaCredito = cantidadListaImagenesTarjetaCredito.first()
                if (cantidadImagenesTarjetaCredito.archivo.isEmpty()) {
                    estadoFormaPago.habilitaPasoSiguiente = false
                    return estadoFormaPago
                }
            }
            FormaPagoEnum.DebitoCuentaBancaria.valor -> {
                if (expediente.formaPago?.tipoCuentaBancaria == null || expediente.formaPago?.cbu == null) {
                    estadoFormaPago.habilitaPasoSiguiente = false
                    return estadoFormaPago
                }
            }
        }

        if (expediente.firmaGrafometrica == null) {
            estadoFormaPago.habilitaPasoSiguiente = false
            return estadoFormaPago
        }

        estadoFormaPago.habilitaPasoSiguiente = true
        return estadoFormaPago
    }

    /***
     * Generar la lista final a guardar cuando existe documentación anexada con anterioridad
     */
    fun generarListaGuardarDocumentacionAnexa(
            element: DocumentacionAnexa,
            _archivosOrigen: ArrayList<Archivo>
    ): ArrayList<Archivo> {
        var archivosOrigen = _archivosOrigen
        var archivosDestino = element.archivo
        archivosDestino = archivosDestino.filter { d ->
            archivosOrigen.any { o -> o.nombre == d.nombre }
        } as ArrayList<Archivo>


        archivosOrigen = archivosOrigen.filter { o ->
            !archivosDestino.any { d -> d.nombre == o.nombre }
        } as ArrayList<Archivo>

        archivosDestino.addAll(archivosOrigen)
        return archivosDestino
    }
}
