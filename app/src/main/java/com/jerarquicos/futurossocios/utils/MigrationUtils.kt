package com.jerarquicos.futurossocios.utils

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

object MigrationUtils {

    val MIGRATION_1_2 = object : Migration(1, 2) {
        override fun migrate(database: SupportSQLiteDatabase) {
            database.execSQL("ALTER TABLE NotificacionFcm ADD COLUMN leida INTEGER NOT NULL DEFAULT 0")
        }
    }
}
