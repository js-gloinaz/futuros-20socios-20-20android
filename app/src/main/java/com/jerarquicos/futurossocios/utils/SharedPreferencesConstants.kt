package com.jerarquicos.futurossocios.utils


object SharedPreferencesConstants {

    const val PREFERENCE_FILENAME = "com.jerarquicos.futurossocios.pref"

    const val JWT_TOKEN_MODEL = "JWT_TOKEN_MODEL"

    const val EXPEDIENTE_ACTUAL_ID = "EXPEDIENTE_ACTUAL_ID"

    const val LOGIN_MODEL = "LOGIN_MODEL"

}
