package com.jerarquicos.futurossocios.utils.enums

enum class CondicionLaboralEnum(var valor: Int) {
    Asalariado(1),
    Monotributista(2),
    Autonomo(3)
}