package com.jerarquicos.futurossocios.utils.enums

enum class EstadoCivilEnum(var valor: Int) {
    Casado(1),
    Soltero(2),
    Divorciado(3),
    Viudo(4),
    Concubinato(6)
}