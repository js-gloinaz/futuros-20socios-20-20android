package com.jerarquicos.futurossocios.utils.enums

enum class EstadoExpedienteEnum(var valor: Int) {
    Inicial(1),
    EnCurso(2),
    Enviado(3),
    Procesado(4),
    Anulado(5)
}