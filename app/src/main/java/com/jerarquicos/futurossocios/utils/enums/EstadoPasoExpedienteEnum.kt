package com.jerarquicos.futurossocios.utils.enums

enum class EstadoPasoExpedienteEnum(var valor: Int) {
    Inicial(1),
    Proceso(2),
    Completo(3)
}