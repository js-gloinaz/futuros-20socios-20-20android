package com.jerarquicos.futurossocios.utils.enums

enum class FormaPagoEnum(var valor: Int) {
    DebitoTarjetaCredito(1),
    DebitoCuentaBancaria(2),
    CajaResumen(3)
}