package com.jerarquicos.futurossocios.utils.enums

enum class OpcionBooleanaEnum(var valor: Int) {
    Si(1),
    No(2)
}