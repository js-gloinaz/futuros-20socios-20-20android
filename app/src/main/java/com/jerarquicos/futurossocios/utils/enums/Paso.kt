package com.jerarquicos.futurossocios.utils.enums

enum class Paso(var valor: Int) {
    PASO_UNO(1),
    PASO_DOS(2),
    PASO_TRES(3)
}