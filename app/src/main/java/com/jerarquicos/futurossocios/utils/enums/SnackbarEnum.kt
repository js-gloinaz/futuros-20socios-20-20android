package com.jerarquicos.futurossocios.utils.enums

import com.google.android.material.snackbar.Snackbar

enum class SnackbarEnum(var duracion: Int) {
    CORTO(3000),
    MEDIO(5000),
    LARGO(8000),
    SIN_DURACION(Snackbar.LENGTH_INDEFINITE)
}