package com.jerarquicos.futurossocios.utils.enums

enum class TipoDocumentacionAnexaEnum(var valor: Int) {
    Dni(1),
    SentenciaDivorcio(2),
    ReciboSueldo(3),
    ConstanciasMonotributo(4),
    CertificadoMatrimonio(5),
    CertificadoConvivencia(6),
    InformacionAdicional(7),
    TarjetaBancaria(8)
}