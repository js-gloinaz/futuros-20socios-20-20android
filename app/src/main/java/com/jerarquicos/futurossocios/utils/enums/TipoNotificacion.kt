package com.jerarquicos.futurossocios.utils.enums

import com.google.gson.annotations.SerializedName

enum class TipoNotificacion {
    @SerializedName("0")
    Exito,
    @SerializedName("1")
    Error,
    @SerializedName("2")
    Advertencia,
    @SerializedName("3")
    Informacion
}