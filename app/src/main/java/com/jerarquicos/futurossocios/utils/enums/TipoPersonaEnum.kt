package com.jerarquicos.futurossocios.utils.enums

enum class TipoPersonaEnum(var valor: Int) {
    Titular(1),
    Conviviente(2),
    PersonasCargo(3)
}