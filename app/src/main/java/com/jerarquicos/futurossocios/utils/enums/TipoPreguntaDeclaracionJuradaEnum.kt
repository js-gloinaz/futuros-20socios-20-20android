package com.jerarquicos.futurossocios.utils.enums

enum class TipoPreguntaDeclaracionJuradaEnum(var valor: Int) {
    Tipo1(1),
    Tipo2(2)
}