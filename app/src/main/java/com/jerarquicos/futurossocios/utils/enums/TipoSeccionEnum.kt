package com.jerarquicos.futurossocios.utils.enums

enum class TipoSeccionEnum(var valor: Int) {
    DatosPersonales(1),
    DeclaracionJurada(2),
    FormaPago(3)
}