package com.jerarquicos.futurossocios.utils.enums

enum class VistaOpcionesEnum(var valor: Int) {
    CondicionLaboralTitular(1),
    CondicionLaboralConyuge(2),
    DeseaAgregarConviviente(3),
    DeseaAgregarPersonaCargo(4),
    DeseaAgregarOtraPersonaCargo(5),
    FormaPago(6),
    TipoPlan(7),
    ProtesisOdontologica(8)

}