package com.jerarquicos.futurossocios.view.aporteObraSocial

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.AporteObraSocialFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_05
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_1
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import kotlinx.android.synthetic.main.aporte_obra_social_fragment.*
import javax.inject.Inject

class AporteObraSocialFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    lateinit var binding: AporteObraSocialFragmentBinding
    private lateinit var aporteObraSocialViewModel: AporteObraSocialViewModel

    companion object {
        private const val BUNDLE_TIENE_OBRA_SOCIAL = "BUNDLE_TIENE_OBRA_SOCIAL"
        private const val BUNDLE_OBRA_SOCIAL_ACTUAL = "BUNDLE_OBRA_SOCIAL_ACTUAL"

        fun newInstance(tieneObraSocial: Boolean?, obraSocialActual: String?): AporteObraSocialFragment {
            val args = Bundle()
            val fragment = AporteObraSocialFragment()
            args.putBoolean(BUNDLE_TIENE_OBRA_SOCIAL, tieneObraSocial ?: false)
            args.putString(BUNDLE_OBRA_SOCIAL_ACTUAL, obraSocialActual)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.aporte_obra_social_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        aporteObraSocialViewModel = ViewModelProviders.of(
                this,
                viewModelFactory
        ).get(AporteObraSocialViewModel::class.java)

        binding.viewModel = aporteObraSocialViewModel
        binding.btAccion.alpha = ALPHA_05

        if (aporteObraSocialViewModel.aporteObraSocialModel.value == null &&
                aporteObraSocialViewModel.sinAporteObraSocial.value == null
        ) {
            aporteObraSocialViewModel.aporteObraSocialModel.value = arguments!!.getString(BUNDLE_OBRA_SOCIAL_ACTUAL)
            aporteObraSocialViewModel.sinAporteObraSocial.value = !arguments!!.getBoolean(BUNDLE_TIENE_OBRA_SOCIAL)
        }

        if (aporteObraSocialViewModel.sinAporteObraSocial.value != false) {
            binding.btAccion.isEnabled = true
            binding.etNombre.isEnabled = false
            binding.btAccion.alpha = ALPHA_1
            binding.etNombre.text = null
        }

        inicializarListeners()
    }

    private fun inicializarListeners() {
        binding.etNombre.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                habilitarBoton()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //not implementing
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //not implementing
            }
        })

        binding.cbObraSocial.setOnClickListener {
            if (binding.cbObraSocial.isChecked) {
                aporteObraSocialViewModel.sinAporteObraSocial.value = true
                binding.btAccion.isEnabled = true
                binding.etNombre.isEnabled = false
                binding.btAccion.alpha = ALPHA_1
                binding.etNombre.text = null
            } else {
                aporteObraSocialViewModel.sinAporteObraSocial.value = false
                binding.btAccion.isEnabled = false
                binding.etNombre.isEnabled = true
                binding.btAccion.alpha = ALPHA_05
            }
        }

        btAccion.setOnClickListener {
            val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteAporteObraSocialFragment(
                    !binding.cbObraSocial.isChecked,
                    binding.etNombre.text.toString()
            )
            navigationController.navegarSiguienteFragment(fragment)
        }
    }

    fun habilitarBoton() {
        if (binding.etNombre.text.toString().count() >= 2) {
            binding.btAccion.isEnabled = true
            binding.btAccion.alpha = ALPHA_1
        } else {
            if (aporteObraSocialViewModel.valorCheckObraSocial()) {
                binding.btAccion.isEnabled = true
                binding.btAccion.alpha = ALPHA_1
            } else {
                binding.btAccion.isEnabled = false
                binding.btAccion.alpha = ALPHA_05
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_oscuro, menu)
    }
}