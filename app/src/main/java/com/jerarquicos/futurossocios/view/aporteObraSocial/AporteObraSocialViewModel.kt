package com.jerarquicos.futurossocios.view.aporteObraSocial

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class AporteObraSocialViewModel @Inject constructor() : ViewModel() {
    var aporteObraSocialModel: MutableLiveData<String> = MutableLiveData()
    var sinAporteObraSocial: MutableLiveData<Boolean> = MutableLiveData()

    fun valorCheckObraSocial(): Boolean {
        return sinAporteObraSocial.value!!
    }

}