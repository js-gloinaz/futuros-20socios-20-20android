package com.jerarquicos.futurossocios.view.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.multidex.MultiDex
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ActivityMainBinding
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.AppConstants.SPLASH_ALPHA_05
import com.jerarquicos.futurossocios.utils.AppConstants.SPLASH_ALPHA_1
import com.jerarquicos.futurossocios.utils.DeviceUtils
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.models.SincronizacionExpedienteModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {
    lateinit var pbPrincipal: ProgressBar
    lateinit var llContainer: LinearLayout
    lateinit var flContainer: FrameLayout
    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    lateinit var dealerViewModel: DealerViewModel
    lateinit var binding: ActivityMainBinding
    @JvmField
    @Inject
    var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>? = null
    @Inject
    lateinit var navigationController: NavigationController

    override fun onCreate(savedInstanceState: Bundle?) {
        MultiDex.install(this.applicationContext)
        super.onCreate(savedInstanceState)
        adjustFontScale(resources.configuration)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        dealerViewModel = ViewModelProviders.of(this, viewModelFactory).get(DealerViewModel::class.java)

        pbPrincipal = binding.pbGeneral
        llContainer = binding.llContainer
        flContainer = binding.container

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_VISIBLE

        FirebaseApp.initializeApp(this)

        if (savedInstanceState == null) {
            ocultarProgressBar()
            navigationController.navegarSplashFragment()
        } else {
            ocultarProgressBar()
        }

        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }

    override fun supportFragmentInjector(): AndroidInjector<Fragment>? {
        return dispatchingAndroidInjector
    }

    fun ocultarProgressBar() {
        pbPrincipal.visibility = View.GONE
        llContainer.visibility = View.GONE
        flContainer.visibility = View.VISIBLE

        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
    }

    fun mostrarProgressBar(ocultarFondo: Boolean = false) {
        window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

        if (!ocultarFondo) {
            pbPrincipal.visibility = View.VISIBLE
            llContainer.visibility = View.VISIBLE
            llContainer.alpha = SPLASH_ALPHA_05
        } else {
            pbPrincipal.visibility = View.VISIBLE
            llContainer.alpha = SPLASH_ALPHA_1
            llContainer.visibility = View.VISIBLE
            flContainer.visibility = View.INVISIBLE
        }
    }

    fun ocultarTeclado() {
        try {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            if (currentFocus != null) {
                imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
            }
        } catch (ignored: Exception) {
            Log.e("Error", ignored.message)
        }
    }

    fun sincronizarExpediente(mostrarProgreso: Boolean, mostrarNotificaciones: Boolean) {
        dealerViewModel.sincronizarExpediente()
                .observe(this, Observer<EstadoRespuesta<RespuestaModel<SincronizacionExpedienteModel>>>
                {this.handleResponse(it, mostrarProgreso, mostrarNotificaciones)
        })
    }

    private fun handleResponse(respuesta: EstadoRespuesta<RespuestaModel<SincronizacionExpedienteModel>>?,
                               mostrarProgreso: Boolean, mostrarNotificaciones: Boolean) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
                    ocultarProgressBar()
                    if (mostrarNotificaciones) {
                        SnackbarHelper(this.flContainer, respuesta.data!!.notificacion).show()
                    }
                }
                Estado.CARGANDO -> {
                    if (mostrarProgreso) {
                        mostrarProgressBar()
                    }
                }
                Estado.EXITO -> {
                    if (mostrarProgreso) {
                        ocultarProgressBar()
                    }
                    if (respuesta.data != null) {
                        val datos: RespuestaModel<SincronizacionExpedienteModel>? = respuesta.data
                        guardarResultado(datos?.modelo!!)
                    }
                }
            }
        }
    }

    private fun guardarResultado(modelo: SincronizacionExpedienteModel) {
        dealerViewModel.guardarExpediente(modelo.expediente)

        if (modelo.expedienteSobrescrito != null && modelo.expedienteSobrescrito == true) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.expediente_sobreescrito_title))
            builder.setMessage(getString(R.string.expediente_sobreescrito_text))
            builder.setPositiveButton(getString(R.string.listo_action)) { _, _ ->

            }
            val dialog: AlertDialog = builder.create()
            dialog.setCancelable(false)
            dialog.show()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        fragment!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        fragment!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.btHome -> {
                navigationController.navegarDashboardFragment()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    private fun adjustFontScale(configuration: Configuration) {
        configuration.fontScale = 1.0.toFloat()
        val metrics = resources.displayMetrics
        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.defaultDisplay.getMetrics(metrics)
        metrics.scaledDensity = configuration.fontScale * metrics.density
        baseContext.resources.updateConfiguration(configuration, metrics)
    }
}