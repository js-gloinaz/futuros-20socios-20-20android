package com.jerarquicos.futurossocios.view.base

import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.DeviceUtils
import javax.inject.Inject

abstract class BaseFragment : Fragment(), Injectable {
    @Inject
    lateinit var baseActivity: BaseActivity

    fun inicializar(toolbar: Toolbar?, nombreClase: String, hasOptionMenu: Boolean) {
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(
                this.activity!!,
                nombreClase,
                nombreClase
        )
        if(toolbar != null) {
            baseActivity.setSupportActionBar(toolbar)
            baseActivity.supportActionBar!!.setDisplayShowTitleEnabled(false)
        }
        DeviceUtils.setTranslucentStatusBar(activity!!.window, R.color.colorAzulStatusBar)
        setHasOptionsMenu(hasOptionMenu)
        baseActivity.ocultarTeclado()
    }

}