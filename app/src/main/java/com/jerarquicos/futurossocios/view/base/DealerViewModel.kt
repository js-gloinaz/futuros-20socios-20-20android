package com.jerarquicos.futurossocios.view.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.db.entity.cuenta.Archivo
import com.jerarquicos.futurossocios.db.entity.cuenta.EstadoPasoExpediente
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MAXIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MAXIMA_IMAGENES_DNI
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MAXIMA_IMAGENES_FORMA_PAGO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MAXIMA_IMAGENES_INFORMACION_ADICIONAL
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MAXIMA_IMAGENES_RECIBO_SUELDO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MINIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MINIMA_IMAGENES_DNI
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MINIMA_IMAGENES_FORMA_PAGO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MINIMA_IMAGENES_INFORMACION_ADICIONAL
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .CANTIDAD_MINIMA_IMAGENES_RECIBO_SUELDO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .DOCUMENTACION_CONVIVIENTE
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .DOCUMENTACION_PERSONA_A_CARGO
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
        .DOCUMENTACION_TITULAR
import com.jerarquicos.futurossocios.utils.DealerHelper
import com.jerarquicos.futurossocios.utils.enums.*
import com.jerarquicos.futurossocios.view.aporteObraSocial.AporteObraSocialFragment
import com.jerarquicos.futurossocios.view.cargaCbu.CargaCbuFragment
import com.jerarquicos.futurossocios.view.comienzo.ComienzoFragment
import com.jerarquicos.futurossocios.view.comienzoSeccion.ComienzoSeccionFragment
import com.jerarquicos.futurossocios.view.consentimientoDeclaracionJurada.ConsentimientoFragment
import com.jerarquicos.futurossocios.view.contexturaFisica.ContexturaFisicaFragment
import com.jerarquicos.futurossocios.view.conviviente.ConvivienteFragment
import com.jerarquicos.futurossocios.view.dashboard.DashboardFragment
import com.jerarquicos.futurossocios.view.declaracionJurada.PreguntaDeclaracionJuradaFragment
import com.jerarquicos.futurossocios.view.declaracionJurada.PreguntaTipoDosFragment
import com.jerarquicos.futurossocios.view.empleador.DatosEmpleadorFragment
import com.jerarquicos.futurossocios.view.finalSeccion.FinalSeccionFragment
import com.jerarquicos.futurossocios.view.firmaGrafometrica.FirmaGrafometricaFragment
import com.jerarquicos.futurossocios.view.firmaGrafometrica.InformacionFirmaGrafometricaFragment
import com.jerarquicos.futurossocios.view.gestorDocumentos.informacionDocumentacionAnexa
        .InformacionDocumentacionAnexaFragment
import com.jerarquicos.futurossocios.view.models.DocumentacionAnexaModel
import com.jerarquicos.futurossocios.view.models.GrupoOpcionesModel
import com.jerarquicos.futurossocios.view.models.OpcionModel
import com.jerarquicos.futurossocios.view.models.SincronizacionExpedienteModel
import com.jerarquicos.futurossocios.view.personaACargo.PersonaACargoFragment
import com.jerarquicos.futurossocios.view.opciones.OpcionesFragment
import com.jerarquicos.futurossocios.view.presentacionSeccion.PresentacionSeccionFragment
import com.jerarquicos.futurossocios.view.presentacionSeccion.presentacionFormaPago.PresentacionFragment
import com.jerarquicos.futurossocios.view.tarjetaDeCredito.TarjetaDeCreditoFragment
import com.jerarquicos.futurossocios.view.titular.TitularFragment
import javax.inject.Inject

class DealerViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository) : ViewModel() {
    var datosIniciales: MutableLiveData<DatosIniciales> = MutableLiveData()

    //region COMUN
    fun obtenerPasoSiguienteSubidaDocumentosFragment(
            tipoDocumentacionAnexaEnum: TipoDocumentacionAnexaEnum,
            listaNombresArchivos: ArrayList<String>,
            propietario: Int, ordenPersona: Int
    ): Fragment {
        when (tipoDocumentacionAnexaEnum) {
            TipoDocumentacionAnexaEnum.Dni -> {
                return analizarPropietarioDni(propietario, listaNombresArchivos, ordenPersona)
            }
            TipoDocumentacionAnexaEnum.InformacionAdicional -> {
                return analizarPropietarioInformacionAdicional(propietario, listaNombresArchivos, ordenPersona)
            }
            TipoDocumentacionAnexaEnum.ReciboSueldo -> {
                return analizarPropietarioReciboSueldo(propietario, listaNombresArchivos)
            }
            TipoDocumentacionAnexaEnum.ConstanciasMonotributo -> {
                return analizarPropietarioConstanciaMonotributo(propietario, listaNombresArchivos)
            }
            TipoDocumentacionAnexaEnum.TarjetaBancaria -> {
                return analizarPropietarioTarjetaBancaria(propietario, listaNombresArchivos)
            }
            else -> {
                return DashboardFragment()
            }
        }
    }

    private fun analizarPropietarioTarjetaBancaria(
            propietario: Int,
            listaNombresArchivos: ArrayList<String>
    ): Fragment {
        return when (propietario) {
            DOCUMENTACION_TITULAR -> {
                anexarDocumentacionTitular(listaNombresArchivos, TipoDocumentacionAnexaEnum.TarjetaBancaria)
                mostrarVistaTarjetaCredito()
            }
            else -> {
                DashboardFragment()
            }
        }
    }

    private fun analizarPropietarioConstanciaMonotributo(
            propietario: Int,
            listaNombresArchivos: ArrayList<String>
    ): Fragment {
        when (propietario) {
            DOCUMENTACION_TITULAR -> {
                val titular = anexarDocumentacionTitular(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.ConstanciasMonotributo
                )
                return if (titular != null)
                    analizarPuedeIngresarConvivienteYVisualizar()
                else
                    DashboardFragment()
            }
            DOCUMENTACION_CONVIVIENTE -> {
                anexarDocumentacionConviviente(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.ConstanciasMonotributo
                )
                return ComienzoSeccionFragment.newInstance(
                        TipoPersonaEnum.PersonasCargo.valor,
                        R.string.unos_datos_mas_title,
                        R.string.personas_cargo_title,
                        R.string.paso_siguiente_text
                )
            }
            else -> {
                return DashboardFragment()
            }
        }
    }

    private fun analizarPropietarioReciboSueldo(
            propietario: Int,
            listaNombresArchivos: ArrayList<String>
    ): Fragment {
        when (propietario) {
            DOCUMENTACION_TITULAR -> {
                val titular = anexarDocumentacionTitular(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.ReciboSueldo
                )
                return if (titular != null)
                    DatosEmpleadorFragment.newInstance(
                            titular.empleador,
                            DOCUMENTACION_TITULAR
                    )
                else
                    DashboardFragment()
            }
            DOCUMENTACION_CONVIVIENTE -> {
                val conviviente = anexarDocumentacionConviviente(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.ReciboSueldo
                )
                return if (conviviente != null)
                    DatosEmpleadorFragment.newInstance(
                            conviviente.empleador,
                            DOCUMENTACION_CONVIVIENTE
                    )
                else
                    DashboardFragment()
            }
            else -> {
                return DashboardFragment()
            }
        }
    }

    private fun analizarPropietarioDni(
            propietario: Int,
            listaNombresArchivos: ArrayList<String>,
            ordenPersona: Int
    ): Fragment {
        when (propietario) {
            DOCUMENTACION_TITULAR -> {
                val titular = anexarDocumentacionTitular(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.Dni
                )
                return TitularFragment.newInstance(titular)
            }
            DOCUMENTACION_CONVIVIENTE -> {
                val conviviente = anexarDocumentacionConviviente(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.Dni
                )
                return ConvivienteFragment.newInstance(conviviente)
            }
            DOCUMENTACION_PERSONA_A_CARGO -> {
                val personaCargo = anexarDocumentacionPersonaCargo(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.Dni,
                        ordenPersona
                )
                return PersonaACargoFragment.newInstance(personaCargo)
            }
            else -> {
                return DashboardFragment()
            }
        }

    }

    private fun analizarPropietarioInformacionAdicional(
            propietario: Int,
            listaNombresArchivos: ArrayList<String>,
            ordenPersona: Int
    ): Fragment {
        when (propietario) {
            DOCUMENTACION_TITULAR -> {
                anexarDocumentacionTitular(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.InformacionAdicional
                )
                return mostrarVistaCondicionLaboral()
            }
            DOCUMENTACION_CONVIVIENTE -> {
                anexarDocumentacionConviviente(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.InformacionAdicional
                )
                return mostrarVistaCondicionLaboralConviviente()
            }
            DOCUMENTACION_PERSONA_A_CARGO -> {
                val personaCargo = anexarDocumentacionPersonaCargo(
                        listaNombresArchivos,
                        TipoDocumentacionAnexaEnum.InformacionAdicional,
                        ordenPersona
                )
                return analizarMostrarVistaOpcionDeseaAgregarOtraPersonaCargo(personaCargo?.posicion!!)
            }
            else -> {
                return DashboardFragment()
            }
        }

    }

    fun obtenerPasoSiguienteOpcionesFragment(idVistaOpcion: Int, idOpcionElegida: Int): Fragment {
        when (idVistaOpcion) {
            VistaOpcionesEnum.CondicionLaboralTitular.valor -> {
                return analizarOpcionSeleccionadaCondicionLaboralTitular(idOpcionElegida)
            }
            VistaOpcionesEnum.CondicionLaboralConyuge.valor -> {
                return analizarOpcionSeleccionadaCondicionLaboralConyuge(idOpcionElegida)
            }
            VistaOpcionesEnum.DeseaAgregarConviviente.valor -> {
                return analizarOpcionSeleccionadaDeseaAgregarConviviente(idOpcionElegida)
            }
            VistaOpcionesEnum.DeseaAgregarPersonaCargo.valor -> {
                return analizarOpcionSeleccionadaDeseaAgregarPersonCargo(idOpcionElegida)
            }
            VistaOpcionesEnum.DeseaAgregarOtraPersonaCargo.valor -> {
                return analizarOpcionSeleccionadaDeseaAgregarOtraPersonCargo(idOpcionElegida)
            }
            VistaOpcionesEnum.FormaPago.valor -> {
                return analizarOpcionSeleccionadaFormaPago(idOpcionElegida)
            }
            VistaOpcionesEnum.TipoPlan.valor -> {
                return analizarOpcionSeleccionadaTipoPlan(idOpcionElegida)
            }
            VistaOpcionesEnum.ProtesisOdontologica.valor -> {
                return analizarOpcionSeleccionadaProtesisOdontologica(idOpcionElegida)
            }
        }
        return DashboardFragment()
    }

    private fun analizarOpcionSeleccionadaProtesisOdontologica(idOpcionElegida: Int): Fragment {
        val expediente = obtenerExpediente()
        expediente?.protesisOdontologicas = idOpcionElegida == OpcionBooleanaEnum.Si.valor
        futurosSociosRepository.guardarExpediente(expediente!!)
        return mostrarFormaPagoFragment()
    }

    private fun analizarOpcionSeleccionadaTipoPlan(idOpcionElegida: Int): Fragment {
        val expediente = obtenerExpediente()
        val datosIniciales = datosIniciales.value
        val plan = datosIniciales?.plan?.first { it.id == idOpcionElegida }
        if (plan != null) {
            expediente?.plan = plan
            futurosSociosRepository.guardarExpediente(expediente!!)
        }
        return mostrarProtesisOdontologica()
    }

    private fun analizarOpcionSeleccionadaDeseaAgregarOtraPersonCargo(idOpcionElegida: Int): Fragment {
        return when (idOpcionElegida) {
            OpcionBooleanaEnum.Si.valor -> {
                val expediente = obtenerExpediente()
                var orden = expediente?.personaDependiente?.last()?.posicion!!
                orden += 1
                cargarDatosSubidaArchivoDniPersonaCargo(orden)
            }
            else -> {
                return PresentacionFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor)
            }
        }
    }

    private fun analizarOpcionSeleccionadaDeseaAgregarPersonCargo(idOpcionElegida: Int): Fragment {
        val expediente = obtenerExpediente()

        return when (idOpcionElegida) {
            OpcionBooleanaEnum.Si.valor -> {
                expediente?.ingresaDependientes = true
                futurosSociosRepository.guardarExpediente(expediente!!)
                cargarDatosSubidaArchivoDniPersonaCargo(0)
            }
            else -> {
                expediente?.ingresaDependientes = false
                futurosSociosRepository.guardarExpediente(expediente!!)
                return PresentacionFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor)
            }
        }

    }

    private fun analizarOpcionSeleccionadaDeseaAgregarConviviente(idOpcionElegida: Int): Fragment {
        val expediente = obtenerExpediente()
        return when (idOpcionElegida) {
            OpcionBooleanaEnum.Si.valor -> {
                expediente?.ingresaConviviente = true
                futurosSociosRepository.guardarExpediente(expediente!!)
                cargarDatosSubidaArchivoDniConyuge()
            }
            else -> {
                expediente?.ingresaConviviente = false
                expediente?.conviviente = null
                futurosSociosRepository.guardarExpediente(expediente!!)
                ComienzoSeccionFragment.newInstance(
                        TipoPersonaEnum.PersonasCargo.valor,
                        R.string.unos_datos_mas_title,
                        R.string.personas_cargo_title,
                        R.string.paso_siguiente_text
                )
            }
        }

    }

    private fun analizarOpcionSeleccionadaCondicionLaboralConyuge(idOpcionElegida: Int): Fragment {
        val datosIniciales = datosIniciales.value
        val condicionLaboral = datosIniciales?.condicionLaboral?.first { it.id == idOpcionElegida }
        val expediente = obtenerExpediente()

        expediente?.conviviente?.condicionLaboral = condicionLaboral
        futurosSociosRepository.guardarExpediente(expediente!!)

        when (idOpcionElegida) {
            CondicionLaboralEnum.Monotributista.valor -> {
                return cargarDatosSubidaArchivoConstanciaMonotributoConviviente()
            }
            CondicionLaboralEnum.Asalariado.valor -> {
                return cargarDatosSubidaArchivoReciboSueldoConviviente()
            }
            CondicionLaboralEnum.Autonomo.valor -> {
                return ComienzoSeccionFragment.newInstance(
                        TipoPersonaEnum.PersonasCargo.valor,
                        R.string.unos_datos_mas_title,
                        R.string.personas_cargo_title,
                        R.string.paso_siguiente_text
                )
            }
            else -> {
                return DashboardFragment()
            }
        }
    }

    private fun analizarOpcionSeleccionadaCondicionLaboralTitular(idOpcionElegida: Int): Fragment {
        val datosIniciales = datosIniciales.value
        val condicionLaboral = datosIniciales?.condicionLaboral?.first {
            it.id == idOpcionElegida
        }
        val expediente = obtenerExpediente()
        expediente?.titular?.condicionLaboral = condicionLaboral
        futurosSociosRepository.guardarExpediente(expediente!!)

        when (idOpcionElegida) {
            CondicionLaboralEnum.Monotributista.valor -> {
                return cargarDatosSubidaArchivoConstanciaMonotributo()
            }
            CondicionLaboralEnum.Asalariado.valor -> {
                return cargarDatosSubidaArchivoReciboSueldoTitular()
            }
            CondicionLaboralEnum.Autonomo.valor -> {
                return AporteObraSocialFragment.newInstance(
                        expediente.titular?.tieneObraSocial,
                        expediente.titular?.obraSocialActual
                )
            }
            else -> {
                return DashboardFragment()
            }
        }

    }

    private fun analizarOpcionSeleccionadaFormaPago(idOpcionElegida: Int): Fragment {
        val datosIniciales = datosIniciales.value
        val tipoFormaDePago = datosIniciales?.tipoFormaPago?.first { it.id == idOpcionElegida }
        val expediente = obtenerExpediente()

        if (expediente?.formaPago == null) {
            expediente?.formaPago = FormaPago()
        }
        expediente?.formaPago?.tipoFormaPago = tipoFormaDePago
        futurosSociosRepository.guardarExpediente(expediente!!)

        when (idOpcionElegida) {
            FormaPagoEnum.DebitoTarjetaCredito.valor -> {
                expediente.formaPago?.cbu = null
                expediente.formaPago?.tipoCuentaBancaria = null
                futurosSociosRepository.guardarExpediente(expediente)
                return cargarDatosSubidaArchivoDebitoTarjetaCredito()
            }
            FormaPagoEnum.DebitoCuentaBancaria.valor -> {
                expediente.formaPago?.numeroTarjeta = null
                expediente.formaPago?.vencimiento = null
                expediente.formaPago?.tipoTarjetaCredito = null
                return CargaCbuFragment.newInstance(expediente.formaPago)
            }
            FormaPagoEnum.CajaResumen.valor -> {
                expediente.formaPago?.cbu = null
                expediente.formaPago?.tipoCuentaBancaria = null
                expediente.formaPago?.numeroTarjeta = null
                expediente.formaPago?.vencimiento = null
                expediente.formaPago?.tipoTarjetaCredito = null
                futurosSociosRepository.guardarExpediente(expediente)
                return InformacionFirmaGrafometricaFragment()
            }
            else -> {
                return DashboardFragment()
            }
        }
    }

    fun obtenerPasoSiguientePresentacionFragment(idTipoSeccion: Int): Fragment {
        when (idTipoSeccion) {
            TipoSeccionEnum.DeclaracionJurada.valor -> {
                return ComienzoFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor,
                        R.string.declaracion_jurada_title,
                        R.string.descripcion_declaracion_jurada_text,
                        R.drawable.ic_declaracion_jurada
                )
            }
            TipoSeccionEnum.FormaPago.valor -> {
                return ComienzoFragment.newInstance(TipoSeccionEnum.FormaPago.valor,
                        R.string.plan_forma_pago_title,
                        R.string.descripcion_forma_pago_text,
                        R.drawable.ic_forma_pago
                )
            }
        }
        return DashboardFragment()
    }

    //endregion
    //region EXPEDIENTES
    private fun obtenerExpediente(): Expediente? {
        return futurosSociosRepository.obtenerExpediente()
    }

    fun sincronizarExpediente(): LiveData<EstadoRespuesta<RespuestaModel<SincronizacionExpedienteModel>>> {
        val expediente = obtenerExpediente()!!
        return futurosSociosRepository.sincronizarExpediente(expediente)
    }

    fun guardarExpediente(expediente: Expediente?) {
        if (expediente != null) {
            futurosSociosRepository.guardarExpediente(expediente)
        }
    }

    fun obtenerEstadosSeccionesExpediente(): ArrayList<EstadoPasoExpediente> {
        val expediente = obtenerExpediente()!!
        val listaEstados = ArrayList<EstadoPasoExpediente>()
        var estadoDatosPersonales = analizarEstadoDatosPersonales(expediente)
        estadoDatosPersonales = DealerHelper.analizarHabilitarPasoSiguienteDatosPersonales(
                expediente,
                estadoDatosPersonales
        )
        var estadoDeclaracionJurada =
                analizarEstadoDeclaracionJurada(expediente)
        estadoDeclaracionJurada = DealerHelper.analizarHabilitarPasoSiguienteDeclaracionJurada(
                expediente,
                datosIniciales.value!!,
                estadoDeclaracionJurada
        )
        var estadoFormaPago = analizarEstadoFormaPago(expediente)
        estadoFormaPago = DealerHelper.analizarHabilitarPasoSiguienteFormaPago(expediente, estadoFormaPago)
        listaEstados.add(estadoDatosPersonales)
        listaEstados.add(estadoDeclaracionJurada)
        listaEstados.add(estadoFormaPago)
        return listaEstados
    }

    //endregion
    //region DASHBOARD
    fun obtenerPasoSiguienteDashboardFragment(): Fragment {
        return PresentacionSeccionFragment()
    }

    //endregion
    //region DATOS PERSONALES
    fun obtenerPasoSiguienteComienzoFragment(tipoSeccion: Int): Fragment {
        when (tipoSeccion) {
            TipoSeccionEnum.DatosPersonales.valor -> {
                return mostrarVistaCargaDniTitular()
            }
            TipoSeccionEnum.DeclaracionJurada.valor -> {
                return mostrarVistaConsentimientoDeclaracionJurada()
            }
            TipoSeccionEnum.FormaPago.valor -> {
                return mostrarVistaSeleccionPlan()
            }
        }
        return DashboardFragment()
    }

    private fun mostrarVistaSeleccionPlan(): Fragment {
        val expediente = obtenerExpediente()
        var idPlan = 0

        if (expediente?.plan != null) {
            idPlan = expediente.plan?.id!!
        }
        val opcionesList = ArrayList<OpcionModel>()
        val modeloOpciones = GrupoOpcionesModel(R.string.plan_text, opcionesList)
        val datosIniciales = datosIniciales.value

        if (datosIniciales?.plan != null) {
            datosIniciales.plan.map { item ->
                if (item.id == idPlan) {
                    opcionesList.add(OpcionModel(
                            item.id!!,
                            item.descripcion!!,
                            item.descripcionAdicional,
                            true
                    ))
                } else {
                    opcionesList.add(OpcionModel(
                            item.id!!,
                            item.descripcion!!,
                            item.descripcionAdicional,
                            false
                    ))
                }
            }
        }
        return OpcionesFragment.newInstance(modeloOpciones, VistaOpcionesEnum.TipoPlan.valor)
    }

    private fun mostrarVistaConsentimientoDeclaracionJurada(): Fragment {
        val expediente = obtenerExpediente()

        if (expediente?.personaDependiente != null) {
            val personasDependientes = expediente.personaDependiente?.filter { e ->
                e.nombre != null
            } as ArrayList<PersonaDependiente>
            expediente.personaDependiente = personasDependientes
        }
        val fragment = ConsentimientoFragment
                .newInstance(expediente?.consentimientoDeclaracionJurada)
        futurosSociosRepository.guardarExpediente(expediente!!)

        return fragment

    }

    private fun mostrarVistaCargaDniTitular(): Fragment {
        val imagen = R.drawable.ic_credencial_virtual
        val titulo = R.string.dni_text
        val descripcion = R.string.descripcion_documentacion_dni_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_DNI
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_DNI
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()
        if (expediente?.titular?.documentacionAnexa != null) {
            val documentacionAnexaDNI = expediente.titular!!.documentacionAnexa!!
                    .filter { it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.Dni.valor }

            documentacionAnexaDNI.forEach { documentacionDNI ->
                documentacionDNI.archivo.forEach { doc ->
                    val documentacion = DocumentacionAnexaModel()
                    documentacion.id = doc.id
                    documentacion.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(documentacion)
                }
            }
        }

        return InformacionDocumentacionAnexaFragment.newInstance(
                imagen, titulo, descripcion, descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.Dni, DOCUMENTACION_TITULAR, 0
        )

    }

    /**
     * Se analiza el estado de la sección de datos personales
     */
    private fun analizarEstadoDatosPersonales(expediente: Expediente): EstadoPasoExpediente {

        val estadoDatosPersonalesTitular = analizarEstadoDatosPersonalesTitular(expediente)
        if(estadoDatosPersonalesTitular != null) {
            return estadoDatosPersonalesTitular
        }

        val estadoDatosPersonalesConviviente = analizarEstadoDatosPersonalesConviviente(expediente)
        if(estadoDatosPersonalesConviviente != null) {
            return estadoDatosPersonalesConviviente
        }

        val estadoDatosPersonalesPersonasCargo = analizarEstadoDatosPersonalesPersonasCargo(expediente)
        if(estadoDatosPersonalesPersonasCargo != null) {
            return estadoDatosPersonalesPersonasCargo
        }


        return EstadoPasoExpediente(
                EstadoPasoExpedienteEnum.Completo.valor,
                R.string.vacio_text,
                ComienzoFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor,
                        R.string.declaracion_jurada_title,
                        R.string.descripcion_declaracion_jurada_text,
                        R.drawable.ic_declaracion_jurada
                )
        )
    }

    private fun analizarEstadoDatosPersonalesTitular(expediente: Expediente): EstadoPasoExpediente? {
        if (expediente.titular == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Inicial.valor,
                    R.string.vacio_text,
                    ComienzoFragment.newInstance(TipoSeccionEnum.DatosPersonales.valor,
                            R.string.datos_personales_title,
                            R.string.datos_personales_titular_text,
                            R.drawable.ic_credencial_virtual
                    )
            )
        }

        if (expediente.titular?.documentacionAnexa == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_dni_titular_error,
                    obtenerPasoSiguienteComienzoFragment(TipoSeccionEnum.DatosPersonales.valor)
            )
        }

        val cantidadImagenesDniTitular = expediente.titular?.documentacionAnexa!!.first { documentacionAnexa ->
            documentacionAnexa.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.Dni.valor
        }
        if (cantidadImagenesDniTitular.archivo.size != 2) {
            // Falta cargar las fotos del DNI del Titular
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_dni_titular_error,
                    obtenerPasoSiguienteComienzoFragment(TipoSeccionEnum.DatosPersonales.valor)
            )
        }

        if (expediente.titular?.nombre == null) {
            // Falta cargar una condición laboral
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos__titular_error,
                    obtenerPasoSiguienteComienzoFragment(TipoSeccionEnum.DatosPersonales.valor)
            )
        }

        if (expediente.titular?.condicionLaboral == null) {
            // Falta cargar una condición laboral
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_condicion_laboral_titular_error,
                    mostrarVistaCondicionLaboral()
            )
        }

        when (expediente.titular?.condicionLaboral?.id) {
            CondicionLaboralEnum.Asalariado.valor -> {
                val listaAsalariado = expediente.titular?.documentacionAnexa!!
                        .filter { documentacionAnexa ->
                            documentacionAnexa.tipoDocumentacionAnexa.id ==
                                    TipoDocumentacionAnexaEnum.ReciboSueldo.valor
                        }

                if (listaAsalariado.isEmpty() || listaAsalariado.first().archivo.isEmpty()) {
                    return EstadoPasoExpediente(EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_asalariado_titular_error,
                            mostrarVistaCondicionLaboral())
                }
                if (expediente.titular?.empleador == null) {
                    // Falta cargar los datos del empleador
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_empleador_titular_error,
                            mostrarVistaCondicionLaboral()
                    )
                }
            }
            CondicionLaboralEnum.Monotributista.valor -> {
                val listaMonotributo = expediente.titular?.documentacionAnexa!!
                        .filter { documentacionAnexa ->
                            documentacionAnexa.tipoDocumentacionAnexa.id ==
                                    TipoDocumentacionAnexaEnum.ConstanciasMonotributo.valor
                        }

                if (listaMonotributo.isEmpty() || listaMonotributo.first().archivo.isEmpty()) {
                    // Falta cargar las fotos del monotributo
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_monotributo_titular_error,
                            mostrarVistaCondicionLaboral()
                    )
                }
            }
            CondicionLaboralEnum.Autonomo.valor -> {
                if (expediente.titular?.tieneObraSocial == null) {
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_obra_social_error,
                            mostrarVistaCondicionLaboral()
                    )
                }
            }
        }
        return null

    }

    private fun analizarEstadoDatosPersonalesPersonasCargo(expediente: Expediente): EstadoPasoExpediente? {
        if (expediente.ingresaDependientes == null) {
            // Falta marcar si agrega personas a cargo o no
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_desea_agregar_persona_cargo_error,
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.PersonasCargo.valor,
                            R.string.unos_datos_mas_title,
                            R.string.personas_cargo_title,
                            R.string.paso_siguiente_text
                    )
            )
        }
        if (expediente.ingresaDependientes!!) {
            if (expediente.personaDependiente == null) {
                // Se marcó que iba a agregar hijos pero nunca agregó a nadie
                return EstadoPasoExpediente(
                        EstadoPasoExpedienteEnum.Proceso.valor,
                        R.string.datos_ingresa_persona_cargo_error,
                        ComienzoSeccionFragment.newInstance(
                                TipoPersonaEnum.PersonasCargo.valor,
                                R.string.unos_datos_mas_title,
                                R.string.personas_cargo_title,
                                R.string.paso_siguiente_text
                        )
                )
            }
            expediente.personaDependiente!!.forEach {
                if (it.documentacionAnexa != null) {
                    val cantidadImagenesDniPersonaDependiente = it.documentacionAnexa!!
                            .first { documentacionAnexa ->
                                documentacionAnexa.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.Dni.valor
                            }
                    if (cantidadImagenesDniPersonaDependiente.archivo.size != 2) {
                        // Falta cargar las fotos del DNI de la persona a cargo
                        return EstadoPasoExpediente(
                                EstadoPasoExpedienteEnum.Proceso.valor,
                                R.string.datos_dni_persona_cargo_error,
                                ComienzoSeccionFragment.newInstance(
                                        TipoPersonaEnum.PersonasCargo.valor,
                                        R.string.unos_datos_mas_title,
                                        R.string.personas_cargo_title,
                                        R.string.paso_siguiente_text
                                )
                        )
                    }
                }
                // Corroboramos que el modelo no sea nulo sólo con comprobar el nombre
                if (it.nombre == null) {
                    // No se guardó el formulario del hijo
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_persona_cargo_error,
                            ComienzoSeccionFragment.newInstance(
                                    TipoPersonaEnum.PersonasCargo.valor,
                                    R.string.unos_datos_mas_title,
                                    R.string.personas_cargo_title,
                                    R.string.paso_siguiente_text
                            )
                    )
                }
            }
        }

        return null

    }

    private fun analizarEstadoDatosPersonalesConviviente(expediente: Expediente): EstadoPasoExpediente? {
        if (expediente.ingresaConviviente == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_agregar_conviviente_error,
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.Conviviente.valor,
                            R.string.conviviente_title,
                            R.string.conviviente_subseccion_title,
                            R.string.paso_siguiente_text
                    )
            )
        }
        if (!expediente.ingresaConviviente!!) {
            return null
        }
        if (expediente.conviviente == null) {
            // Falta cargar el conviviente
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_dni_conviviente_error,
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.Conviviente.valor,
                            R.string.conviviente_title,
                            R.string.conviviente_subseccion_title,
                            R.string.paso_siguiente_text
                    )
            )
        }

        if (expediente.conviviente?.documentacionAnexa != null) {
            val cantidadImagenesDniConviviente = expediente.conviviente?.documentacionAnexa!!
                    .first { documentacionAnexa ->
                        documentacionAnexa.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.Dni.valor
                    }

            if (cantidadImagenesDniConviviente.archivo.size != 2) {
                // Falta cargar las fotos del DNI del Conviviente
                return EstadoPasoExpediente(
                        EstadoPasoExpedienteEnum.Proceso.valor,
                        R.string.datos_dni_conviviente_error,
                        ComienzoSeccionFragment.newInstance(
                                TipoPersonaEnum.Conviviente.valor,
                                R.string.conviviente_title,
                                R.string.conviviente_subseccion_title,
                                R.string.paso_siguiente_text
                        )
                )
            }
        } else {
            // Falta cargar las fotos del DNI del Conviviente
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_dni_conviviente_error,
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.Conviviente.valor,
                            R.string.conviviente_title,
                            R.string.conviviente_subseccion_title,
                            R.string.paso_siguiente_text
                    )
            )
        }

        if (expediente.conviviente?.nombre == null) {
            // Falta cargar el conviviente
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_conviviente_error,
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.Conviviente.valor,
                            R.string.conviviente_title,
                            R.string.conviviente_subseccion_title,
                            R.string.paso_siguiente_text
                    )
            )
        }

        if (expediente.conviviente?.condicionLaboral?.id == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_condicion_laboral_conviviente_error,
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.Conviviente.valor,
                            R.string.conviviente_title,
                            R.string.conviviente_subseccion_title,
                            R.string.paso_siguiente_text
                    )
            )
        }

        when (expediente.conviviente?.condicionLaboral?.id) {
            CondicionLaboralEnum.Asalariado.valor -> {
                val listaAsalariado = expediente.conviviente?.documentacionAnexa!!
                        .filter { documentacionAnexa ->
                            documentacionAnexa.tipoDocumentacionAnexa.id ==
                                    TipoDocumentacionAnexaEnum.ReciboSueldo.valor
                        }

                if (listaAsalariado.isEmpty() || listaAsalariado.first().archivo.isEmpty()) {
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_asalariado_conviviente_error,
                            ComienzoSeccionFragment.newInstance(
                                    TipoPersonaEnum.Conviviente.valor,
                                    R.string.conviviente_title,
                                    R.string.conviviente_subseccion_title,
                                    R.string.paso_siguiente_text
                            )
                    )
                }
            }
            CondicionLaboralEnum.Monotributista.valor -> {
                val listaMonotributo = expediente.conviviente?.documentacionAnexa!!
                        .filter { documentacionAnexa ->
                            documentacionAnexa.tipoDocumentacionAnexa.id ==
                                    TipoDocumentacionAnexaEnum.ConstanciasMonotributo.valor
                        }

                if (listaMonotributo.isEmpty() || listaMonotributo.first().archivo.isEmpty()) {
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_monotributo_conviviente_error,
                            ComienzoSeccionFragment.newInstance(
                                    TipoPersonaEnum.Conviviente.valor,
                                    R.string.conviviente_title,
                                    R.string.conviviente_subseccion_title,
                                    R.string.paso_siguiente_text
                            )
                    )
                }
            }
        }
        return null
    }


    //endregion
    //region TITULAR
    fun obtenerPasoSiguienteTitularFragment(titular: Titular): Fragment {
        val expediente = obtenerExpediente()

        if (expediente != null) {
            // Si existe una documentación anexa previamente cargada a un titular vacío, se la asignamos a la nueva
            titular.documentacionAnexa = expediente.titular?.documentacionAnexa

            if (expediente.titular == null) {
                expediente.declaracionJurada = null
            } else {
                // Si el titular no es nulo, se procede a modificarlo, conservando el id ya existente
                titular.id = expediente.titular?.id!!
            }
            expediente.titular = titular
            actualizarDeclaracionJurada(expediente, titular)

            if (expediente.titular?.estadoCivil?.id != EstadoCivilEnum.Casado.valor &&
                    expediente.titular?.estadoCivil?.id != EstadoCivilEnum.Concubinato.valor) {
                expediente.ingresaConviviente = false
            }
            futurosSociosRepository.guardarExpediente(expediente)
            return cargarDatosSubidaArchivoAdicional()
        }
        return DashboardFragment()
    }

    private fun cargarDatosSubidaArchivoAdicional(): Fragment {
        val imagen = R.drawable.ic_anadir_documentacion
        val titulo = R.string.informacion_adicional_text
        val descripcion = R.string.descripcion_documentacion_adicional_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_INFORMACION_ADICIONAL
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_INFORMACION_ADICIONAL
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.titular?.documentacionAnexa != null) {
            val documentacionAnexaDivorcio =
                    expediente.titular!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.InformacionAdicional.valor
                    }
            documentacionAnexaDivorcio.forEach { documentacionDivorcio ->
                documentacionDivorcio.archivo.forEach { doc ->
                    val documentacion = DocumentacionAnexaModel()
                    documentacion.id = doc.id
                    documentacion.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(documentacion)
                }
            }
        }

        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.InformacionAdicional,
                DOCUMENTACION_TITULAR, 0)
    }

    private fun mostrarVistaCondicionLaboral(): Fragment {
        val expediente = obtenerExpediente()
        var idCondicionLaboralSeleccionada = 0

        if (expediente?.titular?.condicionLaboral != null) {
            idCondicionLaboralSeleccionada = expediente.titular?.condicionLaboral?.id!!
        }
        val modeloOpciones = generarOpcionesCondicionLaboral(idCondicionLaboralSeleccionada)

        return OpcionesFragment.newInstance(modeloOpciones, VistaOpcionesEnum.CondicionLaboralTitular.valor)
    }

    private fun anexarDocumentacionTitular(
            nombreArchivos: ArrayList<String>,
            tipoDocumentacionAnexaEnum: TipoDocumentacionAnexaEnum
    ): Titular? {
        val idDocumentacionAnexaTitular = tipoDocumentacionAnexaEnum.valor
        val archivosOrigen = ArrayList<Archivo>()
        val datosIniciales = datosIniciales.value
        lateinit var tipoDocumentacionAnexa: TipoDocumentacionAnexa
        if (datosIniciales?.tipoDocumentacionAnexa != null) {
            tipoDocumentacionAnexa = datosIniciales.tipoDocumentacionAnexa.first {
                it.id == idDocumentacionAnexaTitular
            }
        }
        val expediente = obtenerExpediente()!!

        // Genero la lista de archivos generadas por el titular
        nombreArchivos.forEachIndexed { _, element ->
            archivosOrigen.add(Archivo(0, element))
        }
        // Se anexa a un tipo de documentación
        val nuevaDocumentacionAnexa = DocumentacionAnexa(0, tipoDocumentacionAnexa, archivosOrigen)

        if (expediente.titular == null) {
            expediente.titular = Titular()
            expediente.titular?.documentacionAnexa = ArrayList()
            expediente.titular?.documentacionAnexa?.add(nuevaDocumentacionAnexa)
            futurosSociosRepository.guardarExpediente(expediente)
            return expediente.titular
        }

        var documentacionReemplazada = false
        expediente.titular?.documentacionAnexa?.forEachIndexed { index, element ->
            if ((element.tipoDocumentacionAnexa.id == idDocumentacionAnexaTitular) &&
                    expediente.titular?.documentacionAnexa?.get(index)?.archivo != null
            ) {
                documentacionReemplazada = true
                element.archivo = DealerHelper.generarListaGuardarDocumentacionAnexa(element, archivosOrigen)
            }
        }
        if (!documentacionReemplazada) {
            expediente.titular?.documentacionAnexa?.add(nuevaDocumentacionAnexa)
        }

        futurosSociosRepository.guardarExpediente(expediente)
        return expediente.titular
    }

    private fun cargarDatosSubidaArchivoReciboSueldoTitular(): Fragment {
        val imagen = R.drawable.ic_resumen
        val titulo = R.string.recibo_sueldo_text
        val descripcion = R.string.descripcion_documentacion_recibo_sueldo_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_RECIBO_SUELDO
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_RECIBO_SUELDO
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.titular?.documentacionAnexa != null) {
            val documentacionAnexa =
                    expediente.titular!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.ReciboSueldo.valor
                    }
            documentacionAnexa.forEach { documentacion ->
                documentacion.archivo.forEach { doc ->
                    val nuevoDocumento = DocumentacionAnexaModel()
                    nuevoDocumento.id = doc.id
                    nuevoDocumento.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(nuevoDocumento)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.ReciboSueldo,
                DOCUMENTACION_TITULAR, 0)
    }

    private fun cargarDatosSubidaArchivoConstanciaMonotributo(): Fragment {
        val imagen = R.drawable.ic_constancia_monotributo
        val titulo = R.string.constancia_monotributo_text
        val descripcion = R.string.descripcion_documentacion_constancia_monotributo_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if(expediente?.titular?.empleador != null) {
            expediente.titular?.empleador = null
            guardarExpediente(expediente)
        }
        if (expediente?.titular?.documentacionAnexa != null) {
            val documentacionAnexa =
                    expediente.titular!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.ConstanciasMonotributo.valor
                    }
            documentacionAnexa.forEach { documentacion ->
                documentacion.archivo.forEach { doc ->
                    val nuevoDocumento = DocumentacionAnexaModel()
                    nuevoDocumento.id = doc.id
                    nuevoDocumento.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(nuevoDocumento)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.ConstanciasMonotributo,
                DOCUMENTACION_TITULAR, 0)
    }

    fun obtenerPasoSiguienteDatosEmpleadorFragment(empleador: Empleador?, tipoDocumentacion: Int): Fragment {
        val expediente = obtenerExpediente()

        if (expediente != null) {
            when (tipoDocumentacion) {
                DOCUMENTACION_TITULAR -> {
                    if (empleador != null) {
                        expediente.titular!!.empleador = empleador
                        futurosSociosRepository.guardarExpediente(expediente)
                    }
                    return analizarPuedeIngresarConvivienteYVisualizar()
                }
                DOCUMENTACION_CONVIVIENTE -> {
                    if (empleador != null) {
                        expediente.conviviente!!.empleador = empleador
                        futurosSociosRepository.guardarExpediente(expediente)
                    }
                    return ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.PersonasCargo.valor,
                            R.string.unos_datos_mas_title,
                            R.string.personas_cargo_title,
                            R.string.paso_siguiente_text
                    )
                }
            }
        }
        return DashboardFragment()
    }

    //endregion
    //region CONYUGE
    private fun mostrarVistaOpcionIngresaConviviente(): Fragment {
        val expediente = obtenerExpediente()
        var opcionSiSeleccionado = false
        var opcionNoSeleccionado = false

        if (expediente?.ingresaConviviente != null) {
            if (expediente.ingresaConviviente!!) {
                opcionSiSeleccionado = true
            } else {
                opcionNoSeleccionado = true
            }
        }
        val opciones = DealerHelper.obtenerListaGenericaOpcionesSiNo(opcionSiSeleccionado, opcionNoSeleccionado)

        val grupoOpciones = GrupoOpcionesModel(R.string.agregar_conviviente_title, opciones)


        return OpcionesFragment.newInstance(grupoOpciones, VistaOpcionesEnum.DeseaAgregarConviviente.valor)
    }

    private fun analizarPuedeIngresarConvivienteYVisualizar(): Fragment {
        val expediente = obtenerExpediente()
        if (expediente != null) {
            return when (expediente.titular?.estadoCivil?.id) {
                EstadoCivilEnum.Concubinato.valor, EstadoCivilEnum.Casado.valor -> {
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.Conviviente.valor,
                            R.string.conviviente_title,
                            R.string.conviviente_subseccion_title,
                            R.string.paso_siguiente_text
                    )
                }
                else -> {
                    ComienzoSeccionFragment.newInstance(
                            TipoPersonaEnum.PersonasCargo.valor,
                            R.string.unos_datos_mas_title,
                            R.string.personas_cargo_title,
                            R.string.paso_siguiente_text
                    )
                }
            }
        }
        return DashboardFragment()
    }

    private fun cargarDatosSubidaArchivoAdicionalConviviente(): Fragment {
        val imagen = R.drawable.ic_anadir_documentacion
        val titulo = R.string.informacion_adicional_text
        val descripcion = R.string.descripcion_documentacion_adicional_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_INFORMACION_ADICIONAL
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_INFORMACION_ADICIONAL
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.conviviente?.documentacionAnexa != null) {
            val documentacionAnexaAdicional = expediente.conviviente!!.documentacionAnexa!!
                    .filter { it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.InformacionAdicional.valor }

            documentacionAnexaAdicional.forEach { documentacionAdicional ->
                documentacionAdicional.archivo.forEach { doc ->
                    val documentacion = DocumentacionAnexaModel()
                    documentacion.id = doc.id
                    documentacion.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(documentacion)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.InformacionAdicional,
                DOCUMENTACION_CONVIVIENTE, 0)
    }

    private fun anexarDocumentacionConviviente(
            nombreArchivos: ArrayList<String>,
            tipoDocumentacionAnexaEnum: TipoDocumentacionAnexaEnum
    ): Conviviente? {
        val idDocumentacionAnexaConviviente = tipoDocumentacionAnexaEnum.valor
        val archivosOrigen = ArrayList<Archivo>()
        val datosIniciales = datosIniciales.value
        lateinit var tipoDocumentacionAnexa: TipoDocumentacionAnexa
        if (datosIniciales?.tipoDocumentacionAnexa != null) {
            tipoDocumentacionAnexa = datosIniciales.tipoDocumentacionAnexa.first {
                it.id == idDocumentacionAnexaConviviente
            }
        }
        val expediente = obtenerExpediente()!!

        nombreArchivos.forEachIndexed { _, element ->
            archivosOrigen.add(Archivo(0, element))
        }
        val nuevaDocumentacionAnexa = DocumentacionAnexa(0, tipoDocumentacionAnexa, archivosOrigen)

        if (expediente.conviviente == null) {
            expediente.conviviente = Conviviente()
            expediente.conviviente?.documentacionAnexa = ArrayList()
            expediente.conviviente?.documentacionAnexa?.add(nuevaDocumentacionAnexa)
            futurosSociosRepository.guardarExpediente(expediente)
            return expediente.conviviente
        }

        var documentacionReemplazada = false
        expediente.conviviente?.documentacionAnexa?.forEachIndexed { index, element ->
            if ((element.tipoDocumentacionAnexa.id == idDocumentacionAnexaConviviente) &&
                    expediente.conviviente?.documentacionAnexa?.get(index)?.archivo != null
            ) {
                documentacionReemplazada = true
                element.archivo = DealerHelper.generarListaGuardarDocumentacionAnexa(element, archivosOrigen)
            }
        }
        if (!documentacionReemplazada) {
            expediente.conviviente?.documentacionAnexa?.add(nuevaDocumentacionAnexa)
        }

        futurosSociosRepository.guardarExpediente(expediente)
        return expediente.conviviente
    }


    fun obtenerPasoSiguienteAporteObraSocialFragment(tieneObraSocial: Boolean, nombreObraSocial: String): Fragment {
        val expediente = futurosSociosRepository.obtenerExpediente()
        expediente.titular?.tieneObraSocial = tieneObraSocial
        expediente.titular?.obraSocialActual = nombreObraSocial
        futurosSociosRepository.guardarExpediente(expediente)

        return analizarPuedeIngresarConvivienteYVisualizar()
    }

    private fun cargarDatosSubidaArchivoDniConyuge(): Fragment {
        val imagen = R.drawable.ic_credencial_virtual
        val titulo = R.string.dni_text
        val descripcion = R.string.descripcion_documentacion_dni_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_DNI
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_DNI
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.conviviente?.documentacionAnexa != null) {
            val documentacionAnexaDNI =
                    expediente.conviviente!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.Dni.valor
                    }
            documentacionAnexaDNI.forEach { documentacionDNI ->
                documentacionDNI.archivo.forEach { doc ->
                    val documentacion = DocumentacionAnexaModel()
                    documentacion.id = doc.id
                    documentacion.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(documentacion)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.Dni,
                DOCUMENTACION_CONVIVIENTE, 0)
    }

    private fun cargarDatosSubidaArchivoReciboSueldoConviviente(): Fragment {
        val imagen = R.drawable.ic_resumen
        val titulo = R.string.recibo_sueldo_text
        val descripcion = R.string.descripcion_documentacion_recibo_sueldo_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_RECIBO_SUELDO
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_RECIBO_SUELDO
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.conviviente?.documentacionAnexa != null) {
            val documentacionAnexa =
                    expediente.conviviente!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.ReciboSueldo.valor
                    }
            documentacionAnexa.forEach { documentacion ->
                documentacion.archivo.forEach { doc ->
                    val nuevoDocumento = DocumentacionAnexaModel()
                    nuevoDocumento.id = doc.id
                    nuevoDocumento.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(nuevoDocumento)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.ReciboSueldo,
                DOCUMENTACION_CONVIVIENTE, 0)
    }

    private fun mostrarVistaCondicionLaboralConviviente(): Fragment {
        val expediente = obtenerExpediente()
        // Puede ser que ya haya seleccionada una condición laboral con anterioridad
        var idCondicionLaboralSeleccionada = 0
        if (expediente?.conviviente?.condicionLaboral != null) {
            idCondicionLaboralSeleccionada = expediente.conviviente?.condicionLaboral?.id!!
        }
        val modeloOpciones = generarOpcionesCondicionLaboral(idCondicionLaboralSeleccionada)

        return OpcionesFragment.newInstance(modeloOpciones, VistaOpcionesEnum.CondicionLaboralConyuge.valor)
    }

    private fun generarOpcionesCondicionLaboral(idCondicionLaboralSeleccionada: Int): GrupoOpcionesModel {
        val opcionesList = ArrayList<OpcionModel>()
        val modeloOpciones = GrupoOpcionesModel(R.string.condicion_laboral_title, opcionesList)
        val datosIniciales = datosIniciales.value

        if (datosIniciales?.condicionLaboral != null) {
            datosIniciales.condicionLaboral.map { item ->
                if (item.id == idCondicionLaboralSeleccionada) {
                    opcionesList.add(OpcionModel(item.id, item.descripcion, item.detalle, true))
                } else {
                    opcionesList.add(OpcionModel(item.id, item.descripcion, item.detalle, false))
                }
            }
        }
        return modeloOpciones
    }

    fun obtenerPasoSiguienteConvivienteFragment(conviviente: Conviviente): Fragment {
        val expediente = obtenerExpediente()

        if (expediente != null) {
            conviviente.documentacionAnexa = expediente.conviviente?.documentacionAnexa

            if (expediente.conviviente == null) {
                expediente.declaracionJurada = null
            } else {
                conviviente.id = expediente.conviviente?.id!!
            }

            actualizarDeclaracionJurada(expediente, conviviente)
            expediente.conviviente = conviviente
            futurosSociosRepository.guardarExpediente(expediente)
            return cargarDatosSubidaArchivoAdicionalConviviente()
        }
        return DashboardFragment()
    }

    private fun cargarDatosSubidaArchivoConstanciaMonotributoConviviente(): Fragment {
        val imagen = R.drawable.ic_constancia_monotributo
        val titulo = R.string.constancia_monotributo_text
        val descripcion = R.string.descripcion_documentacion_constancia_monotributo_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_CONSTANCIA_MONOTRIBUTO
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.conviviente?.documentacionAnexa != null) {
            val documentacionAnexa =
                    expediente.conviviente!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.ConstanciasMonotributo.valor
                    }
            documentacionAnexa.forEach { documentacion ->
                documentacion.archivo.forEach { doc ->
                    val nuevoDocumento = DocumentacionAnexaModel()
                    nuevoDocumento.id = doc.id
                    nuevoDocumento.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(nuevoDocumento)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.ConstanciasMonotributo,
                DOCUMENTACION_CONVIVIENTE, 0)
    }

    //endregion
    //region PERSONAS A CARGO
    private fun analizarMostrarVistaOpcionDeseaAgregarOtraPersonaCargo(
            ordenPersonaRecientementeModificada: Int
    ): Fragment {
        val expediente = obtenerExpediente()
        val ordenUltimaPersonaCargada = expediente?.personaDependiente!!.last().posicion

        return if (ordenUltimaPersonaCargada == ordenPersonaRecientementeModificada) {
            val grupoOpciones = GrupoOpcionesModel(R.string.agregar_persona_cargo_title,
                    DealerHelper.obtenerListaGenericaOpcionesSiNo(false, false)
            )
            return OpcionesFragment.newInstance(grupoOpciones, VistaOpcionesEnum.DeseaAgregarOtraPersonaCargo.valor)
        } else {
            // Se estuvo viendo el formulario de una persona a cargo pero aún quedan más personas a cargo para mostrar
            cargarDatosSubidaArchivoDniPersonaCargo(ordenPersonaRecientementeModificada + 1)
        }
    }

    fun obtenerPasoSiguienteComienzoSeccionFragment(tipoSeccion: Int): Fragment {
        when (tipoSeccion) {
            TipoPersonaEnum.PersonasCargo.valor -> {
                val expediente = obtenerExpediente()
                var opcionSiSeleccionado = false
                var opcionNoSeleccionado = false

                if (expediente?.ingresaDependientes != null) {
                    if (expediente.ingresaDependientes!!) {
                        opcionSiSeleccionado = true
                    } else {
                        opcionNoSeleccionado = true
                    }
                }
                val opciones = DealerHelper.obtenerListaGenericaOpcionesSiNo(
                        opcionSiSeleccionado,
                        opcionNoSeleccionado
                )

                val grupoOpcionesModel = GrupoOpcionesModel(
                        R.string.agregar_primera_persona_cargo_title,
                        opciones
                )

                return OpcionesFragment.newInstance(
                        grupoOpcionesModel,
                        VistaOpcionesEnum.DeseaAgregarPersonaCargo.valor
                )
            }
            TipoPersonaEnum.Conviviente.valor -> {
                return mostrarVistaOpcionIngresaConviviente()
            }
            else -> {
                return mostrarVistaOpcionIngresaConviviente()
            }
        }
    }

    private fun cargarDatosSubidaArchivoDniPersonaCargo(ordenPersona: Int): Fragment {
        val imagen = R.drawable.ic_credencial_virtual
        val titulo = R.string.dni_text
        val descripcion = R.string.descripcion_documentacion_dni_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_DNI
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_DNI
        var orden = ordenPersona
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()
        if (expediente?.personaDependiente != null && expediente.personaDependiente!!.size > 0) {
            val personaDependienteFiltrada = expediente.personaDependiente?.filter { it.posicion == orden }

            if (personaDependienteFiltrada?.size == 1) {
                personaDependienteFiltrada.forEach { personaDependiente ->
                    val documentacionAnexaDNI =
                            personaDependiente.documentacionAnexa!!.filter {
                                it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.Dni.valor
                            }
                    documentacionAnexaDNI.forEach { documentacionDNI ->
                        documentacionDNI.archivo.forEach { doc ->
                            val documentacion = DocumentacionAnexaModel()
                            documentacion.id = doc.id
                            documentacion.nombreArchivo = doc.nombre
                            documentacionAnexaArrayList.add(documentacion)
                        }
                    }
                }
            } else {
                orden = expediente.personaDependiente!!.last().posicion!!
                orden += 1
                val nuevaPersonaDependiente = PersonaDependiente(orden)
                expediente.personaDependiente?.add(nuevaPersonaDependiente)
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(
                imagen, titulo, descripcion, descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.Dni, DOCUMENTACION_PERSONA_A_CARGO, orden)
    }

    private fun cargarDatosSubidaArchivoAdicionalPersonaCargo(orden: Int): Fragment {
        val imagen = R.drawable.ic_anadir_documentacion
        val titulo = R.string.informacion_adicional_text
        val descripcion = R.string.descripcion_documentacion_adicional_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_INFORMACION_ADICIONAL
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_INFORMACION_ADICIONAL
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()
        val personaCargo = expediente?.personaDependiente?.find { it.posicion == orden }

        if (personaCargo?.documentacionAnexa != null) {
            val documentacionAnexaAdicional =
                    personaCargo.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.InformacionAdicional.valor
                    }
            documentacionAnexaAdicional.forEach { documentacionAdicional ->
                documentacionAdicional.archivo.forEach { doc ->
                    val documentacion = DocumentacionAnexaModel()
                    documentacion.id = doc.id
                    documentacion.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(documentacion)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.InformacionAdicional,
                DOCUMENTACION_PERSONA_A_CARGO, orden)
    }

    fun obtenerPasoSiguientePersonaCargoFragment(personaDependiente: PersonaDependiente): Fragment {
        val expediente = obtenerExpediente()

        if (expediente != null) {
            expediente.personaDependiente?.forEachIndexed { index, persona ->
                if (persona.posicion == personaDependiente.posicion) {
                    personaDependiente.documentacionAnexa = persona.documentacionAnexa
                    if (expediente.personaDependiente!![index].nombre == null) {
                        expediente.declaracionJurada = null
                    } else {
                        personaDependiente.id = expediente.personaDependiente!![index].id
                    }
                    expediente.personaDependiente!![index] = personaDependiente
                    actualizarDeclaracionJurada(expediente, personaDependiente)
                }
            }
            futurosSociosRepository.guardarExpediente(expediente)
            return cargarDatosSubidaArchivoAdicionalPersonaCargo(personaDependiente.posicion!!)
        }
        return DashboardFragment()
    }

    private fun anexarDocumentacionPersonaCargo(
            nombreArchivos: ArrayList<String>,
            tipoDocumentacionAnexaEnum: TipoDocumentacionAnexaEnum,
            ordenPersona: Int
    ): PersonaDependiente? {
        val idDocumentacionAnexaConviviente = tipoDocumentacionAnexaEnum.valor
        val archivosOrigen = ArrayList<Archivo>()
        lateinit var personaExpedienteAfectada: PersonaDependiente
        val datosIniciales = datosIniciales.value
        lateinit var tipoDocumentacionAnexa: TipoDocumentacionAnexa
        if (datosIniciales?.tipoDocumentacionAnexa != null) {
            tipoDocumentacionAnexa = datosIniciales.tipoDocumentacionAnexa.first {
                it.id == idDocumentacionAnexaConviviente
            }
        }
        val expediente = obtenerExpediente()!!

        // Genero la lista de archivos generadas por alguna persona a cargo
        nombreArchivos.forEachIndexed { _, element ->
            archivosOrigen.add(Archivo(0, element))
        }
        // Se anexa a un tipo de documentación
        val nuevaDocumentacionAnexa = DocumentacionAnexa(0, tipoDocumentacionAnexa, archivosOrigen)

        if (expediente.personaDependiente == null) {
            expediente.personaDependiente = ArrayList()
            val nuevaPersonaDependiente = PersonaDependiente(ordenPersona)
            nuevaPersonaDependiente.documentacionAnexa = ArrayList()
            nuevaPersonaDependiente.documentacionAnexa?.add(nuevaDocumentacionAnexa)
            expediente.personaDependiente!!.add(nuevaPersonaDependiente)
            personaExpedienteAfectada = nuevaPersonaDependiente
            futurosSociosRepository.guardarExpediente(expediente)
            return personaExpedienteAfectada
        }

        val persona = expediente.personaDependiente?.find { it.posicion == ordenPersona }
        if (persona == null) {
            val nuevaPersonaDependiente = PersonaDependiente(ordenPersona)
            nuevaPersonaDependiente.documentacionAnexa = ArrayList()
            nuevaPersonaDependiente.documentacionAnexa?.add(nuevaDocumentacionAnexa)
            expediente.personaDependiente!!.add(nuevaPersonaDependiente)
            personaExpedienteAfectada = nuevaPersonaDependiente
        }
        // Puede existir una documentación anexa de la persona dependiente, filtramos el posicion
        expediente.personaDependiente!!.filter { it.posicion == ordenPersona }.forEach {
            if (it.posicion == ordenPersona && it.documentacionAnexa != null) {
                var documentacionReemplazada = false
                it.documentacionAnexa?.forEachIndexed { index, element ->
                    if ((element.tipoDocumentacionAnexa.id == idDocumentacionAnexaConviviente) &&
                            it.documentacionAnexa?.get(index)?.archivo != null
                    ) {
                        documentacionReemplazada = true
                        element.archivo = DealerHelper.generarListaGuardarDocumentacionAnexa(element, archivosOrigen)
                    }
                }

                if (!documentacionReemplazada) {
                    it.documentacionAnexa?.add(nuevaDocumentacionAnexa)
                }
                personaExpedienteAfectada = it
            }
        }

        futurosSociosRepository.guardarExpediente(expediente)
        return personaExpedienteAfectada
    }

    //endregion
    //region DECLARACIÓN JURADA
    private fun actualizarDeclaracionJurada(expediente: Expediente, persona: Persona) {
        if (expediente.declaracionJurada != null) {
            expediente.declaracionJurada!!.respuestaPreguntaDeclaracionJurada?.forEach { respuesta ->
                respuesta.respuestaPatologia?.forEach { item ->
                    item.personaPatologia?.forEach { p ->
                        if (p.persona?.dni == persona.dni) {
                            p.persona!!.nombre = persona.nombre
                            p.persona!!.apellido = persona.apellido
                            p.persona!!.dni = persona.dni
                        }
                    }
                }
            }
        }
    }

    fun obtenerPasoSiguienteConsentimientoDeclaracionJuradaFragment(): Fragment {
        val expediente = obtenerExpediente()
        if (expediente != null) {
            expediente.consentimientoDeclaracionJurada = true
            futurosSociosRepository.guardarExpediente(expediente)
        }
        return analizarMostrarPreguntasDeclaracionJurada(0, null)
    }

    fun analizarMostrarPreguntasDeclaracionJurada(
            ordenSiguientePregunta: Int,
            respuesta: RespuestaPreguntaDeclaracionJurada?
    ): Fragment {
        val expediente = obtenerExpediente()
        var ordenPreguntaActual = ordenSiguientePregunta
        if (ordenSiguientePregunta > 0) {
            ordenPreguntaActual = ordenSiguientePregunta - 1
        }
        if (respuesta != null) {
            if (expediente!!.declaracionJurada == null) {
                expediente.declaracionJurada = DeclaracionJurada()
                expediente.declaracionJurada!!.respuestaPreguntaDeclaracionJurada = ArrayList()
                expediente.declaracionJurada!!.respuestaPreguntaDeclaracionJurada!!.add(respuesta)
            } else {
                if (expediente.declaracionJurada!!.respuestaPreguntaDeclaracionJurada!!.size > ordenPreguntaActual) {
                    expediente.declaracionJurada!!.respuestaPreguntaDeclaracionJurada!![ordenPreguntaActual] = respuesta
                } else {
                    expediente.declaracionJurada!!.respuestaPreguntaDeclaracionJurada!!.add(respuesta)
                }
            }
            futurosSociosRepository.guardarExpediente(expediente)
        }
        val datosIniciales = datosIniciales.value

        if (datosIniciales!!.preguntaDeclaracionJurada.size <= ordenSiguientePregunta) {
            return ContexturaFisicaFragment.newInstance(expediente)
        }
        val pregunta = datosIniciales.preguntaDeclaracionJurada[ordenSiguientePregunta]
        return when {
            pregunta.tipo == TipoPreguntaDeclaracionJuradaEnum.Tipo1.valor ->
                PreguntaDeclaracionJuradaFragment.newInstance(pregunta, ordenSiguientePregunta + 1)
            pregunta.tipo == TipoPreguntaDeclaracionJuradaEnum.Tipo2.valor ->
                PreguntaTipoDosFragment.newInstance(pregunta, ordenSiguientePregunta + 1)
            else -> DashboardFragment()
        }
    }

    fun obtenerPasoSiguienteContexturaFisicaFragment(expediente: Expediente): Fragment {
        futurosSociosRepository.guardarExpediente(expediente)
        return PresentacionFragment.newInstance(TipoSeccionEnum.FormaPago.valor)
    }

    /**
     * Se analiza el estado de la sección de declaración jurada
     */
    private fun analizarEstadoDeclaracionJurada(expediente: Expediente): EstadoPasoExpediente {
        if (expediente.declaracionJurada == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Inicial.valor,
                    R.string.vacio_text,
                    ComienzoFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor,
                            R.string.declaracion_jurada_title,
                            R.string.descripcion_declaracion_jurada_text,
                            R.drawable.ic_declaracion_jurada
                    )
            )
        }
        val datosIniciales = datosIniciales.value
        val cantidadPreguntas = datosIniciales?.preguntaDeclaracionJurada?.size

        if (cantidadPreguntas != expediente.declaracionJurada?.respuestaPreguntaDeclaracionJurada?.size) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_declaracion_jurada_error,
                    ComienzoFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor,
                            R.string.declaracion_jurada_title,
                            R.string.descripcion_declaracion_jurada_text,
                            R.drawable.ic_declaracion_jurada
                    )
            )
        }

        if (expediente.titular?.contexturaFisica?.peso == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_declaracion_jurada_contextura_fisica_error,
                    ContexturaFisicaFragment.newInstance(expediente)
            )
        }
        return EstadoPasoExpediente(
                EstadoPasoExpedienteEnum.Completo.valor,
                R.string.vacio_text, PresentacionFragment.newInstance(
                TipoSeccionEnum.FormaPago.valor
        )
        )
    }

    //endregion
    //region PLAN Y FORMA DE PAGO
    fun obtenerPasoSiguienteCargaCbuFragment(formaPago: FormaPago): Fragment {
        val expediente = futurosSociosRepository.obtenerExpediente()
        expediente.formaPago = formaPago
        futurosSociosRepository.guardarExpediente(expediente)
        return InformacionFirmaGrafometricaFragment()
    }

    private fun mostrarProtesisOdontologica(): Fragment {
        val expediente = obtenerExpediente()
        var opcionSiSeleccionado = false
        var opcionNoSeleccionado = false

        if (expediente?.protesisOdontologicas != null) {
            if (expediente.protesisOdontologicas!!) {
                opcionSiSeleccionado = true
            } else {
                opcionNoSeleccionado = true
            }
        }

        val opciones = DealerHelper.obtenerListaGenericaOpcionesSiNo(opcionSiSeleccionado, opcionNoSeleccionado)
        val grupoOpcionesModel = GrupoOpcionesModel(R.string.desea_agregar_protesis_odontologica_title, opciones)
        return OpcionesFragment.newInstance(grupoOpcionesModel, VistaOpcionesEnum.ProtesisOdontologica.valor)
    }

    private fun mostrarFormaPagoFragment(): Fragment {
        val expediente = obtenerExpediente()
        var idTipoFormaPago = 0

        if (expediente?.formaPago?.tipoFormaPago != null) {
            idTipoFormaPago = expediente.formaPago?.tipoFormaPago?.id!!
        }
        val opcionesList = ArrayList<OpcionModel>()
        val modeloOpciones = GrupoOpcionesModel(R.string.forma_pago_text, opcionesList)

        if (datosIniciales.value?.tipoFormaPago != null) {
            datosIniciales.value?.tipoFormaPago!!.map { item ->
                if (item.id == idTipoFormaPago) {
                    opcionesList.add(
                            OpcionModel(
                                    item.id,
                                    item.descripcion!!,
                                    item.descripcionAdicional,
                                    true
                            )
                    )
                } else {
                    opcionesList.add(
                            OpcionModel(
                                    item.id,
                                    item.descripcion!!,
                                    item.descripcionAdicional,
                                    false
                            )
                    )
                }
            }
        }
        return OpcionesFragment.newInstance(modeloOpciones, VistaOpcionesEnum.FormaPago.valor)
    }

    private fun cargarDatosSubidaArchivoDebitoTarjetaCredito(): Fragment {
        val imagen = R.drawable.ic_tarjeta_debito_credito
        val titulo = R.string.debito_tarjeta_credito_text
        val descripcion = R.string.descripcion_documentacion_tarjeta_credito_text
        val descripcionBoton = R.string.continuar_action
        val cantidadMinimaImagenes = CANTIDAD_MINIMA_IMAGENES_FORMA_PAGO
        val cantidadMaximaImagenes = CANTIDAD_MAXIMA_IMAGENES_FORMA_PAGO
        val expediente = obtenerExpediente()
        val documentacionAnexaArrayList = ArrayList<DocumentacionAnexaModel>()

        if (expediente?.titular?.documentacionAnexa != null) {
            val documentacionAnexaFormaPago =
                    expediente.titular!!.documentacionAnexa!!.filter {
                        it.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.TarjetaBancaria.valor
                    }
            documentacionAnexaFormaPago.forEach { documentacionAdjunta ->
                documentacionAdjunta.archivo.forEach { doc ->
                    val documentacion = DocumentacionAnexaModel()
                    documentacion.id = doc.id
                    documentacion.nombreArchivo = doc.nombre
                    documentacionAnexaArrayList.add(documentacion)
                }
            }
        }
        return InformacionDocumentacionAnexaFragment.newInstance(imagen, titulo, descripcion,
                descripcionBoton, cantidadMinimaImagenes, cantidadMaximaImagenes,
                documentacionAnexaArrayList, TipoDocumentacionAnexaEnum.TarjetaBancaria,
                DOCUMENTACION_TITULAR, 0)
    }

    private fun mostrarVistaTarjetaCredito(): Fragment {
        val expediente = obtenerExpediente()
        return TarjetaDeCreditoFragment.newInstance(expediente?.formaPago)
    }

    fun obtenerPasoSiguienteTarjetaDeCreditoFragment(formaPago: FormaPago): Fragment {
        val expediente = obtenerExpediente()
        if (expediente?.formaPago != null) {
            formaPago.id = expediente.formaPago?.id!!
        }
        expediente?.formaPago = formaPago
        futurosSociosRepository.guardarExpediente(expediente!!)
        return InformacionFirmaGrafometricaFragment()
    }

    /**
     * Se analiza el estado de la sección de forma de pago
     */
    private fun analizarEstadoFormaPago(expediente: Expediente): EstadoPasoExpediente {
        if (expediente.formaPago == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Inicial.valor,
                    R.string.vacio_text,
                    ComienzoFragment.newInstance(
                            TipoSeccionEnum.FormaPago.valor,
                            R.string.plan_forma_pago_title,
                            R.string.descripcion_forma_pago_text,
                            R.drawable.ic_forma_pago
                    )
            )
        }

        if (expediente.plan == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_plan_error,
                    obtenerPasoSiguienteComienzoFragment(TipoSeccionEnum.FormaPago.valor
                    )
            )
        }
        if (expediente.formaPago?.tipoFormaPago == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_tipo_forma_pago_error,
                    mostrarFormaPagoFragment()
            )
        }

        when (expediente.formaPago?.tipoFormaPago?.id) {
            FormaPagoEnum.DebitoTarjetaCredito.valor -> {
                val cantidadListaImagenesTarjetaCredito = expediente.titular?.documentacionAnexa!!
                        .filter { documentacionAnexa ->
                    documentacionAnexa.tipoDocumentacionAnexa.id == TipoDocumentacionAnexaEnum.TarjetaBancaria.valor
                }
                if (cantidadListaImagenesTarjetaCredito.isEmpty()) {
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_tarjeta_credito_error,
                            mostrarFormaPagoFragment()
                    )
                }
                val cantidadImagenesTarjetaCredito = cantidadListaImagenesTarjetaCredito.first()
                if (cantidadImagenesTarjetaCredito.archivo.isEmpty()) {
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_tarjeta_credito_error,
                            mostrarFormaPagoFragment()
                    )
                }
            }
            FormaPagoEnum.DebitoCuentaBancaria.valor -> {
                if (expediente.formaPago?.tipoCuentaBancaria == null || expediente.formaPago?.cbu == null) {
                    return EstadoPasoExpediente(
                            EstadoPasoExpedienteEnum.Proceso.valor,
                            R.string.datos_cbu_tipo_cuenta_error, mostrarFormaPagoFragment()
                    )
                }
            }
        }

        if (expediente.firmaGrafometrica == null) {
            return EstadoPasoExpediente(
                    EstadoPasoExpedienteEnum.Proceso.valor,
                    R.string.datos_firma_grafometrica_error,
                    mostrarFormaPagoFragment()
            )
        }

        return EstadoPasoExpediente(
                EstadoPasoExpedienteEnum.Completo.valor,
                R.string.vacio_text,
                ComienzoFragment.newInstance(
                        TipoSeccionEnum.FormaPago.valor,
                        R.string.plan_forma_pago_title,
                        R.string.descripcion_forma_pago_text,
                        R.drawable.ic_forma_pago
                )
        )
    }

    /**
     * Se analiza si se habilita o no el paso que le sigue a la sección de forma de pago
     */


    //endregion
    //region FIRMA GRAFOMÉTRICA
    fun obtenerPasoSiguienteInformacionFirmaGrafometricaFragment(): Fragment {
        return FirmaGrafometricaFragment()
    }

    fun obtenerPasoSiguienteFirmaGrafometricaFragment(svg: String): Fragment {
        val expediente = obtenerExpediente()
        expediente!!.firmaGrafometrica = svg
        futurosSociosRepository.guardarExpediente(expediente)
        return mostrarVistaFinalSeccion()
    }

    //endregion
    //region FINAL SECCION
    private fun mostrarVistaFinalSeccion(): Fragment {
        return FinalSeccionFragment.newInstance(R.string.final_seccion_text, R.string.aceptar_action)
    }
    //endregion
}