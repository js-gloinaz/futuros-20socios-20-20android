package com.jerarquicos.futurossocios.view.cambiarContrasenia

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.CambiarContraseniaFragmentBinding
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.AppConstants
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class CambiarContraseniaFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    private lateinit var cambiarContraseniaViewModel: CambiarContraseniaViewModel
    lateinit var binding: CambiarContraseniaFragmentBinding
    private lateinit var validadorContrasenia: EditTextValidator
    private lateinit var validadorRepetirContrasenia: EditTextValidator
    private lateinit var validadorContraseniaActual: EditTextValidator

    companion object {
        private const val BUNDLE_USUARIO = "BUNDLE_USUARIO"

        fun newInstance(usuario: String): CambiarContraseniaFragment {
            val args = Bundle()
            args.putString(BUNDLE_USUARIO, usuario)

            val fragment = CambiarContraseniaFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.cambiar_contrasenia_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(null, javaClass.name, true)


        /**recibo el parámetro para saber de que fragment llamo a CambiarContraseniaFragment*/
        if (usuarioHelper.sesionIniciada()) {
            binding.etPasswordActual.hint = resources.getString(R.string.contrasenia_anterior_text)
            binding.etPasswordActual.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            binding.tvProvisoria.alpha = AppConstants.SPLASH_ALPHA_0
        } else {
            binding.etPasswordActual.hint = resources.getString(R.string.contrasenia_provisoria_text)
        }

        cambiarContraseniaViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(CambiarContraseniaViewModel::class.java)
        binding.viewModel = cambiarContraseniaViewModel
        val estaAutenticado = getUser()
        binding.viewModel!!.setUser(estaAutenticado)
        binding.viewModel!!.esAutenticado(usuarioHelper.sesionIniciada())

        binding.btnAceptar.setOnClickListener {
            if (validarContrasenia()) {
                cambiarContraseniaViewModel.cambiarContraseniaUser()
                        .observe(this, Observer<EstadoRespuesta<RespuestaModel<Any>>> { this.handleResponse(it) })
            }
        }
        iniciarValidaciones()
    }

    private fun iniciarValidaciones() {
        validadorContrasenia = EditTextValidator(binding.tiPassword)
        validadorRepetirContrasenia = EditTextValidator(binding.tiRepeatPassword)
        validadorContraseniaActual = EditTextValidator(binding.tiPasswordActual)
    }

    private fun validarContrasenia(): Boolean {
        val valContraseniaActual: Boolean = validadorContraseniaActual.esContrasenia()
        val valEsContrasenia: Boolean = validadorContrasenia.esContrasenia()
        val valRepetirContrasenia: Boolean = validadorRepetirContrasenia.esContrasenia()
        val valContraseniaNueva: Boolean = validadorRepetirContrasenia.esIgualA(binding.etPassword)
        return valContraseniaActual && valContraseniaNueva && valEsContrasenia && valRepetirContrasenia
    }

    private fun handleResponse(respuesta: EstadoRespuesta<RespuestaModel<Any>>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
                    baseActivity.ocultarProgressBar()
                    SnackbarHelper(view!!, respuesta.data!!.notificacion).show()
                }
                Estado.CARGANDO -> {
                    baseActivity.mostrarProgressBar()
                }
                Estado.EXITO -> {
                    baseActivity.ocultarProgressBar()
                    val datos: RespuestaModel<Any>? = respuesta.data

                    SnackbarHelper(view!!, datos!!.notificacion).show()

                    if (usuarioHelper.sesionIniciada()) {
                        navigationController.navegarDashboardFragment()
                    } else {
                        navigationController.navegarLoginFragment()
                    }
                }
            }
        }
    }

    private fun getUser(): String {
        return arguments?.getString(BUNDLE_USUARIO)!!
    }
}