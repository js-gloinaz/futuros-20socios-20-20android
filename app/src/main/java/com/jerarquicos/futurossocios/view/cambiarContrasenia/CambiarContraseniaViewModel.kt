package com.jerarquicos.futurossocios.view.cambiarContrasenia

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.cuenta.CambiarContraseniaModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class CambiarContraseniaViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository
) : ViewModel() {
    var cambiarContraseniaModel: MutableLiveData<CambiarContraseniaModel> = MutableLiveData()

    init {
        cambiarContraseniaModel.value = CambiarContraseniaModel()
    }

    fun cambiarContraseniaUser(): LiveData<EstadoRespuesta<RespuestaModel<Any>>> {
        return futurosSociosRepository.cambiarContrasenia(cambiarContraseniaModel.value!!)
    }

    fun setUser(userName: String) {
        cambiarContraseniaModel.value?.userName = userName
    }

    fun esAutenticado(estaAutenticado: Boolean) {
        cambiarContraseniaModel.value?.autenticado = estaAutenticado
    }
}