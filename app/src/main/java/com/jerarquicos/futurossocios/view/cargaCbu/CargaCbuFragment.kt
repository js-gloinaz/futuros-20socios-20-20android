package com.jerarquicos.futurossocios.view.cargaCbu

import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.CargaCbuFragmentBinding
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.FormaPago
import com.jerarquicos.futurossocios.db.entity.TipoCuentaBancaria
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import java.util.*
import javax.inject.Inject

class CargaCbuFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    lateinit var binding: CargaCbuFragmentBinding
    private lateinit var cargaCbuViewModel: CargaCbuViewModel
    private lateinit var validadorCbu: EditTextValidator

    companion object {
        private const val BUNDLE_CUENTA = "BUNDLE_CUENTA"

        fun newInstance(formaPago: FormaPago?): CargaCbuFragment {
            val fragment = CargaCbuFragment()
            val args = Bundle()
            if (formaPago != null) {
                args.putSerializable(BUNDLE_CUENTA, formaPago)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.carga_cbu_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        cargaCbuViewModel = ViewModelProviders.of(this, viewModelFactory).get(CargaCbuViewModel::class.java)
        binding.viewModel = cargaCbuViewModel

        val formaDePago = arguments!!.getSerializable(BUNDLE_CUENTA) as FormaPago
        cargaCbuViewModel.cargaCbuModel.value = formaDePago

        if (formaDePago.tipoCuentaBancaria?.descripcion != null) {
            cargaCbuViewModel.cargarTipoDeCuenta(formaDePago.tipoCuentaBancaria!!)
        }

        binding.btAccion.setOnClickListener {
            if (validarCbu()) {
                val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteCargaCbuFragment(
                        cargaCbuViewModel.cargaCbuModel.value!!
                )
                navigationController.navegarSiguienteFragment(fragment)
            }
        }

        binding.spTipoDeCuenta.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                cargaCbuViewModel.guardarTipoDeCuentaSeleccionada(p2)
            }
        }

        cargarSpinnerTipoDeCuenta()
        iniciarValidaciones()
    }

    private fun cargarSpinnerTipoDeCuenta() {
        cargaCbuViewModel.obtenerTiposDeCuenta()
                .observe(this, Observer<EstadoRespuesta<DatosIniciales>> { this.handleResponseTipoDeCuenta(it) })
    }

    private fun iniciarValidaciones() {
        validadorCbu = EditTextValidator(binding.tiCbu)
    }

    private fun validarCbu(): Boolean {
        return validadorCbu.cbuValido()
    }

    private fun cargarSpTipoDeCuenta(tipoCuenta: List<TipoCuentaBancaria>) {
        val arrTipoCuenta: ArrayList<String> = ArrayList()

        for (i in tipoCuenta.indices) {
            var descripcion = ""
            descripcion += tipoCuenta[i].descripcion
            arrTipoCuenta.add(descripcion)
        }

        val adapter = ArrayAdapter<String>(
                context!!,
                R.layout.spinner_item_seleccionado_layout,
                arrTipoCuenta
        )

        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spTipoDeCuenta.adapter = adapter
        binding.spTipoDeCuenta.visibility = View.VISIBLE

    }

    private fun handleResponseTipoDeCuenta(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpTipoDeCuenta(respuesta.data.tipoCuentaBancaria)
                        cargaCbuViewModel.guardarTipoDeCuenta(respuesta.data.tipoCuentaBancaria)
                        binding.spTipoDeCuenta.setSelection(cargaCbuViewModel.obtenerPosicionTipoDeCuentaSeleccionada())

                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_oscuro, menu)
    }
}