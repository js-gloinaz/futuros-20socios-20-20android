package com.jerarquicos.futurossocios.view.cargaCbu

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.FormaPago
import com.jerarquicos.futurossocios.db.entity.TipoCuentaBancaria
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class CargaCbuViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository
) : ViewModel() {

    var cargaCbuModel: MutableLiveData<FormaPago> = MutableLiveData()
    private var tipoCuentaBancaria: MutableLiveData<List<TipoCuentaBancaria>> = MutableLiveData()

    fun obtenerTiposDeCuenta(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun obtenerExpediente(): Expediente {
        return futurosSociosRepository.obtenerExpediente()
    }

    fun guardarTipoDeCuenta(cuenta: List<TipoCuentaBancaria>) {
        this.tipoCuentaBancaria.value = cuenta
    }

    fun cargarTipoDeCuenta(tipoCuentaBancaria: TipoCuentaBancaria) {
        cargaCbuModel.value?.tipoCuentaBancaria = tipoCuentaBancaria
    }

    fun guardarTipoDeCuentaSeleccionada(posicion: Int) {
        cargaCbuModel.value?.tipoCuentaBancaria = tipoCuentaBancaria.value!![posicion]
    }

    fun obtenerPosicionTipoDeCuentaSeleccionada(): Int {
        var index = 0
        if (cargaCbuModel.value != null) {
            this.tipoCuentaBancaria.value!!.forEachIndexed { i, element ->
                if (element.id == cargaCbuModel.value?.tipoCuentaBancaria?.id) {
                    index = i
                }
            }
        }
        return index
    }
}