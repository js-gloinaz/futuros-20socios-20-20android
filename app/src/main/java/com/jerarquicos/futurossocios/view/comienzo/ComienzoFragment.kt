package com.jerarquicos.futurossocios.view.comienzo

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ComienzoFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.DeviceUtils
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class ComienzoFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	@Inject
	lateinit var usuarioHelper: UsuarioHelper
	lateinit var binding: ComienzoFragmentBinding
	lateinit var comienzoViewModel: ComienzoViewModel
	
	companion object {
		private const val BUNDLE_TIPO_SECCION = "BUNDLE_TIPO_SECCION"
		private const val BUNDLE_TITULO = "BUNDLE_TITULO"
		private const val BUNDLE_DESCRIPCION = "BUNDLE_DESCRIPCION"
		private const val BUNDLE_ICONO = "BUNDLE_ICONO"
		
		fun newInstance(tipoSeccion: Int, titulo: Int, descripcion: Int, icono: Int): ComienzoFragment {
			val args = Bundle()
			args.putInt(BUNDLE_TIPO_SECCION, tipoSeccion)
			args.putInt(BUNDLE_TITULO, titulo)
			args.putInt(BUNDLE_DESCRIPCION, descripcion)
			args.putInt(BUNDLE_ICONO, icono)
			val fragment = ComienzoFragment()
			fragment.arguments = args
			return fragment
		}
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.comienzo_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		inicializar(binding.toolbar, javaClass.name, true)
		
		comienzoViewModel = ViewModelProviders.of(this, viewModelFactory).get(ComienzoViewModel::class.java)
		binding.viewModel = comienzoViewModel
		
		obtenerParametros()
		
		binding.ivSiguiente.setOnClickListener {
			val nuevoFragment = baseActivity.dealerViewModel
					.obtenerPasoSiguienteComienzoFragment(comienzoViewModel.tipoSeccion.value!!)
			navigationController.navegarSiguienteFragment(nuevoFragment)
		}
		
		binding.btComenzar.setOnClickListener {
			val nuevoFragment = baseActivity.dealerViewModel
					.obtenerPasoSiguienteComienzoFragment(comienzoViewModel.tipoSeccion.value!!)
			navigationController.navegarSiguienteFragment(nuevoFragment)
		}
	}
	
	private fun obtenerParametros() {
		comienzoViewModel.tipoSeccion.value = arguments!!.getInt(BUNDLE_TIPO_SECCION)
		comienzoViewModel.titulo.value = getString(arguments!!.getInt(BUNDLE_TITULO))
		comienzoViewModel.descripcion.value = getString(arguments!!.getInt(BUNDLE_DESCRIPCION))
		binding.ivIcono.setImageResource(arguments!!.getInt(BUNDLE_ICONO))
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_base, menu)
	}
}