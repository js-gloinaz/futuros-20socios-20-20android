package com.jerarquicos.futurossocios.view.comienzo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class ComienzoViewModel @Inject constructor() : ViewModel() {
    var tipoSeccion: MutableLiveData<Int> = MutableLiveData()
    var titulo: MutableLiveData<String> = MutableLiveData()
    var descripcion: MutableLiveData<String> = MutableLiveData()
}