package com.jerarquicos.futurossocios.view.comienzoSeccion

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ComienzoSeccionFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class ComienzoSeccionFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    lateinit var binding: ComienzoSeccionFragmentBinding
    private lateinit var comienzoSeccionViewModel: ComienzoSeccionViewModel

    companion object {
        private const val BUNDLE_TIPO_SECCION = "BUNDLE_TIPO_SECCION"
        private const val BUNDLE_TITULO = "BUNDLE_TITULO"
        private const val BUNDLE_TITULO_SECCION = "BUNDLE_TITULO_SECCION"
        private const val BUNDLE_DESCRIPCION = "BUNDLE_DESCRIPCION"

        fun newInstance(tipoSeccion: Int, titulo: Int, tituloSeccion: Int, descripcion: Int): ComienzoSeccionFragment {
            val args = Bundle()
            args.putInt(BUNDLE_TIPO_SECCION, tipoSeccion)
            args.putInt(BUNDLE_TITULO, titulo)
            args.putInt(BUNDLE_TITULO_SECCION, tituloSeccion)
            args.putInt(BUNDLE_DESCRIPCION, descripcion)

            val fragment = ComienzoSeccionFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.comienzo_seccion_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        comienzoSeccionViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ComienzoSeccionViewModel::class.java)

        binding.viewModel = comienzoSeccionViewModel

        obtenerParametros()

        binding.btComenzar.setOnClickListener {
            val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteComienzoSeccionFragment(
                    comienzoSeccionViewModel.tipoSeccion.value!!
            )
            navigationController.navegarSiguienteFragment(fragment)

        }
    }

    private fun obtenerParametros() {
        comienzoSeccionViewModel.tipoSeccion.value = arguments!!.getInt(BUNDLE_TIPO_SECCION)
        comienzoSeccionViewModel.titulo.value = getString(arguments!!.getInt(BUNDLE_TITULO))
        comienzoSeccionViewModel.tituloSeccion.value = getString(arguments!!.getInt(BUNDLE_TITULO_SECCION))
        comienzoSeccionViewModel.descripcion.value = getString(arguments!!.getInt(BUNDLE_DESCRIPCION))
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }
}