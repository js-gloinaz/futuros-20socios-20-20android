package com.jerarquicos.futurossocios.view.comienzoSeccion

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class ComienzoSeccionViewModel @Inject constructor() : ViewModel() {
    var tipoSeccion: MutableLiveData<Int> = MutableLiveData()
    var titulo: MutableLiveData<String> = MutableLiveData()
    var tituloSeccion: MutableLiveData<String> = MutableLiveData()
    var descripcion: MutableLiveData<String> = MutableLiveData()
}