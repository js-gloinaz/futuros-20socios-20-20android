package com.jerarquicos.futurossocios.view.common

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.Archivo.Documento
import com.jerarquicos.futurossocios.db.entity.Domicilio
import com.jerarquicos.futurossocios.utils.AppConstants.REQUEST_CODE
import com.jerarquicos.futurossocios.utils.AppConstants.VISUALIZAR_IMAGEN
import com.jerarquicos.futurossocios.utils.enums.TipoDocumentacionAnexaEnum
import com.jerarquicos.futurossocios.utils.enums.TipoSeccionEnum
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.cambiarContrasenia.CambiarContraseniaFragment
import com.jerarquicos.futurossocios.view.comienzo.ComienzoFragment
import com.jerarquicos.futurossocios.view.comienzoSeccion.ComienzoSeccionFragment
import com.jerarquicos.futurossocios.view.dashboard.DashboardFragment
import com.jerarquicos.futurossocios.view.domicilio.DomicilioFragment
import com.jerarquicos.futurossocios.view.gestorDocumentos.gestorDocumentos.GestorDocumentosFragment
import com.jerarquicos.futurossocios.view.gestorDocumentos.visualizadorImagen.VisualizadorImagenFragment
import com.jerarquicos.futurossocios.view.inicio.InicioFragment
import com.jerarquicos.futurossocios.view.listaResultado.ListaResultadoFragment
import com.jerarquicos.futurossocios.view.login.LoginFragment
import com.jerarquicos.futurossocios.view.models.DocumentacionAnexaModel
import com.jerarquicos.futurossocios.view.models.OpcionModel
import com.jerarquicos.futurossocios.view.notificacionFcm.NotificacionFcmFragment
import com.jerarquicos.futurossocios.view.registro.RegistroFragment
import com.jerarquicos.futurossocios.view.restablecerContrasenia.RestablecerContraseniaFragment
import com.jerarquicos.futurossocios.view.splash.SplashFragment
import javax.inject.Inject


class NavigationController @Inject constructor(baseActivity: BaseActivity) {
    private val containerId: Int = R.id.container
    private val fragmentManager: FragmentManager = baseActivity.supportFragmentManager

    fun navegarLoginFragment() {
        val fragment = LoginFragment()
        limpiarPila()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarInicioFragment() {
        val fragment = InicioFragment()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .commitAllowingStateLoss()
    }


    fun navegarCambiarContraseniaFragment(userName: String, variable: Int = 0) {
        val fragment = CambiarContraseniaFragment.newInstance(userName)
        if (variable == 0) {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .setReorderingAllowed(true)
                    .replace(containerId, fragment)
                    .addToBackStack(null)
                    .commitAllowingStateLoss()
        } else {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                    .setReorderingAllowed(true)
                    .replace(containerId, fragment)
                    .commitAllowingStateLoss()
        }
    }

    fun navegarRestablecerContraseniaFragment() {
        val fragment = RestablecerContraseniaFragment()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarSplashFragment() {
        val fragment = SplashFragment()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .add(containerId, fragment)
                .commitAllowingStateLoss()
    }

    fun navegarRegistroFragment() {
        val fragment = RegistroFragment()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }



    fun navegarDashboardFragment() {
        val fragment = DashboardFragment()
        limpiarPila()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .commitAllowingStateLoss()
    }

    private fun limpiarPila() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun navegarVisualizadorImagenFragment(context: GestorDocumentosFragment, documento: Documento) {
        val fragment = VisualizadorImagenFragment.newInstance(documento)
        fragment.setTargetFragment(context, VISUALIZAR_IMAGEN)
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarDomicilioFragment(context: Fragment, domicilio: Domicilio?) {
        val fragment = DomicilioFragment.newInstance(domicilio)
        fragment.setTargetFragment(context, REQUEST_CODE)
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarListaResultadoFragment(context: DomicilioFragment, opciones: ArrayList<OpcionModel>) {
        val fragment = ListaResultadoFragment.newInstance(opciones)
        fragment.setTargetFragment(context, REQUEST_CODE)
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarSiguienteFragment(fragment: Fragment) {

        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(containerId, fragment)
                .setReorderingAllowed(true)
                .addToBackStack(fragment.javaClass.simpleName)
                .commitAllowingStateLoss()
    }

    fun navegarGestorDocumentosFragment(
            mCantidadMinima: Int,
            mCantidadMaxima: Int,
            mDocumentacionAnexa: ArrayList<DocumentacionAnexaModel>,
            mTipoDocumentacion: TipoDocumentacionAnexaEnum,
            propietario: Int,
            ordenPersona: Int
    ) {
        val fragment = GestorDocumentosFragment.newInstance(
                mCantidadMinima,
                mCantidadMaxima,
                mDocumentacionAnexa,
                mTipoDocumentacion,
                propietario,
                ordenPersona
        )
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarPrincipalFragment() {
        val fragment = ComienzoFragment.newInstance(TipoSeccionEnum.DatosPersonales.valor,
                R.string.datos_personales_title,
                R.string.datos_personales_titular_text,
                R.drawable.ic_credencial_virtual
        )
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarComienzoFormaDePagoFragment() {
        val fragment = ComienzoFragment.newInstance(TipoSeccionEnum.FormaPago.valor,
                R.string.plan_forma_pago_title,
                R.string.descripcion_forma_pago_text,
                R.drawable.ic_forma_pago
        )
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarComienzoDeclaracionJuradaFragment() {
        val fragment = ComienzoFragment.newInstance(TipoSeccionEnum.DeclaracionJurada.valor,
                R.string.declaracion_jurada_title,
                R.string.descripcion_declaracion_jurada_text,
                R.drawable.ic_declaracion_jurada
        )
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }

    fun navegarEliminarPersonaCargo() {
        fragmentManager.popBackStack(ComienzoSeccionFragment().javaClass.simpleName, 0)
    }

    fun navegarEliminarConviviente() {
        fragmentManager.popBackStack(ComienzoSeccionFragment().javaClass.simpleName, 0)
    }

    fun navegarNotificacionFcmFragment() {
        val fragment = NotificacionFcmFragment()
        fragmentManager.beginTransaction()
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
                .setReorderingAllowed(true)
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
    }


}
