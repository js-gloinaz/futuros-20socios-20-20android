package com.jerarquicos.futurossocios.view.common.utils


import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import com.jerarquicos.futurossocios.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {
    private var anio: Int = 0
    private var mes: Int = 0
    private var dia: Int = 0
    private var date = Date()

    var etFechaNac: EditText? = null

    fun setView(vista: EditText) {
        //Ingreso desde Fecha de Nacimiento
        etFechaNac = vista
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        //Se levanta el date picker
        return DatePickerDialog(
                context!!,
                R.style.AppCompatAlertDialogStyle,
                this,
                GregorianCalendar.getInstance().get(GregorianCalendar.YEAR),
                GregorianCalendar.getInstance().get(GregorianCalendar.MONTH),
                GregorianCalendar.getInstance().get(GregorianCalendar.DAY_OF_MONTH))

    }

    override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        //se selecciono una fecha del date_picker
        anio = year
        mes = monthOfYear + 1
        dia = dayOfMonth

        if (etFechaNac != null) {
            //se llama al get para cargar la fechaString
            etFechaNac!!.setText(fechaString)
        }

        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            date = format.parse(fechaString)
            System.out.println(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }


    }

    private val fechaString: String
        get() {
            val armarFecha = StringBuilder()
            if (dia < 10) {
                armarFecha.append("0")
            }
            armarFecha.append(dia)
            armarFecha.append("/")
            if (mes < 10) {
                armarFecha.append("0")
            }
            armarFecha.append(mes)
            armarFecha.append("/")
            armarFecha.append(anio)

            return armarFecha.toString()
        }

}