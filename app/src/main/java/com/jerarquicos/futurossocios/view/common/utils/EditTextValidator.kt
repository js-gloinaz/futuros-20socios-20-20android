package com.jerarquicos.futurossocios.view.common.utils

import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.jerarquicos.futurossocios.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by fgagneten on 22/10/2018.
 * Clase creada para concentrar las validaciones comunes.
 */
class EditTextValidator(private val vistaAValidar: TextInputLayout?) {
	private var cadena: String? = null
	private var esValido: Boolean = false
	
	init {
		esValido = true
	}
	
	companion object {
		private const val EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
				"[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
		private const val NOMBRE_PATTERN = "^[a-zA-ZÁÉÍÓÚÑáéíóúñ \\s]+'?([a-zA-ZÁÉÍÓÚÑáéíóúñ \\s]+)+\$"
		private const val NOMBRE_EMPLEADOR_PATTERN = "^[A-Za-zÁÉÍÓÚÑáéíóúñ0-9 \\s]+'?([A-Za-zÁÉÍÓÚÑáéíóúñ0-9 \\s]+)+\$"
		private const val TELEFONOCELULAR = "^[0-9]{6,}$"
		private const val TELEFONOPARTIICULAR = "^[0-9]{5,}$"
		private const val TARJETADECREDITO = "^[0-9 \\s]{19}$"
	}
	
	fun esNoVacio(): Boolean {
		setCadena()
		esValido = cadena!!.isNotEmpty()
		if (!esValido)
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		else
			vistaAValidar!!.error = null
		return esValido
	}
	
	fun esEmailOVacio(): Boolean {
		setCadena()
		esValido = cadena!!.matches(EMAIL_PATTERN.toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.email_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esEmailUOmitir(): Boolean {
		setCadena()
		esValido = cadena!!.matches(EMAIL_PATTERN.toRegex())
		if (cadena!!.isEmpty()) {
			esValido = true
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.email_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esContrasenia(): Boolean {
		setCadena()
		esValido = cadena!!.matches("^.{8,12}".toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.contrasenia_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esNumeroDocumento(): Boolean {
		setCadena()
		esValido = cadena!!.matches("^\\d{6,10}".toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.dni_error)
			else
				vistaAValidar!!.error = null
		}
		
		return esValido
	}
	
	//Para fechas menores y mayores de edad
	fun esFechaNacimientoValida(mayorEdad: Boolean): Boolean {
		setCadena()
		val format = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
		var fecha: Date? = null
		var fechaActual: Date? = null
		try {
			fecha = format.parse(cadena)
			fechaActual = format.parse(cadena)
		} catch (ignore: Exception) {
			//Exception ignored
		}
		when {
			cadena!!.isEmpty() -> {
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
				return false
			}
			!(cadena!!.matches("^(0?[1-9]|[12][0-9]|3[01])[/](0?[1-9]|1[012])[/](19|20)\\d{2}$".toRegex())) -> {
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.fecha_invalida_error)
				return false
			}
			!fechaActual!!.before(Calendar.getInstance().time) -> {
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.fecha_futura_error)
				return false
			}
			mayorEdad -> {
				if (!diferenciaEdad(fecha!!)) {
					vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.mayor_edad_error)
					return false
				}
				else {
					vistaAValidar!!.error = null
					return true
				}
			}
			!mayorEdad -> {
				vistaAValidar!!.error = null
				return true
			}
		}
		return false
	}
	
	private fun diferenciaEdad(fechaNacimiento: Date): Boolean {
		val formatter = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
		val fecNac = Integer.parseInt(formatter.format(fechaNacimiento))
		val actual = Integer.parseInt(formatter.format(Calendar.getInstance().time))
		return (actual - fecNac) / 10000 >= 18
	}
	
	fun esIgualA(textBox: EditText): Boolean {
		var cadenaAComparar = ""
		if (textBox.text != null)
			cadenaAComparar = textBox.text.toString().trim { it <= ' ' }
		setCadena()
		esValido = cadena == cadenaAComparar
		if (!esValido)
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.contrasenias_distintas_error)
		else
			vistaAValidar!!.error = null
		return esValido
	}
	
	fun domiciliosIguales(textBox: EditText): Boolean {
		var cadenaAComparar = ""
		if (textBox.text != null)
			cadenaAComparar = textBox.text.toString().trim { it <= ' ' }
		setCadena()
		esValido = cadena?.toUpperCase() == cadenaAComparar.toUpperCase()
		
		return esValido
	}
	
	private fun setCadena() {
		cadena = ""
		if (this.vistaAValidar != null)
			cadena = this.vistaAValidar.editText!!.text.toString().trim { it <= ' ' }
	}
	
	fun cbuValido(): Boolean {
		setCadena()
		if (cadena!!.length < 22) {
			vistaAValidar!!.error = vistaAValidar.context!!.resources!!.getString(R.string.cbu_error)
			return false
		}
		
		try {
			val ponderador = "97139713971397139713971397139713"
			var i = 0
			var nDigito: Int
			var nPond: Int
			val bloque1: String = '0' + cadena!!.substring(0, 7)
			val bloque2: String = "000" + cadena!!.substring(8, 8 + 13)
			var nTotal = 0.0
			
			while (i <= 7) {
				nDigito = Integer.parseInt(bloque1.substring(i, i + 1))
				nPond = Integer.parseInt(ponderador.substring(i, i + 1))
				nTotal = nTotal + nPond * nDigito - Math.floor((nPond * nDigito / 10).toDouble()) * 10
				i++
			}
			
			i = 0
			while (Math.floor((nTotal + i) / 10) * 10 != nTotal + i) {
				i += 1
			}
			// i es digito verificador
			if (Integer.parseInt(cadena!!.substring(7, 8)) != i) {
				vistaAValidar!!.error = vistaAValidar.context!!.resources!!.getString(R.string.cbu_invalido_error)
				return false
			}
			
			nTotal = 0.0
			i = 0
			while (i <= 15) {
				nDigito = Integer.parseInt(bloque2.substring(i, i + 1))
				nPond = Integer.parseInt(ponderador.substring(i, i + 1))
				nTotal = nTotal + nPond * nDigito - Math.floor((nPond * nDigito / 10).toDouble()) * 10
				i++
			}
			
			i = 0
			while (Math.floor((nTotal + i) / 10) * 10 != nTotal + i) {
				i += 1
			}
			// i es digito verificador
			if (Integer.parseInt(cadena!!.substring(21, 22)) != i) {
				vistaAValidar!!.error = vistaAValidar.context!!.resources!!.getString(R.string.cbu_invalido_error)
				return false
			}
			vistaAValidar!!.error = ""
			return true
		} catch (ex: Exception) {
			vistaAValidar!!.error = ""
			return true
		}
	}
	
	fun esNombreValido(): Boolean {
		setCadena()
		esValido = cadena!!.matches(NOMBRE_PATTERN.toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.caracteres_invalidos_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esNombreEmpleadorValido(): Boolean {
		setCadena()
		esValido = cadena!!.matches(NOMBRE_EMPLEADOR_PATTERN.toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.caracteres_invalidos_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esTelefonoCelular(): Boolean {
		setCadena()
		esValido = cadena!!.matches(TELEFONOCELULAR.toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.telefono_celular_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esCodInternacionalObligatorio(): Boolean {
		setCadena()
		esValido = cadena!!.matches("^\\d{2,5}".toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.codigo_internacional_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esCodInternacionalNoObligatorio(cadena2: String, cadena3: String): Boolean {
		setCadena()
		esValido = cadena!!.matches("^\\d{2,5}".toRegex())
		if (cadena!!.isEmpty() && cadena2.isEmpty() && cadena3.isEmpty()) {
			esValido = true
			vistaAValidar!!.error = null
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.codigo_internacional_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esCodAreaNumeroNoObligatorio(cadena2: String): Boolean {
		setCadena()
		esValido = cadena!!.matches("^\\d{2,5}".toRegex())
		if (cadena!!.isEmpty() && cadena2.isEmpty()) {
			esValido = true
			vistaAValidar!!.error = null
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.codigo_area_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esNumeroAreaNoObligatorioParticular(cadena2: String): Boolean {
		setCadena()
		esValido = cadena!!.matches(TELEFONOPARTIICULAR.toRegex())
		if (cadena!!.isEmpty() && cadena2.isEmpty()) {
			esValido = true
			vistaAValidar!!.error = null
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.telefono_particular_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esNumeroAreaNoObligatorioCelular(cadena2: String): Boolean {
		setCadena()
		esValido = cadena!!.matches(TELEFONOPARTIICULAR.toRegex())
		if (cadena!!.isEmpty() && cadena2.isEmpty()) {
			esValido = true
			vistaAValidar!!.error = null
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.telefono_celular_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esCodAreaObligatorio(): Boolean {
		setCadena()
		esValido = cadena!!.matches("^\\d{2,5}".toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.codigo_area_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun esNumeroDeTarjetaValido(): Boolean {
		setCadena()
		esValido = cadena!!.matches(TARJETADECREDITO.toRegex())
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
		}
		else {
			if (!esValido)
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.numero_tarjeta_error)
			else
				vistaAValidar!!.error = null
		}
		return esValido
	}
	
	fun validarTelefonosIguales(codAreaParticular: String, codAreaCelular: String, numeroParticular: String): Boolean {
		setCadena()
		esValido = false
		if (codAreaParticular == codAreaCelular) {
			if (cadena!! == numeroParticular) {
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.telefonos_iguales_error)
				esValido = false
			}
			else {
				esValido = true
			}
		}
		else {
			esValido = true
		}
		return esValido
	}
	
	fun esVacio(): Boolean {
		setCadena()
		if (cadena!!.isEmpty()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.campo_vacio_error)
			return false
		}
		vistaAValidar!!.error = null
		return true
	}
	
	fun esCuilValido(dni: String): Boolean {
		setCadena()
		esValido = cadena!!.matches("^\\d{11}".toRegex())
		if (cadena!!.isEmpty())
			return true
		if (esValido) {
			val prefijo = (cadena!![0].toInt() - 48) * 10 + (cadena!![1].toInt() - 48)
			
			if (prefijo == 20 || prefijo == 23 || prefijo == 27 || prefijo == 30) {
				if(!cuilCorrespondeDNI(dni)) return false
				checkSumatoriaCuil()
			}
			else {
				vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.cuil_error)
				esValido = false
			}
		}
		else {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.cuil_error)
		}
		return esValido
	}
	
	private fun cuilCorrespondeDNI(dni: String): Boolean{
		if (dni.count() == 7 && cadena!!.substring(3,10).toInt() != dni.toInt()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.cuil_no_corresponde_dni_error)
			return false
		}
		if (cadena!!.substring(2,10).toInt() != dni.toInt()) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.cuil_no_corresponde_dni_error)
			return false
		}
		return true
	}
	
	private fun checkSumatoriaCuil(){
		val multiplicadores: IntArray = intArrayOf(5, 4, 3, 2, 7, 6, 5, 4, 3, 2)
		var sumatoria = 0
		
		for (i in 0 .. (multiplicadores.count() - 1))
			sumatoria += (cadena!![i].toInt() - 48) * multiplicadores[i]
		
		sumatoria = 11 - (sumatoria % 11)
		
		if (sumatoria == 11)
			sumatoria = 0
		
		if (sumatoria != (cadena!![10].toInt() - 48)) {
			vistaAValidar!!.error = vistaAValidar.context.resources.getString(R.string.cuil_error)
			esValido = false
		}
		else {
			vistaAValidar!!.error = null
		}
	}
}