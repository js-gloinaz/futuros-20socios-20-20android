package com.jerarquicos.futurossocios.view.common.utils

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonParser
import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.LoginModel
import com.jerarquicos.futurossocios.utils.SharedPreferencesConstants.JWT_TOKEN_MODEL
import com.jerarquicos.futurossocios.utils.SharedPreferencesConstants.LOGIN_MODEL
import com.securepreferences.SecurePreferences
import javax.inject.Inject

class SecurePreferencesHelper @Inject constructor(var securePreferences: SecurePreferences) {


    private var editor: SecurePreferences.Editor = securePreferences.edit()

    fun getString(key: String): String {
        return securePreferences.getString(key, null)
    }

    fun getInt(key: String): Int {
        return securePreferences.getInt(key, -1)
    }

    fun setJwtTokenModel(token: JwtTokenModel) {
        val gson = Gson()
        val json = gson.toJson(token)
        editor.putString(JWT_TOKEN_MODEL, json)
        editor.apply()
    }

    fun getJwtTokenModel(): JwtTokenModel? {
        val jsonGenerico: JsonElement
        val builder = GsonBuilder()
        val gson = builder.create()
        val parser = JsonParser()
        val json = securePreferences.getString(JWT_TOKEN_MODEL, null)
        if (json == null) {
            return null
        } else {
            jsonGenerico = parser.parse(json)
            return gson.fromJson(jsonGenerico, JwtTokenModel::class.java)
        }
    }

    fun setLoginModel(token: LoginModel) {
        val gson = Gson()
        val json = gson.toJson(token)
        editor.putString(LOGIN_MODEL, json)
        editor.apply()
    }

    fun getLoginModel(): LoginModel? {
        val jsonGenerico: JsonElement
        val builder = GsonBuilder()
        val gson = builder.create()
        val parser = JsonParser()
        val json = securePreferences.getString(LOGIN_MODEL, null)
        if (json == null) {
            return null
        } else {
            jsonGenerico = parser.parse(json)
            return gson.fromJson(jsonGenerico, LoginModel::class.java)
        }
    }

    fun setString(key: String, valor: String) {
        editor.putString(key, valor)
        editor.apply()
    }

    fun removeAll() {
        editor.clear()
        editor.apply()
    }
}