package com.jerarquicos.futurossocios.view.common.utils

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion

/**Clase creada para concentrar los métodos que gestionan un snackbar
 *
 * PARÁMETROS:
 *      notificación <NotificacionModel>
 *      vista <View>
 *
 *  MÉTODOS DISPONIBLES
 *      OBLIGATORIOS
 *          show()
 *      OPCIONALES
 *          setDuracion ( duracion <SnackbarEnum> )
 *          setMensajePersonalizado (mensaje <String>)
 *          setColores (fondo <Int>, letras <Int>)
 *          setAccion()     Se muestra la estructura de una acción que se pasa como parámetro.
 *              OBLIGATORIOS
 *                  tituloDeAccion <String>
 *                  accionListener <Snackbar.Callback>
 *              OPCIONALES
 *                  colorDeLetras <Int>
 *          cerrarSnackbar()
 */

class SnackbarHelper(private var vista: View, private var notificacion: NotificacionModel? = null) {

    private var colorFondo: Int? = null
    private var colorLetras: Int? = null
    private var mensaje: String? = null
    private var duracionDefault = SnackbarEnum.MEDIO
    private var snackbar: Snackbar? = null
    private var snackbarView: View? = null
    private var tvSnackbar: TextView? = null

    init {
        if (notificacion == null) {
            notificacion = NotificacionModel("", TipoNotificacion.Informacion)
        }
        this.mensaje = notificacion?.mensaje
        getColores()
        crearSnackbar()
    }

    private fun getColores() {
        when (notificacion?.tipoNotificacion) {
            TipoNotificacion.Exito -> {
                colorFondo = ContextCompat.getColor(vista.context, R.color.colorVerde)
                colorLetras = ContextCompat.getColor(vista.context, R.color.colorBlanco)
            }
            TipoNotificacion.Error -> {
                colorFondo = ContextCompat.getColor(vista.context, R.color.colorRed)
                colorLetras = ContextCompat.getColor(vista.context, R.color.colorBlanco)
            }
            TipoNotificacion.Informacion -> {
                colorFondo = ContextCompat.getColor(vista.context, R.color.colorAzulInstitucional)
                colorLetras = ContextCompat.getColor(vista.context, R.color.colorBlanco)
            }
            TipoNotificacion.Advertencia -> {
                colorFondo = ContextCompat.getColor(vista.context, R.color.colorNaranja)
                colorLetras = ContextCompat.getColor(vista.context, R.color.colorBlanco)
            }
        }
    }

    private fun crearSnackbar() {
        snackbar = Snackbar.make(vista, mensaje!!, duracionDefault.duracion)
        setearColores()
    }

    fun show() {
        snackbar!!.show()
    }

    fun cerrarSnackbar() {
        if (snackbar!!.isShown) {
            snackbar!!.dismiss()
        }
    }

    fun setMensajePersonalizado(mensajePersonalizado: String) {
        snackbar!!.setText(mensajePersonalizado)
    }

    fun setDuration(duracionPersonalizada: SnackbarEnum) {
        snackbar!!.duration = duracionPersonalizada.duracion
    }

    fun setAccion(tituloAccion: String, callback: Snackbar.Callback, colorLetrasAccion: Int = 0) {
        snackbar!!.setAction(tituloAccion) {}
        if (colorLetrasAccion == 0) {
            snackbar!!.setActionTextColor(ContextCompat.getColor(vista.context, R.color.colorBlanco))
        } else {
            snackbar!!.setActionTextColor(colorLetrasAccion)
        }
        snackbar!!.addCallback(callback)
    }

    fun mostrarSnackBar(mensaje: String, color: Int, duracion: SnackbarEnum = SnackbarEnum.CORTO) {
        setMensajePersonalizado(mensaje)
        setearColores(ContextCompat.getColor(vista.context, color))
        setDuration(duracion)
        show()
    }

    fun setearColores(colorFondoPersonalizado: Int = 0, colorLetrasPersonalizado: Int = 0) {
        snackbarView = snackbar!!.view
        tvSnackbar = snackbarView!!.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        tvSnackbar!!.maxLines = 10

        //Si viene un color personalizado se setea, si no se utiliza el color de acuerdo al tipo de notificacion
        if (colorFondoPersonalizado == 0) {
            snackbarView!!.setBackgroundColor(colorFondo!!)
        } else {
            snackbarView!!.setBackgroundColor(colorFondoPersonalizado)
        }
        if (colorLetrasPersonalizado == 0) {
            tvSnackbar!!.setTextColor(colorLetras!!)
        } else {
            tvSnackbar!!.setTextColor(colorLetrasPersonalizado)
        }
    }
    /*La estructura de una acción que se debe pasar por parámetro desde donde se invoca es la siguiente*/
    /*fun nombreAccion() : Snackbar.Callback{
        return object : Snackbar.Callback() {
            override fun onDismissed(snackbar: Snackbar?, event: Int) {
                /*Esto se ejecuta cuando se cierra el Snackbar
                sin importar el tipo de evento que ocurre */
                Log.d("INFO", "Entré al onDismiss")
                when(event){
                    DISMISS_EVENT_ACTION->{
                        Log.d("INFO", "El evento es: DISMISS_EVENT_ACTION")
                        /*Esto se ejecuta cuando se en el Snackbar
                        se presiona el botón de la acción agregada */
                    }
                    DISMISS_EVENT_CONSECUTIVE->{
                        Log.d("INFO", "El evento es: DISMISS_EVENT_CONSECUTIVE")
                        /*Esto se ejecuta cuando se cierra el Snackbar
                        debido a que un nuevo snackbar se muestra */
                    }
                    DISMISS_EVENT_SWIPE->{
                        Log.d("INFO", "El evento es: DISMISS_EVENT_SWIPE")
                        /*Esto se ejecuta cuando se cierra el Snackbar
                        deslizando el dedo hacia algún lado */
                    }
                    DISMISS_EVENT_TIMEOUT->{
                        Log.d("INFO", "El evento es: DISMISS_EVENT_TIMEOUT")
                        /*Esto se ejecuta cuando se cierra el Snackbar
                        debido a que caduca el tiempo de duración*/
                    }
                }
            }
        }
    }*/
}