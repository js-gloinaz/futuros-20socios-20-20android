package com.jerarquicos.futurossocios.view.common.utils

import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.LoginModel
import javax.inject.Inject

class UsuarioHelper @Inject constructor() {

    @Inject
    lateinit var securePreferencesHelper: SecurePreferencesHelper

    fun guardarToken(token: JwtTokenModel?) {
        securePreferencesHelper.setJwtTokenModel(token!!)
    }

    fun guardarLogin(login: LoginModel?) {
        securePreferencesHelper.setLoginModel(login!!)
    }

    fun getToken(): JwtTokenModel? {
        return securePreferencesHelper.getJwtTokenModel()
    }

    fun cerrarSesion() {
        return securePreferencesHelper.removeAll()
    }

    fun sesionIniciada(): Boolean {
        return securePreferencesHelper.getJwtTokenModel() != null
    }

    fun getUsuario(): String? {
        val token = securePreferencesHelper.getJwtTokenModel()
        if (token == null) {
            return null
        } else {
            token.usuario
        }
        return token.usuario
    }

    fun getNombre(): String? {
        val token = securePreferencesHelper.getJwtTokenModel()
        if (token == null) {
            return null
        } else {
            token.nombre
        }
        return token.nombre
    }

    fun getApellido(): String? {
        val token = securePreferencesHelper.getJwtTokenModel()
        if (token == null) {
            return null
        } else {
            token.apellido
        }
        return token.apellido
    }
}