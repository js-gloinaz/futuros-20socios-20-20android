package com.jerarquicos.futurossocios.view.common.utils.documentos

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Log
import java.io.*

class DocumentosHelper {

    private val NOMBRE_ALBUM = "Jerarquicos"
    private var NEW_REQUIRED_SIZE = 3072
    private var mIAlbumStorageDirFactory: IAlbumStorageDirFactory? = null

    init {
        mIAlbumStorageDirFactory = FroyoIAlbumDirFactory()
    }

    fun writeToTempImageAndGetPathUri(context: Context, image: Bitmap): String? {
        val bytes = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 75, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver,
                image, "Title", null)
        val url = getRealPath(context, Uri.parse(path))
        return url
    }

    fun getImageUrlWithAuthority(context: Context, uri: Uri): String? {
        var iss: InputStream? = null
        if (uri.authority != null) {
            try {
                iss = context.contentResolver.openInputStream(uri)
                val bmp = BitmapFactory.decodeStream(iss)
                return writeToTempImageAndGetPathUri(context, bmp)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } finally {
                try {
                    iss?.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
        return null
    }

    fun getRealPath(context: Context, uri: Uri): String? {
		when {
			DocumentsContract.isDocumentUri(context, uri) -> {
				val docId = DocumentsContract.getDocumentId(uri)
				when(uri.authority){
					// ExternalStorageProvider
					"com.android.externalstorage.documents" -> {
						val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
						val type = split[0]
						if ("primary".equals(type, ignoreCase = true)) {
							return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
						}
					}
					//DownloadsProvider
					"com.android.providers.downloads.documents" -> {
						val contentUri = ContentUris.withAppendedId(
								Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(docId))
						return getDataColumn(context, contentUri, null, null)
					}
					//MediaProvider
					"com.android.providers.media.documents" -> {
						val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
						val type = split[0]
						var contentUri: Uri? = null
						contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
						val selection = "_id=?"
						val selectionArgs = arrayOf(split[1])
						return getDataColumn(context, contentUri, selection, selectionArgs)
					}
				}
			}
			"content".equals(uri.scheme!!, ignoreCase = true) -> {
				return if ("com.google.android.apps.photos.content" == uri.authority) uri.lastPathSegment
				else getDataColumn(context, uri, null, null)
			}
			"file".equals(uri.scheme!!, ignoreCase = true) -> return uri.path
		}
        return null
    }

    private fun getDataColumn(
            context: Context,
            uri: Uri?,
            selection: String?,
            selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    fun obtenerDirectorioAlbum(): File? {
        var storageDir: File? = null
        if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            storageDir = mIAlbumStorageDirFactory!!.getAlbumStorageDir(NOMBRE_ALBUM)
			if (storageDir != null && !storageDir.mkdirs() && !storageDir.exists()) {
				Log.d("CameraSample", "Falló al crear el directorio")
				return null
			}
        } else {
            Log.v("seamossocios", "Almacenamiento externo no agregado para LECTURA/ESCRITURA.")
        }
        return storageDir
    }

    @Throws(Exception::class)
    fun copyData(inn: InputStream, out: OutputStream) {
        val buffer = ByteArray(8 * 1024)
        var len = inn.read(buffer)
        while (len > 0) {
            out.write(buffer, 0, len)
            len = inn.read(buffer)
        }
    }

    fun stream2file(contexto: Context, inn: InputStream): File? {
        return try {
            val outputDir = contexto.getExternalFilesDir(
                    Environment.getExternalStorageDirectory().toString() + "/Documents"
            )
            val tempFile = File.createTempFile("jerarquicos", ".pdf", outputDir)
            tempFile.deleteOnExit()
            val out: FileOutputStream
            out = FileOutputStream(tempFile)
            copyData(inn, out)
            tempFile
        } catch (ignored: IOException) {
            null
        } catch (e: Exception) {
            null
        }
    }

    fun obtenerTamanio(path: String): Float {
        val file = File(path)
        return file.length() / 1024f / 1024f
    }

    fun obtenerBitmap(filePath: String?): Bitmap? {
        val opciones = BitmapFactory.Options()
        opciones.inJustDecodeBounds = true
        BitmapFactory.decodeFile(filePath, opciones)
        var widthTmp = opciones.outWidth
        var heightTmp = opciones.outHeight
        var scale = 1
        while (true) {
            if (widthTmp < NEW_REQUIRED_SIZE && heightTmp < NEW_REQUIRED_SIZE)
                break
            widthTmp /= 2
            heightTmp /= 2
            scale *= 2
        }
        val opcionesDos = BitmapFactory.Options()
        opcionesDos.inSampleSize = scale
        val bitmap2 = BitmapFactory.decodeFile(filePath, opcionesDos)
        var ei: ExifInterface?
        var bitmap: Bitmap? = null
        try {
            ei = ExifInterface(filePath)
            val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED)
            bitmap = when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(bitmap2, 90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(bitmap2, 180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(bitmap2, 270F)
                ExifInterface.ORIENTATION_NORMAL -> bitmap2
                else -> bitmap2
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bitmap
    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height, matrix, true)
    }
}