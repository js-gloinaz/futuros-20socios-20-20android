package com.jerarquicos.futurossocios.view.common.utils.documentos

import android.os.Environment
import java.io.File

/**
 * Created by fgagneten on 06/04/2017.
 */

class FroyoIAlbumDirFactory : IAlbumStorageDirFactory {

    override fun getAlbumStorageDir(albumName: String): File {
        return File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), albumName)
    }
}
