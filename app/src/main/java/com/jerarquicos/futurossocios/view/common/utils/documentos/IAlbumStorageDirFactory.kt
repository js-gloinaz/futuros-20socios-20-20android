package com.jerarquicos.futurossocios.view.common.utils.documentos

import java.io.File

/**
 * Created by fgagneten on 06/04/2017.
 */

internal interface IAlbumStorageDirFactory {
    fun getAlbumStorageDir(albumName: String): File
}