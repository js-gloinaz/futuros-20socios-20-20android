package com.jerarquicos.futurossocios.view.common.utils.documentos

import android.os.Handler
import android.os.Looper
import okhttp3.MediaType
import okhttp3.RequestBody
import okio.BufferedSink
import java.io.File
import java.io.FileInputStream
import java.io.IOException

/**
 * Created by fgagneten on 1/9/2017.
 */

class ProgressRequestBodyFile(var mFile: File, var mListener: UploadCallbacks) : RequestBody() {
    var mPath: String? = null


    interface UploadCallbacks {
        fun onProgressUpdate(percentage: Int)
        fun onError()
        fun onFinish()
    }

    override fun contentType(): MediaType? {
        return MediaType.parse("image/*")
    }

    @Throws(IOException::class)
    override fun contentLength(): Long {
        return mFile.length()
    }

    @Throws(IOException::class)
    override fun writeTo(sink: BufferedSink) {
        var fileLength = contentLength()
        var buffer = ByteArray(DEFAULT_BUFFER_SIZE)
        var inn = FileInputStream(mFile)
        var uploaded: Long = 0

        try {
            var handler = Handler(Looper.getMainLooper())
            var tiempo: Long = System.currentTimeMillis() + 1000
            var read = inn.read(buffer)

            while (read != -1) {
                uploaded += read
                if (System.currentTimeMillis() > tiempo) {
                    // update progress on UI thread
                    handler.post(ProgressUpdater(uploaded, fileLength))
                    tiempo = System.currentTimeMillis() + 1000
                }
                sink.write(buffer, 0, read)
                read = inn.read(buffer)
            }
        } finally {
            inn.close()
        }
    }

    private inner class ProgressUpdater(private val mUploaded: Long, private val mTotal: Long) : Runnable {

        override fun run() {
            mListener.onProgressUpdate((100 * mUploaded / mTotal).toInt())
        }
    }

    companion object {

        private val DEFAULT_BUFFER_SIZE = 2048
    }
}