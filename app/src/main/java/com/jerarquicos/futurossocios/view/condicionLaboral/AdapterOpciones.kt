package com.jerarquicos.futurossocios.view.condicionLaboral

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.view.models.OpcionModel
import kotlinx.android.synthetic.main.opcion_item_layout.view.*


class AdapterOpciones(private val mOpcionCallback: (OpcionModel) -> Unit)
    : RecyclerView.Adapter<AdapterOpciones.ViewHolder>() {


    private var opcionesLista: MutableList<OpcionModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val vista = LayoutInflater.from(parent.context)
                .inflate(R.layout.opcion_item_layout, parent, false)
        return ViewHolder(vista)

    }

    override fun getItemCount(): Int {
        return opcionesLista!!.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = opcionesLista!![position]
        if (item.seleccionado) {
            holder.itemView.clgeneral.setBackgroundColor(
                    ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.colorAzulInstitucional
                    )
            )
            holder.itemView.tvItem.setTextColor(
                    ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.colorBlancoGris
                    )
            )
            holder.itemView.ivArrow.setImageResource(R.drawable.icon_arrow_white)
        }
        holder.itemView.tvItem.text = item.descripcion
        holder.bind(item)
        if (!item.detalle.isNullOrEmpty()) {
            holder.itemView.tvItemOpcional.visibility = View.VISIBLE
        }
        holder.itemView.setOnClickListener {
            holder.itemView.clgeneral.setBackgroundColor(
                    ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.colorAzulInstitucional
                    )
            )
            holder.itemView.tvItem.setTextColor(
                    ContextCompat.getColor(
                            holder.itemView.context,
                            R.color.colorBlancoGris)
            )
            holder.itemView.ivArrow.setImageResource(R.drawable.icon_arrow_white)
            mOpcionCallback(opcionesLista!![position])
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun setData(mutableList: MutableList<OpcionModel>) {
        opcionesLista = mutableList
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: OpcionModel) {
            with(itemView) {
                tvItem.text = item.descripcion
            }
        }
    }


}