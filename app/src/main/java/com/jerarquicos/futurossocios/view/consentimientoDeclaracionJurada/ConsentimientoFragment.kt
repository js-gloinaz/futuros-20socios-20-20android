package com.jerarquicos.futurossocios.view.consentimientoDeclaracionJurada

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ConsentimientoDeclaracionJuradaFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_05
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_1
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import javax.inject.Inject

class ConsentimientoFragment : BaseFragment(), Injectable {

    companion object {
        private const val BUNDLE_CONSENTIMIENTO = "BUNDLE_CONSENTIMIENTO"

        fun newInstance(tieneConsentimiento: Boolean?): ConsentimientoFragment {
            val args = Bundle()
            if (tieneConsentimiento != null) {
                args.putBoolean(BUNDLE_CONSENTIMIENTO, tieneConsentimiento)
            }
            val fragment = ConsentimientoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    lateinit var binding: ConsentimientoDeclaracionJuradaFragmentBinding
    private lateinit var consentimientoViewModel: ConsentimientoViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.consentimiento_declaracion_jurada_fragment,
                container,
                false
        )
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)
        consentimientoViewModel = ViewModelProviders.of(
                this,
                viewModelFactory
        ).get(ConsentimientoViewModel::class.java)
        binding.viewModel = consentimientoViewModel

        if (consentimientoViewModel.declaracionJuradaModel.value == null) {
            consentimientoViewModel.declaracionJuradaModel.value = arguments!!.getBoolean(BUNDLE_CONSENTIMIENTO)
        }

        binding.tvTitulo.setText(R.string.consentimiento_title)
        binding.btAccion.setText(R.string.continuar_action)

        binding.btAccion.setOnClickListener {
            val fragment = baseActivity.dealerViewModel
                    .obtenerPasoSiguienteConsentimientoDeclaracionJuradaFragment()
            navigationController.navegarSiguienteFragment(fragment)
        }

        if (consentimientoViewModel.declaracionJuradaModel.value!!) {
            binding.btAccion.isEnabled = true
            binding.btAccion.alpha = ALPHA_1
        } else {
            binding.btAccion.isEnabled = false
            binding.btAccion.alpha = ALPHA_05
        }

        binding.cbConsentimiento.setOnClickListener {
            if (binding.cbConsentimiento.isChecked) {
                consentimientoViewModel.declaracionJuradaModel.value = true
                binding.btAccion.isEnabled = true
                binding.btAccion.alpha = ALPHA_1
            } else {
                consentimientoViewModel.declaracionJuradaModel.value = false
                binding.btAccion.isEnabled = false
                binding.btAccion.alpha = ALPHA_05
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_oscuro, menu)
    }
}