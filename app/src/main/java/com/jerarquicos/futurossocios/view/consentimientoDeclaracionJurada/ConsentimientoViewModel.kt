package com.jerarquicos.futurossocios.view.consentimientoDeclaracionJurada

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class ConsentimientoViewModel @Inject constructor() : ViewModel() {

    var declaracionJuradaModel: MutableLiveData<Boolean> = MutableLiveData()

}