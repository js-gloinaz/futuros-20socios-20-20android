package com.jerarquicos.futurossocios.view.contexturaFisica

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ContexturaFisicaFragmentBinding
import com.jerarquicos.futurossocios.databinding.PesoTallaLayoutBinding
import com.jerarquicos.futurossocios.db.entity.ContexturaFisica
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.Persona
import com.jerarquicos.futurossocios.db.entity.PersonaDependiente
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import javax.inject.Inject

class ContexturaFisicaFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	private lateinit var contexturaFisicaViewModel: ContexturaFisicaViewModel
	private lateinit var binding: ContexturaFisicaFragmentBinding
	private var listaBindings: ArrayList<PesoTallaLayoutBinding> = ArrayList()
	
	companion object {
		private const val BUNDLE_EXPEDIENTE = "BUNDLE_EXPEDIENTE"
		
		fun newInstance(expediente: Expediente?): ContexturaFisicaFragment {
			val fragment = ContexturaFisicaFragment()
			val args = Bundle()
			args.putSerializable(BUNDLE_EXPEDIENTE, expediente)
			fragment.arguments = args
			return fragment
		}
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.contextura_fisica_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		inicializar(binding.toolbar, javaClass.name, true)
		contexturaFisicaViewModel = ViewModelProviders.of(
				this,
				viewModelFactory
		).get(ContexturaFisicaViewModel::class.java)
		
		binding.viewModel = contexturaFisicaViewModel
		
		contexturaFisicaViewModel.expediente.value = arguments!!.getSerializable(BUNDLE_EXPEDIENTE) as Expediente
		
		
		binding.btValidar.setOnClickListener {
			if (validarDatos()) {
				// Obtengo próxima acción
				val fragment = baseActivity.dealerViewModel
						.obtenerPasoSiguienteContexturaFisicaFragment(contexturaFisicaViewModel.expediente.value!!)
				// Sincronizo con el servidor
				baseActivity.sincronizarExpediente(true, true)
				// Ejecuto próxima vista
				navigationController.navegarSiguienteFragment(fragment)
			}
		}
		
		agregarPersona(contexturaFisicaViewModel.expediente.value!!.titular!!)
		
		if (contexturaFisicaViewModel.expediente.value!!.conviviente != null) {
			agregarPersona(contexturaFisicaViewModel.expediente.value!!.conviviente!!)
		}
		if (contexturaFisicaViewModel.expediente.value!!.personaDependiente != null &&
				contexturaFisicaViewModel.expediente.value!!.personaDependiente!!.isNotEmpty()) {
			contexturaFisicaViewModel.expediente.value!!.personaDependiente!!.forEach { personaDependiente ->
				if(validarEdad(personaDependiente)) {
					agregarPersona(personaDependiente)
				}
			}
		}
	}
	
	private fun agregarPersona(persona: Persona) {
		val inflater = LayoutInflater.from(context)
		val bindingNuevo: PesoTallaLayoutBinding = DataBindingUtil.inflate(
				inflater,
				R.layout.peso_talla_layout,
				null,
				false
		)
		bindingNuevo.tvNombre.text = getString(R.string.nombre_persona_text, persona.nombre, persona.apellido)
		bindingNuevo.tvDni.text = getString(R.string.dni_persona_text, persona.dni)
		
		setPropiedadesNumberPicker(bindingNuevo)
		
		bindingNuevo.enteraPeso.setOnValueChangedListener { _, _, newVal ->
			persona.contexturaFisica!!.peso = "$newVal".toFloat()
		}
		
		bindingNuevo.enteraTalla.setOnValueChangedListener { _, _, newVal ->
			persona.contexturaFisica!!.talla = "$newVal".toFloat()
		}
		
		if (persona.contexturaFisica != null) {
			bindingNuevo.enteraPeso.value = persona.contexturaFisica!!.peso!!.toInt()
			bindingNuevo.enteraTalla.value = persona.contexturaFisica!!.talla!!.toInt()
		}
		else {
			persona.contexturaFisica = ContexturaFisica()
			bindingNuevo.enteraPeso.value = 50
			bindingNuevo.enteraTalla.value = 150
			persona.contexturaFisica!!.peso = 50F
			persona.contexturaFisica!!.talla = 100F
		}
		
		binding.clSegundo.addView(bindingNuevo.root)
		listaBindings.add(bindingNuevo)
	}
	
	private fun setPropiedadesNumberPicker(binding: PesoTallaLayoutBinding) {
		binding.enteraPeso.minValue = 0
		binding.enteraPeso.maxValue = 499
		binding.enteraTalla.minValue = 0
		binding.enteraTalla.maxValue = 299
		binding.enteraPeso.wrapSelectorWheel = true
		binding.enteraTalla.wrapSelectorWheel = true
	}

	private fun validarEdad(personaDependiente: PersonaDependiente):Boolean{
		return contexturaFisicaViewModel.diferenciaEdad(personaDependiente.fechaNacimiento!!)
	}

	private fun validarDatos(): Boolean {
		var datosValidos = true
		
		listaBindings.forEach { binding ->
			if (binding.enteraPeso.value == 0) {
				binding.llPesoError.visibility = View.VISIBLE
				datosValidos = false
			}
			else {
				binding.llPesoError.visibility = View.GONE
			}
			if (binding.enteraTalla.value == 0) {
				binding.llTallaError.visibility = View.VISIBLE
				datosValidos = false
			}
			else {
				binding.llTallaError.visibility = View.GONE
			}
		}
		return datosValidos
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_base, menu)
	}
}