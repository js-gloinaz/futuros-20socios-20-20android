package com.jerarquicos.futurossocios.view.contexturaFisica

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.EstadoExpediente
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.utils.enums.EstadoPasoExpedienteEnum
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ContexturaFisicaViewModel @Inject constructor() : ViewModel() {
    var expediente: MutableLiveData<Expediente> = MutableLiveData()

    init {
        val estadoExpediente = EstadoExpediente(0, EstadoPasoExpedienteEnum.Inicial.name)
        expediente.value = Expediente(estadoExpediente)
    }

    fun diferenciaEdad(fechaNacimiento: Date): Boolean {
        val formatter = SimpleDateFormat("yyyyMMdd", Locale.getDefault())
        val fecNac = Integer.parseInt(formatter.format(fechaNacimiento))
        val actual = Integer.parseInt(formatter.format(Calendar.getInstance().time))
        return (actual - fecNac) / 10000 >= 18
    }

}
