package com.jerarquicos.futurossocios.view.conviviente

import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ConvivienteFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Conviviente
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Sexo
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.DatePickerFragment
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import javax.inject.Inject

class ConvivienteFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    private lateinit var convivienteViewModel: ConvivienteViewModel
    private lateinit var binding: ConvivienteFragmentBinding

    lateinit var snackbar: SnackbarHelper
    private lateinit var validadorNombre: EditTextValidator
    private lateinit var validadorApellido: EditTextValidator
    private lateinit var validadorDni: EditTextValidator
    private lateinit var validadorFecNac: EditTextValidator
    private lateinit var validadorEmail: EditTextValidator
    private lateinit var validadorCuil: EditTextValidator
    private lateinit var validadorTelCodIntCelular: EditTextValidator
    private lateinit var validadorTelCodAreaCelular: EditTextValidator
    private lateinit var validadorTelNumeroCelular: EditTextValidator

    private val TAG_DATE_PICKER = "TAG_DATE_PICKER"

    companion object {

        private const val BUNDLE_CONVIVIENTE = "BUNDLE_CONVIVIENTE"
        fun newInstance(conviviente: Conviviente?): ConvivienteFragment {
            val fragment = ConvivienteFragment()
            val args = Bundle()
            if (conviviente != null) {
                args.putSerializable(BUNDLE_CONVIVIENTE, conviviente)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.conviviente_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        snackbar = SnackbarHelper(this.view!!, null)

        baseActivity.ocultarTeclado()

        convivienteViewModel = ViewModelProviders
                .of(this, viewModelFactory).get(ConvivienteViewModel::class.java)
        binding.viewModel = convivienteViewModel

        obtenerParametros()
        inicializarListeners()
        convivienteViewModel.setearCodigosInternacionales(context!!)
        cargarSpinnerSexo()
        iniciarValidaciones()
    }

    private fun inicializarListeners() {
        binding.etFechaNac.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val dlgSeleccionarFecha = DatePickerFragment()
                dlgSeleccionarFecha.setView(binding.etFechaNac)
                dlgSeleccionarFecha.show(baseActivity.supportFragmentManager, TAG_DATE_PICKER)
            }
        }

        binding.spSexo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                convivienteViewModel.guardarSexo(p2)
            }
        }

        binding.btValidar.setOnClickListener {
            if (validarDatos()) {
                if (validarDniExistente()) {
                    binding.tiDni.error = getString(R.string.dni_existente_error)
                } else {
                    binding.tiDni.error = null
                    convivienteViewModel.guardarFechaNacimiento()
                    convivienteViewModel.guardarTelefono(binding.etCodInt.text.toString(),
                            binding.etCodArea.text.toString(), binding.etNumero.text.toString())
                    // Obtengo próxima acción
                    val fragment = baseActivity.dealerViewModel
                            .obtenerPasoSiguienteConvivienteFragment(convivienteViewModel.convivienteModel.value!!)
                    // Sincronizo con el servidor
                    baseActivity.sincronizarExpediente(true, true)
                    // Ejecuto próxima vista
                    navigationController.navegarSiguienteFragment(fragment)
                }
            }
        }
    }

    private fun obtenerParametros(){
        val conviviente = arguments!!.getSerializable(BUNDLE_CONVIVIENTE) as Conviviente
        if (conviviente.nombre != null) {
            convivienteViewModel.convivienteModel.value = conviviente
            convivienteViewModel.cargarFechaNacimiento()
            convivienteViewModel.cargarSexo(conviviente.sexo!!)
        }
    }
    private fun validarDniExistente(): Boolean {
        return convivienteViewModel.validarDniExistente()
    }

    private fun iniciarValidaciones() {
        validadorNombre = EditTextValidator(binding.tiNombre)
        validadorApellido = EditTextValidator(binding.tiApellido)
        validadorDni = EditTextValidator(binding.tiDni)
        validadorEmail = EditTextValidator(binding.tiEmail)
        validadorFecNac = EditTextValidator(binding.tiFechaNac)
        validadorCuil = EditTextValidator(binding.tiCuil)
        validadorEmail = EditTextValidator(binding.tiEmail)
        validadorTelCodIntCelular = EditTextValidator(binding.tiCodInt)
        validadorTelCodAreaCelular = EditTextValidator(binding.tiCodArea)
        validadorTelNumeroCelular = EditTextValidator(binding.tiNumero)

    }

    private fun validarDatos(): Boolean {
        var hayError = true
        hayError = validadorNombre.esNombreValido() && hayError
        hayError = validadorApellido.esNombreValido() && hayError
        hayError = validadorDni.esNumeroDocumento() && hayError
        hayError = validadorEmail.esEmailOVacio() && hayError
        hayError = validadorFecNac.esFechaNacimientoValida(true) && hayError
        hayError = validadorCuil.esCuilValido(binding.etDni.text.toString()) && hayError
        hayError = validadorTelCodIntCelular
                .esCodInternacionalNoObligatorio(binding.etCodArea.text.toString(),
                        binding.etNumero.text.toString()) && hayError
        hayError = validadorTelCodAreaCelular
                .esCodAreaNumeroNoObligatorio(binding.etNumero.text.toString()) && hayError
        hayError = validadorTelNumeroCelular
                .esNumeroAreaNoObligatorioCelular(binding.etCodArea.text.toString()) && hayError

        return hayError
    }

    private fun mostrarSnackBar(mensaje: String, color: Int, duracion: SnackbarEnum = SnackbarEnum.MEDIO) {
        snackbar.setMensajePersonalizado(mensaje)
        snackbar.setearColores(ContextCompat.getColor(activity!!.applicationContext, color))
        snackbar.setDuration(duracion)
        snackbar.show()
    }

    private fun cargarSpinnerSexo() {
        convivienteViewModel.obtenerSexo()
                .observe(this, Observer<EstadoRespuesta<DatosIniciales>> { this.handleResponseSexo(it) })
    }

    private fun cargarSpSexos(sexos: List<Sexo>) {
        convivienteViewModel.cargarSpSexos(sexos)
        val adapter = ArrayAdapter<String>(
                context!!,
                R.layout.spinner_item_seleccionado_layout,
                convivienteViewModel.arrSexos.value!!
        )
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spSexo.adapter = adapter
        binding.spSexo.visibility = View.VISIBLE
    }

    private fun handleResponseSexo(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpSexos(respuesta.data.sexo)
                        convivienteViewModel.guardarSexos(respuesta.data.sexo)
                        binding.spSexo.setSelection(convivienteViewModel.obtenerPosicionSexoSeleccionado())
                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_con_eliminacion, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.btDelete -> {
                val builder = AlertDialog.Builder(context!!)
                builder.setMessage(getString(R.string.eliminar_conviviente_text))
                builder.setPositiveButton(getString(R.string.aceptar_action)) { _, _ ->
                    convivienteViewModel
                            .eliminarConviviente(convivienteViewModel.convivienteModel.value!!.dni.toString())
                    navigationController.navegarEliminarConviviente()
                    mostrarSnackBar(getString(R.string.conviviente_eliminado_text), R.color.colorVerde)
                }
                builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

}