package com.jerarquicos.futurossocios.view.conviviente

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.view.persona.PersonaViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class ConvivienteViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : PersonaViewModel(futurosSociosRepository) {
    var convivienteModel: MutableLiveData<Conviviente> = MutableLiveData()
    var fechaDeNacimiento: MutableLiveData<String> = MutableLiveData()
    var expediente: MutableLiveData<Expediente> = MutableLiveData()

    init {
        convivienteModel.value = Conviviente()
        expediente.value = futurosSociosRepository.obtenerExpediente()
    }


    fun eliminarConviviente(dni: String) {
        if (convivienteModel.value?.dni.equals(dni)) {
            expediente.value = futurosSociosRepository.obtenerExpediente()
            expediente.value?.conviviente = null
            expediente.value?.ingresaConviviente = false
            expediente.value?.declaracionJurada = null
            futurosSociosRepository.guardarExpediente(expediente.value!!)
        }
    }

    fun guardarFechaNacimiento() {
        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val date = format.parse(fechaDeNacimiento.value!!)
            convivienteModel.value?.fechaNacimiento = date
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun cargarFechaNacimiento() {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val dateTime = dateFormat.format(convivienteModel.value?.fechaNacimiento!!)
            fechaDeNacimiento.value = dateTime
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun guardarTelefono(codigoInternacional: String?, codigoArea: String?, numero: String?) {
        if (numero != null) {
            convivienteModel.value?.telefonoCelular?.codigoInternacional = codigoInternacional
            convivienteModel.value?.telefonoCelular?.codigoArea = codigoArea
            convivienteModel.value?.telefonoCelular?.numero = numero
        }
        if (numero?.equals("")!!) {
            convivienteModel.value?.telefonoCelular = null
        }
    }

    fun setearCodigosInternacionales(context: Context) {
        if (convivienteModel.value?.telefonoCelular?.id == null) {
            convivienteModel.value?.telefonoCelular = Telefono(0, context.
                    getString(R.string.codigo_internacional_argentina_text), null, null)
        }
    }

    fun guardarSexos(sexo: List<Sexo>) {
        this.sexo.value = sexo
    }

    fun cargarSexo(sexo: Sexo) {
        convivienteModel.value?.sexo = sexo
    }

    fun guardarSexo(posicion: Int) {
        convivienteModel.value?.sexo = sexo.value!![posicion]
    }

    fun obtenerPosicionSexoSeleccionado(): Int {
        var index = 0
        if (convivienteModel.value?.sexo != null) {
            this.sexo.value!!.forEachIndexed { i, element ->
                if (element.id == convivienteModel.value?.sexo!!.id) {
                    index = i
                }
            }
        }
        return index
    }

    fun validarDniExistente(): Boolean {
        val dni = convivienteModel.value!!.dni
        val expediente = expediente.value

        if (expediente!!.titular?.dni.equals(dni)) {
            return true
        }
        expediente.personaDependiente?.forEach { p ->
            if (p.dni.equals(dni)) {
                return true
            }
        }
        return false
    }
}