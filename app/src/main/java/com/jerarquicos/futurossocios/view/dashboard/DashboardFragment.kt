package com.jerarquicos.futurossocios.view.dashboard

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.Crashlytics
import com.google.firebase.iid.FirebaseInstanceId
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.DashboardFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.cuenta.DispositivoModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.AppConstants.USUARIO
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import com.jerarquicos.futurossocios.view.expedientesEnviados.ExpedientesEnviadosFragment
import com.jerarquicos.futurossocios.view.models.DashboardModel
import com.jerarquicos.futurossocios.view.models.ExpedienteEnviadoModel
import javax.inject.Inject

class DashboardFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	private lateinit var mDashboardViewModel: DashboardViewModel
	lateinit var binding: DashboardFragmentBinding
	@Inject
	lateinit var usuarioHelper: UsuarioHelper
	private var adaptador: ExpedienteAdapter? = null
	var layoutManager: RecyclerView.LayoutManager? = null
	private var expedientesPendientesTemporalesServidor: ArrayList<Expediente> = ArrayList()
	private var listaExpedientesPendientesLocal: ArrayList<Expediente> = ArrayList()
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER
		inicializar(binding.toolbar, javaClass.name, true)
		
		mDashboardViewModel = ViewModelProviders.of(this, viewModelFactory).get(DashboardViewModel::class.java)
		
		setListaExpedientesPendientes()
		
		binding.tvNombre.text = getString(R.string.nombre_persona_text, usuarioHelper.getNombre(), usuarioHelper
				.getApellido())
		
		mDashboardViewModel.obtenerCantidadNotificacionesSinLeer().observe(this, Observer<EstadoRespuesta<Int>>
		{ this.manejarCantidadNotificacionesSinLeer(it) })
		
		mDashboardViewModel.obtenerExpedientesOnline()
				.observe(this, Observer<EstadoRespuesta<RespuestaModel<DashboardModel>>>
				{ this.manejarExpedientesOnlineResponse(it) })
		
		binding.btIniciarNuevoExpediente.setOnClickListener {
			mDashboardViewModel.nuevoExpediente()
			val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteDashboardFragment()
			navigationController.navegarSiguienteFragment(fragment)
		}
		binding.btVerExpedientesEnviados.setOnClickListener {
			lateinit var fragment: ExpedientesEnviadosFragment
			if (mDashboardViewModel.listaExpedientesEnviados.value != null) {
				fragment = ExpedientesEnviadosFragment
						.newInstance(mDashboardViewModel.listaExpedientesEnviados.value as ArrayList<ExpedienteEnviadoModel>)
			}
			else {
				fragment = ExpedientesEnviadosFragment.newInstance(ArrayList())
			}
			navigationController.navegarSiguienteFragment(fragment)
		}
		binding.btVerNotificaciones.setOnClickListener {
			navigationController.navegarNotificacionFcmFragment()
		}
		
		binding.tvErrorConexion.setOnClickListener {
			mDashboardViewModel.obtenerExpedientesOnline()
					.observe(this, Observer<EstadoRespuesta<RespuestaModel<DashboardModel>>>
					{ this.manejarExpedientesOnlineResponse(it) })
		}
	}
	
	private fun manejarExpedientesOnlineResponse(respuesta: EstadoRespuesta<RespuestaModel<DashboardModel>>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
					expedientesPendientesTemporalesServidor = ArrayList()
					mDashboardViewModel.listaExpedientesEnviados.value = ArrayList()
					mDashboardViewModel.mostrarSoloOffline.value = true

					cargarExpedientesPendientes()
					mostrarInformacion()
					baseActivity.ocultarProgressBar()
				}
				Estado.CARGANDO -> {
					baseActivity.mostrarProgressBar()
				}
				Estado.EXITO -> {
					mDashboardViewModel.mostrarSoloOffline.value = false
					mDashboardViewModel.listaExpedientesEnviados.value = respuesta.data?.modelo?.expedientesEnviados!!
					expedientesPendientesTemporalesServidor = respuesta.data.modelo?.expedientesEnCurso!!

					cargarExpedientesPendientes()
					mostrarInformacion()
					baseActivity.ocultarProgressBar()
				}
			}
		}
	}
	
	private fun manejarCantidadNotificacionesSinLeer(respuesta: EstadoRespuesta<Int>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.EXITO -> {
					if (respuesta.data != null) {
						mDashboardViewModel.cantidadNotificacionesSinLeer.value = respuesta.data
						binding.tvCantidadNotificacionesSinLeer.text = mDashboardViewModel
								.cantidadNotificacionesSinLeer.value!!.toString()
					}
				}
				else -> {
					//not implementing
				}
			}
		}
		else{
			mDashboardViewModel.cantidadNotificacionesSinLeer.value = 0
		}
	}

	private fun mostrarInformacion() {
		binding.tvCantidadExpedientesPendientes.text = mDashboardViewModel
				.listaExpedientesPendientes.value!!.size.toString()
		binding.tvCantidadExpedientesEnviados.text = mDashboardViewModel
				.listaExpedientesEnviados.value!!.size.toString()
		binding.tvCantidadNotificacionesSinLeer.text = mDashboardViewModel
				.cantidadNotificacionesSinLeer.value!!.toString()
		
		if (mDashboardViewModel.listaExpedientesPendientes.value!!.isNotEmpty()) {
			binding.rvItems.visibility = View.VISIBLE
			binding.tvSinExpedientes.visibility = View.GONE
			adaptador!!.setData(mDashboardViewModel.listaExpedientesPendientes.value
					as MutableList<Expediente>, baseActivity.dealerViewModel)
		}
		else {
			binding.rvItems.visibility = View.GONE
			binding.tvSinExpedientes.visibility = View.VISIBLE
		}
		
		if (mDashboardViewModel.mostrarSoloOffline.value!!) {
			binding.clErrorSincronizacion.visibility = View.VISIBLE
		}
		else {
			binding.clErrorSincronizacion.visibility = View.GONE
		}
	}
	
	private fun cargarExpedientesPendientes() {
		listaExpedientesPendientesLocal = mDashboardViewModel.obtenerExpedientes() as ArrayList<Expediente>
		mDashboardViewModel.agregarExpedientes(listaExpedientesPendientesLocal, expedientesPendientesTemporalesServidor)
	}
	
	private fun setListaExpedientesPendientes() {
		binding.rvItems.setHasFixedSize(true)
		layoutManager = LinearLayoutManager(this.context)
		binding.rvItems.layoutManager = layoutManager
		adaptador = ExpedienteAdapter { mOpcionModel: Expediente ->
			mDashboardViewModel.seleccionarExpediente(mOpcionModel.idAplicacion)
			baseActivity.mostrarProgressBar()
			val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteDashboardFragment()
			baseActivity.ocultarProgressBar()
			navigationController.navegarSiguienteFragment(fragment)
		}
		binding.rvItems.adapter = adaptador
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_dashboard, menu)
	}
	
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when (item?.itemId) {
			R.id.btCambiarContrasenia -> {
				navigationController.navegarCambiarContraseniaFragment(usuarioHelper.getToken()!!.usuario)
				return true
			}
			R.id.btCerrarSesion -> {
				cerrarSesion()
				return true
			}
			else -> {
				return super.onOptionsItemSelected(item)
			}
		}
	}
	
	private fun cerrarSesion() {
		val builder = AlertDialog.Builder(context!!)
		builder.setTitle(getString(R.string.cerrar_sesion_action))
		builder.setMessage(getString(R.string.cerrar_sesion_text))
		builder.setPositiveButton(getString(R.string.aceptar_action)) { _, _ ->
			eliminarDispositivo()
			mDashboardViewModel.borrarDatos()
			navigationController.navegarInicioFragment()
		}
		builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->
		}
		val dialog: AlertDialog = builder.create()
		dialog.show()
	}
	
	private fun eliminarDispositivo() {
		val dispositivoModel = DispositivoModel()
		FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceId ->
			val packageInfo = this.context!!.packageManager.getPackageInfo(this.context!!.packageName, 0)
			dispositivoModel.usuario = usuarioHelper.getUsuario()!!
			dispositivoModel.registrationId = instanceId.token
			dispositivoModel.manufacter = Build.MANUFACTURER
			dispositivoModel.model = Build.MODEL
			dispositivoModel.versionAndroid = Build.VERSION.RELEASE
			dispositivoModel.serial = Build.SERIAL
			dispositivoModel.versionName = packageInfo.versionName
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
				dispositivoModel.versionCode = packageInfo.longVersionCode.toString()
			}
			else {
				dispositivoModel.versionCode = packageInfo.versionCode.toString()
			}
			dispositivoModel.uuid = Settings.Secure
					.getString(this.activity!!.contentResolver, Settings.Secure.ANDROID_ID)
			mDashboardViewModel.dispositivoModel.value = dispositivoModel
			mDashboardViewModel.eliminarDispositivo(mDashboardViewModel.dispositivoModel.value!!)
					.observe(this, Observer<EstadoRespuesta<RespuestaModel<Boolean>>> {})
			usuarioHelper.cerrarSesion()
			Crashlytics.setString(USUARIO, getString(R.string.usuario_deslogueado_text))
		}
	}
}