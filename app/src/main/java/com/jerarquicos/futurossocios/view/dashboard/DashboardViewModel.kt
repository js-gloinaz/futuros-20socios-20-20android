package com.jerarquicos.futurossocios.view.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.EstadoExpediente
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.cuenta.DispositivoModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.enums.EstadoExpedienteEnum
import com.jerarquicos.futurossocios.view.models.DashboardModel
import com.jerarquicos.futurossocios.view.models.ExpedienteEnviadoModel
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class DashboardViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
	: ViewModel() {
	var expediente = MutableLiveData<Expediente>()
	var mostrarSoloOffline = MutableLiveData<Boolean>()
	var dispositivoModel: MutableLiveData<DispositivoModel> = MutableLiveData()
	var listaExpedientesPendientes: MutableLiveData<ArrayList<Expediente>> = MutableLiveData()
	var listaExpedientesEnviados: MutableLiveData<ArrayList<ExpedienteEnviadoModel>> = MutableLiveData()
	var cantidadNotificacionesSinLeer: MutableLiveData<Int> = MutableLiveData()
	
	fun nuevoExpediente() {
		val id = UUID.randomUUID().toString()
		val estadoExpediente = EstadoExpediente(EstadoExpedienteEnum.Inicial.valor, EstadoExpedienteEnum.Inicial.name)
		expediente.value = Expediente(id, estadoExpediente, Date())
		return futurosSociosRepository.crearNuevoExpediente(expediente.value!!)
	}
	
	fun seleccionarExpediente(idAplicacion: String) {
		return futurosSociosRepository.seleccionarExpediente(idAplicacion)
	}
	
	fun obtenerExpedientes(): List<Expediente> {
		return futurosSociosRepository.obtenerExpedientes()
	}
	
	fun obtenerExpedientesOnline(): LiveData<EstadoRespuesta<RespuestaModel<DashboardModel>>> {
		return futurosSociosRepository.obtenerExpedientesOnline()
	}
	
	fun obtenerCantidadNotificacionesSinLeer(): LiveData<EstadoRespuesta<Int>> {
		return futurosSociosRepository.obtenerCantidadNotificacionesSinLeer()
	}
	
	fun borrarDatos() {
		futurosSociosRepository.borrarExpedientes()
		futurosSociosRepository.borrarNotificaciones()
	}
	
	fun eliminarDispositivo(dispositivoModel: DispositivoModel): LiveData<EstadoRespuesta<RespuestaModel<Boolean>>> {
		return futurosSociosRepository.eliminarDispositivo(dispositivoModel)
	}
	
	fun agregarExpedientes(expedienteLocales: ArrayList<Expediente>, expedienteServidor: ArrayList<Expediente>) {
		val listaTotal = ArrayList<Expediente>()

		expedienteServidor.forEach { expedienteServidor ->
			var duplicado = false
			expedienteLocales.forEach { expedienteLocal ->
				if (expedienteServidor.id == expedienteLocal.id) {
					duplicado = true
					if (expedienteLocal.version!! >= expedienteServidor.version!!) {
						listaTotal.add(expedienteLocal)
					}
					else {
						listaTotal.add(expedienteServidor)
					}
				}
			}
			if (!duplicado) {
				listaTotal.add(expedienteServidor)
			}
		}

		expedienteLocales.forEach { expedienteLocal ->
			if (expedienteLocal.id == null || expedienteLocal.id == 0 || mostrarSoloOffline.value!!) {
				listaTotal.add(expedienteLocal)
			}
		}

		listaExpedientesPendientes.value = listaTotal
		futurosSociosRepository.guardarExpedientes(listaExpedientesPendientes.value!!)
	}
}