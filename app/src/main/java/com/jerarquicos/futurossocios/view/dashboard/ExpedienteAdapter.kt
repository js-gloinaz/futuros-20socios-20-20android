package com.jerarquicos.futurossocios.view.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.view.base.DealerViewModel
import kotlinx.android.synthetic.main.item_expediente_layout.view.*

class ExpedienteAdapter(private val mOpcionCallback: (Expediente) -> Unit)
	: RecyclerView.Adapter<ExpedienteAdapter.ViewHolder>() {
	private lateinit var dealer: DealerViewModel
	private var opcionesLista: MutableList<Expediente> = ArrayList()
	
	override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
		val vista = LayoutInflater.from(parent.context)
				.inflate(R.layout.item_expediente_layout, parent, false)
		return ViewHolder(vista)
	}
	
	override fun getItemCount(): Int {
		return opcionesLista.size
	}
	
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val expediente = opcionesLista[position]

		if (expediente.titular?.nombre != null) {
			holder.itemView.tvItem.text =
					holder.itemView.context.resources.getString(R.string.nombre_persona_text,
							expediente.titular!!.nombre!!, expediente.titular!!.apellido)
		}
		else {
			holder.itemView.tvItem.text = holder.itemView.context.resources.
					getString(R.string.expediente_incompleto_text)
		}
		
		if (expediente.id!! == 0) {
			holder.itemView.tvNroExpediente.visibility = View.GONE
			holder.itemView.tvNroExpediente.text = holder.itemView.context.resources.getString(R.string.vacio_text)
		}
		else {
			holder.itemView.tvNroExpediente.visibility = View.VISIBLE
			holder.itemView.tvNroExpediente.text = holder.itemView.context.resources.
					getString(R.string.numero_expediente_text, expediente.id!!)
		}

		holder.itemView.setOnClickListener { mOpcionCallback(expediente) }
	}
	
	override fun getItemId(position: Int): Long {
		return position.toLong()
	}
	
	override fun getItemViewType(position: Int): Int {
		return position
	}
	
	fun setData(mutableList: MutableList<Expediente>, d: DealerViewModel) {
		dealer = d
		opcionesLista = mutableList
		notifyDataSetChanged()
	}
	
	inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}