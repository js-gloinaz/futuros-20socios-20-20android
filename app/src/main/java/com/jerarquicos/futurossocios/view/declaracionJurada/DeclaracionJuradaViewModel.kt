package com.jerarquicos.futurossocios.view.declaracionJurada

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.view.models.PreguntaBasicoModel
import javax.inject.Inject

open class DeclaracionJuradaViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository
) : ViewModel() {

    var expediente: MutableLiveData<Expediente> = MutableLiveData()
    var respuesta: MutableLiveData<RespuestaPreguntaDeclaracionJurada> = MutableLiveData()
    var pregunta: MutableLiveData<PreguntaDeclaracionJurada> = MutableLiveData()
    var preguntaActual: MutableLiveData<Int> = MutableLiveData()

    fun obtenerExpediente(): Expediente {
        expediente.value = futurosSociosRepository.obtenerExpediente()
        return expediente.value!!
    }

    fun cargarRespuesta() {
        if ((expediente.value!!.declaracionJurada?.respuestaPreguntaDeclaracionJurada != null) &&
                (preguntaActual.value!! < expediente.value!!.declaracionJurada!!
                        .respuestaPreguntaDeclaracionJurada!!.size) ) {
                    respuesta.value =
                            expediente.value!!.declaracionJurada!!
                                    .respuestaPreguntaDeclaracionJurada!![preguntaActual.value!!]
        }
        if (respuesta.value == null) {
            // Nunca se completó el formulario
            respuesta.value = RespuestaPreguntaDeclaracionJurada()
            respuesta.value!!.preguntaDeclaracionJurada = nuevaPreguntaBasico(pregunta.value!!.id)
            respuesta.value!!.respuestaPatologia = ArrayList()
            pregunta.value!!.patologia!!.forEach { patologia ->
                val respuestaPatologia = RespuestaPatologia()
                respuestaPatologia.patologia = patologia
                respuestaPatologia.valor = false
                respuesta.value!!.respuestaPatologia!!.add(respuestaPatologia)
            }
        }
    }

    private fun nuevaPreguntaBasico(id: Int): PreguntaBasicoModel {
        val preguntaBasico = PreguntaBasicoModel()
        preguntaBasico.id = id
        return preguntaBasico
    }
}
