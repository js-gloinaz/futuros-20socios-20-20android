package com.jerarquicos.futurossocios.view.declaracionJurada

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.OpcionPreguntaItemLayoutBinding
import com.jerarquicos.futurossocios.databinding.PersonaPatologiaItemLayoutBinding
import com.jerarquicos.futurossocios.databinding.PersonasPatologiaLayoutBinding
import com.jerarquicos.futurossocios.db.entity.PersonaPatologia
import com.jerarquicos.futurossocios.db.entity.RespuestaPatologia
import com.jerarquicos.futurossocios.db.entity.RespuestaPreguntaDeclaracionJurada
import com.jerarquicos.futurossocios.di.Injectable
import javax.inject.Inject
import com.google.android.material.bottomsheet.BottomSheetDialog
import android.widget.FrameLayout
import androidx.annotation.NonNull





class PersonasBottomSheet : BottomSheetDialogFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	private lateinit var personasBottomSheetViewModel: PersonasBottomSheetViewModel
	private lateinit var bindingNuevo: OpcionPreguntaItemLayoutBinding
	lateinit var binding: PersonasPatologiaLayoutBinding
	private var callbackListener: MyDialogInterface? = null
	
	companion object {
		private const val BUNDLE_LISTA_PERSONAS = "BUNDLE_LISTA_PERSONAS"
		private const val BUNDLE_RESPUESTA_PATOLOGIA = "BUNDLE_RESPUESTA_PATOLOGIA"
		
		fun newInstance(
				listaPersonas: ArrayList<PersonaPatologia>,
				respuestaPatologia: RespuestaPatologia
		): PersonasBottomSheet {
			val fragment = PersonasBottomSheet()
			val args = Bundle()
			args.putSerializable(BUNDLE_LISTA_PERSONAS, listaPersonas)
			args.putSerializable(BUNDLE_RESPUESTA_PATOLOGIA, respuestaPatologia)
			fragment.arguments = args
			return fragment
		}
	}
	
	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog
		dialog.setOnShowListener { dialog ->
			val d = dialog as BottomSheetDialog
			val bottomSheet = d.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
			BottomSheetBehavior.from(bottomSheet!!).state = BottomSheetBehavior.STATE_EXPANDED
		}
		return dialog
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.personas_patologia_layout, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		personasBottomSheetViewModel = ViewModelProviders.of(this, viewModelFactory)
				.get(PersonasBottomSheetViewModel::class.java)
		binding.viewModel = personasBottomSheetViewModel
		
		obtenerParametros()
		mostrarInformacion()
		
		binding.btAccion.setOnClickListener {
			if (callbackListener != null) {
				callbackListener!!.onDismissBottomSheet(
						personasBottomSheetViewModel.respuestaPatologia.value?.personaPatologia!!, bindingNuevo
				)
				super.dismiss()
			}
		}
		retainInstance = true
	}

	override fun onDismiss(dialog: DialogInterface?) {
		if (personasBottomSheetViewModel.respuestaPatologia.value?.personaPatologia != null && callbackListener != null) {
				callbackListener!!.onDismissBottomSheet(
						personasBottomSheetViewModel.respuestaPatologia.value?.personaPatologia!!, bindingNuevo
				)
		}
		super.onDismiss(dialog)
	}
	
	private fun agregarPersona(persona: PersonaPatologia, poseePatologia: Boolean) {
		val bindingNuevo: PersonaPatologiaItemLayoutBinding = DataBindingUtil
				.inflate(layoutInflater, R.layout.persona_patologia_item_layout, binding.llPersonas, false)
		bindingNuevo.tvNombre.text = getString(R.string.nombre_persona_text, persona.persona!!.nombre, persona
				.persona!!.apellido)
		bindingNuevo.etDescripcion.hint = personasBottomSheetViewModel
				.respuestaPatologia.value!!.patologia!!.leyendaObservacion
		bindingNuevo.checkBox.isChecked = poseePatologia
		
		if (poseePatologia &&
				(personasBottomSheetViewModel.respuestaPatologia.value!!.patologia!!.requiereObservacion!!)) {
			bindingNuevo.etDescripcion.visibility = View.VISIBLE
			bindingNuevo.etDescripcion.setText(persona.observacion)
		}
		inicializarListener(persona,bindingNuevo)
		binding.llPersonas.addView(bindingNuevo.root)
	}

	private fun inicializarListener(persona: PersonaPatologia, bindingNuevo: PersonaPatologiaItemLayoutBinding){
		bindingNuevo.checkBox.setOnCheckedChangeListener { _, isChecked ->
			if (isChecked) {
				var personaAnadida = false
				personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia!!
						.forEachIndexed { index, personaConPatologia ->
							if (personaConPatologia.persona!!.dni == persona.persona!!.dni) {
								personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia!![index] = persona
								personaAnadida = true
							}
						}
				if (!personaAnadida) {
					personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia!!.add(persona)
				}
				if (personasBottomSheetViewModel.respuestaPatologia.value!!.patologia!!.requiereObservacion!!) {
					bindingNuevo.etDescripcion.visibility = View.VISIBLE
				}
			}
			else {
				bindingNuevo.etDescripcion.visibility = View.GONE
				persona.observacion = null
				personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia!!.remove(persona)
			}
		}

		bindingNuevo.etDescripcion.addTextChangedListener(object : TextWatcher {
			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
				//not implementing
			}
			override fun afterTextChanged(s: Editable?) {
				persona.observacion = s.toString()
			}

			override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
				if (!bindingNuevo.etDescripcion.text.toString().isNullOrEmpty()) {
					persona.observacion = s!!.toString()
				}
				else {
					persona.observacion = ""
				}
			}
		})
	}
	private fun indicePersonaConPatologia(personaDelTotal: PersonaPatologia): Int {
		personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia!!
				.forEachIndexed { index, personaConPatologia ->
					if (personaConPatologia.persona!!.dni == personaDelTotal.persona!!.dni) {
						return index
					}
				}
		return -1
	}
	
	private fun obtenerParametros() {
		personasBottomSheetViewModel.listaPersonas.value =
				arguments!!.getSerializable(BUNDLE_LISTA_PERSONAS) as ArrayList<PersonaPatologia>
		personasBottomSheetViewModel.respuestaPatologia.value =
				arguments!!.getSerializable(BUNDLE_RESPUESTA_PATOLOGIA) as RespuestaPatologia
	}
	
	private fun mostrarInformacion() {
		if (personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia == null) {
			personasBottomSheetViewModel.respuestaPatologia.value!!.personaPatologia = ArrayList()
		}
		personasBottomSheetViewModel.listaPersonas.value!!.forEach { personaDelTotal ->
			val indicePersonaConPatologia = indicePersonaConPatologia(personaDelTotal)
			if (indicePersonaConPatologia > -1) {
				agregarPersona(personasBottomSheetViewModel
						.respuestaPatologia.value!!.personaPatologia!![indicePersonaConPatologia], true)
			}
			else {
				agregarPersona(personaDelTotal, false)
			}
		}
	}
	
	interface MyDialogInterface {
		fun onDismissBottomSheet(
				personaPatologia: ArrayList<PersonaPatologia>,
				bindingNuevo: OpcionPreguntaItemLayoutBinding
		)
	}
	
	fun setCallbackListener(callbackListener: MyDialogInterface, bindingNuevo: OpcionPreguntaItemLayoutBinding) {
		this.callbackListener = callbackListener
		this.bindingNuevo = bindingNuevo
	}
}