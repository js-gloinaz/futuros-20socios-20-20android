package com.jerarquicos.futurossocios.view.declaracionJurada

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.PersonaPatologia
import com.jerarquicos.futurossocios.db.entity.RespuestaPatologia
import javax.inject.Inject

class PersonasBottomSheetViewModel @Inject constructor() : ViewModel() {

    var listaPersonas: MutableLiveData<ArrayList<PersonaPatologia>> = MutableLiveData()
    var respuestaPatologia: MutableLiveData<RespuestaPatologia> = MutableLiveData()

}