package com.jerarquicos.futurossocios.view.declaracionJurada

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.OpcionPreguntaItemLayoutBinding
import com.jerarquicos.futurossocios.databinding.PreguntaTipoUnoFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.PersonaPatologia
import com.jerarquicos.futurossocios.db.entity.PreguntaDeclaracionJurada
import com.jerarquicos.futurossocios.db.entity.RespuestaPatologia
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import java.util.*
import javax.inject.Inject

class PreguntaDeclaracionJuradaFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    private lateinit var preguntaDeclaracionJuradaViewModel: PreguntaDeclaracionJuradaViewModel
    lateinit var binding: PreguntaTipoUnoFragmentBinding
    lateinit var expediente: Expediente
    private var mensajeError: String = ""
    lateinit var snackbar: SnackbarHelper

    companion object {
        private const val BUNDLE_PREGUNTA = "BUNDLE_PREGUNTA"
        private const val BUNDLE_SIGUIENTE_PREGUNTA = "BUNDLE_SIGUIENTE_PREGUNTA"

        fun newInstance(
                pregunta: PreguntaDeclaracionJurada,
                siguientePregunta: Int
        ): PreguntaDeclaracionJuradaFragment {
            val args = Bundle()
            args.putSerializable(BUNDLE_PREGUNTA, pregunta)
            args.putInt(BUNDLE_SIGUIENTE_PREGUNTA, siguientePregunta)
            val fragment = PreguntaDeclaracionJuradaFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.pregunta_tipo_uno_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)
        preguntaDeclaracionJuradaViewModel = ViewModelProviders.of(
                this,
                viewModelFactory).get(PreguntaDeclaracionJuradaViewModel::class.java
        )
        binding.viewModel = preguntaDeclaracionJuradaViewModel

        snackbar = SnackbarHelper(this.view!!, null)

        obtenerParametros()
        preguntaDeclaracionJuradaViewModel.cargarPersonas()
        preguntaDeclaracionJuradaViewModel.cargarRespuesta()
        mostrarInformacion()
        inicializarListeners()
    }

    private fun inicializarListeners() {
        binding.btConfirmar.setOnClickListener {
            if (!respuestasValidas()) {
                mostrarSnackBar(mensajeError, R.color.colorNaranja)
            } else {
                val fragment = baseActivity.dealerViewModel.analizarMostrarPreguntasDeclaracionJurada(
                        preguntaDeclaracionJuradaViewModel.siguientePregunta.value!!,
                        preguntaDeclaracionJuradaViewModel.respuesta.value!!
                )
                navigationController.navegarSiguienteFragment(fragment)
            }
        }
    }

    private fun agregarPatologia(respuestaPatologia: RespuestaPatologia) {
        val inflater = LayoutInflater.from(context)
        val bindingNuevo: OpcionPreguntaItemLayoutBinding = DataBindingUtil.inflate(
                inflater, R.layout.opcion_pregunta_item_layout, binding.llPatologias, false
        )
        bindingNuevo.tvNombre.text = respuestaPatologia.patologia!!.descripcionExterna
        bindingNuevo.respuestaPatologia = respuestaPatologia
        bindingNuevo.listeners = Listeners(respuestaPatologia, bindingNuevo)

        if (respuestaPatologia.personaPatologia != null && respuestaPatologia.personaPatologia!!.size > 0) {
            bindingNuevo.tvBadge.visibility = View.VISIBLE
            bindingNuevo.tvBadge.text = respuestaPatologia.personaPatologia!!.size.toString()
        } else {
            bindingNuevo.tvBadge.visibility = View.GONE
        }

        if (respuestaPatologia.valor!!) {
            bindingNuevo.tvValor.text = baseActivity.getString(R.string.si_action)
        } else {
            bindingNuevo.tvValor.text = baseActivity.getString(R.string.no_action)
        }

        bindingNuevo.tvNombre.setOnClickListener {
            if (respuestaPatologia.valor!!) {
                val personasBottomSheet: PersonasBottomSheet = PersonasBottomSheet.newInstance(
                        preguntaDeclaracionJuradaViewModel.listaPersonas.value!!, respuestaPatologia
                )
                personasBottomSheet.setCallbackListener(onActionSheetDismiss, bindingNuevo)
                personasBottomSheet.show(activity!!.supportFragmentManager, "bottom_sheet")
            }
        }
        bindingNuevo.tvBadge.setOnClickListener {
            if (respuestaPatologia.valor!!) {
                val personasBottomSheet: PersonasBottomSheet = PersonasBottomSheet.newInstance(
                        preguntaDeclaracionJuradaViewModel.listaPersonas.value!!, respuestaPatologia
                )
                personasBottomSheet.setCallbackListener(onActionSheetDismiss, bindingNuevo)
                personasBottomSheet.show(activity!!.supportFragmentManager, "bottom_sheet")
            }
        }
        binding.llPatologias.addView(bindingNuevo.root)
    }

    private val onActionSheetDismiss = object : PersonasBottomSheet.MyDialogInterface {
        override fun onDismissBottomSheet(
                personaPatologia: ArrayList<PersonaPatologia>,
                bindingNuevo: OpcionPreguntaItemLayoutBinding
        ) {
            if (personaPatologia.size > 0) {
                bindingNuevo.tvBadge.visibility = View.VISIBLE
                bindingNuevo.tvBadge.text = personaPatologia.size.toString()
                bindingNuevo.tvValor.text = baseActivity.getString(R.string.si_action)
            } else {
                bindingNuevo.swOpcion.isChecked = false
                bindingNuevo.tvBadge.visibility = View.GONE
                bindingNuevo.tvValor.text = baseActivity.getString(R.string.no_action)
            }
        }
    }

    private fun respuestasValidas(): Boolean {
        preguntaDeclaracionJuradaViewModel.respuesta.value!!.respuestaPatologia!!.forEach { respuestaPatologia ->
            if (respuestaPatologia.valor!!) {
                if (respuestaPatologia.personaPatologia != null && respuestaPatologia.personaPatologia!!.isEmpty()) {
                    mensajeError = baseActivity.getString(R.string.patologia_sin_personas_error,
                            respuestaPatologia.patologia!!.descripcionExterna)
                    return false
                } else {
                    if (respuestaPatologia.patologia!!.requiereObservacion!!) {
                        respuestaPatologia.personaPatologia!!.forEach { persona ->
                            if (persona.observacion.isNullOrEmpty()) {
                                mensajeError = baseActivity.
                                        getString(R.string.observacion_patologia_vacia_error,
                                                persona.persona!!.nombre, persona.persona!!.apellido)
                                return false
                            }
                        }
                    }
                }
            }
        }
        return true
    }

    private fun mostrarInformacion() {
        binding.tvTitular.text = preguntaDeclaracionJuradaViewModel.pregunta.value!!.descripcion
        binding.tvTituloBloque.text = preguntaDeclaracionJuradaViewModel.pregunta.value!!.titulo
        preguntaDeclaracionJuradaViewModel.respuesta.value!!.respuestaPatologia!!
                .forEachIndexed { _, respuestaPatologia ->
                    agregarPatologia(respuestaPatologia)
                }
    }

    private fun obtenerParametros() {
        expediente = preguntaDeclaracionJuradaViewModel.obtenerExpediente()
        preguntaDeclaracionJuradaViewModel.pregunta.value = arguments!!
                .getSerializable(BUNDLE_PREGUNTA) as PreguntaDeclaracionJurada
        preguntaDeclaracionJuradaViewModel.siguientePregunta.value = arguments!!
                .getInt(BUNDLE_SIGUIENTE_PREGUNTA)
        preguntaDeclaracionJuradaViewModel.preguntaActual.value =
                preguntaDeclaracionJuradaViewModel.siguientePregunta.value!! - 1
    }

    private fun mostrarSnackBar(mensaje: String, color: Int, duracion: SnackbarEnum = SnackbarEnum.CORTO) {
        snackbar.setMensajePersonalizado(mensaje)
        snackbar.setearColores(ContextCompat.getColor(activity!!.applicationContext, color))
        snackbar.setDuration(duracion)
        snackbar.setAccion(baseActivity.getString(R.string.vacio_text), snackbarAccion())
        snackbar.show()
    }

    private fun snackbarAccion(): Snackbar.Callback {
        return object : Snackbar.Callback() {
            override fun onDismissed(s: Snackbar?, event: Int) {
                snackbar = SnackbarHelper(s!!.view, null)
            }
        }
    }

    inner class Listeners(respuesta: RespuestaPatologia, binding: OpcionPreguntaItemLayoutBinding) {
        private var respuestaPatologia = respuesta
        private var bindingNuevo = binding

        fun onCheckedChange(view: View, checked: Boolean) {
            if (checked) {
                if (respuestaPatologia.valor != checked) {
                    respuestaPatologia.valor = true
                    bindingNuevo.tvValor.text = baseActivity.getString(R.string.si_action)
                    val personasBottomSheet: PersonasBottomSheet = PersonasBottomSheet.newInstance(
                            preguntaDeclaracionJuradaViewModel.listaPersonas.value!!,
                            respuestaPatologia
                    )
                    personasBottomSheet.show(activity!!.supportFragmentManager, "bottom_sheet")
                    personasBottomSheet.setCallbackListener(onActionSheetDismiss, bindingNuevo)
                }
            } else {
                respuestaPatologia.valor = false
                bindingNuevo.tvValor.text = baseActivity.getString(R.string.no_action)
                bindingNuevo.tvBadge.visibility = View.GONE
                respuestaPatologia.personaPatologia = null
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }
}