package com.jerarquicos.futurossocios.view.declaracionJurada

import androidx.lifecycle.MutableLiveData
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.view.models.PersonaBasicoModel
import javax.inject.Inject

class PreguntaDeclaracionJuradaViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository
) : DeclaracionJuradaViewModel(futurosSociosRepository) {

    var siguientePregunta: MutableLiveData<Int> = MutableLiveData()
    var listaPersonas: MutableLiveData<ArrayList<PersonaPatologia>> = MutableLiveData()

    fun cargarPersonas() {
        listaPersonas.value = ArrayList()
        val personaPatologiaTitular = PersonaPatologia()

        personaPatologiaTitular.persona = nuevaPersonaBasico(expediente.value!!.titular!!.id,
                expediente.value!!.titular!!.dni!!,expediente.value!!.titular!!.nombre!!,
                expediente.value!!.titular!!.apellido!!)
        listaPersonas.value!!.add(personaPatologiaTitular)

        if (expediente.value!!.conviviente != null) {
            val personaPatologiaConviviente = PersonaPatologia()
            personaPatologiaConviviente.persona = nuevaPersonaBasico(expediente.value!!.conviviente!!.id,
                expediente.value!!.conviviente!!.dni!!, expediente.value!!.conviviente!!.nombre!!,
                    expediente.value!!.conviviente!!.apellido!!)
            listaPersonas.value!!.add(personaPatologiaConviviente)
        }

        if (expediente.value!!.personaDependiente != null && expediente.value!!.personaDependiente!!.isNotEmpty()) {
            expediente.value!!.personaDependiente!!.forEach { personaDependiente ->
                val personaPatologiaDependiente = PersonaPatologia()
                personaPatologiaDependiente.persona = nuevaPersonaBasico(personaDependiente.id,
                        personaDependiente.dni!!, personaDependiente.nombre!!, personaDependiente.apellido!!)
                listaPersonas.value!!.add(personaPatologiaDependiente)
            }
        }
    }

    private fun nuevaPersonaBasico(id: Int, dni: String, nombre: String, apellido: String): PersonaBasicoModel {
        val personaBasico = PersonaBasicoModel()
        personaBasico.id = id
        personaBasico.dni = dni
        personaBasico.nombre = nombre
        personaBasico.apellido = apellido
        return personaBasico
    }
}