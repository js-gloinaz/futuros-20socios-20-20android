package com.jerarquicos.futurossocios.view.declaracionJurada

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.OpcionPreguntaTipo2ItemLayoutBinding
import com.jerarquicos.futurossocios.databinding.PreguntaTipoDosFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.PersonaPatologia
import com.jerarquicos.futurossocios.db.entity.PreguntaDeclaracionJurada
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class PreguntaTipoDosFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	@Inject
	lateinit var usuarioHelper: UsuarioHelper
	lateinit var expediente: Expediente
	private var siguientePregunta: Int = 0
	lateinit var snackbar: SnackbarHelper
	private var mensajeError: String = ""
	private lateinit var preguntaTipoDosViewModel: PreguntaTipoDosViewModel
	lateinit var binding: PreguntaTipoDosFragmentBinding
	
	companion object {
		private const val BUNDLE_PREGUNTA = "BUNDLE_PREGUNTA"
		private const val BUNDLE_SIGUIENTE_PREGUNTA = "BUNDLE_SIGUIENTE_PREGUNTA"
		
		fun newInstance(pregunta: PreguntaDeclaracionJurada, siguientePregunta: Int): PreguntaTipoDosFragment {
			val args = Bundle()
			args.putSerializable(BUNDLE_PREGUNTA, pregunta)
			args.putInt(BUNDLE_SIGUIENTE_PREGUNTA, siguientePregunta)
			val fragment = PreguntaTipoDosFragment()
			fragment.arguments = args
			return fragment
		}
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.pregunta_tipo_dos_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		inicializar(binding.toolbar, javaClass.name, true)
		
		preguntaTipoDosViewModel = ViewModelProviders.of(this, viewModelFactory)
				.get(PreguntaTipoDosViewModel::class.java)
		binding.viewModel = preguntaTipoDosViewModel
		
		snackbar = SnackbarHelper(this.view!!, null)
		
		expediente = preguntaTipoDosViewModel.obtenerExpediente()
		obtenerParametros()
		preguntaTipoDosViewModel.cargarPersonas()
		preguntaTipoDosViewModel.cargarRespuesta()
		mostrarInformacion()
		
		binding.btConfirmar.setOnClickListener {
			if (!respuestasValidas()) {
				mostrarSnackBar(mensajeError, R.color.colorNaranja)
			}
			else {
				val fragment = baseActivity.dealerViewModel.analizarMostrarPreguntasDeclaracionJurada(
						siguientePregunta,
						preguntaTipoDosViewModel.respuesta.value!!
				)
				navigationController.navegarSiguienteFragment(fragment)
			}
		}
	}
	
	private fun agregarPersona(personaPatologia: PersonaPatologia, poseePatologia: Boolean) {
		val inflater = LayoutInflater.from(context)
		val bindingNuevo: OpcionPreguntaTipo2ItemLayoutBinding = DataBindingUtil.inflate(
				inflater, R.layout.opcion_pregunta_tipo2_item_layout, null, true)
		bindingNuevo.tvNombre.text =
				getString(R.string.nombre_persona_text, personaPatologia.persona!!.nombre, personaPatologia.persona!!.apellido)
		bindingNuevo.etDescripcion.hint = preguntaTipoDosViewModel
				.respuesta.value?.respuestaPatologia!![0].patologia?.leyendaObservacion
		bindingNuevo.valor = poseePatologia
		bindingNuevo.listeners = Listeners(personaPatologia, bindingNuevo)
		
		if (poseePatologia) {
			bindingNuevo.valor = true
			bindingNuevo.tvValor.text = getString(R.string.si_action)
			bindingNuevo.etDescripcion.visibility = View.VISIBLE
			bindingNuevo.descripcion = personaPatologia.observacion
		}
		else {
			bindingNuevo.valor = false
			bindingNuevo.tvValor.text = getString(R.string.no_action)
			bindingNuevo.descripcion = ""
			bindingNuevo.etDescripcion.visibility = View.GONE
		}
		
		bindingNuevo.etDescripcion.addTextChangedListener(object : TextWatcher {
			override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
				//not implementing
			}
			override fun afterTextChanged(s: Editable?) {
				personaPatologia.observacion = s.toString()
			}
			
			override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
				if (!bindingNuevo.etDescripcion.text.toString().isEmpty()) {
					personaPatologia.observacion = s!!.toString()
				}
				else {
					personaPatologia.observacion = ""
				}
			}
		})
		binding.llPatologias.addView(bindingNuevo.root)
	}
	
	private fun mostrarInformacion() {
		binding.tvTitular.text = preguntaTipoDosViewModel.pregunta.value!!.descripcion
		if (preguntaTipoDosViewModel.respuesta.value?.respuestaPatologia!![0].personaPatologia == null) {
			preguntaTipoDosViewModel.respuesta.value?.respuestaPatologia!![0].personaPatologia = ArrayList()
		}
		preguntaTipoDosViewModel.listaPersonas.value!!.forEach { personaDelTotal ->
			val indicePersonaConPatologia = indicePersonaConPatologia(personaDelTotal)
			if (indicePersonaConPatologia > -1) {
				agregarPersona(preguntaTipoDosViewModel.respuesta.value!!
						.respuestaPatologia!![0].personaPatologia!![indicePersonaConPatologia], true)
			}
			else {
				agregarPersona(personaDelTotal, false)
			}
		}
	}
	
	private fun obtenerParametros() {
		preguntaTipoDosViewModel.pregunta.value = arguments!!.getSerializable(BUNDLE_PREGUNTA)
				as PreguntaDeclaracionJurada
		siguientePregunta = arguments!!.getInt(BUNDLE_SIGUIENTE_PREGUNTA)
		preguntaTipoDosViewModel.preguntaActual.value = siguientePregunta - 1
	}
	
	private fun respuestasValidas(): Boolean {
		preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!!.forEach { respuestaPatologia ->
			if (respuestaPatologia.patologia!!.requiereObservacion!!) {
				respuestaPatologia.personaPatologia!!.forEach { persona ->
					if (persona.observacion.isNullOrEmpty()) {
						mensajeError = activity!!.resources.getString(R.string.observacion_patologia_vacia_error,
								persona.persona!!.nombre, persona.persona!!.apellido)
						return false
					}
				}
			}
		}
		return true
	}
	
	private fun indicePersonaConPatologia(personaDelTotal: PersonaPatologia): Int {
		preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0].personaPatologia!!
				.forEachIndexed { index, personaConPatologia ->
					if (personaConPatologia.persona!!.dni == personaDelTotal.persona!!.dni) {
						return index
					}
				}
		return -1
	}
	
	private fun mostrarSnackBar(mensaje: String, color: Int, duracion: SnackbarEnum = SnackbarEnum.CORTO) {
		snackbar.setMensajePersonalizado(mensaje)
		snackbar.setearColores(ContextCompat.getColor(activity!!.applicationContext, color))
		snackbar.setDuration(duracion)
		snackbar.setAccion(getString(R.string.vacio_text), snackbarAccion())
		snackbar.show()
	}
	
	inner class Listeners(persona: PersonaPatologia, binding: OpcionPreguntaTipo2ItemLayoutBinding) {
		private var personaPatologia = persona
		private var bindingNuevo = binding
		
		fun onCheckedChange(checked: Boolean) {
			if (checked) {
				bindingNuevo.valor = true
				var personaAnadida = false
				preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0].personaPatologia!!
						.forEachIndexed { index, personaConPatologia ->
							if (personaConPatologia.persona!!.dni == personaPatologia.persona!!.dni) {
								preguntaTipoDosViewModel.respuesta.value!!
										.respuestaPatologia!![0].personaPatologia!![index] = personaPatologia
								personaAnadida = true
							}
						}
				if (!personaAnadida) {
					preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0]
							.personaPatologia!!.add(personaPatologia)
				}
				
				if (preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0]
								.patologia!!.requiereObservacion!!) {
					bindingNuevo.etDescripcion.visibility = View.VISIBLE
					bindingNuevo.tvValor.text = getString(R.string.si_action)
				}
				preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0].valor = true
			}
			else {
				bindingNuevo.valor = false
				bindingNuevo.tvValor.text = getString(R.string.no_action)
				bindingNuevo.descripcion = ""
				bindingNuevo.etDescripcion.visibility = View.GONE
				personaPatologia.observacion = null
				preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0].valor = false
				preguntaTipoDosViewModel.respuesta.value!!.respuestaPatologia!![0]
						.personaPatologia!!.remove(personaPatologia)
			}
		}
	}
	
	private fun snackbarAccion(): Snackbar.Callback {
		return object : Snackbar.Callback() {
			override fun onDismissed(s: Snackbar?, event: Int) {
				snackbar = SnackbarHelper(s!!.view, null)
			}
		}
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_base, menu)
	}
}