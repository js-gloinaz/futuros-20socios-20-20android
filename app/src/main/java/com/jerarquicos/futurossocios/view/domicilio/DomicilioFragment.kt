package com.jerarquicos.futurossocios.view.domicilio

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.DomicilioFragmentBinding
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Domicilio
import com.jerarquicos.futurossocios.db.entity.LocalidadPlano
import com.jerarquicos.futurossocios.db.entity.Provincia
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.AppConstants
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.models.OpcionModel
import javax.inject.Inject

class DomicilioFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var baseActivity: BaseActivity
    lateinit var binding: DomicilioFragmentBinding
    private var domicilio: Domicilio? = null
    private lateinit var domicilioViewModel: DomicilioViewModel
    private lateinit var validadorCalle: EditTextValidator
    private lateinit var validadorNumero: EditTextValidator
    private lateinit var validadorLocalidad: EditTextValidator
    private lateinit var validadorProvincia: EditTextValidator
    private lateinit var codigoPostal: String
    private lateinit var localidades: ArrayList<LocalidadPlano>
    private var esProvincia: Boolean = true

    companion object {
        private const val BUNDLE_DOMICILIO = "BUNDLE_DOMICILIO"

        fun newInstance(domicilio: Domicilio?): DomicilioFragment {
            val fragment = DomicilioFragment()
            val args = Bundle()
            if (domicilio != null) {
                args.putSerializable(BUNDLE_DOMICILIO, domicilio)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.domicilio_fragment, container, false)
        FirebaseAnalytics.getInstance(context!!)
                .setCurrentScreen(this.activity!!, this.javaClass.simpleName, this.javaClass.simpleName)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        baseActivity.ocultarTeclado()

        domicilioViewModel = ViewModelProviders.of(this, viewModelFactory).get(DomicilioViewModel::class.java)
        binding.viewModel = domicilioViewModel

        obtenerParametros()
        inicializarListeners()
        iniciarValidaciones()
    }

    private fun obtenerParametros(){
        if (arguments!!.containsKey(DomicilioFragment.BUNDLE_DOMICILIO) && domicilio == null) {
            domicilio = arguments!!.getSerializable(BUNDLE_DOMICILIO) as Domicilio
            domicilioViewModel.domicilioModel.value = domicilio
        }

        if ((domicilio != null) && (domicilio?.calle?.equals(context!!.resources.getString(R.string.vacio_text))!!)) {
            domicilio?.localidad = null
            domicilio?.localidad?.provincia = null
        }
    }

    private fun inicializarListeners() {
        binding.etProvincia.setOnClickListener {
            domicilioViewModel.obtenerProvincias().observe(this, Observer<EstadoRespuesta<DatosIniciales>>
            { this.handleResponse(it) })
            baseActivity.mostrarProgressBar()
            esProvincia = true
            domicilioViewModel.limpiarLocalidad()

            if (Build.VERSION.SDK_INT >= 21) {
                binding.etProvincia.showSoftInputOnFocus = false
            } else {
                binding.etProvincia.setRawInputType(InputType.TYPE_CLASS_TEXT)
                binding.etProvincia.setTextIsSelectable(true)
            }
        }

        binding.etLocalidad.setOnClickListener {
            if (validarSeleccionProvincia()) {
                domicilioViewModel.obtenerLocalidades().observe(this, Observer<EstadoRespuesta<DatosIniciales>>
                { this.handleResponseLocalidad(it) })
                baseActivity.mostrarProgressBar()
                esProvincia = false
            }
                if (Build.VERSION.SDK_INT >= 21) {
                    binding.etLocalidad.showSoftInputOnFocus = false
                } else {
                    binding.etLocalidad.setRawInputType(InputType.TYPE_CLASS_TEXT)
                    binding.etLocalidad.setTextIsSelectable(true)
                }
        }

        binding.btAccion.setOnClickListener {
            if (validarDatos()) {
                val domicilioFinal = domicilioViewModel.crearDomicilio()
                targetFragment!!.onActivityResult(
                        targetRequestCode,
                        Activity.RESULT_OK,
                        activity!!.intent.putExtra("domicilio", domicilioFinal))
                fragmentManager!!.popBackStack()
            }
        }
    }

    private fun handleResponse(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        procesarListaProvincias(respuesta.data.provincia)
                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    private fun handleResponseLocalidad(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        localidades = respuesta.data.localidad
                        procesarListaLocalidad(localidades, domicilioViewModel.domicilioModel.value?.
                                localidad?.provincia?.id!!)
                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    private fun procesarListaProvincias(provincias: ArrayList<Provincia>) {
        val listaOpciones = ArrayList<OpcionModel>()
        for (item in provincias) {
            listaOpciones.add(OpcionModel(item.id!!, item.descripcion!!, "", false))
        }
        navigationController.navegarListaResultadoFragment(this, listaOpciones)
    }

    private fun procesarListaLocalidad(localidad: ArrayList<LocalidadPlano>, provinciaId: Int) {
        val listaOpcionesLocalidades = ArrayList<OpcionModel>()
        for (item in localidad) {
            if (provinciaId == item.idProvincia) {
                listaOpcionesLocalidades.add(OpcionModel(item.id!!, item.descripcion!!, "", false))
            }
        }
        navigationController.navegarListaResultadoFragment(this, listaOpcionesLocalidades)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == AppConstants.REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val descripcionDato = data?.getStringExtra("descripcion_seleccionada")!!
            val idDato = data.getIntExtra("id_seleccionado", 0)
            if (esProvincia) {
                domicilioViewModel.domicilioModel.value?.localidad?.provincia?.id = idDato
                domicilioViewModel.guardarResultadoProvinciaSeleccionada(idDato, descripcionDato)
            } else {
                for (item in localidades) {
                    if (idDato == item.id) {
                        codigoPostal = item.codigoPostal!!
                    }
                }
                domicilioViewModel.guardarResultadoLocalidadSeleccionada(idDato, descripcionDato, codigoPostal)
            }
        }
    }

    private fun iniciarValidaciones() {
        validadorCalle = EditTextValidator(binding.tiCalle)
        validadorNumero = EditTextValidator(binding.tiNumero)
        validadorProvincia = EditTextValidator(binding.tiProvincia)
        validadorLocalidad = EditTextValidator(binding.tiLocalidad)
    }

    private fun validarDatos(): Boolean {
        val valCalle = validadorCalle.esNoVacio()
        val valNumero = validadorNumero.esNoVacio()
        val valProvincia: Boolean = validadorProvincia.esVacio()
        val valLocalidad: Boolean = validadorLocalidad.esVacio()
        return valCalle && valNumero && valProvincia && valLocalidad
    }

    private fun validarSeleccionProvincia(): Boolean {
        return validadorProvincia.esVacio()
    }
}