package com.jerarquicos.futurossocios.view.domicilio

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Domicilio
import com.jerarquicos.futurossocios.db.entity.Localidad
import com.jerarquicos.futurossocios.db.entity.Provincia
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class DomicilioViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {

    private var descripcionProvincia: MutableLiveData<String> = MutableLiveData()
    var domicilioModel: MutableLiveData<Domicilio> = MutableLiveData()

    init {
        domicilioModel.value = Domicilio(1)
    }

    fun limpiarLocalidad() {
        domicilioModel.value?.localidad = null
    }

    fun guardarResultadoProvinciaSeleccionada(id: Int, descripcion: String) {
        domicilioModel.value?.localidad = Localidad()
        domicilioModel.value?.localidad?.provincia = Provincia()
        domicilioModel.value?.localidad?.provincia?.id = id
        domicilioModel.value?.localidad?.provincia?.descripcion = descripcion
    }

    fun guardarResultadoLocalidadSeleccionada(id: Int, descripcion: String, cp: String) {
        if (domicilioModel.value?.localidad == null) {
            domicilioModel.value?.localidad = Localidad()
        }
        domicilioModel.value?.localidad?.id = id
        domicilioModel.value?.localidad?.descripcion = descripcion
        domicilioModel.value?.localidad?.codigoPostal = cp
    }

    fun obtenerProvincias(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun obtenerLocalidades(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun crearDomicilio(): Domicilio {
        descripcionProvincia.value = domicilioModel.value?.localidad?.provincia?.descripcion
        if (domicilioModel.value?.piso == null) {
            domicilioModel.value?.piso = ""
        }
        if (domicilioModel.value?.departamento == null) {
            domicilioModel.value?.departamento = ""
        }

        if (domicilioModel.value?.id == null) {
            domicilioModel.value = Domicilio(1, 0,
                    domicilioModel.value?.calle!!,
                    domicilioModel.value?.numero!!,
                    domicilioModel.value?.piso!!,
                    domicilioModel.value?.departamento,
                    domicilioModel.value?.localidad,
                    false)
        } else {
            domicilioModel.value = Domicilio(1, domicilioModel.value?.id,
                    domicilioModel.value?.calle!!,
                    domicilioModel.value?.numero!!,
                    domicilioModel.value?.piso!!,
                    domicilioModel.value?.departamento,
                    domicilioModel.value?.localidad,
                    false)
        }
        return domicilioModel.value!!
    }
}