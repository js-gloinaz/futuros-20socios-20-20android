package com.jerarquicos.futurossocios.view.empleador

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.DatosEmpleadorFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Domicilio
import com.jerarquicos.futurossocios.db.entity.Empleador
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import javax.inject.Inject

class DatosEmpleadorFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    lateinit var binding: DatosEmpleadorFragmentBinding
    private lateinit var datosEmpleadorViewModel: DatosEmpleadorViewModel
    private lateinit var validadorNombre: EditTextValidator
    private lateinit var validadorTelefono: EditTextValidator
    private lateinit var validadorEmail: EditTextValidator
    private lateinit var validadorDomicilio: EditTextValidator
    private lateinit var validadorTelefonoCodInt: EditTextValidator
    private lateinit var validadorTelefonoCodArea: EditTextValidator
    private lateinit var validadorTelefonoNumero: EditTextValidator

    companion object {
        private const val BUNDLE_EMPLEADOR = "BUNDLE_EMPLEADOR"
        private const val BUNDLE_TIPO_DOCUMENTACION = "BUNDLE_TIPO_DOCUMENTACION"

        fun newInstance(empleador: Empleador?, tipoDocumentacion: Int): DatosEmpleadorFragment {
            val fragment = DatosEmpleadorFragment()
            val args = Bundle()
            if (empleador != null) {
                args.putSerializable(BUNDLE_EMPLEADOR, empleador)
            }
            args.putInt(BUNDLE_TIPO_DOCUMENTACION, tipoDocumentacion)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.datos_empleador_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        datosEmpleadorViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(DatosEmpleadorViewModel::class.java)
        binding.viewModel = datosEmpleadorViewModel

        datosEmpleadorViewModel.tipoDocumentacion.value = arguments!!.getInt(BUNDLE_TIPO_DOCUMENTACION)


        obtenerParametros()
        inicializarListeners()
        datosEmpleadorViewModel.setearCodigosInternacionales(context!!)
        iniciarValidaciones()
    }

    private fun obtenerParametros(){
        val empleadorAux = arguments!!.getSerializable(BUNDLE_EMPLEADOR)
        if (empleadorAux != null) {
            val empleador = empleadorAux as Empleador
            if (empleador.domicilio?.calle.equals(context!!.resources.getString(R.string.vacio_text)) ||
                    empleador.domicilio?.numero.equals(context!!.resources.getString(R.string.vacio_text))) {
                datosEmpleadorViewModel.domicilio.value?.localidad = null
                datosEmpleadorViewModel.domicilio.value?.localidad?.provincia = null
            }
            datosEmpleadorViewModel.domicilio.value = empleador.domicilio
            datosEmpleadorViewModel.datosEmpleadorModel.value = empleador
            datosEmpleadorViewModel.setearDomicilio(empleador.domicilio!!)
        }
    }

    private fun inicializarListeners() {
        binding.btCancelar.setOnClickListener {
            val fragment = baseActivity.dealerViewModel
                    .obtenerPasoSiguienteDatosEmpleadorFragment(null,
                            datosEmpleadorViewModel.tipoDocumentacion.value!!)
            navigationController.navegarSiguienteFragment(fragment)
        }

        binding.etDomicilio.setOnClickListener {
            if (Build.VERSION.SDK_INT >= 21) {
                binding.etDomicilio.showSoftInputOnFocus = false
            } else {
                binding.etDomicilio.setRawInputType(InputType.TYPE_CLASS_TEXT)
                binding.etDomicilio.setTextIsSelectable(true)
            }
            navigationController.navegarDomicilioFragment(this, datosEmpleadorViewModel.domicilio.value)
        }

        binding.btAccion.setOnClickListener {
            if (validarDatos()) {
                datosEmpleadorViewModel.guardarEmpleador(datosEmpleadorViewModel.domicilio.value!!,
                        datosEmpleadorViewModel.datosEmpleadorModel.value, binding.etCodInt.text.toString(),
                        binding.etCodArea.text.toString(), binding.etNumero.text.toString())
                val fragment = baseActivity.dealerViewModel
                        .obtenerPasoSiguienteDatosEmpleadorFragment(datosEmpleadorViewModel.datosEmpleadorModel.value!!,
                                datosEmpleadorViewModel.tipoDocumentacion.value!!)
                // Sincronizo con el servidor
                baseActivity.sincronizarExpediente(true, false)
                // Ejecuto próxima vista
                navigationController.navegarSiguienteFragment(fragment)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        datosEmpleadorViewModel.domicilio.value = data?.getSerializableExtra("domicilio") as Domicilio
        datosEmpleadorViewModel.setearDomicilio(datosEmpleadorViewModel.domicilio.value!!)
    }

    private fun iniciarValidaciones() {
        validadorNombre = EditTextValidator(binding.tiNombre)
        validadorTelefono = EditTextValidator(binding.tiCodInt)
        validadorTelefono = EditTextValidator(binding.tiCodArea)
        validadorTelefono = EditTextValidator(binding.tiNumero)
        validadorEmail = EditTextValidator(binding.tiEmail)
        validadorDomicilio = EditTextValidator(binding.tiDomicilio)
        validadorTelefonoCodInt = EditTextValidator(binding.tiCodInt)
        validadorTelefonoCodArea = EditTextValidator(binding.tiCodArea)
        validadorTelefonoNumero = EditTextValidator(binding.tiNumero)
    }

    private fun validarDatos(): Boolean {
        var hayError = true
        hayError = validadorNombre.esNombreEmpleadorValido() && hayError
        hayError = validadorEmail.esEmailUOmitir() && hayError
        hayError = validadorDomicilio.esNoVacio() && hayError
        hayError = validadorTelefonoCodInt
                .esCodInternacionalNoObligatorio(binding.etCodArea.text.toString(),
                        binding.etNumero.text.toString()) && hayError
        hayError = validadorTelefonoCodArea
                .esCodAreaNumeroNoObligatorio(binding.etNumero.text.toString()) && hayError
        hayError = validadorTelefonoNumero
                .esNumeroAreaNoObligatorioParticular(binding.etCodArea.text.toString()) && hayError

        return hayError
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_oscuro, menu)
    }
}