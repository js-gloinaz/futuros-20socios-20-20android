package com.jerarquicos.futurossocios.view.empleador

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.Domicilio
import com.jerarquicos.futurossocios.db.entity.Empleador
import com.jerarquicos.futurossocios.db.entity.Localidad
import com.jerarquicos.futurossocios.db.entity.Telefono
import javax.inject.Inject

class DatosEmpleadorViewModel @Inject constructor() : ViewModel() {

    var datosEmpleadorModel: MutableLiveData<Empleador> = MutableLiveData()
    var descripcionDomicilio: MutableLiveData<String> = MutableLiveData()
    var domicilio: MutableLiveData<Domicilio> = MutableLiveData()
    var tipoDocumentacion: MutableLiveData<Int> = MutableLiveData()

    init {
        datosEmpleadorModel.value = Empleador()
    }

    fun guardarEmpleador(modeloDomicilio: Domicilio,modeloEmpleador: Empleador?, codigoInternacional: String?,
                         codigoArea: String?, numero: String?) {

        if (modeloDomicilio.id == null) {
            val domicilio = Domicilio(1, 0, modeloDomicilio.calle, modeloDomicilio.numero,
                    modeloDomicilio.piso, modeloDomicilio.departamento,
                    Localidad(modeloDomicilio.localidad?.id, modeloDomicilio.localidad?.descripcion,
                            modeloDomicilio.localidad?.codigoPostal, modeloDomicilio.localidad?.provincia),
                    null)
            datosEmpleadorModel.value?.domicilio = domicilio
        } else {
            val domicilio = Domicilio(1, modeloDomicilio.id, modeloDomicilio.calle, modeloDomicilio.numero,
                    modeloDomicilio.piso, modeloDomicilio.departamento,
                    Localidad(modeloDomicilio.localidad?.id, modeloDomicilio.localidad?.descripcion,
                            modeloDomicilio.localidad?.codigoPostal, modeloDomicilio.localidad?.provincia),
                    null)
            datosEmpleadorModel.value?.domicilio = domicilio
        }

        if (numero != null) {
            datosEmpleadorModel.value?.telefono?.codigoInternacional = codigoInternacional
            datosEmpleadorModel.value?.telefono?.codigoArea = codigoArea
            datosEmpleadorModel.value?.telefono?.numero = numero
        }
        if (numero?.equals("")!!) {
            datosEmpleadorModel.value?.telefono = null
        }

        if(datosEmpleadorModel.value?.id != 0) {
            datosEmpleadorModel.value = Empleador(datosEmpleadorModel.value?.id,
                    datosEmpleadorModel.value?.denominacion, datosEmpleadorModel.value?.cuit,
                    datosEmpleadorModel.value?.cargo, datosEmpleadorModel.value?.sueldoBruto,
                    datosEmpleadorModel.value?.fechaInicio, datosEmpleadorModel.value?.telefono,
                    datosEmpleadorModel.value?.email, datosEmpleadorModel.value?.domicilio)
        }

    }

    fun setearCodigosInternacionales(context: Context) {
        if (datosEmpleadorModel.value?.telefono?.id == null) {
            datosEmpleadorModel.value?.telefono = Telefono(0, context
                    .getString(R.string.codigo_internacional_argentina_text), null, null)
        }
    }


    fun setearDomicilio(domicilio: Domicilio) {
        val mostrarCalleNum = domicilio.calle + " " + domicilio.numero
        var mostrarPiso = ""
        var mostrarDepto = ""
        if (!domicilio.piso.equals("")) {
            mostrarPiso = " - Piso: " + domicilio.piso
        }
        if (!domicilio.departamento.equals("")) {
            mostrarDepto = " - Depto: " + domicilio.departamento
        }
        descripcionDomicilio.value = mostrarCalleNum + mostrarPiso + mostrarDepto

    }
}