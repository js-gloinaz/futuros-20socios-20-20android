package com.jerarquicos.futurossocios.view.expedientesEnviados

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.view.base.DealerViewModel
import com.jerarquicos.futurossocios.view.models.ExpedienteEnviadoModel
import kotlinx.android.synthetic.main.item_expediente_enviado_layout.view.*
import java.text.SimpleDateFormat
import java.util.*

class ExpedienteEnviadoAdapter(private val mOpcionCallback: (ExpedienteEnviadoModel) -> Unit)
	: RecyclerView.Adapter<ExpedienteEnviadoAdapter.ViewHolder>() {
	private lateinit var dealer: DealerViewModel
	private var opcionesLista: MutableList<ExpedienteEnviadoModel> = ArrayList()
	
	override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
		val vista = LayoutInflater.from(parent.context).inflate(R.layout.item_expediente_enviado_layout,
				parent, false)
		return ViewHolder(vista)
	}
	
	override fun getItemCount(): Int {
		return opcionesLista.size
	}
	
	@SuppressLint("SetTextI18n")
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val expediente = opcionesLista[position]
		val format = SimpleDateFormat("dd/MM/yyy", Locale.getDefault())
		
		holder.itemView.tvItem.text = holder.itemView.context.resources.getString(R.string.nombre_persona_text,
				expediente.nombre, expediente.apellido)
		holder.itemView.tvFechaEnvio.text = holder.itemView.context.resources.getString(R.string.enviado_text, format
				.format(expediente.fechaActualizacion))
		
		holder.itemView.setOnClickListener { mOpcionCallback(expediente) }
	}
	
	override fun getItemId(position: Int): Long {
		return position.toLong()
	}
	
	override fun getItemViewType(position: Int): Int {
		return position
	}
	
	fun setData(mutableList: MutableList<ExpedienteEnviadoModel>, d: DealerViewModel) {
		dealer = d
		opcionesLista = mutableList
		notifyDataSetChanged()
	}
	
	inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}