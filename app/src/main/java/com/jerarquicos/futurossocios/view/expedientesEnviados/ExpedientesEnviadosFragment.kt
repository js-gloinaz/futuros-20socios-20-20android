package com.jerarquicos.futurossocios.view.expedientesEnviados

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Browser
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.BuildConfig
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ExpedientesEnviadosFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import com.jerarquicos.futurossocios.view.models.ExpedienteEnviadoModel
import javax.inject.Inject

class ExpedientesEnviadosFragment : BaseFragment(), Injectable {
    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper

    private lateinit var expedientesEnviadosViewModel: ExpedientesEnviadosViewModel
    private lateinit var binding: ExpedientesEnviadosFragmentBinding
    private var adaptador: ExpedienteEnviadoAdapter? = null
    var layoutManager: RecyclerView.LayoutManager? = null

    companion object {
        private const val BUNDLE_LISTA_ENVIADOS = "BUNDLE_LISTA_ENVIADOS"

        fun newInstance(listaExpedientesEnviados: ArrayList<ExpedienteEnviadoModel>): ExpedientesEnviadosFragment {
            val args = Bundle()
            args.putSerializable(BUNDLE_LISTA_ENVIADOS, listaExpedientesEnviados)
            val fragment = ExpedientesEnviadosFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil
                .inflate(inflater, R.layout.expedientes_enviados_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        expedientesEnviadosViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ExpedientesEnviadosViewModel::class.java)
        binding.viewModel = expedientesEnviadosViewModel

        setListaExpedientesPendientes()
        obtenerParametros()
        mostrarInformacion()
    }

    private fun setListaExpedientesPendientes() {

        binding.rvItems.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this.context)
        binding.rvItems.layoutManager = layoutManager
        adaptador = ExpedienteEnviadoAdapter { mOpcionModel: ExpedienteEnviadoModel ->
            val url = "${BuildConfig.API_URL}Expedientes/generarreporte/${mOpcionModel.id}"
            val token = "Bearer ${usuarioHelper.getToken()!!.value}"
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            val bundle = Bundle()
            bundle.putString("Authorization", token)
            browserIntent.putExtra(Browser.EXTRA_HEADERS, bundle)
            startActivity(browserIntent)
        }
        binding.rvItems.adapter = adaptador
    }

    private fun obtenerParametros() {
        expedientesEnviadosViewModel.listaExpedientesEnviados.value = arguments!!
                .getSerializable(BUNDLE_LISTA_ENVIADOS) as ArrayList<ExpedienteEnviadoModel>
    }

    private fun mostrarInformacion() {
        if (expedientesEnviadosViewModel.listaExpedientesEnviados.value.isNullOrEmpty()) {
            binding.tvSinExpedientes.visibility = View.VISIBLE
        } else {
            binding.tvSinExpedientes.visibility = View.GONE
            adaptador!!.setData(expedientesEnviadosViewModel.listaExpedientesEnviados.value
                    as MutableList<ExpedienteEnviadoModel>, baseActivity.dealerViewModel)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }
}