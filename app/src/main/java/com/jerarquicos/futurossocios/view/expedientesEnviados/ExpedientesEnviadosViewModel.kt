package com.jerarquicos.futurossocios.view.expedientesEnviados

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.view.models.ExpedienteEnviadoModel
import javax.inject.Inject

class ExpedientesEnviadosViewModel @Inject constructor() : ViewModel() {
    var listaExpedientesEnviados: MutableLiveData<ArrayList<ExpedienteEnviadoModel>> = MutableLiveData()

}
