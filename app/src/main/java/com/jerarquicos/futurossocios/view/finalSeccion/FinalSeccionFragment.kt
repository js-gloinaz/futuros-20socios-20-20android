package com.jerarquicos.futurossocios.view.finalSeccion

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.FinalSeccionFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import javax.inject.Inject

class FinalSeccionFragment : Fragment(), Injectable {

    companion object {
        private const val BUNDLE_TITULO = "BUNDLE_TITULO"
        private const val BUNDLE_TEXT_BOTON = "BUNDLE_TEXT_BOTON"

        fun newInstance(titulo: Int, textoBoton: Int): FinalSeccionFragment {
            val args = Bundle()
            args.putInt(BUNDLE_TITULO, titulo)
            args.putInt(BUNDLE_TEXT_BOTON, textoBoton)
            val fragment = FinalSeccionFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity
    @Inject
    lateinit var navigationController: NavigationController
    lateinit var binding: FinalSeccionFragmentBinding

    lateinit var titulo: String
    private lateinit var descripcionBoton: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.final_seccion_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(this.activity!!,
                this.javaClass.simpleName, this.javaClass.simpleName)

        baseActivity.ocultarTeclado()

        obtenerParametros()

        binding.btAccion.text = descripcionBoton
        binding.tvTitulo.text = titulo

        binding.btAccion.setOnClickListener {
            // Sincronizo con el servidor
            baseActivity.sincronizarExpediente(true, true)
            // Ejecuto próxima vista
            navigationController.navegarDashboardFragment()

        }
    }

    private fun obtenerParametros() {
        titulo = getString(arguments!!.getInt(BUNDLE_TITULO))
        descripcionBoton = getString(arguments!!.getInt(BUNDLE_TEXT_BOTON))
    }
}