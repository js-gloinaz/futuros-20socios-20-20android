package com.jerarquicos.futurossocios.view.firmaGrafometrica

import android.graphics.Bitmap
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.FirmaGrafometricaFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.documentos.DocumentosHelper
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception
import javax.inject.Inject

class FirmaGrafometricaFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	private lateinit var firmaGrafometricaViewModel: FirmaGrafometricaViewModel
	lateinit var binding: FirmaGrafometricaFragmentBinding
	lateinit var snackbar: SnackbarHelper
	lateinit var expediente: Expediente
	lateinit var documentosHelper: DocumentosHelper
	private val PNG_FILE_PREFIX = ".png"
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.firma_grafometrica_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		inicializar(binding.toolbar, javaClass.name, false)
		binding.toolbar.setTitle(R.string.firma_grafometrica_title)
		
		firmaGrafometricaViewModel = ViewModelProviders.of(this, viewModelFactory)
				.get(FirmaGrafometricaViewModel::class.java)
		binding.viewModel = firmaGrafometricaViewModel
		
		documentosHelper = DocumentosHelper()
		expediente = firmaGrafometricaViewModel.obtenerExpediente()
		
		cargarFirma()
		
		binding.btCrearNuevaFirma.setOnClickListener {
			expediente.firmaGrafometrica = null
			binding.firma.visibility = View.VISIBLE
			binding.tvDescripcion.visibility = View.VISIBLE
			binding.tvFirmaRegistrada.visibility = View.GONE
			binding.btCrearNuevaFirma.visibility = View.GONE
			setHasOptionsMenu(true)
		}
		
		binding.btnGuardarFirma.setOnClickListener {
			if (expediente.firmaGrafometrica != null) {
				guardarFirmaExpediente(expediente.firmaGrafometrica!!)
			}
			else {
				if (binding.firma.isEmpty) {
					snackbar = SnackbarHelper(this.view!!, null)
					snackbar.mostrarSnackBar(activity!!.resources.getString(R.string.firma_vacia_error),
							R.color.colorNaranja)
				}
				else {
					guardarFirmaPNG(binding.firma.transparentSignatureBitmap)
					guardarFirmaExpediente(binding.firma.signatureSvg)
				}
			}
		}
	}
	
	private fun cargarFirma() {
		binding.firma.setMinWidth(2.0F)
		binding.firma.setMaxWidth(2.5F)
		
		if (expediente.firmaGrafometrica != null) {
			binding.tvFirmaRegistrada.visibility = View.VISIBLE
			binding.btCrearNuevaFirma.visibility = View.VISIBLE
			binding.firma.visibility = View.GONE
			binding.tvDescripcion.visibility = View.GONE
			setHasOptionsMenu(false)
		}
		else {
			binding.firma.visibility = View.VISIBLE
			binding.tvDescripcion.visibility = View.VISIBLE
			binding.tvFirmaRegistrada.visibility = View.GONE
			binding.btCrearNuevaFirma.visibility = View.GONE
			setHasOptionsMenu(true)
		}
	}
	
	private fun guardarFirmaExpediente(svg: String) {
		val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteFirmaGrafometricaFragment(svg)
		baseActivity.sincronizarExpediente(true, true)
		navigationController.navegarSiguienteFragment(fragment)
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_firma, menu)
	}
	
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when (item?.itemId) {
			R.id.btBorrarFirma -> {
				binding.firma.clear()
				return true
			}
			else -> {
				return super.onOptionsItemSelected(item)
			}
		}
	}
	
	private fun guardarFirmaPNG(bmp: Bitmap) {
		try {
			val imageFileName = getString(R.string.nombre_archivo_firma_text, expediente.id!!)
			val albumF = documentosHelper.obtenerDirectorioAlbum()
			val file = File.createTempFile(imageFileName, PNG_FILE_PREFIX, albumF)
			val fOut = FileOutputStream(file)
			bmp.compress(Bitmap.CompressFormat.PNG, 85, fOut)
			fOut.flush()
			fOut.close()
		} catch (e: Exception) {
			//Not implemented
		}
	}
}