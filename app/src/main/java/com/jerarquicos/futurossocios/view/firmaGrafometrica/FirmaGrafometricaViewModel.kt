package com.jerarquicos.futurossocios.view.firmaGrafometrica

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import java.lang.Exception
import javax.inject.Inject

class FirmaGrafometricaViewModel @Inject constructor(
		private val futurosSociosRepository: FuturosSociosRepository) : ViewModel() {
	var expediente: MutableLiveData<Expediente> = MutableLiveData()
	
	fun obtenerExpediente(): Expediente {
		expediente.value = futurosSociosRepository.obtenerExpediente()
		return expediente.value!!
	}
}