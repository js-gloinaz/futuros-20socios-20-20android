package com.jerarquicos.futurossocios.view.firmaGrafometrica

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.InformacionDocumentacionAnexaFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import javax.inject.Inject

class InformacionFirmaGrafometricaFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity
    @Inject
    lateinit var navigationController: NavigationController
    lateinit var binding: InformacionDocumentacionAnexaFragmentBinding
    private lateinit var descripcionTitulo: String
    private lateinit var descripcionDetalle: String
    private lateinit var descripcionBoton: String
    private var imagenId: Int = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil
                .inflate(inflater, R.layout.informacion_documentacion_anexa_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setHasOptionsMenu(true)

        baseActivity.ocultarTeclado()

        descripcionTitulo = getString(R.string.solo_falta_firma_title)
        descripcionDetalle = getString(R.string.descripcion_consentimiento_firma_text)
        descripcionBoton = getString(R.string.continuar_action)
        imagenId = R.drawable.ic_editar

        binding.btAccion.setOnClickListener {
            val fragment = baseActivity.dealerViewModel
                    .obtenerPasoSiguienteInformacionFirmaGrafometricaFragment()
            navigationController.navegarSiguienteFragment(fragment)
        }
        mostrarInformacion()
    }

    private fun mostrarInformacion() {
        binding.ivImagen.setImageResource(imagenId)
        binding.tvTitulo.text = descripcionTitulo
        binding.tvDescripcion.text = descripcionDetalle
        binding.btAccion.text = descripcionBoton
    }
}