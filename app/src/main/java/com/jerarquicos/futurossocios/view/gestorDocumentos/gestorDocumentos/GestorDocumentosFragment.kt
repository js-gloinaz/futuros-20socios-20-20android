package com.jerarquicos.futurossocios.view.gestorDocumentos.gestorDocumentos

import android.app.Activity.RESULT_OK
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.Intent.FLAG_GRANT_READ_URI_PERMISSION
import android.content.Intent.FLAG_GRANT_WRITE_URI_PERMISSION
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jerarquicos.futurossocios.BuildConfig
import com.jerarquicos.futurossocios.BuildConfig.API_URL
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ImagenCarruselLayoutBinding
import com.jerarquicos.futurossocios.databinding.SubirDocumentacionFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Archivo.Documento
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.apiService.controllers.IDocumentos
import com.jerarquicos.futurossocios.utils.AppConstants.ELIMINAR_IMAGEN
import com.jerarquicos.futurossocios.utils.AppConstants.SOLICITUD_SELECCIONAR_FOTO
import com.jerarquicos.futurossocios.utils.AppConstants.SOLICITUD_SELECCIONAR_PDF
import com.jerarquicos.futurossocios.utils.AppConstants.SOLICITUD_TOMAR_FOTO
import com.jerarquicos.futurossocios.utils.AppConstants.VISUALIZAR_IMAGEN
import com.jerarquicos.futurossocios.utils.ConfiguracionExpedienteConstants
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.utils.enums.TipoDocumentacionAnexaEnum
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.documentos.DocumentosHelper
import com.jerarquicos.futurossocios.view.common.utils.documentos.ProgressRequestBodyFile
import com.jerarquicos.futurossocios.view.models.DocumentacionAnexaModel
import com.jerarquicos.futurossocios.view.models.UploadArchivoModel
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import java.io.IOException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class GestorDocumentosFragment : BaseFragment(), Injectable {
    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController

    private val permisoCamara = android.Manifest.permission.CAMERA
    private val permisoWriteStorage = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    private val permisoReadStorage = android.Manifest.permission.READ_EXTERNAL_STORAGE
    private val JPEG_FILE_PREFIX = "IMG_"
    private val JPEG_FILE_SUFFIX = ".jpg"
    private val PROGRESO_MAXIMO = 100
    private val LISTA_ARCHIVOS = "LISTA_ARCHIVOS"
    private val LISTA_NOMBRES_ARCHIVOS = "LISTA_NOMBRES_ARCHIVOS"
    private val CANTIDAD_ARCHIVOS = "CANTIDAD_ARCHIVOS"
    private val TAMANIO_ARCHIVOS = "TAMANIO_ARCHIVOS"
    private val ID_ARCHIVO_ACTUAL = "ID_ARCHIVO_ACTUAL"
    private val CAMPO_ACCION = "CAMPO_ACCION"
    private val ID_ARCHIVO = "ID_ARCHIVO"
    private val IMAGENES_YA_CARGADAS = "IMAGENES_YA_CARGADAS"
    private val FORMATO_JPEG = "image/jpeg"
    private val FORMATO_PNG = "image/png"
    private val FORMATO_PDF = "application/pdf"
    private val MAXIMO_TAMANIO_MB = 1.5f
    private val OBTENIENDO_IMAGEN = 1
    private val OBTENIENDO_FALLIDO = 2
    private val SUBIDA_PROGRESO = 3
    private val SUBIDA_EXITOSA = 4
    private val SUBIDA_FALLIDA = 5

    var urlFotoActual = ""
    var listaDocumentos: ArrayList<Documento> = ArrayList()
    var listaNombresArchivos: ArrayList<String> = ArrayList()
    private var idExpedienteAplicacion = ""
    private var cantidadImagenes = 0
    private var tamanioArchivos = 0.00f
    private var idArchivo = 0
    private var eliminarArchivo: AlertDialog.Builder? = null
    var imagenesYaCargadas: Boolean = false

    private var mCantidadMinima = 0
    private var mCantidadMaxima = 0
    private var propietarioDocumentacion = 0
    private var ordenPersona = 0

    lateinit var tipoDocumentacion: TipoDocumentacionAnexaEnum
    lateinit var binding: SubirDocumentacionFragmentBinding
    private lateinit var gestorDocumentosViewModel: GestorDocumentosViewModel

    lateinit var snackbar: SnackbarHelper
    lateinit var documentosHelper: DocumentosHelper

    companion object {
        private const val BUNDLE_CANTIDAD_MINIMA_IMAGENES = "BUNDLE_CANTIDAD_MINIMA_IMAGENES"
        private const val BUNDLE_CANTIDAD_MAXIMA_IMAGENES = "BUNDLE_CANTIDAD_MAXIMA_IMAGENES"
        private const val BUNDLE_DOCUMENTACION_ANEXA = "BUNDLE_DOCUMENTACION_ANEXA"
        private const val BUNDLE_TIPO_DOCUMENTACION = "BUNDLE_TIPO_DOCUMENTACION"
        private const val BUNDLE_PROPIETARIO_DOCUMENTACION = "BUNDLE_PROPIETARIO_DOCUMENTACION"
        private const val BUNDLE_ORDEN_PERSONA = "BUNDLE_ORDEN_PERSONA"

        fun newInstance(mCantidadMinima: Int, mCantidadMaxima: Int, documentacionAnexa:
        ArrayList<DocumentacionAnexaModel>, tipoDocumentacion: TipoDocumentacionAnexaEnum, propietario: Int,
                        ordenPersona: Int): GestorDocumentosFragment {
            val args = Bundle()
            args.putInt(BUNDLE_CANTIDAD_MINIMA_IMAGENES, mCantidadMinima)
            args.putInt(BUNDLE_CANTIDAD_MAXIMA_IMAGENES, mCantidadMaxima)
            args.putSerializable(BUNDLE_TIPO_DOCUMENTACION, tipoDocumentacion)
            args.putSerializable(BUNDLE_DOCUMENTACION_ANEXA, documentacionAnexa)
            args.putInt(BUNDLE_PROPIETARIO_DOCUMENTACION, propietario)
            args.putInt(BUNDLE_ORDEN_PERSONA, ordenPersona)

            val fragment = GestorDocumentosFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.subir_documentacion_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        gestorDocumentosViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(GestorDocumentosViewModel::class.java)
        binding.viewModel = gestorDocumentosViewModel

        documentosHelper = DocumentosHelper()

        idExpedienteAplicacion = gestorDocumentosViewModel.obtenerExpediente().idAplicacion

        obtenerParametros(savedInstanceState)
        mostrarInformacion()

        binding.texSinImagenes.setOnClickListener {
            solicitarFuenteDeArchivo()
        }
        binding.imgAgregarImagen.setOnClickListener {
            solicitarFuenteDeArchivo()
        }
    }

    private fun solicitarFuenteDeArchivo() {
        if (cantidadImagenes == mCantidadMaxima) {
            mostrarMensaje(
                    activity!!.resources.getString(R.string.cantidad_maxima_imagenes_error),
                    R.color.colorNaranja
            )
        } else {
            val builder = AlertDialog.Builder(activity!!)
            builder.setTitle(R.string.subir_documentos_title)
            builder.setMessage(R.string.mensaje_documentos_text)
            builder.setPositiveButton(R.string.galeria_text) { _, _ ->
                seleccionarFoto()
            }
            builder.setNegativeButton(R.string.camara_text) { _, _ ->
                tomarFoto()
            }
            builder.setNeutralButton(R.string.pdf_text) { _, _ ->
                seleccionarPDF()
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }

    private fun seleccionarFoto() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permisoReadStorage)
            activity!!.requestPermissions(arrayOf(permisoReadStorage, permisoWriteStorage), SOLICITUD_SELECCIONAR_FOTO)
        } else {
            abrirGaleria()
        }
    }

    private fun tomarFoto() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permisoCamara)
            activity!!.requestPermissions(arrayOf(permisoCamara, permisoReadStorage, permisoWriteStorage),
                    SOLICITUD_TOMAR_FOTO)
        } else {
            abrirCamara()
        }
    }

    private fun seleccionarPDF() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permisoReadStorage)
            activity!!.requestPermissions(arrayOf(permisoReadStorage, permisoWriteStorage), SOLICITUD_SELECCIONAR_PDF)
        } else {
            abrirGestorDocumentos()
        }
    }

    private fun abrirGaleria() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(FLAG_GRANT_WRITE_URI_PERMISSION)
        intent.type = "image/*"
        activity!!.startActivityForResult(Intent.createChooser(intent, "Seleccionar una foto"),
                SOLICITUD_SELECCIONAR_FOTO)
    }

    private fun abrirCamara() {
        val takePictureIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        val f: File
        try {
            f = setUpArchivoImagen()
            urlFotoActual = f.absolutePath

            val resolvedIntentActivities: List<ResolveInfo> = activity!!.applicationContext.packageManager
                    .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolvedIntentInfo: ResolveInfo in resolvedIntentActivities) {
                val packageName: String = resolvedIntentInfo.activityInfo.packageName
                activity!!.applicationContext.grantUriPermission(packageName, FileProvider.getUriForFile(activity!!,
                        activity!!.applicationContext.packageName, f), Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
                activity!!.applicationContext.grantUriPermission(packageName, FileProvider.getUriForFile(activity!!,
                        activity!!.applicationContext.packageName, f), Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(activity!!,
                    activity!!.applicationContext.packageName, f))
            takePictureIntent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
            takePictureIntent.addFlags(FLAG_GRANT_WRITE_URI_PERMISSION)
        } catch (e: IOException) {
            e.printStackTrace()
            urlFotoActual = ""
        }
        activity!!.startActivityForResult(takePictureIntent, SOLICITUD_TOMAR_FOTO)
    }

    private fun abrirGestorDocumentos() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addFlags(FLAG_GRANT_READ_URI_PERMISSION)
        intent.addFlags(FLAG_GRANT_WRITE_URI_PERMISSION)
        intent.type = "*/*"
        activity!!.startActivityForResult(intent, SOLICITUD_SELECCIONAR_PDF)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            SOLICITUD_TOMAR_FOTO -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    abrirCamara()
                } else {
                    mostrarMensaje(activity!!.resources.getString(
							R.string.permisos_camara_denegados_error), R.color.colorRed)
                }
            }
            SOLICITUD_SELECCIONAR_FOTO -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    abrirGaleria()
                } else {
                    mostrarMensaje(
                            activity!!.resources.getString(R.string.permisos_galeria_denegados_error),
                            R.color.colorRed
                    )
                }
            }
            SOLICITUD_SELECCIONAR_PDF -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    abrirGestorDocumentos()
                } else {
                    mostrarMensaje(activity!!.resources.getString(
							R.string.permisos_documento_denegados_error), R.color.colorRed)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == VISUALIZAR_IMAGEN) {
            val accion = data!!.getIntExtra(CAMPO_ACCION, 0)
            val idArchivo = data.getIntExtra(ID_ARCHIVO, 0)
            when (accion) {
                ELIMINAR_IMAGEN -> {
                    quitarImagen(idArchivo)
                }
            }
        }
        if (resultCode == RESULT_OK) {
            try {
                when(requestCode){
                    SOLICITUD_TOMAR_FOTO -> {
                        generarDocumentoDesdeCamara()
                    }
                    SOLICITUD_SELECCIONAR_FOTO, SOLICITUD_SELECCIONAR_PDF  -> {
                        generarDocumentoDesdeGaleria(data)
                    }
                }
            } catch (ex: Exception) {
                if (requestCode == SOLICITUD_SELECCIONAR_FOTO) {
                    mostrarMensaje(activity!!.resources.getString(R.string.seleccion_camara_error),
                            R.color.colorRed, SnackbarEnum.MEDIO)
                } else {
                    mostrarMensaje(activity!!.resources.getString(R.string.formato_invalido_error),
                            R.color.colorRed, SnackbarEnum.MEDIO)
                }
            }
        }
    }

    private fun generarDocumentoDesdeCamara() {
        val bitmap: Bitmap = documentosHelper.obtenerBitmap(urlFotoActual)!!
        urlFotoActual = documentosHelper.writeToTempImageAndGetPathUri(activity!!, bitmap)!!
        if (documentosHelper.obtenerTamanio(urlFotoActual) > MAXIMO_TAMANIO_MB) {
            mostrarMensaje(activity!!.resources.getString(
					R.string.tamanio_maximo_documento_error, MAXIMO_TAMANIO_MB ), R.color.colorNaranja)
        } else {
            generarDocumentoImagen(urlFotoActual)
        }
    }
    private fun generarDocumentoDesdeGaleria(data: Intent?) {
		val formato = activity!!.contentResolver.getType(data?.data!!)
		val uriArchivo: Uri? = data.data
		when(formato){
			FORMATO_JPEG, FORMATO_PNG -> {
				if (uriArchivo!!.toString().contains("com.google.android.apps")) {
					urlFotoActual = documentosHelper.getImageUrlWithAuthority(activity!!, uriArchivo)!!
				}
				else {
					urlFotoActual = documentosHelper.getRealPath(activity!!, uriArchivo)!!
					val bitmap: Bitmap = documentosHelper.obtenerBitmap(urlFotoActual)!!
					urlFotoActual =
							documentosHelper.writeToTempImageAndGetPathUri(activity!!.applicationContext, bitmap)!!
				}
				if (documentosHelper.obtenerTamanio(urlFotoActual) > MAXIMO_TAMANIO_MB)
					mostrarMensaje(activity!!.resources.getString(
							R.string.tamanio_maximo_documento_error,MAXIMO_TAMANIO_MB), R.color.colorNaranja)
				else
					generarDocumentoImagen(urlFotoActual)
			}
			FORMATO_PDF -> {
				if (uriArchivo!!.toString().contains("com.google.android.apps")) {
					val iss = activity!!.contentResolver.openInputStream(uriArchivo)
					urlFotoActual = documentosHelper.stream2file(activity!!, iss!!)!!.path
				} else {
					urlFotoActual = documentosHelper.getRealPath(activity!!, data.data!!)!!
				}
				if (File(urlFotoActual).exists())
					generarDocumentoPdf(urlFotoActual)
				else
					mostrarMensaje(activity!!.resources.getString(R.string.lectura_documento_error),
							R.color.colorRed)
			}
			else -> {
				mostrarMensaje(activity!!.resources.getString(R.string.formato_invalido_error),
						R.color.colorRed, SnackbarEnum.MEDIO)
			}
		}
    }
    private fun obtenerImagenesCargadas() {
        val documentacion = arguments!!.getSerializable(BUNDLE_DOCUMENTACION_ANEXA)
                as ArrayList<DocumentacionAnexaModel>
        documentacion.forEach { documento ->
            listaNombresArchivos.add(documento.nombreArchivo!!)
        }
        listaNombresArchivos.forEach { nombre ->
            val archivo = Documento()
            archivo.tamanio = 0F
            archivo.path = API_URL + "DocumentacionAnexa/preview/$idExpedienteAplicacion/$nombre"
            archivo.esPdf = nombre.endsWith(".pdf", true)
            archivo.archivoAgregadoExitosamente = true
            archivo.cargando = true
            agregarArchivo(archivo)
        }
        imagenesYaCargadas = true
    }
	
    private fun agregarImagenACarrusel(documento: Documento) {
        val inflater = LayoutInflater.from(context)
        val bindingNuevo: ImagenCarruselLayoutBinding = DataBindingUtil
                .inflate(inflater, R.layout.imagen_carrusel_layout, binding.linHorizontalScroll, false)
	
		mostrarImagen(documento, bindingNuevo)

        bindingNuevo.linSubidaFallida.setOnClickListener {
            mostrarMensaje(SUBIDA_PROGRESO, bindingNuevo)
            val tarea = TaskSubirArchivo()
            tarea.documento = documento
            tarea.binding = bindingNuevo
            tarea.execute()
        }
        bindingNuevo.imgNueva.setOnClickListener {
			abrirVisualizador(documento)
        }

        bindingNuevo.imgNueva.setOnLongClickListener {
			eliminarImagen(documento)
        }

        binding.linHorizontalScroll.addView(bindingNuevo.root,
                LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT))
        binding.linHorizontalScroll.layoutParams = FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT)

        if (binding.linHorizontalScroll.childCount >= 1) {
            binding.texSinImagenes.visibility = View.GONE
        }

        if (documento.archivoAgregadoExitosamente) {
            bindingNuevo.progressBar.progressDrawable.setColorFilter(ContextCompat.getColor(activity!!,
                    R.color.colorVerde), android.graphics.PorterDuff.Mode.SRC_IN)
            bindingNuevo.progressBar.progress = PROGRESO_MAXIMO
            if (!documento.cargando) {
                mostrarMensaje(SUBIDA_EXITOSA, bindingNuevo)
            }
        } else {
            val tarea = TaskSubirArchivo()
            tarea.documento = documento
            tarea.binding = bindingNuevo
            tarea.execute()
        }
    }
	/**
	 * Muestra y carga el contenido en el carrusel dependiendo el tipo de archivo y el tipo de url de la imagen
	 * */
	private fun mostrarImagen(documento: Documento, bindingNuevo: ImagenCarruselLayoutBinding){
		if (documento.esPdf) {
			mostrarMensaje(SUBIDA_EXITOSA, bindingNuevo)
			documento.cargando = false
			Picasso.get().load(R.drawable.pdf_logo).fit().centerCrop().into(bindingNuevo.imgNueva)
		} else {
			if (documento.path!!.contains(API_URL)) {
				documento.cargando = true
				mostrarMensaje(OBTENIENDO_IMAGEN, bindingNuevo)
				Picasso.get().load(documento.path).fit().centerCrop().into(bindingNuevo.imgNueva, object : Callback {
					override fun onSuccess() {
						mostrarMensaje(SUBIDA_EXITOSA, bindingNuevo)
						documento.cargando = false
					}
					override fun onError(e: java.lang.Exception?) {
						mostrarMensaje(OBTENIENDO_FALLIDO, bindingNuevo)
						documento.cargando = false
					}
				})
			} else {
				Picasso.get().load("file://" + documento.path).fit().centerCrop().into(bindingNuevo.imgNueva)
			}
		}
	}
	
	/**
	 * Abre la imagen en tamaño completo, y si es PDF intenta abrir un gestor de PDFs
	 * */
	private fun abrirVisualizador(documento: Documento){
		if (documento.esPdf) {
			if (documento.path!!.contains(API_URL)) {
				val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(documento.path!!))
				startActivity(browserIntent)
			} else {
				val file = File(documento.path)
				val target = Intent(Intent.ACTION_VIEW)
				target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
				target.setDataAndType(FileProvider.
						getUriForFile(activity!!, activity!!.applicationContext.packageName, file),
						"application/pdf")
				val intent = Intent.createChooser(target, activity!!.resources.getString(R.string.abrir_pdf_text))
				try {
					startActivity(intent)
				} catch (e: ActivityNotFoundException) {
					mostrarMensaje(activity!!.resources.getString(R.string.pdf_no_existe_error),
							R.color.colorNaranja)
				}
			}
		} else {
			navigationController.navegarVisualizadorImagenFragment(this, documento)
		}
	}
	
	private fun eliminarImagen(documento: Documento): Boolean{
		eliminarArchivo = AlertDialog.Builder(activity!!, R.style.AppCompatAlertDialogStyle)
		eliminarArchivo!!.setTitle(getString(R.string.eliminar_documento_title))
		eliminarArchivo!!.setMessage(getString(R.string.eliminar_documento_text))
		eliminarArchivo!!.setPositiveButton(getString(R.string.eliminar_action)) { _, _ ->
			quitarImagen(documento.id)
		}
		eliminarArchivo!!.setNegativeButton(getString(R.string.cancelar_action)) { _, _ -> }
		eliminarArchivo!!.show()
		return true
	}
	
    private fun quitarImagen(id: Int) {
        for (i in 0 until cantidadImagenes) {
            if (listaDocumentos[i].id == id) {
                val otherSymbols = DecimalFormatSymbols(Locale.US)
                otherSymbols.groupingSeparator = '.'
                val formato = DecimalFormat("##.##", otherSymbols)
                val tamanioArchivosAux = tamanioArchivos -
                        java.lang.Float.parseFloat(formato.format(listaDocumentos[i].tamanio))
                if (tamanioArchivos < 0) {
                    this.tamanioArchivos = 0.0f
                } else {
                    this.tamanioArchivos = tamanioArchivosAux
                }
                this.cantidadImagenes = cantidadImagenes - 1
                listaDocumentos[i].reciclarImagenBitmap()
                if (listaDocumentos[i].archivoAgregadoExitosamente) {
                    listaNombresArchivos.removeAt(i)
                }
                listaDocumentos.removeAt(i)
                binding.linHorizontalScroll.removeViewAt(1 + i)
                break
            }
        }
        //Se compara con 1 porque siempre tiene una vista hija que corresponde al linear que
        // muestra los textos en caso que no haya imagenes
        if (binding.linHorizontalScroll.childCount == 1) {
            mostrarTextoSinImagenes()
        }

        if (listaNombresArchivos.size > 0) mostrarContinuar() else mostrarOmitir()
    }

    private fun generarDocumentoPdf(path: String) {
        val bitmap: Bitmap = BitmapFactory.decodeResource(activity!!.resources, R.drawable.pdf_logo)
        try {
            val archivo = Documento()
            archivo.esPdf = true
            archivo.path = path
            archivo.tamanio = documentosHelper.obtenerTamanio(path)
            bitmap.recycle()
            agregarArchivo(archivo)
            agregarImagenACarrusel(archivo)
        } catch (ex: Exception) {
            mostrarMensaje(activity!!.resources.getString(R.string.carga_documento_error), R.color.colorRed)
        }
    }

    private fun generarDocumentoImagen(path: String) {
        BitmapFactory.decodeFile(path)
        try {
            val archivo = Documento()
            archivo.tamanio = documentosHelper.obtenerTamanio(path)
            archivo.path = path
            archivo.esPdf = false
            agregarArchivo(archivo)
            agregarImagenACarrusel(archivo)
        } catch (ex: Exception) {
            mostrarMensaje(activity!!.resources.getString(R.string.carga_documento_error), R.color.colorRed)
        }
    }

    @Throws(IOException::class)
    private fun setUpArchivoImagen(): File {
        return crearArchivoImagen()
    }

    @Throws(IOException::class)
    private fun crearArchivoImagen(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = JPEG_FILE_PREFIX + timeStamp + "_"
        val albumF = documentosHelper.obtenerDirectorioAlbum()
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF)
    }

    private fun generarNuevoId(): Int {
        idArchivo++
        return idArchivo
    }

    private fun agregarArchivo(documento: Documento) {
        val otherSymbols = DecimalFormatSymbols(Locale.US)
        otherSymbols.groupingSeparator = '.'
        val formato = DecimalFormat("##.##", otherSymbols)
        documento.id = generarNuevoId()
        listaDocumentos.add(documento)
        cantidadImagenes += 1
        tamanioArchivos += java.lang.Float.parseFloat(formato.format(documento.tamanio))
    }

    private fun agregarArchivoEnviado(nombreArchivo: String, idArchivo: Int): Boolean {
        listaDocumentos.forEach { archivo ->
            if (archivo.id == idArchivo) {
                archivo.rutaArchivo = nombreArchivo
                archivo.archivoAgregadoExitosamente = true
                listaNombresArchivos.add(nombreArchivo)
                return true
            }
        }
        return false
    }

    private inner class TaskSubirArchivo : AsyncTask<Void, Void, String>(), ProgressRequestBodyFile.UploadCallbacks {
        internal var documento: Documento? = null
        internal var binding: ImagenCarruselLayoutBinding? = null
        internal var exitoso: Boolean = false
        inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)
        override fun onPreExecute() {
            super.onPreExecute()
            binding!!.progressBar.progressDrawable = ContextCompat.getDrawable(activity!!, R.drawable.progress_success)
            binding!!.progressBar.max = PROGRESO_MAXIMO
            binding!!.progressBar.progress = 0
            mostrarMensaje(SUBIDA_PROGRESO, binding!!)
        }

        override fun doInBackground(vararg params: Void): String? {
            // Creo un documento a partir de la imagen/pdf y mando al servidor
            val file = File(documento!!.path)
            val fileBody = ProgressRequestBodyFile(file, this)
            val client: OkHttpClient = OkHttpClient.Builder().build()
            val service = Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(client).build()
                    .create(IDocumentos::class.java)
            val filePart = MultipartBody.Part.createFormData("file", file.name, fileBody)

            val req: Call<ResponseBody> = service.enviarDocumento(filePart, idExpedienteAplicacion)
            req.enqueue(object : retrofit2.Callback<ResponseBody> {
                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                    exitoso = response.isSuccessful && response.code() == 200
                    if (!exitoso) {
                        generarError()
                    } else {
                        val cadena: String = response.body()!!.string()
                        val turnsType = object : TypeToken<RespuestaModel<UploadArchivoModel>>() {}.type
                        val respuestaModel = Gson()
                                .fromJson<RespuestaModel<UploadArchivoModel>>(cadena, turnsType)
                        if (respuestaModel.exito) {
                            generarExito()
                            agregarArchivoEnviado(respuestaModel.modelo!!.nombreArchivo!!, documento!!.id)
                            if (listaNombresArchivos.size > 0) mostrarContinuar() else mostrarOmitir()
                        } else {
                            generarError()
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    generarError()
                }
            })
            return null
        }

        private fun generarExito() {
            binding!!.progressBar.progressDrawable.setColorFilter(ContextCompat.getColor(activity!!,
                    R.color.colorVerde), android.graphics.PorterDuff.Mode.SRC_IN)
            binding!!.progressBar.progress = 100
            mostrarMensaje(SUBIDA_EXITOSA, binding!!)
        }

        private fun generarError() {
            binding!!.progressBar.progressDrawable.setColorFilter(ContextCompat.getColor(activity!!,
                    R.color.colorRed), android.graphics.PorterDuff.Mode.SRC_IN)
            binding!!.progressBar.progress = 100
            mostrarMensaje(SUBIDA_FALLIDA, binding!!)
        }

        override fun onProgressUpdate(percentage: Int) {
            binding!!.progressBar.progress = percentage
        }

        override fun onError() {
			//No hace nada
		}
        override fun onFinish() {
            binding!!.progressBar.progress = binding!!.progressBar.max
        }
    }

    private fun pasarSiguientePaso() {
        if (cantidadImagenes < mCantidadMinima) {
            mostrarMensaje(activity!!.resources.getString(
					R.string.cantidad_minima_imagenes_error, mCantidadMinima), R.color.colorNaranja)
        } else {
            var existeArchivoNoSubido = false
            listaDocumentos.forEach { documento ->
                if (!documento.archivoAgregadoExitosamente) {
                    existeArchivoNoSubido = true
                }
            }
            if (!existeArchivoNoSubido) {
                val fragment = baseActivity.dealerViewModel
                        .obtenerPasoSiguienteSubidaDocumentosFragment(tipoDocumentacion, listaNombresArchivos,
                                propietarioDocumentacion, ordenPersona)

                sincronizarExpediente()
                navigationController.navegarSiguienteFragment(fragment)
            } else {
                mostrarMensaje(activity!!.resources.getString(R.string.archivos_no_subidos_error), R.color.colorNaranja)
            }
        }
    }

    private fun sincronizarExpediente() {
        val expediente = gestorDocumentosViewModel.obtenerExpediente()

        when (propietarioDocumentacion) {
            ConfiguracionExpedienteConstants.DOCUMENTACION_TITULAR -> {
                if (expediente.titular?.nombre != null) {
                    baseActivity.sincronizarExpediente(true, true)
                }
            }
            ConfiguracionExpedienteConstants.DOCUMENTACION_CONVIVIENTE -> {
                if (expediente.conviviente?.nombre != null) {
                    baseActivity.sincronizarExpediente(true, true)
                }
            }
            ConfiguracionExpedienteConstants.DOCUMENTACION_PERSONA_A_CARGO -> {
                if (expediente.personaDependiente != null && expediente.personaDependiente!!.size > 0 &&
						expediente.personaDependiente!![ordenPersona].nombre != null) {
					baseActivity.sincronizarExpediente(true, true)
				}
            }
        }
    }

    private fun omitirCarga() {
        val fragment = baseActivity.dealerViewModel
                .obtenerPasoSiguienteSubidaDocumentosFragment(tipoDocumentacion, listaNombresArchivos,
                        propietarioDocumentacion, ordenPersona)
        navigationController.navegarSiguienteFragment(fragment)
    }

    private fun obtenerParametros(savedInstanceState: Bundle?) {
        mCantidadMinima = arguments!!.getInt(BUNDLE_CANTIDAD_MINIMA_IMAGENES)
        mCantidadMaxima = arguments!!.getInt(BUNDLE_CANTIDAD_MAXIMA_IMAGENES)
        propietarioDocumentacion = arguments!!.getInt(BUNDLE_PROPIETARIO_DOCUMENTACION)
        ordenPersona = arguments!!.getInt(BUNDLE_ORDEN_PERSONA)
        tipoDocumentacion = arguments!!.getSerializable(BUNDLE_TIPO_DOCUMENTACION) as TipoDocumentacionAnexaEnum

        if (savedInstanceState != null && savedInstanceState.containsKey(LISTA_NOMBRES_ARCHIVOS)) {
            val listaNombresArchivosAux = savedInstanceState
                    .getStringArrayList(LISTA_NOMBRES_ARCHIVOS) as ArrayList<String>
            val listaArchivosAux = savedInstanceState
                    .getParcelableArrayList<Documento>(LISTA_ARCHIVOS) as ArrayList<Documento>
            cantidadImagenes = savedInstanceState.getInt(CANTIDAD_ARCHIVOS)
            idArchivo = savedInstanceState.getInt(ID_ARCHIVO_ACTUAL)
            tamanioArchivos = savedInstanceState.getFloat(TAMANIO_ARCHIVOS)
            imagenesYaCargadas = savedInstanceState.getBoolean(IMAGENES_YA_CARGADAS)

            for (i in 0 until listaNombresArchivosAux.size) {
                listaNombresArchivos.add(listaNombresArchivosAux[i])
            }
            for (i in 0 until listaArchivosAux.size) {
                listaDocumentos.add(listaArchivosAux[i])
            }
            savedInstanceState.clear()
        } else {
            if (!imagenesYaCargadas) {
                obtenerImagenesCargadas()
            }
        }
    }

    private fun mostrarInformacion() {
        if (cantidadImagenes != 0) {
            for (i in 0 until cantidadImagenes) {
                agregarImagenACarrusel(listaDocumentos[i])
            }
        }
        if (binding.linHorizontalScroll.childCount == 1) {
            mostrarTextoSinImagenes()
        }
        if (listaNombresArchivos.size > 0) mostrarContinuar() else mostrarOmitir()
    }

    private fun mostrarMensaje(tipoMensaje: Int, bindingNuevo: ImagenCarruselLayoutBinding) {
        when (tipoMensaje) {
            OBTENIENDO_IMAGEN -> {
                bindingNuevo.linObteniendo.visibility = View.VISIBLE
                bindingNuevo.linObteniendoFallido.visibility = View.GONE
                bindingNuevo.linSubidaProgreso.visibility = View.GONE
                bindingNuevo.linSubidaExitosa.visibility = View.GONE
                bindingNuevo.linSubidaFallida.visibility = View.GONE
            }
            OBTENIENDO_FALLIDO -> {
                bindingNuevo.linObteniendo.visibility = View.GONE
                bindingNuevo.linObteniendoFallido.visibility = View.VISIBLE
                bindingNuevo.linSubidaProgreso.visibility = View.GONE
                bindingNuevo.linSubidaExitosa.visibility = View.GONE
                bindingNuevo.linSubidaFallida.visibility = View.GONE
            }
            SUBIDA_PROGRESO -> {
                bindingNuevo.linObteniendo.visibility = View.GONE
                bindingNuevo.linObteniendoFallido.visibility = View.GONE
                bindingNuevo.linSubidaProgreso.visibility = View.VISIBLE
                bindingNuevo.linSubidaExitosa.visibility = View.GONE
                bindingNuevo.linSubidaFallida.visibility = View.GONE
            }
            SUBIDA_EXITOSA -> {
                bindingNuevo.linObteniendo.visibility = View.GONE
                bindingNuevo.linObteniendoFallido.visibility = View.GONE
                bindingNuevo.linSubidaProgreso.visibility = View.GONE
                bindingNuevo.linSubidaExitosa.visibility = View.VISIBLE
                bindingNuevo.linSubidaFallida.visibility = View.GONE
            }
            SUBIDA_FALLIDA -> {
                bindingNuevo.linObteniendo.visibility = View.GONE
                bindingNuevo.linObteniendoFallido.visibility = View.GONE
                bindingNuevo.linSubidaProgreso.visibility = View.GONE
                bindingNuevo.linSubidaExitosa.visibility = View.GONE
                bindingNuevo.linSubidaFallida.visibility = View.VISIBLE
            }
        }
    }

    private fun mostrarTextoSinImagenes() {
        binding.texSinImagenes.visibility = View.VISIBLE
        binding.texSinImagenes.gravity = Gravity.CENTER_VERTICAL
    }

    private fun mostrarContinuar() {
        binding.btAccion.text = activity!!.resources.getString(R.string.continuar_action)
        binding.btAccion.setOnClickListener {
            pasarSiguientePaso()
        }
    }

    private fun mostrarOmitir() {
        binding.btAccion.text = activity!!.resources.getString(R.string.omitir_action)
        binding.btAccion.setOnClickListener {
            omitirCarga()
        }
    }

    private fun mostrarMensaje(mensaje: String, color: Int, duracion: SnackbarEnum = SnackbarEnum.CORTO) {
        snackbar = SnackbarHelper(this.view!!, null)
        snackbar.mostrarSnackBar(mensaje, color, duracion)
    }
	
    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putStringArrayList(LISTA_NOMBRES_ARCHIVOS, listaNombresArchivos)
        outState.putParcelableArrayList(LISTA_ARCHIVOS, listaDocumentos)
        outState.putInt(CANTIDAD_ARCHIVOS, cantidadImagenes)
        outState.putInt(ID_ARCHIVO_ACTUAL, idArchivo)
        outState.putFloat(TAMANIO_ARCHIVOS, tamanioArchivos)
        outState.putBoolean(IMAGENES_YA_CARGADAS, imagenesYaCargadas)
        super.onSaveInstanceState(outState)
    }
}