package com.jerarquicos.futurossocios.view.gestorDocumentos.gestorDocumentos

import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import javax.inject.Inject

class GestorDocumentosViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository
) : ViewModel() {
    lateinit var expediente: Expediente

    fun obtenerExpediente(): Expediente {
        expediente = futurosSociosRepository.obtenerExpediente()
        return expediente
    }
}