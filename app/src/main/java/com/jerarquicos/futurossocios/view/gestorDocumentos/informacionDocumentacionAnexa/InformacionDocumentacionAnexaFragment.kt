package com.jerarquicos.futurossocios.view.gestorDocumentos.informacionDocumentacionAnexa

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.InformacionDocumentacionAnexaFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.enums.TipoDocumentacionAnexaEnum
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.models.DocumentacionAnexaModel
import javax.inject.Inject


class InformacionDocumentacionAnexaFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null

    @Inject
    lateinit var navigationController: NavigationController

    private lateinit var informacionDocumentacionAnexaViewModel: InformacionDocumentacionAnexaViewModel
    lateinit var binding: InformacionDocumentacionAnexaFragmentBinding

    private lateinit var descripcionTitulo: String
    private lateinit var descripcionDetalle: String
    private lateinit var descripcionBoton: String
    private lateinit var documentacionAnexa: ArrayList<DocumentacionAnexaModel>
    private lateinit var tipoDocumentacion: TipoDocumentacionAnexaEnum

    private var imagenId: Int = 0
    private var mCantidadMinima: Int = 0
    private var mCantidadMaxima: Int = 0
    private var propietarioDocumentacion: Int = 0
    private var ordenPersona: Int = 0

    companion object {
        private const val BUNDLE_IMAGEN = "BUNDLE_IMAGEN"
        private const val BUNDLE_DESCRIPCION_TITULO = "BUNDLE_DESCRIPCION_TITULO"
        private const val BUNDLE_DESCRIPCION_DETALLE = "BUNDLE_DESCRIPCION_DETALLE"
        private const val BUNDLE_DESCRIPCION_BOTON = "BUNDLE_DESCRIPCION_BOTON"
        private const val BUNDLE_CANTIDAD_MINIMA_IMAGENES = "BUNDLE_CANTIDAD_MINIMA_IMAGENES"
        private const val BUNDLE_CANTIDAD_MAXIMA_IMAGENES = "BUNDLE_CANTIDAD_MAXIMA_IMAGENES"
        private const val BUNDLE_DOCUMENTACION_ANEXA = "BUNDLE_DOCUMENTACION_ANEXA"
        private const val BUNDLE_TIPO_DOCUMENTACION = "BUNDLE_TIPO_DOCUMENTACION"
        private const val BUNDLE_PROPIETARIO_DOCUMENTACION = "BUNDLE_PROPIETARIO_DOCUMENTACION"
        private const val BUNDLE_ORDEN_PERSONA = "BUNDLE_ORDEN_PERSONA"

        fun newInstance(mImagen: Int,
                        mDescripcionTitulo: Int,
                        mDescripcionDetalle: Int,
                        mDescripcionBoton: Int,
                        mCantidadMinima: Int,
                        mCantidadMaxima: Int,
                        mDocumentacionAnexa: ArrayList<DocumentacionAnexaModel>,
                        mTipoDocumentacion: TipoDocumentacionAnexaEnum,
                        mPropietario: Int,
                        mOrden: Int): InformacionDocumentacionAnexaFragment {
            val args = Bundle()
            args.putInt(BUNDLE_IMAGEN, mImagen)
            args.putInt(BUNDLE_DESCRIPCION_TITULO, mDescripcionTitulo)
            args.putInt(BUNDLE_DESCRIPCION_DETALLE, mDescripcionDetalle)
            args.putInt(BUNDLE_DESCRIPCION_BOTON, mDescripcionBoton)
            args.putInt(BUNDLE_CANTIDAD_MINIMA_IMAGENES, mCantidadMinima)
            args.putInt(BUNDLE_CANTIDAD_MAXIMA_IMAGENES, mCantidadMaxima)
            args.putSerializable(BUNDLE_DOCUMENTACION_ANEXA, mDocumentacionAnexa)
            args.putSerializable(BUNDLE_TIPO_DOCUMENTACION, mTipoDocumentacion)
            args.putInt(BUNDLE_PROPIETARIO_DOCUMENTACION, mPropietario)
            args.putInt(BUNDLE_ORDEN_PERSONA, mOrden)
            val fragment = InformacionDocumentacionAnexaFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.informacion_documentacion_anexa_fragment, container,
                false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        obtenerParametros()

        informacionDocumentacionAnexaViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(InformacionDocumentacionAnexaViewModel::class.java)
        binding.viewModel = informacionDocumentacionAnexaViewModel

        binding.btAccion.setOnClickListener {
            navigationController.navegarGestorDocumentosFragment(mCantidadMinima, mCantidadMaxima,
                    documentacionAnexa, tipoDocumentacion, propietarioDocumentacion, ordenPersona)
        }

        mostrarInformacion()
    }

    private fun mostrarInformacion() {
        binding.ivImagen.setImageResource(imagenId)
        binding.tvTitulo.text = descripcionTitulo
        binding.tvDescripcion.text = descripcionDetalle
        binding.btAccion.text = descripcionBoton
    }

    private fun obtenerParametros() {
        descripcionTitulo = getString(arguments!!.getInt(BUNDLE_DESCRIPCION_TITULO))
        descripcionDetalle = getString(arguments!!.getInt(BUNDLE_DESCRIPCION_DETALLE))
        descripcionBoton = getString(arguments!!.getInt(BUNDLE_DESCRIPCION_BOTON))
        imagenId = arguments!!.getInt(BUNDLE_IMAGEN)
        mCantidadMinima = arguments!!.getInt(BUNDLE_CANTIDAD_MINIMA_IMAGENES)
        mCantidadMaxima = arguments!!.getInt(BUNDLE_CANTIDAD_MAXIMA_IMAGENES)
        documentacionAnexa = arguments!!.getSerializable(BUNDLE_DOCUMENTACION_ANEXA)
                as ArrayList<DocumentacionAnexaModel>
        tipoDocumentacion = arguments!!.getSerializable(BUNDLE_TIPO_DOCUMENTACION) as TipoDocumentacionAnexaEnum
        propietarioDocumentacion = arguments!!.getInt(BUNDLE_PROPIETARIO_DOCUMENTACION)
        ordenPersona = arguments!!.getInt(BUNDLE_ORDEN_PERSONA)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }
}