package com.jerarquicos.futurossocios.view.gestorDocumentos.informacionDocumentacionAnexa

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class InformacionDocumentacionAnexaViewModel @Inject constructor() : ViewModel()