package com.jerarquicos.futurossocios.view.gestorDocumentos.visualizadorImagen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.BuildConfig
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.PopupImagenFragmentBinding
import com.jerarquicos.futurossocios.db.entity.Archivo.Documento
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.AppConstants.CERRAR_IMAGEN
import com.jerarquicos.futurossocios.utils.AppConstants.ELIMINAR_IMAGEN
import com.jerarquicos.futurossocios.utils.AppConstants.VISUALIZAR_IMAGEN
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import com.squareup.picasso.Picasso
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*
import javax.inject.Inject

class VisualizadorImagenFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity


    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    private lateinit var visualizadorImagenViewModel: VisualizadorImagenViewModel
    lateinit var binding: PopupImagenFragmentBinding
    private var eliminarArchivo: AlertDialog.Builder? = null
    private val CAMPO_ACCION = "CAMPO_ACCION"
    private val ID_ARCHIVO = "ID_ARCHIVO"

    private var documento: Documento? = null

    companion object {
        private const val BUNDLE_ARCHIVO = "BUNDLE_ARCHIVO"

        fun newInstance(documento: Documento): VisualizadorImagenFragment {
            val args = Bundle()
            args.putSerializable(BUNDLE_ARCHIVO, documento)
            val fragment = VisualizadorImagenFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.popup_imagen_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(this.activity!!, this.javaClass.simpleName,
                this.javaClass.simpleName)
        setHasOptionsMenu(true)

        visualizadorImagenViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(VisualizadorImagenViewModel::class.java)
        binding.viewModel = visualizadorImagenViewModel

        documento = arguments!!.getSerializable(BUNDLE_ARCHIVO) as Documento
        if (documento!!.path!!.contains(BuildConfig.API_URL)) {
            Picasso.get().load(documento!!.path).fit().centerInside().into(binding.imgFondo)
        } else {
            Picasso.get().load("file://" + documento!!.path).fit().centerInside().into(binding.imgFondo)
        }

        val otherSymbols = DecimalFormatSymbols(Locale.US)
        otherSymbols.groupingSeparator = '.'
        val formato = DecimalFormat("##.##", otherSymbols)
        val textoCantidadMB = String.format(activity!!.resources.getString(R.string.cantidad_mb_text),
                java.lang.Float.parseFloat(formato.format(documento!!.tamanio)))
        binding.texTamanioImagen.text = textoCantidadMB

        binding.imgBorrar.setOnClickListener {
            eliminarArchivo = AlertDialog.Builder(activity!!, R.style.AppCompatAlertDialogStyle)
            eliminarArchivo!!.setTitle(getString(R.string.eliminar_documento_title))
            eliminarArchivo!!.setMessage(getString(R.string.eliminar_documento_text))
            eliminarArchivo!!.setPositiveButton(getString(R.string.eliminar_action)) { _, _ ->
                targetFragment!!.onActivityResult(targetRequestCode, VISUALIZAR_IMAGEN, activity!!.intent
                        .putExtra(CAMPO_ACCION, ELIMINAR_IMAGEN).putExtra(ID_ARCHIVO, documento!!.id))
                fragmentManager!!.popBackStack()
            }
            eliminarArchivo!!.setNegativeButton(getString(R.string.cancelar_action)) { _, _ -> }
            eliminarArchivo!!.show()

        }
        binding.imgCerrar.setOnClickListener {
            targetFragment!!.onActivityResult(targetRequestCode, VISUALIZAR_IMAGEN, activity!!.intent
                    .putExtra(CAMPO_ACCION, CERRAR_IMAGEN))
            fragmentManager!!.popBackStack()
        }
    }
}