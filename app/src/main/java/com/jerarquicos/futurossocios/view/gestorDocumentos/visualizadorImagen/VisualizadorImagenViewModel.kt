package com.jerarquicos.futurossocios.view.gestorDocumentos.visualizadorImagen

import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import javax.inject.Inject

class VisualizadorImagenViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel()