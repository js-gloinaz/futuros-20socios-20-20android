package com.jerarquicos.futurossocios.view.inicio

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.InicioFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import javax.inject.Inject

class InicioFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity
    @Inject
    lateinit var navigationController: NavigationController
    private lateinit var mInicioViewModel: InicioViewModel
    lateinit var binding: InicioFragmentBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.inicio_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(this.activity!!, this.javaClass.simpleName,
                this.javaClass.simpleName)
        setHasOptionsMenu(true)

        baseActivity.ocultarTeclado()

        mInicioViewModel = ViewModelProviders.of(this, viewModelFactory).get(InicioViewModel::class.java)
        binding.viewModel = mInicioViewModel

        binding.btIniciarSesion.setOnClickListener {
            navigationController.navegarLoginFragment()
        }

        binding.btRegistrarse.setOnClickListener {
            navigationController.navegarRegistroFragment()
        }
        binding.btVerInformacion.setOnClickListener {
            Toast.makeText(this.context, "Hay que abrir la vista de informacion general", Toast.LENGTH_LONG).show()
        }
        return
    }
}