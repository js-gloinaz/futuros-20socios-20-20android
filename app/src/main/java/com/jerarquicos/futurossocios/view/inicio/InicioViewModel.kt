package com.jerarquicos.futurossocios.view.inicio

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class InicioViewModel @Inject constructor() : ViewModel()