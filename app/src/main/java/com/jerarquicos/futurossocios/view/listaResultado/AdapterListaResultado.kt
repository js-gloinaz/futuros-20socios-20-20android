package com.jerarquicos.futurossocios.view.listaResultado

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.view.models.OpcionModel
import kotlinx.android.synthetic.main.lista_resultado_item_layout.view.*

class AdapterListaResultado(private val mOpcionCallback: (OpcionModel) -> Unit) :
        RecyclerView.Adapter<AdapterListaResultado.ViewHolder>() {

    private var opcionesLista: MutableList<OpcionModel>? = null
    private var copiaOpcionesLista: MutableList<OpcionModel>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterListaResultado.ViewHolder {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.lista_resultado_item_layout,
                parent, false)
        return ViewHolder(vista)
    }

    override fun getItemCount(): Int {
        return opcionesLista!!.count()
    }

    override fun onBindViewHolder(holder: AdapterListaResultado.ViewHolder, position: Int) {
        val item = opcionesLista!![position]
        holder.itemView.ctvItem.text = item.descripcion
        holder.bind(item)
        holder.itemView.setOnClickListener { mOpcionCallback(item) }

        copiaOpcionesLista = ArrayList(opcionesLista!!)
    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun setDataLista(mutableList: MutableList<OpcionModel>) {
        opcionesLista = mutableList
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: OpcionModel) {
            with(itemView) {
                ctvItem.text = item.descripcion
            }
        }
    }

}