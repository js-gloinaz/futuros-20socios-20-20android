package com.jerarquicos.futurossocios.view.listaResultado

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ListaResultadoFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.models.OpcionModel
import java.util.*
import javax.inject.Inject

class ListaResultadoFragment : Fragment(), Injectable {

    companion object {
        private const val BUNDLE_LISTA_OPCIONES = "BUNDLE_LISTA_OPCIONES"

        fun newInstance(modelOpciones: ArrayList<OpcionModel>): ListaResultadoFragment {
            val args = Bundle()
            args.putSerializable(BUNDLE_LISTA_OPCIONES, modelOpciones)
            val fragment = ListaResultadoFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var baseActivity: BaseActivity
    lateinit var binding: ListaResultadoFragmentBinding
    private lateinit var listaResultadoViewModel: ListaResultadoViewModel

    private var opcionesLista: ArrayList<OpcionModel> = ArrayList()

    private var adaptador: AdapterListaResultado? = null
    private var lista: RecyclerView? = null
    var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.lista_resultado_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        baseActivity.ocultarProgressBar()
        listaResultadoViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ListaResultadoViewModel::class.java)

        binding.viewModel = listaResultadoViewModel
        opcionesLista = arguments!!.getSerializable(ListaResultadoFragment.BUNDLE_LISTA_OPCIONES)
                as ArrayList<OpcionModel>

        val searchManager = activity!!.getSystemService(Context.SEARCH_SERVICE)
                as SearchManager //es un servicio que me permite implementar la búsqueda
        val searchView = binding.svFiltro

        //de esta manera es como "configuro" el servicio por asi decirlo
        searchView.setSearchableInfo(searchManager.getSearchableInfo(activity!!.componentName))
        searchView.queryHint = "Buscar..."
        searchView.setIconifiedByDefault(false)
        searchView.clearFocus()

        adaptador = AdapterListaResultado { mOpcionModel: OpcionModel ->
            targetFragment!!.onActivityResult(
                    targetRequestCode,
                    Activity.RESULT_OK,
                    activity!!.intent.putExtra("id_seleccionado", mOpcionModel.id)
                            .putExtra("descripcion_seleccionada", mOpcionModel.descripcion))
            fragmentManager!!.popBackStack()
        }

        adaptador!!.setDataLista(opcionesLista)

        lista?.setHasFixedSize(true)
        lista = binding.rvResultado

        layoutManager = LinearLayoutManager(this.context)

        lista?.layoutManager = layoutManager

        lista?.adapter = adaptador

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(title: String): Boolean {
                //navigationController.navegarDomicilioFragment()
                return false
            }

            override fun onQueryTextChange(title: String): Boolean {
                buscarOpciones(title)
                return true
            }
        })

    }

    fun buscarOpciones(title: String) {
        val filteredModelList: List<OpcionModel> = filter(opcionesLista, title)
        adaptador!!.setDataLista(filteredModelList as MutableList<OpcionModel>)
        adaptador!!.notifyDataSetChanged()
    }

    private fun filter(models: List<OpcionModel>, query: String): List<OpcionModel> {
        val finalQuery = query.toLowerCase()
        val filteredModelList: MutableList<OpcionModel> = ArrayList()
        for (model: OpcionModel in models) {
            val text: String = model.descripcion.toLowerCase()
            if (text.contains(finalQuery)) {
                filteredModelList.add(model)
            }
        }
        return filteredModelList
    }

}




