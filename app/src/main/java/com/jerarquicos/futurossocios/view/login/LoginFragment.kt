package com.jerarquicos.futurossocios.view.login

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.crashlytics.android.Crashlytics
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.iid.FirebaseInstanceId
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.LoginFragmentBinding
import com.jerarquicos.futurossocios.db.entity.cuenta.DispositivoModel
import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.AppConstants.USUARIO
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class LoginFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    private lateinit var loginViewModel: LoginViewModel
    lateinit var binding: LoginFragmentBinding
    private lateinit var validadorDni: EditTextValidator
    private lateinit var validadorPassword: EditTextValidator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.login_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_USER
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(this.activity!!, this.javaClass.simpleName,
                this.javaClass.simpleName)
        setHasOptionsMenu(true)

        baseActivity.ocultarTeclado()

        loginViewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel::class.java)
        binding.viewModel = loginViewModel

        binding.btnServerLogin.setOnClickListener {
            if (validarDatos()) {
                loginViewModel.loadUser().observe(this, Observer<EstadoRespuesta<RespuestaModel<JwtTokenModel>>>
                { this.handleResponse(it) })
            }
        }
        binding.tvOlvidoContrasenia.setOnClickListener {
            navigationController.navegarRestablecerContraseniaFragment()
        }

        iniciarValidaciones()
    }

    private fun iniciarValidaciones() {
        validadorDni = EditTextValidator(binding.tiDni)
        validadorPassword = EditTextValidator(binding.tiPassword)
    }

    private fun validarDatos(): Boolean {
        val dniValido: Boolean = validadorDni.esNumeroDocumento()
        val passwordValido: Boolean = validadorPassword.esContrasenia()
        return dniValido && passwordValido
    }

    private fun handleResponse(respuesta: EstadoRespuesta<RespuestaModel<JwtTokenModel>>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
                    baseActivity.ocultarProgressBar()
                    SnackbarHelper(this.view!!, respuesta.data!!.notificacion).show()
                }
                Estado.CARGANDO -> {
                    baseActivity.mostrarProgressBar()
                }
                Estado.EXITO -> {
                    baseActivity.ocultarProgressBar()
                    baseActivity.ocultarTeclado()
                    setDispositivoModel()
                    // Agregar a Crashalytics la persona logueada
                    val datos: RespuestaModel<JwtTokenModel>? = respuesta.data
                    Crashlytics.setString(USUARIO, datos!!.modelo!!.usuario)
                    usuarioHelper.guardarToken(datos.modelo!!)
                    usuarioHelper.guardarLogin(loginViewModel.loginModel.value)
                    navigationController.navegarDashboardFragment()
                }
            }
        }
    }

    private fun setDispositivoModel() {
        val dispositivoModel = DispositivoModel()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceId ->
            val packageInfo = this.context!!.packageManager.getPackageInfo(this.context!!.packageName, 0)
            dispositivoModel.usuario = usuarioHelper.getUsuario()!!
            dispositivoModel.registrationId = instanceId.token

            Log.d("TOKEN", dispositivoModel.registrationId)
            dispositivoModel.manufacter = Build.MANUFACTURER
            dispositivoModel.model = Build.MODEL
            dispositivoModel.versionAndroid = Build.VERSION.RELEASE
            dispositivoModel.serial = Build.SERIAL
            dispositivoModel.versionName = packageInfo.versionName
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                dispositivoModel.versionCode = packageInfo.longVersionCode.toString()
            } else {
                dispositivoModel.versionCode = packageInfo.versionCode.toString()
            }
            dispositivoModel.uuid = Settings.Secure.getString(this.activity!!.contentResolver,
                    Settings.Secure.ANDROID_ID)
            loginViewModel.dispositivoModel.value = dispositivoModel
            loginViewModel.guardarDispositivo(loginViewModel.dispositivoModel.value!!)
                    .observe(this, Observer<EstadoRespuesta<RespuestaModel<Boolean>>> {})
        }
    }
}