package com.jerarquicos.futurossocios.view.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.cuenta.DispositivoModel
import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.LoginModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository) : ViewModel() {

    var loginModel: MutableLiveData<LoginModel> = MutableLiveData()
    var dispositivoModel: MutableLiveData<DispositivoModel> = MutableLiveData()

    init {
        loginModel.value = LoginModel()
    }

    fun loadUser(): LiveData<EstadoRespuesta<RespuestaModel<JwtTokenModel>>> {
        return futurosSociosRepository.login(loginModel.value!!)
    }

    fun guardarDispositivo(dispositivoModel: DispositivoModel): LiveData<EstadoRespuesta<RespuestaModel<Boolean>>> {
        return futurosSociosRepository.guardarDispositivo(dispositivoModel)
    }
}