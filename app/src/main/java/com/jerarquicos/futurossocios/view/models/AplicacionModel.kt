package com.jerarquicos.futurossocios.view.models

import java.io.Serializable
import java.util.*

class AplicacionModel(
        var nombre: String?,
        var versionActual: Int?,
        var versionMinimaRequerida: Int?,
        var nombreVersion: String?,
        var versionDatosgenerales: Int?,
        var mostrarAvisoNuevaVersion: Boolean?,
        var fechaCaducidad: Date?,
        var urlGooglePlay: String?

) : Serializable {
    constructor() : this(null, null, null, null,
            null, null, null, null)
}