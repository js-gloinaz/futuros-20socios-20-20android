package com.jerarquicos.futurossocios.view.models

import com.jerarquicos.futurossocios.db.entity.Expediente
import java.io.Serializable

class DashboardModel(
        var usuario: String?,
        var expedientesEnviados: ArrayList<ExpedienteEnviadoModel>?,
        var expedientesEnCurso: ArrayList<Expediente>?

) : Serializable {
    constructor() : this(null, null, null)
}
