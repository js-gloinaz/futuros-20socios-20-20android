package com.jerarquicos.futurossocios.view.models

import java.io.Serializable

class DocumentacionAnexaModel(var id: Int?, var tipoDocumentacionAnexa: TipoDocumentacionAnexaModel?,
                              var nombreArchivo: String?) : Serializable {
    constructor() : this(null, null, null)
}

