package com.jerarquicos.futurossocios.view.models

import java.io.Serializable
import java.util.*

class ExpedienteEnviadoModel(
        var id: Int?,
        var idAplicacion: String?,
        var nombre: String?,
        var apellido: String?,
        var fechaActualizacion: Date?

) : Serializable {
    constructor() : this(null, null, null, null, null)
}