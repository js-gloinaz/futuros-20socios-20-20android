package com.jerarquicos.futurossocios.view.models

import java.io.Serializable

class OpcionModel(var id: Int, var descripcion: String, var detalle: String?, var seleccionado: Boolean) : Serializable