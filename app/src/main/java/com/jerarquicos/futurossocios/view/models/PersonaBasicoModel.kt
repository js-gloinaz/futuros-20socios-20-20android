package com.jerarquicos.futurossocios.view.models

import java.io.Serializable

class PersonaBasicoModel(
        var id: Int,
        var nombre: String?,
        var apellido: String?,
        var dni: String?

) : Serializable {
    constructor() : this(0, null, null, null)
}