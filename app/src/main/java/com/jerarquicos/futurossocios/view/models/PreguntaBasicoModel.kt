package com.jerarquicos.futurossocios.view.models

import java.io.Serializable

class PreguntaBasicoModel(
        var id: Int,
        var orden: Int?,
        var titulo: String?,
        var descripcion: String?
) : Serializable {
    constructor() : this(0, null, null, null)
}