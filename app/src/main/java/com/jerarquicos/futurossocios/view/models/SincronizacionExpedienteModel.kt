package com.jerarquicos.futurossocios.view.models

import com.jerarquicos.futurossocios.db.entity.Expediente
import java.io.Serializable

class SincronizacionExpedienteModel(
        var expedienteSobrescrito: Boolean?,
        var expediente: Expediente?
) : Serializable {
    constructor() : this(null, null)
}