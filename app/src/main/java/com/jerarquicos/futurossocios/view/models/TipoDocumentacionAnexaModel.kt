package com.jerarquicos.futurossocios.view.models

import java.io.Serializable

class TipoDocumentacionAnexaModel(var id: Int?, var descripcion: String?) : Serializable {
    constructor() : this(null, null)
}