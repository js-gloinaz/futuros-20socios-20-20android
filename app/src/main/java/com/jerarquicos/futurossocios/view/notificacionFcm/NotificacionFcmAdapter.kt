package com.jerarquicos.futurossocios.view.notificacionFcm

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
import kotlinx.android.synthetic.main.notificacion_item_layout.view.*

class NotificacionFcmAdapter(private val mOpcionCallback: (NotificacionFcm) -> Unit)
	: RecyclerView.Adapter<NotificacionFcmAdapter.ViewHolder>() {
	private var opcionesLista: MutableList<NotificacionFcm> = ArrayList()
	
	override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
		val vista = LayoutInflater.from(parent.context).inflate(R.layout.notificacion_item_layout, parent,
				false)
		return ViewHolder(vista)
	}
	
	override fun getItemCount(): Int {
		return opcionesLista.size
	}
	
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		val notificacionFcmModel = opcionesLista[position]
		
		holder.itemView.tvTitulo.text = notificacionFcmModel.titulo
		holder.itemView.tvMensaje.text = notificacionFcmModel.mensaje
		holder.itemView.setOnClickListener { mOpcionCallback(notificacionFcmModel) }
	}
	
	fun removeItem(position: Int) {
		opcionesLista.removeAt(position)
		notifyItemRemoved(position)
		notifyItemRangeChanged(position, opcionesLista.size)
	}
	
	fun restoreItem(model: NotificacionFcm, position: Int) {
		opcionesLista.add(position, model)
		notifyItemInserted(position)
	}
	
	override fun getItemId(position: Int): Long {
		return position.toLong()
	}
	
	override fun getItemViewType(position: Int): Int {
		return position
	}
	
	fun setData(mutableList: MutableList<NotificacionFcm>) {
		opcionesLista = mutableList
		notifyDataSetChanged()
	}
	
	inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}