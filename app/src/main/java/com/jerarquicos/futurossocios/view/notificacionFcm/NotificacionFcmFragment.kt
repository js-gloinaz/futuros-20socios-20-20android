package com.jerarquicos.futurossocios.view.notificacionFcm

import android.graphics.*
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.NotificacionFcmFragmentBinding
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class NotificacionFcmFragment : BaseFragment(), Injectable {
	
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	@Inject
	lateinit var usuarioHelper: UsuarioHelper
	private lateinit var notificacionFcmViewModel: NotificacionFcmViewModel
	lateinit var binding: NotificacionFcmFragmentBinding
	private var adaptador: NotificacionFcmAdapter? = null
	var layoutManager: RecyclerView.LayoutManager? = null
	private var listaNotificacionFcmBaseLocal: ArrayList<NotificacionFcm> = ArrayList()
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.notificacion_fcm_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		inicializar(binding.toolbar, javaClass.name, true)
		
		notificacionFcmViewModel = ViewModelProviders.of(this, viewModelFactory)
				.get(NotificacionFcmViewModel::class.java)
		
		binding.viewModel = notificacionFcmViewModel
		
		notificacionFcmViewModel.actualizarEstadoNotificaciones(true)
		
		listaNotificacionesFcm()
		obtenerNotificacion()
	}
	
	private fun listaNotificacionesFcm() {
		binding.rvResultado.setHasFixedSize(true)
		layoutManager = LinearLayoutManager(this.context)
		binding.rvResultado.layoutManager = layoutManager
		adaptador = NotificacionFcmAdapter { mOpcionModel: NotificacionFcm -> }
		binding.rvResultado.adapter = adaptador
	}
	
	private fun obtenerNotificacion() {
		notificacionFcmViewModel.obtenerNotificaciones().observe(this,
				Observer<EstadoRespuesta<List<NotificacionFcm>>> {
					this.handleResponseNotificacion(it)
				})
	}
	
	private fun handleResponseNotificacion(respuesta: EstadoRespuesta<List<NotificacionFcm>>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.EXITO -> {
					if (respuesta.data != null) {
						listaNotificacionFcmBaseLocal = respuesta.data as ArrayList<NotificacionFcm>
						notificacionFcmViewModel.agregarNotificaciones(listaNotificacionFcmBaseLocal)
						mostrarInformacion()
					}
				}
				else -> {
					//not implementing
				}
			}
		}
	}
	
	private fun mostrarInformacion() {
		if (!notificacionFcmViewModel.listaNotificacionFcm.value.isNullOrEmpty()) {
			binding.rvResultado.visibility = View.VISIBLE
			binding.tvSinNotificacion.visibility = View.GONE
			adaptador!!.setData(notificacionFcmViewModel.listaNotificacionFcm.value as MutableList<NotificacionFcm>)
			swipeToRemove()
			
		}
		else {
			binding.rvResultado.visibility = View.GONE
			binding.tvSinNotificacion.visibility = View.VISIBLE
		}
	}
	
	private fun swipeToRemove() {
		val simpleItemTouchCallback = object
			: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
			override fun onMove(
					recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder)
					: Boolean {
				return false
			}
			
			override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
				val position = viewHolder.adapterPosition
				
				if (direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT) {
					val notificacion = notificacionFcmViewModel.listaNotificacionFcm.value!![position]
					adaptador!!.removeItem(position)
					mostrarSnackbarNotificacionEliminada(notificacion, position)
				}
			}
			
			override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
									 dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
				if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
					dibujarFondo(viewHolder,c, dX)
				}
				super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
			}
		}
		val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
		itemTouchHelper.attachToRecyclerView(binding.rvResultado)
	}
	
	private fun dibujarFondo(viewHolder: RecyclerView.ViewHolder, c: Canvas, dX: Float){
		val p = Paint()
		p.color = ContextCompat.getColor(viewHolder.itemView.context, R.color.colorRed)
		val icon = BitmapFactory.decodeResource(resources, R.drawable.delete)
		
		val itemView = viewHolder.itemView
		val height = itemView.bottom.toFloat() - itemView.top.toFloat()
		val width = height / 3
		if (dX > 0) {
			val background = RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
			c.drawRect(background, p)
			val iconDest = RectF(
					itemView.left.toFloat() + width,
					itemView.top.toFloat() + width,
					itemView.left.toFloat() + 2 * width,
					itemView.bottom.toFloat() - width)
			c.drawBitmap(icon, null, iconDest, p)
		}
		else {
			val background = RectF(
					itemView.right.toFloat() + dX,
					itemView.top.toFloat(),
					itemView.right.toFloat(),
					itemView.bottom.toFloat())
			c.drawRect(background, p)
			val iconDest = RectF(
					itemView.right.toFloat() - 2 * width,
					itemView.top.toFloat() + width,
					itemView.right.toFloat() - width,
					itemView.bottom.toFloat() - width)
			c.drawBitmap(icon, null, iconDest, p)
		}
	}
	
	private fun mostrarSnackbarNotificacionEliminada(notificacion: NotificacionFcm, position: Int) {
		val snackbarHelper = SnackbarHelper(this.view!!)
		snackbarHelper.setAccion(activity!!.resources.getString(R.string.deshacer_action), deshacerEliminacion(notificacion,
				position))
		snackbarHelper.mostrarSnackBar(activity!!.resources.getString(R.string.notificacion_eliminada_text), R.color
				.colorNaranja)
		notificacionFcmViewModel.borrarNotificacion(notificacion)
	}
	
	fun deshacerEliminacion(notificacion: NotificacionFcm, position: Int): Snackbar.Callback {
		return object : Snackbar.Callback() {
			override fun onDismissed(snackbar: Snackbar?, event: Int) {
				if (event == DISMISS_EVENT_ACTION) {
					adaptador!!.restoreItem(notificacion, position)
					notificacionFcmViewModel.guardarNotificacion(notificacion)
				}
			}
		}
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_base, menu)
	}
}