package com.jerarquicos.futurossocios.view.notificacionFcm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.NotificacionFcm
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class NotificacionFcmViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {
    var listaNotificacionFcm: MutableLiveData<ArrayList<NotificacionFcm>> = MutableLiveData()

    fun obtenerNotificaciones(): LiveData<EstadoRespuesta<List<NotificacionFcm>>> {
        return futurosSociosRepository.obtenerNotificacionFcmBaseLocal()
    }

    fun agregarNotificaciones(notificacionesLocales: ArrayList<NotificacionFcm>) {
        listaNotificacionFcm.value = notificacionesLocales
    }
	
	fun guardarNotificacion(notificacion: NotificacionFcm) {
		futurosSociosRepository.guardarNotificacionFcm(notificacion)
    }
    
    fun borrarNotificacion(notificacion: NotificacionFcm) {
        futurosSociosRepository.borrarNotificacion(notificacion)
    }
	
	fun actualizarEstadoNotificaciones(estado: Boolean) {
        futurosSociosRepository.actualizarEstadoNotificaciones(estado)
    }
}