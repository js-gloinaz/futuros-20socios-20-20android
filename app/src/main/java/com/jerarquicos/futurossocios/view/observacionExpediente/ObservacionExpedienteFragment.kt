package com.jerarquicos.futurossocios.view.observacionExpediente

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ObservacionExpedienteFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import javax.inject.Inject

class ObservacionExpedienteFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    private lateinit var observacionExpedienteViewModel: ObservacionExpedienteViewModel
    lateinit var binding: ObservacionExpedienteFragmentBinding
    lateinit var snackbar: SnackbarHelper
    var observacion: String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.observacion_expediente_fragment, container,
                false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        snackbar = SnackbarHelper(this.view!!, null)

        observacionExpedienteViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(ObservacionExpedienteViewModel::class.java)
        binding.viewModel = observacionExpedienteViewModel
        binding.observacion = observacionExpedienteViewModel.obtenerObservacion()

        binding.btAccion.setOnClickListener {
            if (binding.observacion != null) {
                guardarObservacion(binding.observacion!!)
            }
        }
    }

    private fun guardarObservacion(observacion: String) {
        observacionExpedienteViewModel.guardarObservacion(observacion)
        baseActivity.sincronizarExpediente(true, true)
        fragmentManager!!.popBackStack()
    }
}