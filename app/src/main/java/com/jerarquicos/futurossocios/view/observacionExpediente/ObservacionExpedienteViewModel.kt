package com.jerarquicos.futurossocios.view.observacionExpediente

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import javax.inject.Inject

class ObservacionExpedienteViewModel @Inject constructor(
        private val futurosSociosRepository: FuturosSociosRepository) : ViewModel() {

    var expediente = MutableLiveData<Expediente>()

    init {
        this.expediente.value = futurosSociosRepository.obtenerExpediente()
    }

    fun obtenerExpediente(): Expediente {
        return expediente.value!!
    }

    fun obtenerObservacion(): String {
        if (expediente.value!!.observacion != null) {
            return expediente.value!!.observacion!!
        } else {
            return ""
        }
    }

    fun guardarObservacion(observacion: String) {
        expediente.value!!.observacion = observacion
        futurosSociosRepository.guardarExpediente(expediente.value!!)
    }
}