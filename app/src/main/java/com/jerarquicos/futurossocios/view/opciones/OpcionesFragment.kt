package com.jerarquicos.futurossocios.view.opciones

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.ElegirOpcionesFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import com.jerarquicos.futurossocios.view.condicionLaboral.AdapterOpciones
import com.jerarquicos.futurossocios.view.models.GrupoOpcionesModel
import com.jerarquicos.futurossocios.view.models.OpcionModel
import javax.inject.Inject

class OpcionesFragment : BaseFragment(), Injectable {
    companion object {
        private const val BUNDLE_OPCIONES = "BUNDLE_OPCIONES"
        private const val BUNDLE_ID_VISTA_OPCION = "BUNDLE_ID_VISTA_OPCION"

        fun newInstance(grupoOpcionesModel: GrupoOpcionesModel, idVistaOpcion: Int): OpcionesFragment {
            val args = Bundle()
            args.putSerializable(BUNDLE_OPCIONES, grupoOpcionesModel)
            args.putSerializable(BUNDLE_ID_VISTA_OPCION, idVistaOpcion)

            val fragment = OpcionesFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private lateinit var grupoOpcionesModel: GrupoOpcionesModel
    private var idVistaOpcion: Int = 0
    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    lateinit var binding: ElegirOpcionesFragmentBinding

    private var adaptador: AdapterOpciones? = null
    private var lista: RecyclerView? = null
    var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.elegir_opciones_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        obtenerParametros()
        mostrarInformacion()
    }

    private fun mostrarInformacion() {
        binding.tvTitulo.text = getString(grupoOpcionesModel.tituloId)
        adaptador = AdapterOpciones { mOpcionModel: OpcionModel ->
            val fragment = baseActivity.dealerViewModel.obtenerPasoSiguienteOpcionesFragment(idVistaOpcion,
                    mOpcionModel.id)
            navigationController.navegarSiguienteFragment(fragment)
        }
        adaptador!!.setData(grupoOpcionesModel.listaOpciones as MutableList<OpcionModel>)
        lista?.setHasFixedSize(true)
        lista = binding.rvItems
        layoutManager = LinearLayoutManager(this.context)
        lista?.layoutManager = layoutManager
        lista?.adapter = adaptador
    }

    private fun obtenerParametros() {
        grupoOpcionesModel = arguments!!.getSerializable(BUNDLE_OPCIONES) as GrupoOpcionesModel
        idVistaOpcion = arguments!!.getSerializable(BUNDLE_ID_VISTA_OPCION) as Int
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_oscuro, menu)
    }
}