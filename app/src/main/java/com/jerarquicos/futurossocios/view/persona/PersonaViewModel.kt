package com.jerarquicos.futurossocios.view.persona

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Sexo
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

open class PersonaViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {
    var sexo: MutableLiveData<List<Sexo>> = MutableLiveData()
    var arrSexos: MutableLiveData<ArrayList<String>> = MutableLiveData()

    fun obtenerSexo(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun cargarSpSexos(sexos: List<Sexo>){
        val arraySexos: ArrayList<String> = ArrayList()
        for (i in sexos.indices) {
            var descripcion = ""
            descripcion += sexos[i].descripcion
            arraySexos.add(descripcion)
        }
        arrSexos.value = arraySexos
    }
}

