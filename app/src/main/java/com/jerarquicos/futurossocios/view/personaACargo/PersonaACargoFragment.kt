package com.jerarquicos.futurossocios.view.personaACargo

import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.PersonaACargoFragmentBinding
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Parentesco
import com.jerarquicos.futurossocios.db.entity.PersonaDependiente
import com.jerarquicos.futurossocios.db.entity.Sexo
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.DatePickerFragment
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import java.util.*
import javax.inject.Inject

class PersonaACargoFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    private lateinit var personaACargoViewModel: PersonaACargoViewModel
    private lateinit var binding: PersonaACargoFragmentBinding
    lateinit var snackbar: SnackbarHelper
    private lateinit var validadorNombre: EditTextValidator
    private lateinit var validadorApellido: EditTextValidator
    private lateinit var validadorDni: EditTextValidator
    private lateinit var validadorFecNac: EditTextValidator
    private lateinit var validadorCuil: EditTextValidator

    private val TAG_DATE_PICKER = "TAG_DATE_PICKER"

    companion object {

        private const val BUNDLE_PERSONAACARGO = "BUNDLE_PERSONAACARGO"
        fun newInstance(personaACargo: PersonaDependiente?): PersonaACargoFragment {
            val fragment = PersonaACargoFragment()
            val args = Bundle()
            if (personaACargo != null) {
                args.putSerializable(BUNDLE_PERSONAACARGO, personaACargo)
            }
            fragment.arguments = args
            return fragment
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.persona_a_cargo_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        snackbar = SnackbarHelper(this.view!!, null)

        personaACargoViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(PersonaACargoViewModel::class.java)
        binding.viewModel = personaACargoViewModel

        cargarPersonaACargo()
        inicializarListeners()
        cargarSpinnerSexo()
        cargarSpinnerParentescos()
        iniciarValidaciones()
    }

    private fun  inicializarListeners() {
        binding.etFechaNac.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val dlgSeleccionarFecha = DatePickerFragment()
                dlgSeleccionarFecha.setView(binding.etFechaNac)
                dlgSeleccionarFecha.show(baseActivity.supportFragmentManager, TAG_DATE_PICKER)
            }
        }

        binding.spParentesco.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                personaACargoViewModel.guardarParentesco(p2)
            }
        }

        binding.spSexo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                personaACargoViewModel.guardarSexo(p2)
            }
        }

        binding.btValidar.setOnClickListener {
            if (validarDatos()) {
                if (validarDniExistente()) {
                    binding.tiDni.error = getString(R.string.dni_existente_error)
                } else {
                    personaACargoViewModel.guardarFechaNacimiento()
                    // Obtengo próxima acción
                    val fragment = baseActivity.dealerViewModel
                            .obtenerPasoSiguientePersonaCargoFragment(personaACargoViewModel.personaACargoModel.value!!)
                    // Sincronizo con el servidor
                    baseActivity.sincronizarExpediente(true, true)
                    // Ejecuto próxima vista
                    navigationController.navegarSiguienteFragment(fragment)
                }
            }
        }
    }

    private fun cargarPersonaACargo(){
        val personaACargo = arguments!!.getSerializable(BUNDLE_PERSONAACARGO) as PersonaDependiente
        personaACargoViewModel.personaACargoModel.value = personaACargo
        if (personaACargo.nombre != null) {
            personaACargoViewModel.cargarParentesco(personaACargo.parentesco!!)
            personaACargoViewModel.cargarSexo(personaACargo.sexo!!)
            personaACargoViewModel.cargarFechaNacimiento()
        }
    }

    private fun cargarSpinnerParentescos() {
        personaACargoViewModel.obtenerParentescos().observe(this, Observer<EstadoRespuesta<DatosIniciales>>
        { this.handleResponseParentescos(it) })
    }

    private fun cargarSpinnerSexo() {
        personaACargoViewModel.obtenerSexo().observe(this, Observer<EstadoRespuesta<DatosIniciales>>
        { this.handleResponseSexo(it) })
    }

    private fun iniciarValidaciones() {
        validadorNombre = EditTextValidator(binding.tiNombre)
        validadorApellido = EditTextValidator(binding.tiApellido)
        validadorDni = EditTextValidator(binding.tiDni)
        validadorCuil = EditTextValidator(binding.tiCuil)
        validadorFecNac = EditTextValidator(binding.tiFechaNac)
    }

    private fun validarDatos(): Boolean {
        var hayError = true
        hayError = validadorNombre.esNombreValido() && hayError
        hayError = validadorApellido.esNombreValido() && hayError
        hayError = validadorDni.esNumeroDocumento() && hayError
        hayError = validadorFecNac.esFechaNacimientoValida(false) && hayError
        hayError = validadorCuil.esCuilValido(binding.etDni.text.toString()) && hayError

        return hayError
    }

    private fun validarDniExistente(): Boolean {
        return personaACargoViewModel.validarDniExistente()
    }

    private fun mostrarSnackBar(mensaje: String, color: Int, duracion: SnackbarEnum = SnackbarEnum.MEDIO) {
        snackbar.setMensajePersonalizado(mensaje)
        snackbar.setearColores(ContextCompat.getColor(activity!!.applicationContext, color))
        snackbar.setDuration(duracion)
        snackbar.show()
    }

    private fun cargarSpParentescos(parentescos: List<Parentesco>) {
        val arrParentescos: ArrayList<String> = ArrayList()
        for (i in parentescos.indices) {
            var descripcion = ""
            descripcion += parentescos[i].descripcion
            arrParentescos.add(descripcion)
        }
        val adapter = ArrayAdapter<String>(context!!, R.layout.spinner_item_seleccionado_layout, arrParentescos)
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spParentesco.adapter = adapter
        binding.spParentesco.visibility = View.VISIBLE
    }

    private fun cargarSpSexos(sexos: List<Sexo>) {
        personaACargoViewModel.cargarSpSexos(sexos)
        val adapter = ArrayAdapter<String>(
                context!!,
                R.layout.spinner_item_seleccionado_layout,
                personaACargoViewModel.arrSexos.value!!
        )
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spSexo.adapter = adapter
        binding.spSexo.visibility = View.VISIBLE
    }

    private fun handleResponseParentescos(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpParentescos(respuesta.data.parentesco)
                        personaACargoViewModel.guardarParentescos(respuesta.data.parentesco)
                        binding.spParentesco
                                .setSelection(personaACargoViewModel.obtenerPosicionParentescoSeleccionado())
                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                        TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_con_eliminacion, menu)
    }

    private fun handleResponseSexo(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpSexos(respuesta.data.sexo)
                        personaACargoViewModel.guardarSexos(respuesta.data.sexo)
                        binding.spSexo.setSelection(personaACargoViewModel.obtenerPosicionSexoSeleccionado())
                    } else {
                        SnackbarHelper(
                                this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)
                        ).show()
                    }
                }
                else -> {
                //not implementing
                }
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.btDelete -> {
                val builder = AlertDialog.Builder(context!!)
                builder.setMessage(getString(R.string.eliminar_persona_a_cargo_text))
                builder.setPositiveButton(getString(R.string.aceptar_action)) { _, _ ->
                    personaACargoViewModel
                            .eliminarPersonaACargo(personaACargoViewModel.personaACargoModel.value!!.dni.toString())
                    navigationController.navegarEliminarPersonaCargo()
                    mostrarSnackBar(getString(R.string.persona_a_cargo_eliminado_text), R.color.colorVerde)
                }
                builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

}