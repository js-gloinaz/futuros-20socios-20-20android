package com.jerarquicos.futurossocios.view.personaACargo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.persona.PersonaViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class PersonaACargoViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : PersonaViewModel(futurosSociosRepository) {
    var personaACargoModel: MutableLiveData<PersonaDependiente> = MutableLiveData()
    var fechaDeNacimiento: MutableLiveData<String> = MutableLiveData()
    private var parentescos: MutableLiveData<List<Parentesco>> = MutableLiveData()
    var expediente: MutableLiveData<Expediente> = MutableLiveData()

    fun eliminarPersonaACargo(dni: String) {
        expediente.value = futurosSociosRepository.obtenerExpediente()
        val listaPersonasDependientes = expediente.value?.personaDependiente!!
                .filter { it.dni != dni }
        listaPersonasDependientes.forEachIndexed { index, personaDependiente -> personaDependiente.posicion = index }
        expediente.value?.personaDependiente = listaPersonasDependientes as ArrayList<PersonaDependiente>
        if (expediente.value?.personaDependiente!!.count() == 0) {
            expediente.value?.personaDependiente = null
        }
        futurosSociosRepository.guardarExpediente(expediente.value!!)
    }

    fun obtenerParentescos(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun guardarParentescos(parentesco: List<Parentesco>) {
        this.parentescos.value = parentesco
    }

    fun cargarParentesco(parentesco: Parentesco) {
        personaACargoModel.value?.parentesco = parentesco
    }

    fun guardarParentesco(posicion: Int) {
        personaACargoModel.value?.parentesco = parentescos.value!![posicion]
    }

    fun obtenerPosicionParentescoSeleccionado(): Int {
        var index = 0
        if (personaACargoModel.value?.parentesco != null) {
            this.parentescos.value!!.forEachIndexed { i, element ->
                if (element.id == personaACargoModel.value?.parentesco!!.id) {
                    index = i
                }
            }
        }
        return index
    }

    fun guardarFechaNacimiento() {
        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val date = format.parse(fechaDeNacimiento.value!!)
            personaACargoModel.value?.fechaNacimiento = date
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun cargarFechaNacimiento() {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val dateTime = dateFormat.format(personaACargoModel.value?.fechaNacimiento!!)
            fechaDeNacimiento.value = dateTime
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun guardarSexos(sexo: List<Sexo>) {
        this.sexo.value = sexo
    }

    fun cargarSexo(sexo: Sexo) {
        personaACargoModel.value?.sexo = sexo
    }

    fun guardarSexo(posicion: Int) {
        personaACargoModel.value?.sexo = sexo.value!![posicion]
    }

    fun obtenerPosicionSexoSeleccionado(): Int {
        var index = 0
        if (personaACargoModel.value?.sexo != null) {
            this.sexo.value!!.forEachIndexed { i, element ->
                if (element.id == personaACargoModel.value?.sexo!!.id) {
                    index = i
                }
            }
        }
        return index
    }

    fun validarDniExistente(): Boolean {
        val dni = personaACargoModel.value!!.dni
        val expediente = futurosSociosRepository.obtenerExpediente()

        if (expediente.titular?.dni.equals(dni)) {
            return true
        }
        if ((expediente.conviviente != null) && (expediente.conviviente?.dni.equals(dni))) {
                return true
        }
        return false
    }
}