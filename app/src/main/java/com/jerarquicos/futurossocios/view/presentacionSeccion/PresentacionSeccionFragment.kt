package com.jerarquicos.futurossocios.view.presentacionSeccion

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.PresentacionSeccionFragmentBinding
import com.jerarquicos.futurossocios.db.entity.cuenta.EstadoPasoExpediente
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_05
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_1
import com.jerarquicos.futurossocios.utils.enums.EstadoExpedienteEnum
import com.jerarquicos.futurossocios.utils.enums.EstadoPasoExpedienteEnum
import com.jerarquicos.futurossocios.utils.enums.SnackbarEnum
import com.jerarquicos.futurossocios.utils.enums.TipoSeccionEnum
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.comienzo.ComienzoFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import com.jerarquicos.futurossocios.view.models.SincronizacionExpedienteModel
import com.jerarquicos.futurossocios.view.observacionExpediente.ObservacionExpedienteFragment
import javax.inject.Inject

class PresentacionSeccionFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	@Inject
	lateinit var usuarioHelper: UsuarioHelper
	lateinit var snackbar: SnackbarHelper
	lateinit var binding: PresentacionSeccionFragmentBinding
	private lateinit var presentacionSeccionViewModel: PresentacionSeccionViewModel
	private var accionSnackbarEjecutada = false
	private val PASOS_COMPLETOS = 0
	private val PASO_1 = 1
	private val PASO_2 = 2
	private val PASO_3 = 3
	private var checkDatosPersonales: EstadoPasoExpediente? = null
	private var checkDeclaracionJurada: EstadoPasoExpediente? = null
	private var checkFormaDePago: EstadoPasoExpediente? = null
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.presentacion_seccion_fragment, container,
				false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
		inicializar(binding.toolbar, javaClass.name, true)
		
		snackbar = SnackbarHelper(this.view!!, null)
		
		presentacionSeccionViewModel = ViewModelProviders.of(this, viewModelFactory)
				.get(PresentacionSeccionViewModel::class.java)
		binding.viewModel = presentacionSeccionViewModel
		val listaEstados = baseActivity.dealerViewModel.obtenerEstadosSeccionesExpediente()
		checkDatosPersonales = listaEstados[0]
		checkDeclaracionJurada = listaEstados[1]
		checkFormaDePago = listaEstados[2]
		
		inicializarListenerBotonPrincipal()
		inicializarListenerTitulo()
		inicializarListenerChecks()
		
		mostrarInformacion()
	}
	
	private fun inicializarListenerBotonPrincipal() {
		binding.btComenzar.setOnClickListener {
			//valido por paso siguiente
			if (checkDatosPersonales!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor
					&& checkDeclaracionJurada!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor
					&& checkFormaDePago!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor) {
				enviarExpediente()
			}
			else if (checkDeclaracionJurada?.habilitaPasoSiguiente!!) {
				navigationController.navegarComienzoFormaDePagoFragment()
			}
			else if (checkDatosPersonales?.habilitaPasoSiguiente!!) {
				navigationController.navegarComienzoDeclaracionJuradaFragment()
			}
			else if (checkDatosPersonales?.idEstado == EstadoExpedienteEnum.Inicial.valor) {
				navigationController.navegarPrincipalFragment()
			}
			else {
				mostrarSnackbarEstadoProceso(checkDatosPersonales!!, checkDeclaracionJurada!!, checkFormaDePago!!)
			}
		}
	}
	
	private fun mostrarSnackbarEstadoProceso(
			checkDatosPersonales: EstadoPasoExpediente,
			checkDeclaracionJurada: EstadoPasoExpediente,
			checkFormaDePago: EstadoPasoExpediente
	) {
		if (checkDatosPersonales.idEstado == EstadoPasoExpedienteEnum.Proceso.valor) {
			mostrarSnackBar(getString(checkDatosPersonales.descripcion), R.color.colorNaranja,
					checkDatosPersonales.fragmentReferenciado)
			return
		}
		if (checkDatosPersonales.idEstado == EstadoPasoExpedienteEnum.Proceso.valor) {
			mostrarSnackBar(getString(checkDeclaracionJurada.descripcion), R.color.colorNaranja,
					checkDeclaracionJurada.fragmentReferenciado)
			return
		}
		if (checkFormaDePago.idEstado == EstadoPasoExpedienteEnum.Proceso.valor) {
			mostrarSnackBar(getString(checkFormaDePago.descripcion), R.color.colorNaranja,
					checkFormaDePago.fragmentReferenciado)
			return
		}
	}
	
	private fun inicializarListenerTitulo() {
		binding.llTitulo.setOnClickListener {
			resaltarPaso(PASO_1)
			navigationController.navegarPrincipalFragment()
		}
		binding.llTituloDJ.setOnClickListener {
			if (checkDatosPersonales?.habilitaPasoSiguiente!!) {
				resaltarPaso(PASO_2)
				navigationController.navegarComienzoDeclaracionJuradaFragment()
			}
			else {
				if (getString(checkDatosPersonales?.descripcion!!) != getString(R.string.vacio_text)) {
					mostrarSnackBar(getString(checkDatosPersonales?.descripcion!!), R.color.colorNaranja,
							ComienzoFragment.newInstance(
									TipoSeccionEnum.DatosPersonales.valor, R.string.datos_personales_title,
									R.string.datos_personales_titular_text, R.drawable.ic_credencial_virtual
							)
					)
				}
			}
		}
		binding.llTituloFP.setOnClickListener {
			if (checkDeclaracionJurada?.habilitaPasoSiguiente!!) {
				resaltarPaso(PASO_3)
				navigationController.navegarComienzoFormaDePagoFragment()
			}
			else {
				if (getString(checkDeclaracionJurada?.descripcion!!) != getString(R.string.vacio_text)) {
					mostrarSnackBar(getString(checkDeclaracionJurada?.descripcion!!), R.color.colorNaranja,
							checkDeclaracionJurada!!.fragmentReferenciado)
				}
			}
		}
	}
	
	private fun inicializarListenerChecks() {
		binding.llCheckCV.setOnClickListener {
			if (getString(checkDatosPersonales?.descripcion!!) != getString(R.string.vacio_text)) {
				mostrarSnackBar(getString(checkDatosPersonales?.descripcion!!), R.color.colorNaranja,
						checkDatosPersonales!!.fragmentReferenciado)
			}
		}
		binding.llCheckDJ.setOnClickListener {
			if (getString(checkDeclaracionJurada?.descripcion!!) != getString(R.string.vacio_text)) {
				mostrarSnackBar(getString(checkDeclaracionJurada?.descripcion!!), R.color.colorNaranja,
						checkDeclaracionJurada!!.fragmentReferenciado)
			}
		}
		binding.llCheckFP.setOnClickListener {
			if (getString(checkFormaDePago?.descripcion!!) != getString(R.string.vacio_text)) {
				mostrarSnackBar(getString(checkFormaDePago?.descripcion!!), R.color.colorNaranja,
						checkFormaDePago!!.fragmentReferenciado)
			}
		}
	}
	
	private fun mostrarInformacion() {
		binding.ivCheck.visibility = View.GONE
		binding.ivCheckDJ.visibility = View.GONE
		binding.ivCheckFP.visibility = View.GONE
		
		mostrarInformacionDatosPersonales()
		mostrarInformacionDeclaracionJurada()
		mostrarInformacionFormaDePago()
		if (checkDatosPersonales!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor
				&& checkDeclaracionJurada!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor
				&& checkFormaDePago!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor) {
			binding.btComenzar.setText(R.string.enviar_expediente_action)
		}
	}
	
	private fun mostrarInformacionDatosPersonales() {
		when (checkDatosPersonales!!.idEstado) {
			EstadoPasoExpedienteEnum.Inicial.valor -> {
				binding.tvTitulo.setText(R.string.necesitamos_conocerte_title)
				binding.btComenzar.setText(R.string.comenzar_action)
				resaltarPaso(PASO_1)
				binding.llTituloFP.isEnabled = false
				binding.ivCheck.visibility = View.GONE
			}
			EstadoPasoExpedienteEnum.Proceso.valor -> {
				if (checkDatosPersonales?.habilitaPasoSiguiente!!) {
					binding.ivCheck.setImageResource(R.drawable.icon_warning)
					binding.ivCheck.visibility = View.VISIBLE
					binding.tvTitulo.setText(R.string.algunas_preguntas_text)
					binding.btComenzar.setText(R.string.continuar_action)
					resaltarPaso(PASO_2)
					binding.llTituloDJ.isEnabled = true
					binding.llTituloFP.isEnabled = false
				}
				else {
					binding.tvTitulo.setText(R.string.necesitamos_conocerte_title)
					resaltarPaso(PASO_1)
					binding.btComenzar.setText(R.string.continuar_action)
					binding.llTituloFP.isEnabled = false
					binding.ivCheck.setImageResource(R.drawable.icon_warning)
					binding.ivCheck.visibility = View.VISIBLE
				}
			}
			EstadoPasoExpedienteEnum.Completo.valor -> {
				binding.ivCheck.setImageResource(R.drawable.icon_check_new)
				binding.ivCheck.visibility = View.VISIBLE
				binding.tvTitulo.setText(R.string.algunas_preguntas_text)
				resaltarPaso(PASO_2)
				binding.btComenzar.setText(R.string.continuar_action)
				binding.llTituloDJ.isEnabled = true
				binding.llTituloFP.isEnabled = false
			}
		}
	}
	
	private fun mostrarInformacionDeclaracionJurada() {
		when (checkDeclaracionJurada!!.idEstado) {
			EstadoPasoExpedienteEnum.Inicial.valor -> {
				binding.btComenzar.setText(R.string.continuar_action)
				binding.llTituloFP.isEnabled = false
				binding.ivCheckDJ.visibility = View.GONE
				if (checkDatosPersonales?.habilitaPasoSiguiente!!) {
					binding.tvTitulo.setText(R.string.algunas_preguntas_text)
					resaltarPaso(PASO_2)
					binding.llTituloDJ.isEnabled = true
				}
				else {
					binding.tvTitulo.setText(R.string.necesitamos_conocerte_title)
					resaltarPaso(PASO_1)
					binding.ivCheck.setImageResource(R.drawable.icon_warning)
				}
			}
			EstadoPasoExpedienteEnum.Proceso.valor -> {
				binding.btComenzar.setText(R.string.continuar_action)
				binding.ivCheckDJ.setImageResource(R.drawable.icon_warning)
				binding.ivCheckDJ.visibility = View.VISIBLE
				if (checkDeclaracionJurada?.habilitaPasoSiguiente!!) {
					binding.tvTitulo.setText(R.string.unos_datos_mas_title)
					resaltarPaso(PASO_3)
					binding.llTituloFP.isEnabled = true
				}
				else {
					binding.tvTitulo.setText(R.string.algunas_preguntas_text)
					resaltarPaso(PASO_2)
					binding.llTituloFP.isEnabled = false
				}
			}
			EstadoPasoExpedienteEnum.Completo.valor -> {
				binding.ivCheckDJ.setImageResource(R.drawable.icon_check_new)
				binding.ivCheckDJ.visibility = View.VISIBLE
				binding.tvTitulo.setText(R.string.unos_datos_mas_title)
				resaltarPaso(PASO_3)
				binding.btComenzar.setText(R.string.continuar_action)
				binding.llTituloFP.isEnabled = true
			}
		}
	}
	
	private fun mostrarInformacionFormaDePago() {
		when (checkFormaDePago!!.idEstado) {
			EstadoPasoExpedienteEnum.Inicial.valor -> {
				if (checkDeclaracionJurada!!.habilitaPasoSiguiente) {
					binding.tvTitulo.setText(R.string.unos_datos_mas_title)
					binding.btComenzar.setText(R.string.continuar_action)
					resaltarPaso(PASO_3)
					binding.llTituloFP.isEnabled = true
					binding.ivCheckFP.visibility = View.GONE
				}
			}
			EstadoPasoExpedienteEnum.Proceso.valor -> {
				if (checkDeclaracionJurada?.habilitaPasoSiguiente!!) {
					binding.ivCheckFP.setImageResource(R.drawable.icon_warning)
					binding.ivCheckFP.visibility = View.VISIBLE
					binding.tvTitulo.setText(R.string.unos_datos_mas_title)
					resaltarPaso(PASO_3)
					binding.btComenzar.setText(R.string.continuar_action)
				}
				else {
					binding.tvTitulo.setText(R.string.unos_datos_mas_title)
					resaltarPaso(PASO_3)
					binding.btComenzar.setText(R.string.continuar_action)
					binding.ivCheckFP.setImageResource(R.drawable.icon_warning)
					binding.ivCheckFP.visibility = View.VISIBLE
				}
			}
			EstadoPasoExpedienteEnum.Completo.valor -> {
				binding.ivCheckFP.setImageResource(R.drawable.icon_check_new)
				binding.ivCheckFP.visibility = View.VISIBLE
				binding.tvTitulo.setText(R.string.unos_datos_mas_title)
				if (checkDatosPersonales!!.idEstado == EstadoPasoExpedienteEnum.Completo.valor &&
						checkDeclaracionJurada!!.idEstado != EstadoPasoExpedienteEnum.Completo.valor) {
					resaltarPaso(PASO_2)
				}
				else {
					resaltarPaso(PASOS_COMPLETOS)
				}
			}
		}
	}
	
	fun sincronizarExpediente() {
		baseActivity.dealerViewModel.sincronizarExpediente()
				.observe(this, Observer<EstadoRespuesta<RespuestaModel<SincronizacionExpedienteModel>>> {
					this.handleSincronizacionResponse(it)
				})
	}
	
	private fun handleSincronizacionResponse(respuesta: EstadoRespuesta<RespuestaModel<SincronizacionExpedienteModel>>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
					baseActivity.ocultarProgressBar()
					SnackbarHelper(view!!, respuesta.data!!.notificacion).show()
				}
				Estado.CARGANDO -> {
					baseActivity.mostrarProgressBar()
				}
				Estado.EXITO -> {
					baseActivity.ocultarProgressBar()
					if (respuesta.data != null) {
						val datos: RespuestaModel<SincronizacionExpedienteModel>? = respuesta.data
						guardarResultado(datos?.modelo!!)
					}
				}
			}
		}
	}
	
	private fun guardarResultado(modelo: SincronizacionExpedienteModel) {
		baseActivity.dealerViewModel.guardarExpediente(modelo.expediente)
		
		if (modelo.expedienteSobrescrito != null && modelo.expedienteSobrescrito == true) {
			val builder = AlertDialog.Builder(context!!)
			builder.setTitle(getString(R.string.expediente_sobreescrito_title))
			builder.setMessage(getString(R.string.expediente_sobreescrito_text))
			builder.setPositiveButton(getString(R.string.listo_action)) { _, _ ->
				navigationController.navegarDashboardFragment()
			}
			val dialog: AlertDialog = builder.create()
			dialog.setCancelable(false)
			dialog.show()
		}
		else{
			presentacionSeccionViewModel.enviarExpediente()
					.observe(this, Observer<EstadoRespuesta<RespuestaModel<Boolean>>> { this.handleResponse(it) })
		}
	}
	
	private fun enviarExpediente() {
		val builder = AlertDialog.Builder(context!!)
		builder.setMessage(getString(R.string.enviar_expediente_text))
		builder.setPositiveButton(getString(R.string.aceptar_action)) { _, _ ->
			sincronizarExpediente()
		}
		builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->
		}
		val dialog: AlertDialog = builder.create()
		dialog.show()
	}
	
	private fun resaltarPaso(paso: Int) {
		when (paso) {
			PASO_1 -> {
				binding.clPasoUno.setBackgroundResource(R.color.colorAzulPresentacion)
				binding.clPasoDos.setBackgroundResource(R.color.colorAzulInstitucional)
				binding.clPasoTres.setBackgroundResource(R.color.colorAzulInstitucional)
				binding.clPasoDos.alpha = ALPHA_05
				binding.clPasoTres.alpha = ALPHA_05
			}
			PASO_2 -> {
				binding.clPasoUno.setBackgroundResource(R.color.colorAzulInstitucional)
				binding.clPasoDos.setBackgroundResource(R.color.colorAzulPresentacion)
				binding.clPasoTres.setBackgroundResource(R.color.colorAzulInstitucional)
				binding.clPasoDos.alpha = ALPHA_1
				binding.clPasoTres.alpha = ALPHA_05
			}
			PASO_3, PASOS_COMPLETOS -> {
				binding.clPasoUno.setBackgroundResource(R.color.colorAzulInstitucional)
				binding.clPasoDos.setBackgroundResource(R.color.colorAzulInstitucional)
				binding.clPasoTres.setBackgroundResource(R.color.colorAzulPresentacion)
				binding.clPasoDos.alpha = ALPHA_1
				binding.clPasoTres.alpha = ALPHA_1
			}
		}
	}
	
	private fun handleResponse(respuesta: EstadoRespuesta<RespuestaModel<Boolean>>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
					baseActivity.ocultarProgressBar()
					SnackbarHelper(this.view!!, respuesta.data!!.notificacion).show()
				}
				Estado.CARGANDO -> {
					baseActivity.mostrarProgressBar()
				}
				Estado.EXITO -> {
					baseActivity.ocultarProgressBar()
					presentacionSeccionViewModel.borrarExpediente()
					navigationController.navegarDashboardFragment()
				}
			}
		}
	}
	
	private fun mostrarSnackBar(mensaje: String, color: Int, fragment: Fragment,
								duracion: SnackbarEnum = SnackbarEnum.MEDIO) {
		snackbar.setMensajePersonalizado(mensaje)
		snackbar.setearColores(ContextCompat.getColor(activity!!.applicationContext, color))
		snackbar.setAccion(getString(R.string.ver_action), snackbarAccion(fragment))
		snackbar.setDuration(duracion)
		snackbar.show()
	}
	
	private fun snackbarAccion(fragment: Fragment): Snackbar.Callback {
		return object : Snackbar.Callback() {
			override fun onDismissed(s: Snackbar?, event: Int) {
				snackbar = SnackbarHelper(s!!.view, null)
				when (event) {
					DISMISS_EVENT_ACTION -> {
						if (!accionSnackbarEjecutada) {
							accionSnackbarEjecutada = true
							navigationController.navegarSiguienteFragment(fragment)
						}
					}
				}
			}
		}
	}
	
	override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
		inflater?.inflate(R.menu.menu_presentacion_expediente, menu)
	}
	
	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		when (item?.itemId) {
			R.id.btAgregarObservacion -> {
				val fragment = ObservacionExpedienteFragment()
				navigationController.navegarSiguienteFragment(fragment)
				return true
			}
			else -> {
				return super.onOptionsItemSelected(item)
			}
		}
	}
	
	override fun onResume() {
		super.onResume()
		accionSnackbarEjecutada = false
	}
}