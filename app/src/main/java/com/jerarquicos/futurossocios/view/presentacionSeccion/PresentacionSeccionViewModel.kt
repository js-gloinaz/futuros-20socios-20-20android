package com.jerarquicos.futurossocios.view.presentacionSeccion

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.cuenta.PrincipalModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class PresentacionSeccionViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {
    private var principalModel: MutableLiveData<PrincipalModel> = MutableLiveData()
    var expediente: MutableLiveData<Expediente> = MutableLiveData()

    init {
        principalModel.value = PrincipalModel()
        expediente.value = futurosSociosRepository.obtenerExpediente()
    }

    fun enviarExpediente(): LiveData<EstadoRespuesta<RespuestaModel<Boolean>>> {
        return futurosSociosRepository.enviarExpediente(expediente.value?.id!!)
    }

    fun borrarExpediente() {
        return futurosSociosRepository.borrarExpediente(expediente.value!!.idAplicacion)
    }


}
