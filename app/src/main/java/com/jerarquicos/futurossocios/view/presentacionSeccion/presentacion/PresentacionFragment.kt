package com.jerarquicos.futurossocios.view.presentacionSeccion.presentacionFormaPago

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.PresentacionSeccionFragmentBinding
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.utils.AppConstants
import com.jerarquicos.futurossocios.utils.AppConstants.ALPHA_1
import com.jerarquicos.futurossocios.utils.enums.TipoSeccionEnum
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import javax.inject.Inject

class PresentacionFragment : BaseFragment(), Injectable {

    companion object {
        private const val BUNDLE_ID_TIPO_SECCION = "BUNDLE_ID_TIPO_SECCION"

        fun newInstance(idTipoSeccion: Int): PresentacionFragment {
            val args = Bundle()
            args.putInt(BUNDLE_ID_TIPO_SECCION, idTipoSeccion)

            val fragment = PresentacionFragment()
            fragment.arguments = args
            return fragment
        }
    }

    @Inject
    lateinit var navigationController: NavigationController
    lateinit var binding: PresentacionSeccionFragmentBinding
    var idTipoSeccion: Int = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.presentacion_seccion_fragment, container,
                false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        binding.btComenzar.setOnClickListener {
            val fragment = baseActivity.dealerViewModel.obtenerPasoSiguientePresentacionFragment(idTipoSeccion)
            navigationController.navegarSiguienteFragment(fragment)
        }
        obtenerParametros()
        mostrarInformacion(idTipoSeccion)

    }

    private fun mostrarInformacion(idTipoSeccion: Int) {
        when(idTipoSeccion){
            TipoSeccionEnum.DeclaracionJurada.valor ->{
                binding.tvTitulo.setText(R.string.algunas_preguntas_text)
                binding.clPasoUno.setBackgroundResource(R.color.colorAzulInstitucional)
                binding.clPasoDos.setBackgroundResource(R.color.colorAzulPresentacion)
                binding.clPasoTres.setBackgroundResource(R.color.colorAzulInstitucional)
                binding.btComenzar.setText(R.string.continuar_action)
                binding.llTituloDJ.isEnabled = true
                binding.llTituloFP.isEnabled = false
                binding.clPasoDos.alpha = ALPHA_1
                binding.clPasoTres.alpha = AppConstants.ALPHA_05
                binding.ivCheck.visibility = View.GONE
                binding.ivCheckDJ.visibility = View.GONE
                binding.ivCheckFP.visibility = View.GONE
            }
            TipoSeccionEnum.FormaPago.valor ->{
                binding.tvTitulo.setText(R.string.unos_datos_mas_title)
                binding.clPasoUno.setBackgroundResource(R.color.colorAzulInstitucional)
                binding.clPasoDos.setBackgroundResource(R.color.colorAzulInstitucional)
                binding.clPasoTres.setBackgroundResource(R.color.colorAzulPresentacion)
                binding.btComenzar.setText(R.string.continuar_action)
                binding.llTituloFP.isEnabled = true
                binding.clPasoTres.alpha = ALPHA_1
                binding.ivCheck.visibility = View.GONE
                binding.ivCheckDJ.visibility = View.GONE
                binding.ivCheckFP.visibility = View.GONE
            }

        }

    }

    private fun obtenerParametros() {
        idTipoSeccion = arguments!!.getInt(BUNDLE_ID_TIPO_SECCION)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }
}