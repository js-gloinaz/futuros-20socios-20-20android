package com.jerarquicos.futurossocios.view.registro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.RegistroFragmentBinding
import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.LoginModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.DatePickerFragment
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import javax.inject.Inject

class RegistroFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    private lateinit var registroViewModel: RegistroViewModel
    private lateinit var binding: RegistroFragmentBinding

    private lateinit var validadorNombre: EditTextValidator
    private lateinit var validadorApellido: EditTextValidator
    private lateinit var validadorDni: EditTextValidator
    private lateinit var validadorEmail: EditTextValidator
    private lateinit var validadorFecNac: EditTextValidator
    private lateinit var validadorPassword: EditTextValidator
    private lateinit var validadorRepetirPassword: EditTextValidator

    private val TAG_DATE_PICKER = "TAG_DATE_PICKER"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.registro_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(this.activity!!, this.javaClass.simpleName,
                this.javaClass.simpleName)
        setHasOptionsMenu(true)

        baseActivity.ocultarTeclado()

        registroViewModel = ViewModelProviders.of(this, viewModelFactory).get(RegistroViewModel::class.java)
        binding.viewModel = registroViewModel

        binding.etFechaNac.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val dlgSeleccionarFecha = DatePickerFragment()
                dlgSeleccionarFecha.setView(binding.etFechaNac)
                dlgSeleccionarFecha.show(baseActivity.supportFragmentManager, TAG_DATE_PICKER)
            }
        }
        binding.btCrearUsuario.setOnClickListener {
            if (validarDatos()) {
                registroViewModel.registroUser()
                        .observe(this, Observer<EstadoRespuesta<RespuestaModel<JwtTokenModel>>>
                        { this.handleResponse(it) })
            }
        }

        iniciarValidaciones()
    }

    private fun iniciarValidaciones() {
        validadorNombre = EditTextValidator(binding.tiNombre)
        validadorApellido = EditTextValidator(binding.tiApellido)
        validadorDni = EditTextValidator(binding.tiDni)
        validadorEmail = EditTextValidator(binding.tiEmail)
        validadorFecNac = EditTextValidator(binding.tiFechaNac)
        validadorPassword = EditTextValidator(binding.tiPassword)
        validadorRepetirPassword = EditTextValidator(binding.tiRepeatPassword)
    }

    private fun validarDatos(): Boolean {
        var hayError = true
        hayError = validadorNombre.esNombreValido() && hayError
        hayError = validadorApellido.esNombreValido() && hayError
        hayError = validadorDni.esNumeroDocumento() && hayError
        hayError = validadorEmail.esEmailOVacio() && hayError
        hayError = validadorFecNac.esFechaNacimientoValida(true) && hayError
        hayError = validadorPassword.esContrasenia() && hayError
        hayError = validadorRepetirPassword.esIgualA(binding.etPassword)
                && validadorRepetirPassword.esContrasenia() && hayError
        return hayError
    }

    private fun handleResponse(respuesta: EstadoRespuesta<RespuestaModel<JwtTokenModel>>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
                    baseActivity.ocultarProgressBar()
                    SnackbarHelper(this.view!!, respuesta.data!!.notificacion).show()
                }
                Estado.CARGANDO -> {
                    baseActivity.mostrarProgressBar()
                }
                Estado.EXITO -> {
                    baseActivity.ocultarProgressBar()
                    val datos: RespuestaModel<JwtTokenModel>? = respuesta.data
                    registroExitoso(datos)
                }
            }
        }
    }

    private fun registroExitoso(datos: RespuestaModel<JwtTokenModel>?) {
        usuarioHelper.guardarToken(datos?.modelo!!)

        val loginModel = LoginModel(registroViewModel.registroModel.value?.numeroDocumento,
                registroViewModel.registroModel.value?.contrasenia)
        usuarioHelper.guardarLogin(loginModel)

        navigationController.navegarDashboardFragment()
        SnackbarHelper(this.view!!, datos.notificacion).show()
    }
}