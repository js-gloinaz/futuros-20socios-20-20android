package com.jerarquicos.futurossocios.view.registro

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.cuenta.JwtTokenModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RegistroModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class RegistroViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {
    var registroModel: MutableLiveData<RegistroModel> = MutableLiveData()
    var fechaNacimiento: MutableLiveData<String> = MutableLiveData()

    init {
        registroModel.value = RegistroModel()
    }

    fun registroUser(): LiveData<EstadoRespuesta<RespuestaModel<JwtTokenModel>>> {
        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

        val date = format.parse(fechaNacimiento.value!!)
        registroModel.value!!.fechaNacimiento = date
        return futurosSociosRepository.registro(registroModel.value!!)
    }
}