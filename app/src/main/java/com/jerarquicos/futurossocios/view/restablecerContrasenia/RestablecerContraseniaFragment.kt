package com.jerarquicos.futurossocios.view.restablecerContrasenia

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.RestablecerContraseniaFragmentBinding
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import javax.inject.Inject

class RestablecerContraseniaFragment : Fragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var baseActivity: BaseActivity
    @Inject
    lateinit var navigationController: NavigationController
    private lateinit var restablecerContraseniaViewModel: RestablecerContraseniaViewModel
    lateinit var binding: RestablecerContraseniaFragmentBinding

    private lateinit var validadorDni: EditTextValidator
    private lateinit var validadorEmail: EditTextValidator

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.restablecer_contrasenia_fragment, container,
                false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        FirebaseAnalytics.getInstance(context!!).setCurrentScreen(this.activity!!, this.javaClass.simpleName,
                this.javaClass.simpleName)
        setHasOptionsMenu(true)

        baseActivity.ocultarTeclado()

        restablecerContraseniaViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(RestablecerContraseniaViewModel::class.java)

        binding.viewModel = restablecerContraseniaViewModel
        binding.btnEnviarContraseniaProvisoria.setOnClickListener {
            if (validarDatos()) {
                restablecerContraseniaViewModel.restablecerContraseniaUser()
                        .observe(this, Observer<EstadoRespuesta<RespuestaModel<Any>>> { this.handleResponse(it) })
            }
        }
        iniciarValidaciones()
    }

    private fun iniciarValidaciones() {
        validadorDni = EditTextValidator(binding.tiDni)
        validadorEmail = EditTextValidator(binding.tiEmail)
    }

    private fun validarDatos(): Boolean {
        val valDni: Boolean = validadorDni.esNumeroDocumento()
        val valEsEmail: Boolean = validadorEmail.esEmailOVacio()
        return valEsEmail && valDni
    }

    private fun handleResponse(respuesta: EstadoRespuesta<RespuestaModel<Any>>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
                    baseActivity.ocultarProgressBar()
                    SnackbarHelper(view!!, respuesta.data!!.notificacion).show()
                }
                Estado.CARGANDO -> {
                    baseActivity.mostrarProgressBar()
                }
                Estado.EXITO -> {
                    baseActivity.ocultarProgressBar()
                    val datos: RespuestaModel<Any> = respuesta.data!!

                    SnackbarHelper(view!!, datos.notificacion).show()
                    navigationController.navegarCambiarContraseniaFragment(binding.etDni.text.toString(), 1)
                }
            }
        }
    }
}