package com.jerarquicos.futurossocios.view.restablecerContrasenia

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.db.entity.cuenta.RestablecerContraseniaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import javax.inject.Inject

class RestablecerContraseniaViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {

    var restablecerContraseniaModel: MutableLiveData<RestablecerContraseniaModel> = MutableLiveData()

    init {
        restablecerContraseniaModel.value = RestablecerContraseniaModel()
    }

    fun restablecerContraseniaUser(): LiveData<EstadoRespuesta<RespuestaModel<Any>>> {
        return futurosSociosRepository.restablecerContrasenia(restablecerContraseniaModel.value!!)
    }
}