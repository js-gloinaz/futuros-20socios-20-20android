package com.jerarquicos.futurossocios.view.splash

import android.content.Intent
import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.BuildConfig
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.SplashFragmentBinding
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import com.jerarquicos.futurossocios.view.models.AplicacionModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class SplashFragment : BaseFragment(), Injectable {
	@Inject
	@JvmField
	var viewModelFactory: ViewModelProvider.Factory? = null
	@Inject
	lateinit var navigationController: NavigationController
	@Inject
	lateinit var usuarioHelper: UsuarioHelper
	lateinit var binding: SplashFragmentBinding
	private lateinit var splashViewModel: SplashViewModel
	private var aplicacionModel = AplicacionModel()
	private val ESTADO_OFFLINE = 0
	private val ESTADO_ONLINE = 1
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		activity!!.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
	}
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, R.layout.splash_fragment, container, false)
		return binding.root
	}
	
	override fun onActivityCreated(savedInstanceState: Bundle?) {
		super.onActivityCreated(savedInstanceState)
        inicializar(null, javaClass.name, false)
		binding.tvVersion.text = BuildConfig.VERSION_NAME
		
		splashViewModel = ViewModelProviders.of(this, viewModelFactory).get(SplashViewModel::class.java)
		
		splashViewModel.obtenerVersiones()
				.observe(this, Observer<EstadoRespuesta<RespuestaModel<AplicacionModel>>>
				{this.manejarRespuestasVersiones(it)
				})
	}
	
	/**
	 * Maneja la respuesta al intentar conectarse al servidor para obtener el modelo de aplicación
	 * Si hay algún error se deben verifica los datos de manera offline
	 * */
	private fun manejarRespuestasVersiones(respuesta: EstadoRespuesta<RespuestaModel<AplicacionModel>>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.NO_AUTENTICADO -> {
					//not implementing
				}
				Estado.ERROR, Estado.SIN_CONEXION_INTERNET -> {
					verificarDatosIniciales(ESTADO_OFFLINE)
				}
				Estado.CARGANDO -> {
					//not implementing
				}
				Estado.EXITO -> {
					aplicacionModel = respuesta.data!!.modelo!!
					if (aplicacionModel.mostrarAvisoNuevaVersion!!) {
						verificarDatosIniciales(ESTADO_ONLINE)
					}
					else {
						verificarDatosIniciales(ESTADO_OFFLINE)
					}
				}
			}
		}
	}
	
	/**
	 * Se el pasa un estado:
	 *      OFFLINE:    indica que los datos iniciales se deben obtener desde la base de datos local
	 *                  sin hacer un chequeo de versión
	 *      ONLINE:     indica que se debe hacer el chequeo de la versión de la aplicación y luego
	 *                  la de los datos iniciales
	 * */
	private fun verificarDatosIniciales(estado: Int) {
		when (estado) {
			ESTADO_OFFLINE -> {
				splashViewModel.datosInicialesBaseLocal.observe(this, Observer<EstadoRespuesta<DatosIniciales>>
				{ this.manejarRespuestaDatosInicialesOffline(it) })
			}
			ESTADO_ONLINE -> {
				controlVersionAplicacion()
			}
		}
	}
	
	/**
	 * Controla la versión de la aplicación y determina si es obsoleta, desactualizada o actualizada
	 *      OBSOLETA:        Se bloquea y no deja continuar sin actualizar
	 *      DESACTUALIZADA:  Se muestra un dialog con la opción para ir al Google Play y actualizarla
	 *      ACTUALIZADA:     Se procede a verificar la versión de los datos iniciales
	 * */
	private fun controlVersionAplicacion() {
		val versionInstalada = BuildConfig.VERSION_CODE
		//Version obsoleta
		if (versionInstalada < aplicacionModel.versionMinimaRequerida!!) {
			val builder = AlertDialog.Builder(context!!)
			builder.setTitle(getString(R.string.actualizacion_disponible_title))
			builder.setMessage(getString(R.string.aplicacion_obsoleta_text))
			builder.setPositiveButton(getString(R.string.actualizar_action)) { _, _ ->
				abrirGooglePlay()
			}
			builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->
				activity!!.finish()
			}
			val dialog: AlertDialog = builder.create()
			dialog.setCancelable(false)
			dialog.show()
		}
		//Version desactualizada
		else if (versionInstalada >= aplicacionModel.versionMinimaRequerida!!
				&& versionInstalada < aplicacionModel.versionActual!!) {
			val format = SimpleDateFormat("dd/MM/yyy", Locale.getDefault())
			val builder = AlertDialog.Builder(context!!)
			builder.setTitle(getString(R.string.actualizacion_disponible_title))
			builder.setMessage(getString(R.string.actualizacion_disponible_text, aplicacionModel.nombreVersion,
					format.format(aplicacionModel.fechaCaducidad)))
			builder.setPositiveButton(getString(R.string.actualizar_action)) { _, _ -> abrirGooglePlay()	}
			builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->	obtenerDatosInicialesLocales()	}
			val dialog: AlertDialog = builder.create()
			dialog.setCancelable(false)
			dialog.show()
		}
		//Version actualizada
		else {
			obtenerDatosInicialesLocales()
		}
	}
	
	/**
	 * Hace la llamada al repositorio local para obtener los datos iniciales
	 * */
	private fun obtenerDatosInicialesLocales() {
		splashViewModel.datosInicialesBaseLocal.observe(this, Observer<EstadoRespuesta<DatosIniciales>>
		{ this.manejarRespuestaDatosInicialesOffline(it) })
	}
	
	/**
	 * Verifica los datos iniciales sin hacer las verificaciones de las versiones de la app ni de
	 * la version de los datos iniciales
	 * Si los datos existen se ejecuta la siguiente vista
	 * Si no se buscan al servidor y no se permite continuar hasta que se obtengan correctamente
	 * */
	private fun manejarRespuestaDatosInicialesOffline(respuesta: EstadoRespuesta<DatosIniciales>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.EXITO -> {
					if (respuesta.data != null) {
						if (aplicacionModel.versionDatosgenerales != respuesta.data.version) {
							splashViewModel.obtenerDatosInicialesOnline()
									.observe(this, Observer<EstadoRespuesta<RespuestaModel<DatosIniciales>>>
									{ it ->	this.manejarRespuestaDatosInicialesOnline(it, respuesta.data)
							})
						}
						else {
							ejecutarProximaVista(respuesta.data)
						}
					}
					else {
						splashViewModel.obtenerDatosInicialesOnline()
								.observe(this, Observer<EstadoRespuesta<RespuestaModel<DatosIniciales>>>
								{ this.manejarDatosInicialesResponse(it) })
					}
				}
				else -> {
					//not implementing
				}
			}
		}
	}
	
	/**
	 * Cuando los datos iniciales se actualizan en el servidor a una nueva versión, se actualizan de
	 * manera transparente al usuario*/
	private fun manejarRespuestaDatosInicialesOnline(respuesta: EstadoRespuesta<RespuestaModel<DatosIniciales>>?,
													 datosInicialesLocales: DatosIniciales) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.ERROR, Estado.NO_AUTENTICADO, Estado.SIN_CONEXION_INTERNET -> {
					baseActivity.ocultarProgressBar()
					ejecutarProximaVista(datosInicialesLocales)
				}
				Estado.CARGANDO -> {
					baseActivity.mostrarProgressBar()
				}
				Estado.EXITO -> {
					baseActivity.ocultarProgressBar()
					ejecutarProximaVista(respuesta.data?.modelo!!)
				}
			}
		}
	}
	
	/**
	 * Maneja la respuesta del servidor cuando es la primera vez que se inicia la app
	 * Si no contiene datos se necesita si o si conexion a internet para obtener los datos iniciales
	 **/
	private fun manejarDatosInicialesResponse(respuesta: EstadoRespuesta<RespuestaModel<DatosIniciales>>?) {
		if (respuesta != null) {
			when (respuesta.estado) {
				Estado.ERROR, Estado.NO_AUTENTICADO -> {
					baseActivity.ocultarProgressBar()
					val builder = AlertDialog.Builder(context!!)
					builder.setTitle(getString(R.string.primer_inicio_error))
					builder.setMessage(respuesta.data?.notificacion?.mensaje!!)
					builder.setPositiveButton(getString(R.string.reintentar_action)) { _, _ ->
						navigationController.navegarSplashFragment()	}
					builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->	activity!!.finish()	}
					val dialog: AlertDialog = builder.create()
					dialog.setCancelable(false)
					dialog.show()
				}
				Estado.SIN_CONEXION_INTERNET -> {
					baseActivity.ocultarProgressBar()
					val builder = AlertDialog.Builder(context!!).setTitle(getString(R.string.vacio_text))
					builder.setMessage(getString(R.string.primer_inicio_sin_conexion_text))
					builder.setPositiveButton(getString(R.string.reintentar_action)) { _, _ ->
						navigationController.navegarSplashFragment()}
					builder.setNegativeButton(getString(R.string.cancelar_action)) { _, _ ->	activity!!.finish()	}
					val dialog: AlertDialog = builder.create()
					dialog.setCancelable(false)
					dialog.show()
				}
				Estado.CARGANDO -> {
					baseActivity.mostrarProgressBar()
				}
				Estado.EXITO -> {
					baseActivity.ocultarProgressBar()
					ejecutarProximaVista(respuesta.data?.modelo!!)
				}
			}
		}
	}
	
	private fun abrirGooglePlay() {
		val appPackageName = activity!!.packageName
		try {
			startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
		} catch (anfe: android.content.ActivityNotFoundException) {
			startActivity(Intent(Intent.ACTION_VIEW,
					Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
		}
		activity!!.finish()
	}
	
	private fun ejecutarProximaVista(datosIniciales: DatosIniciales) {
		baseActivity.dealerViewModel.datosIniciales.value = datosIniciales
		if (usuarioHelper.sesionIniciada()) {
			navigationController.navegarDashboardFragment()
		}
		else {
			navigationController.navegarInicioFragment()
		}
	}
}