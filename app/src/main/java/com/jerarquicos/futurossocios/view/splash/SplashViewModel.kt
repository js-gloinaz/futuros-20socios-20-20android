package com.jerarquicos.futurossocios.view.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.cuenta.RespuestaModel
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.models.AplicacionModel
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository) : ViewModel() {

    val datosInicialesBaseLocal: LiveData<EstadoRespuesta<DatosIniciales>>
            = futurosSociosRepository.obtenerDatosInicialesBaseLocal()

    fun obtenerDatosInicialesOnline(): LiveData<EstadoRespuesta<RespuestaModel<DatosIniciales>>> {
        return futurosSociosRepository.obtenerDatosIniciales()
    }

    fun obtenerVersiones(): LiveData<EstadoRespuesta<RespuestaModel<AplicacionModel>>> {
        return futurosSociosRepository.obtenerVersiones()
    }

}