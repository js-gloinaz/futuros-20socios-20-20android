package com.jerarquicos.futurossocios.view.tarjetaDeCredito

import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.TarjetaDeCreditoFragmentBinding
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.FormaPago
import com.jerarquicos.futurossocios.db.entity.TipoTarjetaCredito
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import com.jerarquicos.futurossocios.view.common.utils.UsuarioHelper
import java.util.*
import javax.inject.Inject


class TarjetaDeCreditoFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController
    @Inject
    lateinit var usuarioHelper: UsuarioHelper
    lateinit var binding: TarjetaDeCreditoFragmentBinding
    private lateinit var tarjetaDeCreditoViewModel: TarjetaDeCreditoViewModel
    private lateinit var validadorTarjetaDeCredito: EditTextValidator

    companion object {
        private const val BUNDLE_CUENTA = "BUNDLE_CUENTA"

        fun newInstance(formaPago: FormaPago?): TarjetaDeCreditoFragment {
            val fragment = TarjetaDeCreditoFragment()
            val args = Bundle()
            if (formaPago != null) {
                args.putSerializable(BUNDLE_CUENTA, formaPago)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.tarjeta_de_credito_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)

        tarjetaDeCreditoViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(TarjetaDeCreditoViewModel::class.java)
        binding.viewModel = tarjetaDeCreditoViewModel

        val formaDePago = arguments!!.getSerializable(BUNDLE_CUENTA) as FormaPago
        tarjetaDeCreditoViewModel.tarjetaDeCreditoModel.value = formaDePago


        binding.tvNombreApellido.text = tarjetaDeCreditoViewModel.nombreTitular()

        inicializarChangeListener()
        inicializarListener()
        cargarSpinnerTarjetasDeCredito()
        setPropiedadesNumberPicker(formaDePago)
        iniciarValidaciones()
    }

    private fun inicializarChangeListener(){
        binding.etNumeroTarjeta.addTextChangedListener(object : TextWatcher {
            private var vacio = ""
            override fun afterTextChanged(p0: Editable?) {
                if (p0.toString() != vacio) {
                    val userInput = p0.toString().replace("[^\\d]".toRegex(), "")
                    if (userInput.length <= 16) {
                        vacio = formatearNumeroTarjeta(userInput)
                        p0?.filters = arrayOfNulls<InputFilter>(0)
                    }
                    p0?.replace(0, p0.length, vacio, 0, vacio.length)
                    binding.tvNumeroTarjeta.text = binding.etNumeroTarjeta.text.toString()

                }

            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //not implementing
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //not implementing
            }
        })
    }

    private fun inicializarListener(){
        binding.npMes.setOnValueChangedListener { _, _, _ ->
            val fecha = binding.npMes.value.toString() + "/" + binding.npAnio.value.toString()
            binding.tvFechaVencimiento.text = fecha
        }
        binding.npAnio.setOnValueChangedListener { _, _, _ ->
            val fecha = binding.npMes.value.toString() + "/" + binding.npAnio.value.toString()
            binding.tvFechaVencimiento.text = fecha
        }

        binding.spTarjetaDeCredito.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2 == 0) {
                    binding.ivIconoTarjeta.setImageResource(R.drawable.nativa)
                }
                if (p2 == 1) {
                    binding.ivIconoTarjeta.setImageResource(R.drawable.ic_visa)
                }
                if (p2 == 2) {
                    binding.ivIconoTarjeta.setImageResource(R.drawable.ic_mastercard)
                }
                if (p2 == 3) {
                    binding.ivIconoTarjeta.setImageResource(R.drawable.cabal)
                }
                tarjetaDeCreditoViewModel.guardarTarjetaDeCredito(p2)
            }
        }
        binding.btAccion.setOnClickListener {
            if (validarDatos()) {
                tarjetaDeCreditoViewModel.guardarFechaVencimiento(binding.npMes.value, binding.npAnio.value)
                val fragment = baseActivity.dealerViewModel
                        .obtenerPasoSiguienteTarjetaDeCreditoFragment(
                                tarjetaDeCreditoViewModel.tarjetaDeCreditoModel.value!!)
                navigationController.navegarSiguienteFragment(fragment)
            }
        }
    }

    private fun iniciarValidaciones() {
        validadorTarjetaDeCredito = EditTextValidator(binding.tiNumeroTarjeta)

    }

    private fun validarDatos(): Boolean {
        val numero = validadorTarjetaDeCredito.esNumeroDeTarjetaValido()
        val fechaTarjetaCredito = validarFechaTarjetaCredito(binding.npMes.value, binding.npAnio.value)
        return numero && fechaTarjetaCredito
    }

    private fun validarFechaTarjetaCredito(mes: Int, anio: Int): Boolean {
        var esValido = false
        val anioActual = Calendar.getInstance().get(Calendar.YEAR)
        val mesActual = Calendar.getInstance().get(Calendar.MONTH) + 1

        if (anio > anioActual) {
            binding.llMesError.visibility = View.GONE
            binding.llAnioError.visibility = View.GONE
            esValido = true
        } else {
            if (anio == anioActual) {
                if (mes >= mesActual) {
                    binding.llMesError.visibility = View.GONE
                    binding.llAnioError.visibility = View.GONE
                    esValido = true
                } else {
                    binding.llMesError.visibility = View.VISIBLE
                    binding.llAnioError.visibility = View.GONE
                    esValido = false
                }
            } else {
                binding.llMesError.visibility = View.VISIBLE
                binding.llAnioError.visibility = View.VISIBLE
                esValido = false
            }
        }
        return esValido
    }

    private fun formatearNumeroTarjeta(userInput: String):String{
        val sb = StringBuilder()
        for (i in 0..userInput.length - 1) {
            if (i % 4 == 0 && i > 0) {
                sb.append(" ")
            }
            sb.append(userInput[i])
        }
        return sb.toString()
    }

    private fun setPropiedadesNumberPicker(formaPago: FormaPago?) {
        binding.npMes.minValue = 1
        binding.npMes.maxValue = 12
        binding.npAnio.minValue = Calendar.getInstance().get(Calendar.YEAR)
        binding.npAnio.maxValue = Calendar.getInstance().get(Calendar.YEAR) + 10
        binding.npMes.wrapSelectorWheel = true
        binding.npAnio.wrapSelectorWheel = true
        if (formaPago?.vencimiento != null) {
            tarjetaDeCreditoViewModel.cargarFechaVencimiento()
            binding.npMes.value = tarjetaDeCreditoViewModel.mesDeVencimiento.value!!
            binding.npAnio.value = tarjetaDeCreditoViewModel.anioDeVencimiento.value!!
            binding.tvFechaVencimiento.text = tarjetaDeCreditoViewModel.fechaVencimiento()
        }
    }

    private fun cargarSpinnerTarjetasDeCredito() {
        tarjetaDeCreditoViewModel.obtenerTarjetasDeCredito()
                .observe(this, Observer<EstadoRespuesta<DatosIniciales>>
                { this.handleResponseTarjetasDeCredito(it) })
    }

    private fun cargarSpTartjetasDeCredito(tarjetasDeCredito: List<TipoTarjetaCredito>) {
        val arrTarjetasDeCredito: ArrayList<String> = ArrayList()
        for (i in tarjetasDeCredito.indices) {
            var descripcion = ""
            descripcion += tarjetasDeCredito[i].descripcion
            arrTarjetasDeCredito.add(descripcion)
        }
        val adapter = ArrayAdapter<String>(
                context!!,
                R.layout.spinner_item_seleccionado_layout,
                arrTarjetasDeCredito
        )
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spTarjetaDeCredito.adapter = adapter
        binding.spTarjetaDeCredito.visibility = View.VISIBLE
    }

    private fun handleResponseTarjetasDeCredito(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpTartjetasDeCredito(respuesta.data.tipoTarjetaCredito)
                        tarjetaDeCreditoViewModel.guardarTarjetasDeCredito(respuesta.data.tipoTarjetaCredito)
                        binding.spTarjetaDeCredito
                                .setSelection(tarjetaDeCreditoViewModel.obtenerPosicionTarjetaDeCreditoSeleccionado())
                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base_oscuro, menu)
    }
}
