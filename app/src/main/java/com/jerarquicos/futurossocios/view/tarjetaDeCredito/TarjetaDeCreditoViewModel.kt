package com.jerarquicos.futurossocios.view.tarjetaDeCredito

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jerarquicos.futurossocios.db.entity.DatosIniciales
import com.jerarquicos.futurossocios.db.entity.Expediente
import com.jerarquicos.futurossocios.db.entity.FormaPago
import com.jerarquicos.futurossocios.db.entity.TipoTarjetaCredito
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class TarjetaDeCreditoViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : ViewModel() {

    var tarjetaDeCreditoModel: MutableLiveData<FormaPago> = MutableLiveData()
    var mesDeVencimiento: MutableLiveData<Int> = MutableLiveData()
    var anioDeVencimiento: MutableLiveData<Int> = MutableLiveData()
    var tarjetasDeCredito: MutableLiveData<List<TipoTarjetaCredito>> = MutableLiveData()
    var expediente: MutableLiveData<Expediente> = MutableLiveData()

    init {
        expediente.value = futurosSociosRepository.obtenerExpediente()
    }

    fun nombreTitular(): String {
        return expediente.value?.titular?.nombre + " " + expediente.value?.titular?.apellido
    }

    fun fechaVencimiento(): String {
        return mesDeVencimiento.value.toString() + "/" + anioDeVencimiento.value.toString()
    }

    fun obtenerTarjetasDeCredito(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun guardarTarjetasDeCredito(tarjetaDeCredito: List<TipoTarjetaCredito>) {
        this.tarjetasDeCredito.value = tarjetaDeCredito
    }

    fun guardarTarjetaDeCredito(posicion: Int) {
        tarjetaDeCreditoModel.value?.tipoTarjetaCredito = tarjetasDeCredito.value!![posicion]
    }

    fun obtenerPosicionTarjetaDeCreditoSeleccionado(): Int {
        var index = 0
        if (tarjetaDeCreditoModel.value != null) {
            this.tarjetasDeCredito.value!!.forEachIndexed { i, element ->
                if (element.id == tarjetaDeCreditoModel.value!!.tipoTarjetaCredito?.id) {
                    index = i
                }
            }
        }
        return index
    }

    fun guardarFechaVencimiento(mesVencimiento: Int, anioVencimiento: Int) {
        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val date = format.parse("01/" + mesVencimiento.toString() + "/" + anioVencimiento.toString())
            tarjetaDeCreditoModel.value?.vencimiento = date
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        mesDeVencimiento.value = tarjetaDeCreditoModel.value?.vencimiento?.month
        anioDeVencimiento.value = tarjetaDeCreditoModel.value?.vencimiento?.year
    }

    fun cargarFechaVencimiento() {
        var fechaVencimiento = null.toString()
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val dateTime = dateFormat.format(tarjetaDeCreditoModel.value?.vencimiento!!)
            fechaVencimiento = dateTime
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val mesAuxiliar = fechaVencimiento.substringBeforeLast('/')
        val mes = mesAuxiliar.substringAfterLast('/')
        val anio = fechaVencimiento.substringAfterLast('/')

        mesDeVencimiento.value = mes.toInt()
        anioDeVencimiento.value = anio.toInt()
    }
}