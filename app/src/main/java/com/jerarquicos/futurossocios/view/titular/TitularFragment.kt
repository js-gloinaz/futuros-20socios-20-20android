package com.jerarquicos.futurossocios.view.titular

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.analytics.FirebaseAnalytics
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.databinding.TitularFragmentBinding
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.db.entity.cuenta.NotificacionModel
import com.jerarquicos.futurossocios.di.Injectable
import com.jerarquicos.futurossocios.repository.util.estado.Estado
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.utils.enums.TipoNotificacion
import com.jerarquicos.futurossocios.view.base.BaseActivity
import com.jerarquicos.futurossocios.view.base.BaseFragment
import com.jerarquicos.futurossocios.view.common.NavigationController
import com.jerarquicos.futurossocios.view.common.utils.DatePickerFragment
import com.jerarquicos.futurossocios.view.common.utils.EditTextValidator
import com.jerarquicos.futurossocios.view.common.utils.SnackbarHelper
import javax.inject.Inject
import kotlin.collections.ArrayList

class TitularFragment : BaseFragment(), Injectable {

    @Inject
    @JvmField
    var viewModelFactory: ViewModelProvider.Factory? = null
    @Inject
    lateinit var navigationController: NavigationController

    private var isDomicilioCorrespondencia = false
    private lateinit var titularViewModel: TitularViewModel
    private lateinit var binding: TitularFragmentBinding
    private lateinit var validadorNombre: EditTextValidator
    private lateinit var validadorApellido: EditTextValidator
    private lateinit var validadorDni: EditTextValidator
    private lateinit var validadorFecNac: EditTextValidator
    private lateinit var validadorEmail: EditTextValidator
    private lateinit var validadorDomicilio: EditTextValidator
    private lateinit var validadorDomicilioAux: EditTextValidator
    private lateinit var validadorCuil: EditTextValidator
    private lateinit var validadorTelCodIntParticular: EditTextValidator
    private lateinit var validadorTelCodIntCelular: EditTextValidator
    private lateinit var validadorTelCodAreaParticular: EditTextValidator
    private lateinit var validadorTelCodAreaCelular: EditTextValidator
    private lateinit var validadorTelNumeroParticular: EditTextValidator
    private lateinit var validadorTelNumeroCelular: EditTextValidator


    private val TAG_DATE_PICKER = "TAG_DATE_PICKER"

    companion object {
        private const val BUNDLE_TITULAR = "BUNDLE_TITULAR"

        fun newInstance(titular: Titular?): TitularFragment {
            val fragment = TitularFragment()
            val args = Bundle()
            if (titular != null) {
                args.putSerializable(BUNDLE_TITULAR, titular)
            }
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.titular_fragment, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        inicializar(binding.toolbar, javaClass.name, true)
        titularViewModel = ViewModelProviders.of(this, viewModelFactory).get(TitularViewModel::class.java)
        binding.viewModel = titularViewModel

        obtenerParametros()
        inicializarListenerDomicilio()
        inicializarListenerSpinners()
        inicializarListener()

        titularViewModel.setearCodigosInternacionales(context!!)
        cargarSpinnerSexo()
        cargarSpinnerEstadoCivil()
        iniciarValidaciones()
    }

    private fun inicializarListenerSpinners(){
        binding.spSexo.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                titularViewModel.guardarSexo(p2)
            }
        }

        binding.spEstadoCivil.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //not implementing
            }
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                titularViewModel.guardarEstadoCivil(p2)
            }
        }
    }

    private fun inicializarListener(){
        binding.etFechaNac.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                val dlgSeleccionarFecha = DatePickerFragment()
                dlgSeleccionarFecha.setView(binding.etFechaNac)
                dlgSeleccionarFecha.show(baseActivity.supportFragmentManager, TAG_DATE_PICKER)
            }
        }

        binding.btValidar.setOnClickListener {
            if (validarDatos()) {
                if (validarDniExistente()) {
                    binding.tiDni.error = getString(R.string.dni_existente_error)
                } else {
                    titularViewModel.guardarDomicilio(titularViewModel.domicilio.value!!,
                            titularViewModel.domicilioCorrespondencia.value)
                    titularViewModel.guardarTelefono(binding.etCodIntParticular.text.toString(),
                            binding.etCodAreaParticular.text.toString(), binding.etNumeroParticular.text.toString(),
                            binding.etCodIntCelular.text.toString(), binding.etCodAreaCelular.text.toString(),
                            binding.etNumeroCelular.text.toString())
                    titularViewModel.guardarFechaNacimiento()
                    val fragment = baseActivity.dealerViewModel
                            .obtenerPasoSiguienteTitularFragment(titularViewModel.titularModel.value!!)
                    // Sincronizo con el servidor
                    baseActivity.sincronizarExpediente(true, true)
                    // Ejecuto próxima vista
                    navigationController.navegarSiguienteFragment(fragment)
                }
            }
        }
    }

    private fun inicializarListenerDomicilio(){
        binding.etDomicilio.setOnClickListener {
            isDomicilioCorrespondencia = false
            if (Build.VERSION.SDK_INT >= 21) {
                binding.etDomicilio.showSoftInputOnFocus = false
            } else {
                binding.etDomicilio.setRawInputType(InputType.TYPE_CLASS_TEXT)
                binding.etDomicilio.setTextIsSelectable(true)
            }
            navigationController.navegarDomicilioFragment(this, titularViewModel.domicilio.value)
        }

        binding.etDomicilioCorrespondencia.setOnClickListener {
            isDomicilioCorrespondencia = true
            binding.tiDomicilioCorrespondencia.error = null
            if (Build.VERSION.SDK_INT >= 21) {
                binding.etDomicilioCorrespondencia.showSoftInputOnFocus = false
            } else {
                binding.etDomicilioCorrespondencia.setRawInputType(InputType.TYPE_CLASS_TEXT)
                binding.etDomicilioCorrespondencia.setTextIsSelectable(true)
            }
            navigationController.navegarDomicilioFragment(this, titularViewModel.domicilioCorrespondencia.value)
        }

        binding.cbDomCorrespondencia.setOnClickListener {
            if (binding.cbDomCorrespondencia.isChecked) {
                binding.tiDomicilioCorrespondencia.error = null
                binding.tvTituloDomicilioDeCorrespondencia.visibility = View.GONE
                binding.tiDomicilioCorrespondencia.visibility = View.GONE
                titularViewModel.domCorrespondencia.value = true
                titularViewModel.domicilioCorrespondencia.value = null
                binding.etDomicilioCorrespondencia.text = null
            } else {
                titularViewModel.domCorrespondencia.value = false
                binding.tvTituloDomicilioDeCorrespondencia.visibility = View.VISIBLE
                binding.tiDomicilioCorrespondencia.visibility = View.VISIBLE
            }
        }
    }

    private fun obtenerParametros(){
        val titular = arguments!!.getSerializable(BUNDLE_TITULAR) as Titular
        if (titular.nombre != null) {
            titularViewModel.titularModel.value = titular

            if (titular.domicilio?.count()!! > 1) {
                titularViewModel.domCorrespondencia.value = false
                binding.tvTituloDomicilioDeCorrespondencia.visibility = View.VISIBLE
                binding.tiDomicilioCorrespondencia.visibility = View.VISIBLE
                titularViewModel.domicilioCorrespondencia.value = titular.domicilio!![1]
            }
            if (titular.personaPoliticamenteExpuesta != null) {
                titularViewModel.titularModel.value!!
                        .personaPoliticamenteExpuesta = titular.personaPoliticamenteExpuesta
            }
            if (titular.domicilio!![0].calle.equals(context!!.resources.getString(R.string.vacio_text))
                    || titular.domicilio!![0].numero.equals(context!!.resources.getString(R.string.vacio_text))) {
                titularViewModel.setearDomicilio(titular.domicilio!![0], false)
                titularViewModel.domicilio.value?.localidad = null
                titularViewModel.domicilio.value?.localidad?.provincia = null
            }

            if (titularViewModel.domicilioCorrespondencia.value?.calle.
                            equals(context!!.resources.getString(R.string.vacio_text))
                    || titularViewModel.domicilioCorrespondencia.value?.numero.
                            equals(context!!.resources.getString(R.string.vacio_text))) {
                titularViewModel.setearDomicilio(titular.domicilio!![1], true)
                titularViewModel.domicilioCorrespondencia.value?.localidad = null
                titularViewModel.domicilioCorrespondencia.value?.localidad?.provincia = null
            }

            titularViewModel.domicilio.value = titular.domicilio!![0]
            titularViewModel.cargarDomicilio(titular.domicilio!!)
            titularViewModel.cargarEstadoCivil(titular.estadoCivil!!)
            titularViewModel.cargarSexo(titular.sexo!!)
            titularViewModel.cargarFechaNacimiento()
        }
    }

    private fun validarDniExistente(): Boolean {
        return titularViewModel.validarDniExistente()
    }

    private fun cargarSpinnerEstadoCivil() {
        titularViewModel.obtenerEstadoCivil().observe(this, Observer<EstadoRespuesta<DatosIniciales>>
        { this.handleResponseEstadoCivil(it) })
    }

    private fun cargarSpinnerSexo() {
        titularViewModel.obtenerSexo().observe(this, Observer<EstadoRespuesta<DatosIniciales>>
        { this.handleResponseSexo(it) })
    }

    private fun iniciarValidaciones() {
        validadorNombre = EditTextValidator(binding.tiNombre)
        validadorApellido = EditTextValidator(binding.tiApellido)
        validadorDni = EditTextValidator(binding.tiDni)
        validadorEmail = EditTextValidator(binding.tiEmail)
        validadorFecNac = EditTextValidator(binding.tiFechaNac)
        validadorCuil = EditTextValidator(binding.tiCuil)
        validadorDomicilio = EditTextValidator(binding.tiDomicilio)
        validadorDomicilioAux = EditTextValidator(binding.tiDomicilioCorrespondencia)
        validadorTelCodIntParticular = EditTextValidator(binding.tiCodIntParticular)
        validadorTelCodIntCelular = EditTextValidator(binding.tiCodIntCelular)
        validadorTelCodAreaParticular = EditTextValidator(binding.tiCodAreaParticular)
        validadorTelCodAreaCelular = EditTextValidator(binding.tiCodAreaCelular)
        validadorTelNumeroParticular = EditTextValidator(binding.tiNumeroParticular)
        validadorTelNumeroCelular = EditTextValidator(binding.tiNumeroCelular)
        validadorEmail = EditTextValidator(binding.tiEmail)
    }

    private fun validarDatos(): Boolean {
        var hayError = true
        hayError = validadorNombre.esNombreValido() && hayError
        hayError = validadorApellido.esNombreValido() && hayError
        hayError = validadorDni.esNumeroDocumento() && hayError
        hayError = validadorEmail.esEmailOVacio() && hayError
        hayError = validadorFecNac.esFechaNacimientoValida(true) && hayError
        hayError = validadorDomicilio.esNoVacio() && hayError
        hayError = validadorCuil.esCuilValido(binding.etDni.text.toString()) && hayError
        hayError  = validadorTelCodIntParticular
                .esCodInternacionalNoObligatorio(binding.etCodAreaParticular.text.toString(),
                        binding.etNumeroParticular.text.toString()) && hayError
        hayError = validadorTelCodAreaParticular
                .esCodAreaNumeroNoObligatorio(binding.etNumeroParticular.text.toString()) && hayError
        hayError = validadorTelNumeroParticular
                .esNumeroAreaNoObligatorioParticular(binding.etCodAreaParticular.text.toString()) && hayError
        hayError = validadorTelCodIntCelular.esCodInternacionalObligatorio() && hayError
        hayError = validadorTelCodAreaCelular.esCodAreaObligatorio() && hayError
        hayError = validadorTelNumeroCelular.esTelefonoCelular() && hayError
        hayError = validadorTelNumeroCelular
                .validarTelefonosIguales(binding.etCodAreaParticular.text.toString(),
                        binding.etCodAreaCelular.text.toString(), binding.etNumeroParticular.text.toString())
                && hayError

        if (!binding.cbDomCorrespondencia.isChecked) {
            hayError = validadorDomicilioAux.esNoVacio() && hayError
            val domicilioIguales = validadorDomicilio.domiciliosIguales(binding.etDomicilioCorrespondencia)
            hayError = !domicilioIguales && hayError
            if (domicilioIguales) {
                binding.tvTituloDomicilioDeCorrespondencia.visibility = View.GONE
                binding.tiDomicilioCorrespondencia.visibility = View.GONE
                titularViewModel.domCorrespondencia.value = true
                titularViewModel.domicilioCorrespondencia.value = null
                binding.etDomicilioCorrespondencia.text = null
                hayError = false
            }
            return hayError
        } else {
            return hayError
        }
    }

    private fun cargarSpEstadoCivil(estadoCivil: List<EstadoCivil>) {
        val arrEstadoCivil: ArrayList<String> = ArrayList()
        for (i in estadoCivil.indices) {
            var descripcion = ""
            descripcion += estadoCivil[i].descripcion
            arrEstadoCivil.add(descripcion)
        }

        val adapter = ArrayAdapter<String>(
                context!!,
                R.layout.spinner_item_seleccionado_layout,
                arrEstadoCivil
        )

        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spEstadoCivil.adapter = adapter
        binding.spEstadoCivil.visibility = View.VISIBLE
    }


    private fun handleResponseEstadoCivil(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpEstadoCivil(respuesta.data.estadoCivil)
                        titularViewModel.guardarEstadosCiviles(respuesta.data.estadoCivil)
                        binding.spEstadoCivil.setSelection(titularViewModel.obtenerPosicionEstadoCivilSeleccionado())

                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    private fun cargarSpSexos(sexos: List<Sexo>) {
        titularViewModel.cargarSpSexos(sexos)
        val adapter = ArrayAdapter<String>(
                context!!,
                R.layout.spinner_item_seleccionado_layout,
                titularViewModel.arrSexos.value!!
        )
        adapter.setDropDownViewResource(R.layout.spinner_item_layout)
        binding.spSexo.adapter = adapter
        binding.spSexo.visibility = View.VISIBLE
    }

    private fun handleResponseSexo(respuesta: EstadoRespuesta<DatosIniciales>?) {
        if (respuesta != null) {
            when (respuesta.estado) {
                Estado.EXITO -> {
                    if (respuesta.data != null) {
                        cargarSpSexos(respuesta.data.sexo)
                        titularViewModel.guardarSexos(respuesta.data.sexo)
                        binding.spSexo.setSelection(titularViewModel.obtenerPosicionSexoSeleccionado())
                    } else {
                        SnackbarHelper(this.view!!,
                                NotificacionModel(getString(R.string.procesar_solicitud_error),
                                TipoNotificacion.Error)).show()
                    }
                }
                else -> {
                    //not implementing
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (isDomicilioCorrespondencia) {
            titularViewModel.domicilioCorrespondencia.value = data?.getSerializableExtra("domicilio") as Domicilio
            titularViewModel.setearDomicilio(titularViewModel.domicilioCorrespondencia.value!!,
                    isDomicilioCorrespondencia)
        } else {
            titularViewModel.domicilio.value = data?.getSerializableExtra("domicilio") as Domicilio
            titularViewModel.setearDomicilio(titularViewModel.domicilio.value!!, isDomicilioCorrespondencia)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_base, menu)
    }
}