package com.jerarquicos.futurossocios.view.titular

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.jerarquicos.futurossocios.R
import com.jerarquicos.futurossocios.db.entity.*
import com.jerarquicos.futurossocios.repository.FuturosSociosRepository
import com.jerarquicos.futurossocios.repository.util.estado.EstadoRespuesta
import com.jerarquicos.futurossocios.view.persona.PersonaViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class TitularViewModel @Inject constructor(private val futurosSociosRepository: FuturosSociosRepository)
    : PersonaViewModel(futurosSociosRepository) {
    var titularModel: MutableLiveData<Titular> = MutableLiveData()
    var domCorrespondencia = MutableLiveData<Boolean>()
    var domicilioCorrespondencia = MutableLiveData<Domicilio>()
    var domicilio = MutableLiveData<Domicilio>()
    var descripcionDomicilio: MutableLiveData<String> = MutableLiveData()
    var fechaDeNacimiento: MutableLiveData<String> = MutableLiveData()
    var expediente: MutableLiveData<Expediente> = MutableLiveData()
    private var estadosCiviles: MutableLiveData<List<EstadoCivil>> = MutableLiveData()
    var descripcionDomicilioCorrespondencia: MutableLiveData<String> = MutableLiveData()
    private val PISO = " - Piso: "
    private val DEPTO = " - Depto: "

    init {
        titularModel.value = Titular()
        domCorrespondencia.value = true
        expediente.value = futurosSociosRepository.obtenerExpediente()
    }

    fun guardarEstadosCiviles(estadoCiviles: List<EstadoCivil>) {
        this.estadosCiviles.value = estadoCiviles
    }

    fun obtenerEstadoCivil(): LiveData<EstadoRespuesta<DatosIniciales>> {
        return futurosSociosRepository.obtenerDatosInicialesBaseLocal()
    }

    fun setearDomicilio(domicilio: Domicilio, esDomicilioCorrespondencia: Boolean) {
        val mostrarCalleNum = domicilio.calle + " " + domicilio.numero
        var mostrarPiso = ""
        var mostrarDepto = ""
        if (!domicilio.piso.equals("")) {
            mostrarPiso = PISO + domicilio.piso
        }
        if (!domicilio.departamento.equals("")) {
            mostrarDepto = DEPTO + domicilio.departamento
        }
        if (esDomicilioCorrespondencia) {
            descripcionDomicilioCorrespondencia.value = mostrarCalleNum + mostrarPiso + mostrarDepto
        } else {
            descripcionDomicilio.value = mostrarCalleNum + mostrarPiso + mostrarDepto
        }
    }

    fun cargarDomicilio(domicilios: ArrayList<Domicilio>) {
        val mostrarCalleNum = domicilios[0].calle + " " + domicilios[0].numero
        var mostrarPiso = ""
        var mostrarDepto = ""
        if (!domicilios[0].piso.equals("")) {
            mostrarPiso = PISO + domicilios[0].piso
        }
        if (!domicilios[0].departamento.equals("")) {
            mostrarDepto = DEPTO + domicilios[0].departamento
        }
        descripcionDomicilio.value = mostrarCalleNum + mostrarPiso + mostrarDepto
        titularModel.value?.domicilio!![0].envioCorrespondencia = true

        if (domicilios.count() > 1) {
            val mostrarCalleNumCorrespondencia = domicilios[1].calle + " " + domicilios[1].numero
            var mostrarPisoCorrespondencia = ""
            var mostrarDeptoCorrespondencia = ""
            if (!domicilios[1].piso.equals("")) {
                mostrarPisoCorrespondencia = " - Piso: " + domicilios[1].piso
            }
            if (!domicilios[1].departamento.equals("")) {
                mostrarDeptoCorrespondencia = " - Depto: " + domicilios[1].departamento
            }
            descripcionDomicilioCorrespondencia.value = mostrarCalleNumCorrespondencia +
                    mostrarPisoCorrespondencia + mostrarDeptoCorrespondencia
            titularModel.value?.domicilio!![0].envioCorrespondencia = false
            titularModel.value?.domicilio!![1].envioCorrespondencia = true
        }
    }

    fun guardarDomicilio(domicilio: Domicilio, domicilioCorrespondencia: Domicilio?) {
        val listaDomicilios = ArrayList<Domicilio>()

        if (domicilio.id == null) {
            domicilio.id = 0
        }

        val domicilioFinal = Domicilio(1, domicilio.id, domicilio.calle, domicilio.numero, domicilio.piso,
                domicilio.departamento,
                Localidad(domicilio.localidad?.id, domicilio.localidad?.descripcion, domicilio.localidad?.codigoPostal,
                        domicilio.localidad?.provincia!!),
                false)
        listaDomicilios.add(domicilioFinal)

        if (domicilioCorrespondencia != null) {
            if (domicilioCorrespondencia.id == null) {
                domicilioCorrespondencia.id = 0
            }
            val domicilioFinalCorrespondencia = Domicilio(2, domicilioCorrespondencia.id,
                    domicilioCorrespondencia.calle, domicilioCorrespondencia.numero, domicilioCorrespondencia.piso,
                    domicilioCorrespondencia.departamento,Localidad(domicilioCorrespondencia.localidad?.id,
                    domicilioCorrespondencia.localidad?.descripcion,domicilioCorrespondencia.localidad?.codigoPostal,
                    domicilioCorrespondencia.localidad?.provincia!!),true)
            listaDomicilios.add(domicilioFinalCorrespondencia)
        }else{
            listaDomicilios[0].envioCorrespondencia = true
        }
        titularModel.value?.domicilio = listaDomicilios
    }

    fun guardarTelefono(codIntParticular: String?, codAreaParticular: String?, numeroParticular: String?,
                        codIntCelular: String, codAreaCelular: String, numeroCelular: String) {
        if (numeroParticular != null) {
            titularModel.value?.telefonoParticular?.codigoInternacional = codIntParticular
            titularModel.value?.telefonoParticular?.codigoArea = codAreaParticular
            titularModel.value?.telefonoParticular?.numero = numeroParticular
        }
        if (numeroParticular?.equals("")!!) {
            titularModel.value?.telefonoParticular = null
        }
        titularModel.value?.telefonoCelular?.codigoInternacional = codIntCelular
        titularModel.value?.telefonoCelular?.codigoArea = codAreaCelular
        titularModel.value?.telefonoCelular?.numero = numeroCelular
    }

    fun setearCodigosInternacionales(context: Context) {
        if (titularModel.value?.telefonoCelular?.id == null) {
            titularModel.value?.telefonoCelular = Telefono(0, context.
                    getString(R.string.codigo_internacional_argentina_text), null, null)
        }
        if (titularModel.value?.telefonoParticular?.id == null) {
            titularModel.value?.telefonoParticular = Telefono(0, context.
                    getString(R.string.codigo_internacional_argentina_text), null, null)
        }
    }

    fun guardarFechaNacimiento() {
        val format = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val date = format.parse(fechaDeNacimiento.value!!)
            titularModel.value?.fechaNacimiento = date
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun cargarFechaNacimiento() {
        val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        try {
            val dateTime = dateFormat.format(titularModel.value?.fechaNacimiento!!)
            fechaDeNacimiento.value = dateTime
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    fun cargarEstadoCivil(estadoCivil: EstadoCivil) {
        titularModel.value?.estadoCivil = estadoCivil
    }

    fun guardarEstadoCivil(posicion: Int) {
        titularModel.value?.estadoCivil = estadosCiviles.value!![posicion]
    }

    fun obtenerPosicionEstadoCivilSeleccionado(): Int {
        var index = 0
        if (titularModel.value?.estadoCivil != null) {
            this.estadosCiviles.value!!.forEachIndexed { i, element ->
                if (element.id == titularModel.value?.estadoCivil!!.id) {
                    index = i
                }
            }
        }
        return index
    }

    fun guardarSexos(sexo: List<Sexo>) {
        this.sexo.value = sexo
    }

    fun cargarSexo(sexo: Sexo) {
        titularModel.value?.sexo = sexo
    }

    fun guardarSexo(posicion: Int) {
        titularModel.value?.sexo = sexo.value!![posicion]
    }

    fun obtenerPosicionSexoSeleccionado(): Int {
        var index = 0
        if (titularModel.value?.sexo != null) {
            this.sexo.value!!.forEachIndexed { i, element ->
                if (element.id == titularModel.value?.sexo!!.id) {
                    index = i
                }
            }
        }
        return index
    }

    fun validarDniExistente(): Boolean {
        val dni = titularModel.value!!.dni
        val expediente = expediente.value

        if ((expediente!!.conviviente != null) && (expediente.conviviente?.dni.equals(dni))) {
                return true
        }
        expediente.personaDependiente?.forEach { p ->
            if (p.dni.equals(dni)) {
                return true
            }
        }
        return false
    }
}